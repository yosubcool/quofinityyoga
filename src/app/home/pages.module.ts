/*******************************************Confidential*****************************************
 * <copyright file = "pages.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PagesRoutingModule } from './pages-routing.module';
import { CommonModule } from '@angular/common';
import { NgxLoadingModule } from 'ngx-loading';
import { HomeComponent } from './home.component';
import { CommonTemplateModule } from '../common/common-template.module';
// import { CommonTemplateModule } from 'src/app/common/common-template.module';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    PagesRoutingModule,
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    NgxLoadingModule,
    CommonTemplateModule
  ]
})
export class PagesModule { }
