/*******************************************Confidential*****************************************
 * <copyright file = "home.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import {
  Component,
  OnInit,
  NgZone,
  Renderer2,
  AfterViewInit,
} from "@angular/core";
import { UserService } from "../user-account/user-service";
import { Router } from "@angular/router";
// import { debug } from "util"; //#Revisit_IgnoredAsNotUsed
declare var $: any;
// import { SettingsService } from "../settings/settings-service"; #Revisit_IgnoredAsNotUsed
import { Pagination } from "../_shared/model/pagination-model";
import { PricingMethod } from "../_shared/model/pricing-model";
// import { DataService } from "src/app/_core/data.service";//#Revisit_IgnoredAsNotUsed
import { User } from "../_shared/model/user-model";
import { Constants } from "../_shared/constant";
import { ToastrService } from "ngx-toastr";
import { HttpClient } from '@angular/common/http';
// import { HubConnection } from '@aspnet/signalr-client';
declare var jQuery: any;
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["home.component.scss"],
})
export class HomeComponent {
  // Popup
  typeofMessage: string;
  messageToDisplay: string | null;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  prisingMethodData: any;
  public loading = false;
  banners: any;
  email: string;
  password: string;
  SubscriptionType: any;
  globalListener: any;
  token_triggered: boolean;
  constants: any;
  contactUs: any;
  RememberMe: boolean;
  methodList: Pagination;
  pricingMethod: PricingMethod;
  message: string;
  ipAddress: any;
  activeUser: User;
  constructor(
    private _route: Router,
    private renderer: Renderer2,
    private _UserService: UserService,
    private ngZone: NgZone,
    private _toastrService: ToastrService,
    // private _settingsService: SettingsService, #Revisit_IgnoredAsNotUsed
    private router: Router,
    private http: HttpClient

  ) {
    // sessionStorage.clear();
    this.methodList = new Pagination();
    this.pricingMethod = new PricingMethod();
    this.RememberMe = false;
    this.constants = Constants;
    this.contactUs = {};
    this.contactUs.FirstName = "";
    this.contactUs.LastName = "";
    this.contactUs.Email = "";
    this.contactUs.ContactNo = "";
    this.contactUs.Message = "";
    this._toastrService.toastrConfig.preventDuplicates = true;

    this.SubscriptionType = [
      { Description: "One-Year Subscription", amount: "250", subType: 1 },
      { Description: "Two-Year Subscription", amount: "450", subType: 2 },
      { Description: "Three-Year Subscription", amount: "675", subType: 5 },
    ];
    this.banners = [
      {
        ImageURl: "assets/img/banner1.jpg",
        heading: "SAY GOODBYE TO FRAUDULENT CHARGEBACKS!",
        description:
          // tslint:disable-next-line:max-line-length
          "With FlexCPQ, prevent chargeback frauds, gain more happy customers, boost your revenue and never ever lose out on fraudulent chargebacks again.",
      },
      {
        ImageURl: "assets/img/banner2.jpg",
        heading: "DYNAMIC CUSTOMER OVERVIEW!",
        description:
          "The FlexCPQ dashboard offers all the customer details you require in a single glance. ",
      },
      {
        ImageURl: "assets/img/banner3.jpg",
        heading: "CHOOSE YOUR PACKAGE!",
        description:
          "Avail FlexCPQ at great prices with our one-year, two-year and three-year subscription plans. ",
      },
    ];
  }

  ngOnInit(): void {
    const string = localStorage.getItem("userAuth");
    if (string != null && string != "null") {
      const user = JSON.parse(atob(string));
      this.email = user.email;
      this.password = user.password;
      this.RememberMe = true;
    }
    $(window).scrollTop(0);
    // sessionStorage.clear(); /****** */

    // const connection = new HubConnection('http://localhost:52075/msg');
    // connection.on('send', data => {
    //   // console.log(data);
    // });
    // connection.start()
    //   .then(() => connection.invoke('send', 'Hello'));

    let Data: any;
    this.http.get("https://jsonip.com").subscribe((data) => {
      Data = data;
      this.ipAddress = Data.ip;
    });
  }

  login(isValid: boolean) {
    this.show = false;
    this.loading = true;
    if (isValid) {
      this._UserService
        .Login(this.email, this.password, this.ipAddress)
        .subscribe(
          (res: any) => {
            this.show = false;
            this.loading = false;
            this.ngZone.run(() => {
              if (res) {
                // window.location.reload();
                // if (res !== 'msg' && res !== 'Already login') {
                if (res !== "msg" && !res.alreadyLogin) {
                  if (this.RememberMe) {
                    const user: any = {};
                    user.email = this.email;
                    user.password = this.password;
                    const string = btoa(JSON.stringify(user));
                    localStorage.setItem("userAuth", string);
                  } else {
                    localStorage.setItem("userAuth", "");
                    // localStorage.setItem("userAuth", null); #Revisit_PreviousCode
                  }
                  this._route.navigate(["/dashboard"]);
                } else if (res === "msg") {
                  this.show = true;
                  this.popupOpen((this.popupFor = "Verify your account"));
                  // } else if (res === 'Already login') {
                } else if (res.alreadyLogin) {
                  // code for multilogin

                  this.show = true;
                  this.activeUser = res.result;
                  this.popupOpen((this.popupFor = "Already login"));
                  // sessionStorage.clear();
                  // localStorage.clear();
                } else {
                  this.show = true;
                  this.popupOpen((this.popupFor = "Invalid details"));
                }
              }
            });
          },
          (err: any) => {
            this.loading = false;
            const error = err;
            if (error.statusCode === 404) {
              this.message = error.message;
              this.show = true;
              this.popupOpen((this.popupFor = "Backend"));
            } else {
              // this._toastrService.error(
              //   'Please provide valid details.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.show = true;
              this.popupOpen((this.popupFor = "Invalid details"));
            }
          }
        );
    } else {
      this.loading = false;
      this.show = true;
      this.popupOpen((this.popupFor = "Invalid details"));
      // this._toastrService.error(
      //   'Please provide valid details.',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  isActive(url: any) {
    return url === this.banners[0].ImageURl;
  }

  // messages display
  popupOpen(_popupFor: string) {
    // this.titletoClose();
    if (this.popupFor === "Invalid details") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Please provide valid details.";
    } else if (this.popupFor === "Verify your account") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = " Verify your account from given email address.";
    } else if (this.popupFor === "Already login") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = ` You presently have an active login session. \n
      <br><br> This is an indication that you are currently logged in on an additional browser or device, or you recently closed your browser or shutdown your device without logging out of the application.\n <br><br> Would you like to continue logging in and cancel your existing session ?`;
    } else if (this.popupFor === "Backend") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = this.message;
    }
  }
  yes(ConfirmModal: any) {
    this.show = false;
    if (this.popupFor === "Already login") {
      this._UserService.forcedLogin(this.activeUser.id).subscribe((res: any) => {
        if (res) {
          sessionStorage.setItem("AccessToken", res.token.value);
          this._route.navigate(["/dashboard"]);
        }
      });
    }
    ConfirmModal.hide();
  }

  no(ConfirmModal: any) {
    // if (this.popupFor === 'Already login') {
    //   this._UserService.forcedLogoutUser(this.activeUser.id).subscribe(res => {
    //     if (res) {
    //       localStorage.clear();
    //       sessionStorage.clear();
    //       localStorage.clear();
    //       this.router.navigate(['pages/app-home']);
    //     }
    //   });
    // }
    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
  }
  // no(ConfirmModal) {
  //   ConfirmModal.hide();
  //   this.popupFor = 'none';
  //   this.show = false;
  // }

  hide(ConfirmationModel: any) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }
}
