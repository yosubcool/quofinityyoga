/*******************************************Confidential*****************************************
 * <copyright file = "pages-routing.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { PricingComponent } from './pricing.component';
// import { PaymentSuccessComponent } from './payment-success.component';
import { HomeComponent } from 'src/app/home/home.component';
import { RouteGaurd } from 'src/app/_core/RouteGaurd';

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      // title: 'Example Pages'
      isLogin: false
    },
    children: [
      {
        path: 'app-home',
        component: HomeComponent,
        data: {
          title: 'Sales Quoting Software'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
