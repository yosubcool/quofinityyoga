import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
// import { Observable } from "rxjs/Observable";
import { DataService } from "src/app/_core/data.service";
// import {} from ''
// const store = require('store');

const MINUTES_UNITL_AUTO_LOGOUT = 5; // in mins
const CHECK_INTERVAL = 2000; // in ms
const STORE_KEY = "lastAction";
@Injectable()
export class AutoLogoutService {
  isCalled = false;
  currentUser: any;
  isTimeout: any;
  private eventCallback = new Subject<string>(); // Source
  eventCallback$ = this.eventCallback.asObservable(); // Stream
  // public getLastAction() {
  //   return JSON.parse(localStorage.getItem('lastAction'));

  // }
  public setLastAction(lastAction: number) {
    localStorage.setItem(STORE_KEY, lastAction.toString());
  }

  constructor(private dataService: DataService, private router: Router) {
    this.initListener();
    this.initInterval();
  }

  initListener() {
    document.body.addEventListener("click", () => this.reset());
    document.body.addEventListener("mouseover", () => this.reset());
    document.body.addEventListener("mouseout", () => this.reset());
    document.body.addEventListener("keydown", () => this.reset());
    document.body.addEventListener("keyup", () => this.reset());
    document.body.addEventListener("keypress", () => this.reset());
  }

  reset() {
    this.setLastAction(Date.now());
  }

  initInterval() {
    setInterval(() => {
      this.check();
    }, CHECK_INTERVAL);
  }

  check() {
    let lastAction = JSON.parse(localStorage.getItem("lastAction"));
    if (lastAction === null) {
      this.setLastAction(Date.now());
      lastAction = JSON.parse(localStorage.getItem("lastAction"));
    }
    const now = Date.now();
    const timeleft = lastAction + MINUTES_UNITL_AUTO_LOGOUT * 60000;
    const diff = timeleft - now;
    const lastTransaction = JSON.parse(localStorage.getItem("lastAction"));
    if (diff === 900000 || (diff < 1109000 && diff > 900000)) {
      if (this.isCalled === false) {
        this.isCalled = true;
        this.updateLoginInfo();
      }
    }
    this.isTimeout = diff < 0;
    if (this.isTimeout) {
      this.eventCallback.next(this.isTimeout);
    }
  }

  updateLoginInfo() {
    const currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    if (currentUser) {
      this.dataService
        .getElementThroughId("Configuration/UpdateLoginDetails", currentUser.id)
        .subscribe((response) => {
          if (response) {
            this.isCalled = false;
          }
        });
    }
  }
}
