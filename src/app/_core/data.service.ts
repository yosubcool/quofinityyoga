/*******************************************Confidential*****************************************
 * <copyright file = "data.service.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpResponse,
  HttpHeaders,
  // RequestOptions,
  HttpParamsOptions,
  HttpParams
} from "@angular/common/http";
// import "rxjs/add/operator/map"; //#revisit_rxjsUnknownImport
import { catchError, map } from "rxjs/operators";
import { Constants } from "src/app/_shared/constant";
import { User } from "src/app/_shared/model/user-model";
import { Router } from "@angular/router";
import { ErrorObserver, Observable, throwError } from "rxjs";
import { RequestOptions } from "@angular/http";
// import { _throw } from 'rxjs/observable/throw';
@Injectable()
export class DataService {
  tempData: Response;

  temp = [];
  data = [];
  user: User;
  serviceURL = Constants;
  constructor(private http: HttpClient, private router: Router) { }


  createHttpHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Cache-control": "no-cache",
      Authorization: "Bearer " + sessionStorage.getItem("AccessToken"),
      Expires: "0",
      Pragma: "no-cache",
    });
    // headers.append(
    //   "Authorization",
    //   "Bearer " + sessionStorage.getItem("AccessToken")
    // );
    return headers;
  }
  createHttpHeadersWithoutAuth(): HttpHeaders {
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Cache-control": "no-cache",
      Expires: "0",
      Pragma: "no-cache",
    });
    return headers;
  }

  getData(methodName): Observable<any> {
    // const headers = new Headers({
    //   "Content-Type": "application/json",
    //   "Cache-control": "no-cache",
    //   Expires: "0",
    //   Pragma: "no-cache",
    // });
    // headers.append(
    //   "Authorization",
    //   "Bearer " + sessionStorage.getItem("AccessToken")
    // );

    const headers = this.createHttpHeaders();
    const options = {
      headers: headers
    };
    const Url = this.serviceURL.BASE_URL + methodName;
    return this.http
      .get(Url, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }
  getElementThroughAnyParam(methodName, model): Observable<any> {
    const headers = this.createHttpHeaders();
    const params = new HttpParams({ fromObject: model });
    let cleanedParams = new HttpParams();
    params.keys().forEach(x => {
      if (params.get(x) != null && params.get(x) != undefined) {
        cleanedParams = cleanedParams.append(x, params.get(x));
      }
    });
    const options = {
      params: params,
      headers: headers
    };

    // const params = new URLSearchParams(); //#Revisit_PreviousCode
    // tslint:disable-next-line:forin
    // for (const key in model) {
    //   params.set(key, model[key]);
    // }
    // const Url =
    //   this.serviceURL.BASE_URL +
    //   methodName +
    //   "/" +
    //   "searchItems" +
    //   "?" +
    //   params.toString();

    const Url =
      this.serviceURL.BASE_URL +
      methodName +
      "/" +
      "searchItems"
    // + "?" +
    // params.toString();
    return this.http
      .get(Url, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .map((res) => {
    //   return res;
    // })
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

  getElementThroughAnyObject(methodName, model): Observable<any> {
    //Making httpcalls wrt HttpClient #revisit_MajorHttpChanges
    // const headers = new HttpHeaders({
    //   "Content-Type": "application/json",
    //   "Cache-control": "no-cache",
    //   Expires: "0",
    //   Pragma: "no-cache",
    // });
    // headers.append(
    //   "Authorization",
    //   "Bearer " + sessionStorage.getItem("AccessToken")
    // );

    const headers = this.createHttpHeaders();
    const params = new HttpParams({ fromObject: model });


    // tslint:disable-next-line:forin //#Revisit_PreviousCode
    // for (const key in model) {
    //   if (model[key] != null && model[key] != undefined)
    //     params.set(key, model[key]);
    // }

    //Removing null/undefined properties to avoid JSON stringifying them to "null" at server side
    let cleanedParams = new HttpParams();
    params.keys().forEach(x => {
      if (params.get(x) != null && params.get(x) != undefined) {
        cleanedParams = cleanedParams.append(x, params.get(x));
      }
    });

    const options = {
      params: cleanedParams,
      headers: headers
    };
    // const Url = this.serviceURL.BASE_URL + methodName + "?" + params.toString(); //#revisit_OldCode_Keep
    const Url = this.serviceURL.BASE_URL + methodName //+ "?" + params.toString();
    return this.http.get(Url, options)
      .pipe(map((res) => {
        return res;
      }))
    // .map((res) => {
    //   return res;
    // });
  }
  postElement(methodName, data): Observable<any> {
    const headers = this.createHttpHeaders();
    const options = {
      headers: headers
    };
    const body = JSON.stringify(data);
    const Url = this.serviceURL.BASE_URL + methodName;
    return this.http
      .post(Url, body, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .map((res) => res)
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

  getElementThroughId(methodName, id): Observable<any> {
    const headers = this.createHttpHeaders();
    const options = {
      headers: headers
    };
    const Url = this.serviceURL.BASE_URL + methodName + "/" + id;
    return this.http
      .get(Url, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .map((res) => {
    //   return res;
    // })
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

  deleteElement(methodName, id): Observable<any> {
    const headers = this.createHttpHeaders();
    const options = {
      headers: headers
    };
    const Url = this.serviceURL.BASE_URL + methodName + "/" + id;
    return this.http
      .delete(Url, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .map((res) => res)
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

  updateElement(methodName, data): Observable<any> {
    const headers = this.createHttpHeaders();
    const options = {
      headers: headers
    };
    const body = JSON.stringify(data);
    const Url = this.serviceURL.BASE_URL + methodName;
    return this.http
      .put(Url, body, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .map((res) => res)
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

  postElementForAllowAnonymous(methodName, data): Observable<any> {
    const headers = this.createHttpHeadersWithoutAuth();
    const options = {
      headers: headers
    };
    const body = JSON.stringify(data);
    const Url = this.serviceURL.BASE_URL + methodName;
    return this.http
      .post(Url, body, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .map((res) => res)
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

  getDataByIdAllowAnonymous(methodName, id): Observable<any> {
    const headers = this.createHttpHeadersWithoutAuth();
    const options = {
      headers: headers
    };
    const Url = this.serviceURL.BASE_URL + methodName + "/" + id;
    return this.http
      .get(Url, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .map((res) => {
    //   return res;
    // })
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

  getDataAllowAnonymous(methodName): Observable<any> {
    const headers = this.createHttpHeadersWithoutAuth();
    const options = {
      headers: headers
    };
    const Url = this.serviceURL.BASE_URL + methodName;
    return this.http
      .get(Url, options)
      .pipe(map((res) => {
        return res;
      }),
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .map((res) => {
    //   return res;
    // })
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }

  UploadFile(methodName, file): Observable<any> {
    const headers = this.createHttpHeadersWithoutAuth();
    const options = {
      headers: headers
    };
    const Url = this.serviceURL + methodName;
    return this.http
      .post(Url, file, { headers: headers })
      .pipe(
        catchError(err => {
          console.log('caught mapping error and rethrowing', err);
          // const error = new Error(err)
          return throwError(() => { this.errorHandler(err) });
          // return throwError(err);
        }))
    // .catch((e: any) => Observable.throw(this.errorHandler(e)));
  }
  errorHandler(error: any): void {
    if (error.status == 401) {
      sessionStorage.clear();
      this.router.navigate(["/account/login"]);
    }
    console.log(error);
  }
}
