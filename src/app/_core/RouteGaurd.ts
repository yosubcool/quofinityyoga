/*******************************************Confidential*****************************************
 * <copyright file = "RouteGuard.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */


import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
@Injectable()
export class RouteGaurd implements CanActivate {
  constructor(private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot) {
    const login: boolean = route.data['isLogin'];
    const token = sessionStorage.getItem('AccessToken');
    // decode the token to get its payload
    // const tokenPayload = decode(token)
    if (token !== null) {
      if (!login) {
        this.router.navigate(['/dashboard']);
        return false;
      }
      return true;
    } else {
      if (!login) {
        return true;
      }
      this.router.navigate(['/account/login']);
      return false;
    }
  }
}
