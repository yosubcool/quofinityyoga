
/*******************************************Confidential*****************************************
 * <copyright file = "RoleGuard.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */


import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
// import { JwtHelper } from 'angular2-jwt'; //#Revisit_PreviousCode

@Injectable()
export class RoleGaurd implements CanActivate {
  constructor(private router: Router) { }
  // tslint:disable-next-line:member-ordering
  canActivate(route: ActivatedRouteSnapshot) {
    const expectedRole = route.data['expectedRole'];
    const token = sessionStorage.getItem('AccessToken');
    // decode the token to get its payload
    if (token !== null) {
      // const jwtHelper: JwtHelper = new JwtHelper();  //#Revisit_PreviousCode
      const jwtHelper: JwtHelperService = new JwtHelperService();
      const tokenPayload = jwtHelper.decodeToken(token)
      const roles = expectedRole.split(',');
      const userRole = roles.filter(item => item === tokenPayload.role);
      if (userRole.length) {
        return true;
      }
    }
    if (expectedRole === 'Super Admin') {
      this.router.navigate(['/superadmin/login']);
      return false;
    }
    this.router.navigate(['/dashboard']);
    return false;
  }
}

