/*******************************************Confidential*****************************************
 * <copyright file = "manage-user.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component, ViewChild, NgZone, OnInit } from '@angular/core';
// import { ModalDirective } from 'ngx-bootstrap/modal/modal.component'; //#Revisit_IgnoredAsNotUsed
import { Router } from '@angular/router';
// import { RegistrationVM } from 'app/_shared/model/RegistrationVM'; //#Revisit_IgnoredAsNotUsed
// import { Company } from 'app/_shared/model/company-model'; //#Revisit_IgnoredAsNotUsed
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user-account/user-service';
import { Constants } from '../_shared/constant';
import { User } from '../_shared/model/user-model';
declare var $: any;
@Component({
  templateUrl: './manage-user.component.html'
})
export class ManageUserComponent implements OnInit {
  public loading = false;
  public myModal;
  public largeModal;
  public smallModal;
  public primaryModal;
  public successModal;
  public warningModal;
  public dangerModal;
  public infoModal;
  user: any;
  active = true;
  personName: string;
  hidden = true;
  constants: any;
  isUserNameDuplicate: boolean;
  company: any;
  isdiabled: boolean;
  GenralUsers: any;
  isEdit: boolean;
  deleteId: string;
  userRoles = [
    { name: 'ADMIN', selected: false },
    { name: 'USER', selected: false }
  ];
  constructor(
    private _UserService: UserService,
    private router: Router,
    private ngZone: NgZone,
    private _toastrService: ToastrService
  ) {
    this._toastrService.toastrConfig.preventDuplicates = true;
    this.user = new User();
    this.isdiabled = false;
    this.constants = Constants;
    this.GenralUsers = new Array<User>();
    this._UserService.GetCompany().subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            this.company = res;
            this.GetGenralUser();
          }
        });
      },
      err => { }
    );
  }
  ngOnInit() {
    $(window).scrollTop(0);
  }
  GetGenralUser() {
    this.loading = true;
    this._UserService.GetGenralUser(this.company.id).subscribe(
      res1 => {
        this.ngZone.run(() => {
          this.GenralUsers = res1;
          this.loading = false;
        });
      },
      err => { this.loading = false; }
    );
  }
  onEdit() {
    this.active = false;
  }

  onSubmit(isValid: boolean, model) {
    if (isValid && !this.isUserNameDuplicate) {
      this.loading = true;
      model.hide();
      this.isdiabled = true;
      this.user.companyId = this.company.id;
      this.user.isActive = true;
      this.user.createdOn = new Date().toDateString();
      this._UserService.AddUser(this.user).subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.GetGenralUser();
              this._toastrService.error('User added successfully!', '', this.constants.toastrConfig);
            } else {
              this._toastrService.error('Email already exists!', '', this.constants.toastrConfig);
            }
            this.isdiabled = false;
            this.loading = false;
          });
        },
        err => {
          this.isdiabled = false;
          this.loading = false;
        }
      );
    }
  }

  editUser(modal, usr) {
    this.isEdit = true;
    modal.show();
    this.user = JSON.parse(JSON.stringify(usr));
  }

  updateUser(isValid: boolean, model) {
    if (isValid) {
      this.loading = true;
      model.hide();
      this.isdiabled = true;
      this.user.updatedOn = new Date().toDateString();
      this._UserService.UpdateUser(this.user).subscribe(
        res => {
          this.ngZone.run(() => {
            this.GetGenralUser();
            this.isdiabled = false;
            this.loading = false;
          });
        },
        err => {
          this.isdiabled = false;
          this.loading = false;
        }
      );
    }
  }

  DeleteUser(id: string) {
    this.loading = true;
    this._UserService.DeleteUser(id).subscribe(
      res => {
        this.ngZone.run(() => {
          this.loading = false;
          this.GetGenralUser();
        });
      },
      err => { this.loading = false; }
    );
  }

  onModelClose() {
    this.user = new User();
  }
  onModelopen(flag: boolean) {
    this.isEdit = flag;
    if (!flag) {
      this.user = new User();
    }
  }

  onEmailBlur() {
    if (!this.isEdit) {
      if (this.user.email) {
        this.user.userName = this.user.email.substring(
          0,
          this.user.email.indexOf('@')
        );
      }
    }
  }
}

