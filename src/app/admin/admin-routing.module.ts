/*******************************************Confidential*****************************************
 * <copyright file = "admin-routing.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGaurd } from '../_core/RoleGuard';
import { RouteGaurd } from '../_core/RouteGaurd';
import { CompanyProfileComponent } from './company.component';
import { ManageUserComponent } from './manage-user.component';
const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      isLogin: true,
      title: 'Admin'
    },
    children: [
      {
        path: 'manage-user',
        component: ManageUserComponent,
        canActivate: [RoleGaurd],
        data: {
          title: 'Manage User',
          expectedRole: 'Admin,Super Admin'
        }
      },
      {
        path: 'company',
        component: CompanyProfileComponent,
        canActivate: [RoleGaurd],
        data: {
          title: 'Company Profile',
          expectedRole: 'Admin,Super Admin'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
