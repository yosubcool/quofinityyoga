/*******************************************Confidential*****************************************
 * <copyright file = "admin.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
// import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';
// import 'rxjs/Rx';
import { NgxPaginationModule } from 'ngx-pagination'; // importing ng2-pagination
import { OrderModule } from 'ngx-order-pipe'; // importing the module
import { NgxLoadingModule } from 'ngx-loading';
import { AlertModule } from 'ngx-bootstrap/alert';
import { NgxMaskModule } from 'ngx-mask';
import { QuillModule } from 'ngx-quill';
import { SharedModule } from '../_shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { AdminRoutingModule } from './admin-routing.module';
import { CompanyProfileComponent } from './company.component';
import { ManageUserComponent } from './manage-user.component';
@NgModule({
  imports: [
    AdminRoutingModule,
    NgxPaginationModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule,
    CommonModule,
    FormsModule,
    AlertModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    NgxMaskModule,
    OrderModule,
    NgxLoadingModule,
    QuillModule,
    SharedModule
  ],
  declarations: [
    ManageUserComponent,
    CompanyProfileComponent
  ],

})
export class AdminModule { }
