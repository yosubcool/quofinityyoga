/*******************************************Confidential*****************************************
 * <copyright file = "company.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component, OnInit, NgZone } from '@angular/core';
// import { User } from 'app/_shared/model/user-model'; //#Revisit_IgnoredAsNotUsed
// import { RegistrationVM } from 'app/_shared/model/RegistrationVM'; //#Revisit_IgnoredAsNotUsed
import { ToastrService } from 'ngx-toastr';
import { DomSanitizer } from '@angular/platform-browser';
import { UserService } from '../user-account/user-service';
import { Constants } from '../_shared/constant';
import { Company } from '../_shared/model/company-model';
declare var $: any;
@Component({
  selector: 'app-company-profile',
  templateUrl: './company.component.html'
})
export class CompanyProfileComponent implements OnInit {
  public loading = false;
  isCompanyDuplicate = false;
  logoSrc: any = {};
  imgUploaded = false;
  isEdit = false;
  company: Company;
  isImageSizeLarge = false;
  constants: any;
  states: any;
  mask: any[] = [
    '+',
    '1',
    ' ',
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  newMask: string = " (000) 000-0000";
  img: any;
  isImageWidthHeightLarge = false;
  constructor(
    private _UserService: UserService,
    private _toastrService: ToastrService,
    private ngZone: NgZone,
    private sanitizer: DomSanitizer
  ) {
    this.company = new Company();
    this.constants = Constants;
    this._toastrService.toastrConfig.preventDuplicates = true;
    this.GetCompanys();
    this.GetStates();
  }

  ngOnInit() {
    $(window).scrollTop(0);
  }

  DynamicHtml(html) {
    if (html) {
      return this.sanitizer.bypassSecurityTrustHtml(html);
    } else {
      return null; //#revisit_CommentForV1
    }
  }

  GetCompanys() {
    this.loading = true;
    this._UserService.GetCompany().subscribe(
      res => {
        this.ngZone.run(() => {
          this.company = res;
          this.loading = false;
        });
      },
      err => {
        this.loading = false;
      }
    );
  }

  GetStates() {
    this._UserService.GetStates().subscribe(
      res => {
        this.ngZone.run(() => {
          this.states = res;
        });
      },
      err => { }
    );
  }

  uploadDoc(evt) {
    const files = evt.target.files;
    const file = files[0];
    let fitWh = false;
    if (files && file) {
      this.img = new Image();
      this.img.src = window.URL.createObjectURL(file);
      this.img.onload = () => {
        const width = this.img.naturalWidth,
          height = this.img.naturalHeight;

        window.URL.revokeObjectURL(this.img.src);
        fitWh = true;
        // if (width <= 125 && height <= 60) {
        //   fitWh = true;
        // } else {
        //   fitWh = false;
        // }
        this.funciotnwritten(fitWh, file);
      };
    }
  }
  funciotnwritten(fitWh, file) {
    if (fitWh) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoadeds.bind(this, file);
      reader.readAsArrayBuffer(file);
      this.isImageWidthHeightLarge = false;
    } else {
      this.isImageWidthHeightLarge = true;
    }
  }

  _handleReaderLoadeds(file, readerEvt) {
    this.isImageSizeLarge = false;
    if (file.type === 'image/png' || file.type === 'image/jpeg') {
      if (file.size < 1051341) {
        const buffer = readerEvt.target.result;
        file.Base64 = this.arrayBufferToBase64(buffer);
        this.isEdit = true;
        this.imgUploaded = true;
        setTimeout(() => {
          this.company.logo = 'data:' + file.type + ';base64,' + file.Base64;
        }, 1);
      } else {
        this.isImageSizeLarge = true;
      }
    } else {
      this.imgUploaded = false;
      // this._toastrService.error(
      //   'Please upload images only.',
      //   '',
      //   this.constants.toastrConfig
      // );
      return;
    }
  }
  arrayBufferToBase64(buffer) {
    let binary = '';
    const bytes = new Uint8Array(buffer);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  onEdit() {
    this.isEdit = true;
  }

  onSubmit(isvalid) {
    if (isvalid) {
      this.isImageSizeLarge = false;
      this.loading = true;
      this.company.updatedOn = new Date();
      // this.company.name = this.company.name.toUpperCase()
      this._UserService.UpdateCompany(this.company).subscribe(
        res => {
          if (res === false) {
            this.isCompanyDuplicate = true;
            this.loading = false;
          } else {
            this.ngZone.run(() => {
              if (res) {
                this.company = res;
              }
              this.loading = false;
              this.isEdit = false;
              this.isCompanyDuplicate = false;
            });
          }
        },
        err => {
          this.loading = false;
        }
      );
    }
  }
  onDiscard() {
    this.GetCompanys();
    this.isEdit = false;
  }
}
