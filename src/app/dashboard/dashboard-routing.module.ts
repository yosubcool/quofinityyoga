/*******************************************Confidential*****************************************
 * <copyright file = "dashboard-routing.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import { RouteGaurd } from '../_core/RouteGaurd';

import { DashboardComponent } from './dashboard.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [RouteGaurd],
    data: {
      isLogin: true,
      title: 'Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
