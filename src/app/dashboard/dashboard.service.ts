import { Router } from '@angular/router';
import { Injectable, NgZone, OnInit } from '@angular/core';
// import { Observable } from 'rxjs/Rx';
// import { debounce } from 'rxjs/operator/debounce';
import { Kit } from '../_shared/model/kit-model';
import { Pagination } from '../_shared/model/pagination-model';
import { DataService } from '../_core/data.service';
import { User } from '../_shared/model/user-model';
import { map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DashboardService implements OnInit {
  constants: any;
  userInfo: any;
  pagination = new Pagination();
  user: User;
  digit: any;
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router,

  ) {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
  }

  ngOnInit() { }

  // gets quotes for last 14 days - Tejaswini
  getQuote(method, quotesInfo) {
    return this.dataService
      .getElementThroughAnyObject(method, quotesInfo)
      .pipe(map(response => {
        return response;
      }));
  }

  // get data -Tejaswini
  getData(method) {
    return this.dataService.getData(method).pipe(map(response => {
      return response;
    }));
  }

  AddQuote(quote) {
    quote.createdBy = this.user.id;
    quote.createdOn = new Date();
    return this.dataService.postElement('Quote', quote).pipe(map(response => {
      return response;
    }));
  }

}
