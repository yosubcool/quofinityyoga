/*******************************************Confidential*****************************************
 * <copyright file = "dashboard.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */


import { NgModule } from '@angular/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { OrderModule } from 'ngx-order-pipe'; // importing the module
import { ModalModule } from 'ngx-bootstrap/modal';
import { KitsService } from '../kits/kits-service';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxLoadingModule } from 'ngx-loading';
import { SharedModule } from '../_shared/shared.module';
import { DashboardService } from './dashboard.service';
import { ProductsService } from '../products/products-service';
import { QuotesService } from '../quotes/quotes-service';
import { AccountsService } from '../accounts/accounts-service';
import { NgChartsModule } from 'ng2-charts';
@NgModule({
  imports: [
    DashboardRoutingModule,
    NgChartsModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    OrderModule,
    BsDropdownModule,
    NgxLoadingModule,
    ModalModule.forRoot(),
    SharedModule
  ],
  declarations: [DashboardComponent],
  providers: [DashboardService, KitsService, ProductsService, AccountsService, QuotesService]
})
export class DashboardModule { }
