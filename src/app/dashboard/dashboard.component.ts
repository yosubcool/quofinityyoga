import { Kit } from './../_shared/model/kit-model';
import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { AccountsService } from './../accounts/accounts-service'; //#Revisit_IgnoredAsNotUsed
import { ToastrService } from 'ngx-toastr';
import { ProductsService } from '../products/products-service';
import { User } from '../_shared/model/user-model';
/*******************************************Confidential*****************************************
 * <copyright file = "dashboard.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */
import { Pagination } from './../_shared/model/pagination-model';
import { Product } from '../_shared/model/product-model';
import { Quote } from '../_shared/model/quote-model';
import { Company } from '../_shared/model/company-model';
import { UserService } from '../user-account/user-service';
import { Constants } from '../_shared/constant';
import { Contact } from '../_shared/model/contact-model';
import { DashboardService } from './dashboard.service';
import { KitsService } from '../kits/kits-service';
import { QuotesService } from '../quotes/quotes-service';
import { Subscription } from 'rxjs';
declare var $: any;

@Component({
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit, OnDestroy {
  status: any;
  reverse = false;
  key: any;
  keyall: any;
  quoteStats: any;
  quotesInfo = new Pagination();
  allquotesInfo = new Pagination();
  pagesCount: any;
  kit: Kit;
  product: Product;
  constants: any;
  quote: Quote;
  duplicatePartNo = false;
  contacts: any = [];
  public loading = false;
  selectedAlpha: string;
  isSlideToggle = false;
  isFormSubmitted = false;
  user: User;
  isAddQuote: boolean;
  subscriptions: Subscription[] = [];
  isAddProduct: boolean;
  isAddKit: boolean;
  isPartNovalid = false;

  searchContact: any;
  contactsList = [];
  presentContact = false;
  showQuoteID = true;

  isOpenPopup = false;
  company: Company;
  constructor(
    private _DashboardService: DashboardService,
    private ngZone: NgZone,
    private _KitsService: KitsService,
    private _ProductsService: ProductsService,
    private _toastrService: ToastrService,
    // private accountsService: AccountsService,
    private _quotesService: QuotesService,
    private router: Router,
    private _UserService: UserService
  ) {
    this.kit = new Kit();
    this.constants = Constants;
    this.product = new Product();
    this.quote = new Quote();
    this.selectedAlpha = 'All';
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));

    this.showQuoteID = window.innerWidth <= 1024 ? false : true;
    this.company = new Company();
  }
  ngOnInit(): void {
    $(window).scrollTop(0);
    $('.btn-expand button').click(function () {
      $('.quote-status-box').slideToggle();
    });
    this.getQuotes(0);
    // this.getAllQuotes(0);
    this.GetCompanys();
    this.getQuoteStats();
    this.getContacts();
    this.getModuleAccess();
    if (document.getElementById('dashboard') !== null) {
      document.getElementById('dashboard').classList.add('active');
    }
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
      // console.log('Done');
    }, 500);
  }

  GetCompanys() {
    this.loading = true;
    this.subscriptions.push(
      this._UserService.GetCompany().subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            this.company = res;
            this.loading = false;
          });
        },
        error: (err) => {
          this.loading = false;
        }
      }

      )
    );
  }
  getModuleAccess() {
    this.subscriptions.push(
      this._ProductsService
        .getData('TenantMaster/GetAccessRoleMatrix/')
        .subscribe((res) => {
          this.isAddQuote = res.quote.create;
          this.isAddProduct = res.products.create;
          this.isAddKit = res.kit.create;
        })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });

    if (document.getElementById('dashboard') !== null) {
      document.getElementById('dashboard').classList.remove('active');
    }
  }
  // gets quotes for last 14 days - Tejaswini
  getQuotes(page) {
    this.quotesInfo.sortBy = 'CreatedOn';
    this.quotesInfo.sortType = 'desc';
    this.loading = true;
    if (this.key) {
      this.quotesInfo.sortBy = this.key;
    }
    if (this.reverse) {
      this.quotesInfo.sortType = 'asc';
    } else {
      this.quotesInfo.sortType = 'desc';
    }
    this.quotesInfo.pageNumber = page;
    this.quotesInfo.pageSize = 6;
    this.quotesInfo.alphabet =
      this.selectedAlpha === 'All' ? '' : this.selectedAlpha;
    this.subscriptions.push(
      this._DashboardService
        .getQuote('Quote/GetQuotesForDays', this.quotesInfo)
        .subscribe({
          next: (res) => {
            this.ngZone.run(() => {
              this.loading = false;
              this.quotesInfo = res;
              this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            });
          },
          error: (err) => {
            this.loading = false;
            // console.log(err);
          }
        }

        )
    );
  }
  goToDetails(id) {
    this.router.navigate(['/quotes/quotes-detail'], {
      queryParams: { id: btoa(id) },
    });
  }
  // Gets Status wise counts and total value of quotes   - Suryakant
  getQuoteStats() {
    this.subscriptions.push(
      this._DashboardService.getData('Quote/GetQuotesStats').subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            this.quoteStats = res;
          });
        },
        error: (err) => { }
      }
      )
    );
  }
  // To Add New Kit - Suryakant
  addKit(isValid: boolean, model) {
    this.isFormSubmitted = true;
    if (isValid) {
      this.kit.name = this.kit.name.replace(/^\s+|\s+$/g, '');
      this.kit.number = this.kit.number.replace(/^\s+|\s+$/g, '');
      model.hide();
      this._KitsService.AddKit(this.kit).subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            if (res) {
              this.isFormSubmitted = false;
              // this.resetforms(kitForm);
              // this._toastrService.success(
              //   'Kit Added Successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.loading = false;
              this.router.navigate(['/kits/kits-detail'], {
                queryParams: { id: btoa(res.id) },
              });
            }
          });
        },
        error: (err) => {
          // this._toastrService.error(
          //   'Please provide valid details.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.loading = false;
        }
      }

      );
    } else {
      this.loading = false;
    }
  }
  addProduct(isValid: boolean, model) {
    this.product.name = this.product.name.replace(/^\s+|\s+$/g, '');
    this.product.partNo = this.product.partNo.replace(/^\s+|\s+$/g, '');
    this._ProductsService.findProductByPartNo(this.product).subscribe({
      next: (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.duplicatePartNo = true;
          } else {
            this.duplicatePartNo = false;
            this.loading = false;
            this.isFormSubmitted = true;
            if (isValid && !this.isPartNovalid && !this.duplicatePartNo) {
              model.hide();
              this._ProductsService
                .postElement('Products', this.product)
                .subscribe({
                  next: (prod) => {
                    this.ngZone.run(() => {
                      this.isFormSubmitted = false;
                      if (prod) {
                        // this._toastrService.success(
                        //   'Product Added Successfully.',
                        //   '',
                        //   this.constants.toastrConfig
                        // );
                        this.router.navigate(['/products/products-detail'], {
                          queryParams: { id: btoa(prod.id) },
                        });
                        this.loading = false;
                      }
                    });
                  },
                  error: (err) => {
                    // this._toastrService.error(
                    //   'Please provide valid details.',
                    //   '',
                    //   this.constants.toastrConfig
                    // );
                    this.loading = false;
                  }
                }

                );
            } else {
              this.loading = false;
            }
          }
        });
      },
      error: (err) => {
        // console.log(err);
      }
    }

    );
  }
  onModelClose() {
    this.kit = new Kit();
  }
  resetforms(form) {
    this.isFormSubmitted = false;
    form.reset();
  }
  getContacts() {
    this.subscriptions.push(
      this._DashboardService.getData('Contact/GetContactWithAccount').subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            this.contacts = res;
          });
        },
        error: (err) => { }
      }

      )
    );
  }

  FindContact() {
    this.quote.contact = new Contact();
    this.contactsList = this.contacts.filter(
      (cont) =>
        cont.firstName === this.searchContact ||
        cont.lastName === this.searchContact
    );
  }

  saveContactOnSelectContact(e) {
    this.quote.contact = e.item;
  }

  AddQuotes(isValid, modal) {
    if (isValid) {
      if (this.quote.contact.id) {
        modal.hide();
        this.quote.name = this.quote.name.replace(/^\s+|\s+$/g, '');
        // this.quote.contact = this.contacts.filter(
        //   x => x.id === this.quote.contact.id
        // )[0];
        this.quote.isActive = true;
        this.quote.createdBy = this.user.id;
        this.quote.createdOn = new Date();
        this.quote.createdOn = new Date(
          this.quote.createdOn.getUTCFullYear(),
          this.quote.createdOn.getUTCMonth(),
          this.quote.createdOn.getUTCDate(),
          this.quote.createdOn.getUTCHours(),
          this.quote.createdOn.getUTCMinutes(),
          this.quote.createdOn.getUTCSeconds()
        );
        this.quote.expirationDate = this.quote.createdOn;
        this.quote.updatedOn = this.quote.createdOn;
        this.quote.expirationDate.setDate(
          this.quote.expirationDate.getDate() + this.constants.Expairy[0]
        );
        this.quote.expairyDays = this.constants.Expairy[0];
        this.quote.ownerId = this.user.id;
        this.quote.ownerName = this.user.firstName + ' ' + this.user.lastName;
        if (this.company) {
          this.quote.termsandConditions = this.company.termsConditions;
        }
        this._DashboardService.AddQuote(this.quote).subscribe((response) => {
          // this._toastrService.success(
          //   'Quote added successfully!',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.router.navigate(['/quotes/quotes-detail'], {
            queryParams: { id: btoa(response.id) },
          });
        });
      } else {
        this.presentContact = true;
      }
    }
  }

  CheckPartNo(item) {
    this.duplicatePartNo = false;
    this.isPartNovalid = false;
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isPartNovalid = true;
    } else {
      this.isPartNovalid = false;
    }
  }

  removeLeadSpace(partNo) {
    this.product.partNo = partNo.replace(/^\s+|\s+$/g, '');
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
    this.getQuotes(0);
  }
  sortQuotes(key) {
    this.keyall = key;
    this.reverse = !this.reverse;
    this.getAllQuotes(0);
  }
  SearchQuotes(e) {
    this.status = e;
    this.getQuotes(0);
  }
  SearchStatusWiseQuotes(e) {
    this.status = e;
    this.getAllQuotes(0);
  }
  pageChanged(no) {
    this.getAllQuotes(no);
  }
  getAllQuotes(pageNo) {
    this.allquotesInfo.sortBy = 'CreatedOn';
    this.allquotesInfo.sortType = 'desc';
    this.loading = true;
    if (this.keyall) {
      this.allquotesInfo.sortBy = this.keyall;
    }
    if (this.reverse) {
      this.allquotesInfo.sortType = 'asc';
    } else {
      this.allquotesInfo.sortType = 'desc';
    }
    this.allquotesInfo.pageNumber = pageNo;
    this.allquotesInfo.pageSize = 10;
    this.allquotesInfo.status = this.status === 'All' ? '' : this.status;
    this.allquotesInfo.alphabet =
      this.selectedAlpha === 'All' ? '' : this.selectedAlpha;
    this.subscriptions.push(
      this._quotesService.getQuote(this.allquotesInfo).subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            this.allquotesInfo = res;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            this.loading = false;
          });
        },
        error: (err) => {
          this.loading = false;
          // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
        }
      }

      )
    );
  }

  goToQuoteList(status) {
    this.router.navigate(['/quotes/quotes-list'], {
      queryParams: { status: btoa(status) },
    });
  }
}
