import { Component, OnInit, NgZone, OnDestroy, ViewChild } from "@angular/core";
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError,
  ActivatedRoute,
} from "@angular/router";
import { KitsService } from "../kits-service";
import { Kit } from "../../_shared/model/kit-model";
import { ToastrService } from "ngx-toastr";
import { CustomItem } from "../../_shared/model/customItem-model";
declare var $: any;
import {
  SubCategories,
  Categories,
} from "../../_shared/model/tenantMaster-model";
import { ProductDetail } from "../../_shared/model/productDetail-model";
import { Constants } from "../../_shared/constant";
import { Product } from "../../_shared/model/product-model";
import { Search } from "../../_shared/model/search-model";
import { DataService } from "../../_core/data.service";
import { NgForm } from "@angular/forms";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { ProductsService } from "src/app/products/products-service";
import { QuotesService } from "src/app/quotes/quotes-service";
import { Actions } from "src/app/_shared/model/actions-model";
@Component({
  selector: "app-kit-details",
  templateUrl: "./kit-details.component.html",
  styleUrls: ["./kit-details.component.scss"],
})
export class KitDetailsComponent implements OnInit, OnDestroy {
  // Popup
  @ViewChild("ConfirmModal")
  confirmModal: ModalDirective;
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;
  allMethods = [];
  constants: any;
  start = null;
  PricingMethods: any = [];
  masterMethodList = [];
  kits: any;
  subCategories: any;
  categories: any;
  subCategory: SubCategories;
  kitId: string;
  kitList: Kit;
  kit: any;
  isSave = false;
  kitTitleId: any;
  customItem: CustomItem;
  isKitTitleEdit = false;
  category: any;
  products: any;
  isSelect = false;
  searchText: string;
  loading = false;
  isCopyKit = false;
  isEditKit = false;
  tobeEdit: string;
  KitNameFormDetails: Kit;
  KitFormDetails: Kit;
  isKitNovalid = false;
  isKitDelete = false;
  isProductDelete = false;
  isItemDelete = false;
  isTagDelete = false;
  tagIndex: any;
  itemIndex: any;
  productIndex: any;
  product: Product;
  config: any;
  accessValid: Actions;
  newKit: any;
  kitName: string;
  subscriptions: Subscription[] = [];
  searched = [];
  showSearch = false;
  search = new Search();
  collection: string;
  collections = [];
  showBtn = 0;
  showClearbtn = false;
  showSearchDropdown = false;
  quill: any;
  callSequenceForSearchProduct = [];
  isCostValid = true;
  isPriceValid = true;
  constructor(
    private route: ActivatedRoute,
    private _quotesService: QuotesService,
    private router: Router,
    private _KitsService: KitsService,
    private ngZone: NgZone,
    private productsService: ProductsService,
    private _toastrService: ToastrService,
    private dataService: DataService
  ) {
    this.accessValid = new Actions();
    this.kitList = new Kit();
    this.tobeEdit = "nun";
    this.kit = new Kit();
    this.kitTitleId = 0;
    this.customItem = new CustomItem();
    this.subCategories = new SubCategories();
    this.categories = new Categories();
    this.category = new Categories();
    this.subCategory = new SubCategories();
    this.category = new Categories();
    this.constants = Constants;
    this.KitNameFormDetails = new Kit();
    this.KitFormDetails = new Kit();
    this.config = {
      removeButtons:
        "About,Cut,Copy,Paste,PasteText,PasteFromWord,Source,Anchor,",
      format_tags: "p;pre",
      addButton: "Save",
      target: "_blank",
    };
    this.newKit = new Kit();

    this.subscriptions.push(
      router.events.subscribe((event: Event) => {
        if (event instanceof NavigationEnd) {
          let id = this.route.snapshot.queryParams["id"];
          id = atob(id);
          if (this.kitId && this.kitId !== id) {
            this.loading = true;
            if (!id) {
              this.router.navigate(["/kits/kits-list"]);
            }
            this.kitId = id;
            this.getPricing();
            this.getModuleAccess();
            this.getKitDetails(this.kitId);
            $(window).scrollTop(0);
            if (document.getElementById("kit") !== null) {
              document.getElementById("kit").classList.add("active");
            }
            this.loading = false;
          }
        }
      })
    );
  }

  ngOnInit() {
    $(window).scrollTop(0);
    const id = this.route.snapshot.queryParams["id"];
    if (!id) {
      this.router.navigate(["/kits/kits-list"]);
    }
    this.kitId = atob(id);
    this.getPricing();
    this.getModuleAccess();
    this.getKitDetails(this.kitId);
    this.GetAllPricingMethods();
    if (document.getElementById("kit") !== null) {
      document.getElementById("kit").classList.add("active");
    }
  }

  GetAllPricingMethods() {
    this.subscriptions.push(
      this._quotesService.getAllPricingMethods().subscribe((res) => {
        this.allMethods = res;
      })
    );
  }
  closeEdit(part, from?) {
    if (this.isEditKit) {
      return;
    }
    this.tobeEdit = part;
    this.isEditKit = true;
  }
  getModuleAccess() {
    this.subscriptions.push(
      this._KitsService.getModuleAccess().subscribe((res) => {
        this.accessValid = res.kit;
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
    if (this.accessValid.update) {
      this.updateKit(this.kitList);
    }
    if (document.getElementById("kit") !== null) {
      document.getElementById("kit").classList.remove("active");
    }
  }
  CheckKitNo(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      this.isKitNovalid = true;
    } else {
      this.isKitNovalid = false;
    }
  }

  getKitDetails(id) {
    this.subscriptions.push(
      this._KitsService.GetKitDetails(id).subscribe(
        (Response) => {
          this.ngZone.run(() => {
            if (Response.products) {
              this.checkMethods(Response);
            }
            this.kit = JSON.parse(JSON.stringify(Response));
            this.kitList = JSON.parse(JSON.stringify(Response));
            this.KitNameFormDetails = JSON.parse(JSON.stringify(this.kit));
            this.KitFormDetails = JSON.parse(JSON.stringify(Response));
            this.kitName = Response.name;
          });
        },
        (err) => { }
      )
    );
  }

  addCustomProduct(form: NgForm, model) {
    this.loading = true;
    if (form.valid) {
      if (this.kitList.customItem === null) {
        this.kitList.customItem = new Array<CustomItem>();
      }
      this.kitList.customItem.isActive = true;
      // this.customItem.price = this.customItem.cost * this.customItem.quantity;
      this.kitList.customItem.push(this.customItem);

      this.updateKit(this.kitList);
      // this._toastrService.success(
      //   'Item Added Successfully.',
      //   '',
      //   this.constants.toastrConfig
      // );
      model.hide();
      this.customItem = new CustomItem();
      form.resetForm();
      this.loading = false;
    } else {
      // this._toastrService.warning(
      //   'Cannot Add Item.',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.show = true;
      this.popupOpen((this.popupFor = "Cannot Add Item"));
      this.loading = false;
    }
  }

  onModelClose() {
    this.customItem = new CustomItem();
  }

  addTitle(isvalid: boolean, kitList) {
    if (isvalid) {
      this.isKitTitleEdit = false;

      this.updateKit(kitList);
      // this._toastrService.success(
      //   'Title Saved Successfully.',
      //   '',
      //   this.constants.toastrConfig
      // );
    } else {
      this.isKitTitleEdit = true;
      // this._toastrService.warning(
      //   'Title Not Saved!.',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  onMarginChange(i, e) {
    this.kitList.products[i].margin = JSON.parse(
      JSON.stringify(this.allMethods.filter((x) => x.id === e)[0])
    );
    if (this.kitList.products[i].margin.method === "List") {
      this.kitList.products[i].margin.value =
        this.kitList.products[i].product.msrp;
    }
    // this.GetTotal();
    this.updateKit(this.kitList);
  }

  discard(newValue) {
    this.getKitDetails(newValue.id);
    this.isKitTitleEdit = false;
    // this._toastrService.info(
    //   'Title Change Discarded.',
    //   '',
    //   this.constants.toastrConfig
    // );
  }

  tagDelete(id) {
    let l = [];
    let index: any;
    l = this.kitList.tags;
    for (let i = 0; i < l.length; i++) {
      if (id === this.kitList.tags[i]) {
        index = i;
      }
    }
    this.kitList.tags.splice(index, 1);

    this.updateKit(this.kitList);
  }

  // To Update Kit Title Inline - Suryakant
  updateKit(newValue) {
    // this.loading = true;
    this._KitsService.UpdateKit(newValue).subscribe(
      (Response) => {
        if (Response) {
          this.loading = false;
          // this.getKitDetails(Response.id);
          this.kitList = JSON.parse(JSON.stringify(Response));

          this.kit = JSON.parse(JSON.stringify(Response));
          this.KitNameFormDetails = JSON.parse(JSON.stringify(this.kit));
          this.KitFormDetails = JSON.parse(JSON.stringify(Response));
          this.kitName = Response.name;
        }
      },
      (err) => {
        // console.log(err);
        this.loading = false;
        if (err.status === 403) {
          // this._toastrService.warning(
          //   'Access denied',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.show = true;
          this.popupOpen((this.popupFor = "Access denied"));
        }
      }
    );
  }

  // To Get All Categories - Suryakant
  getAllCategories() {
    this.subscriptions.push(
      this._KitsService.GetCategories().subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.categories = Response;
          });
        },
        (err) => { }
      )
    );
  }

  // To Get All Sub Categories - Suryakant
  getSubCategories(evt) {
    this.subscriptions.push(
      this._KitsService.GetSubCategories(evt).subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.subCategories = Response;
          });
        },
        (err) => { }
      )
    );
  }

  tagsArray(event) {
    if (this.kitList.tags === null) {
      const arr = [];
      arr.push(event);
      this.kitList.tags = arr;
    } else {
      this.kitList.tags.push(event);
    }
    this.updateKit(this.kitList);
  }

  // edit category
  editCategory(event) {
    if (this.kitList.category === null) {
      this.kitList.category = event;
    } else {
      this.kitList.category.push(event[0]);
    }
    this.updateKit(this.kitList);
  }

  // edit subcategory
  editSubCategories(event) {
    if (this.kitList.subCategory === null) {
      this.kitList.subCategory = event;
    } else {
      this.kitList.subCategory.push(event[0]);
    }
    this.updateKit(this.kitList);
  }

  categoryDelete(index) {
    const cate = this.kitList.category[index];
    let i = 0;
    if (this.kitList.subCategory) {
      const dSubcategory = JSON.parse(JSON.stringify(this.kitList.subCategory));
      dSubcategory.forEach((sub) => {
        if (sub.categories.id === cate.id) {
          this.kitList.subCategory.splice(i, 1);
        } else {
          i = i + 1;
        }
      });
    }
    this.kitList.category.splice(index, 1);
    this.updateKit(this.kitList);
  }

  subcategoryDelete(index) {
    this.kitList.subCategory.splice(index, 1);
    this.updateKit(this.kitList);
  }

  getAllKits() {
    this.subscriptions.push(
      this._KitsService.GetAllKits().subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.kits = Response;
          });
        },
        (err) => { }
      )
    );
  }

  addProduct(e) {
    if (e.cost > 0) {
      const Prod: ProductDetail = new ProductDetail();
      Prod.product = e;
      Prod.quantity = 1;
      if (!this.kitList.products) {
        this.kitList.products = new Array<CustomItem>();
      }
      this.kitList.products.push(Prod);
      Prod.product.isUsed = Prod.product.isUsed + 1;
      // this.updateProduct(Prod.product);

      Prod.product["flag"] = 1;
      this.updateProductCount(Prod.product);

      this.updateKit(this.kitList);
      this.searchText = null;
      this.showSearchDropdown = false;
      // console.log('kitList: ', this.kitList);
    } else {
      this.showSearchDropdown = false;
      this.show = true;
      this.popupOpen((this.popupFor = "verify product cost"));
    }
  }

  FindProduct() {
    this._KitsService.FindProduct(this.searchText).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.products = res;
        });
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  GetTotal() {
    let customTotal: any = 0;
    let productTotal: any = 0;
    if (this.kitList.products != null) {
      this.kitList.products.forEach((element) => {
        if (element.product) {
          if (element.margin.id) {
            if (element.margin.method === this.constants.PricingMethods.List) {
              productTotal =
                // productTotal + parseFloat(element.product.msrp.toFixed(2));
                productTotal +
                parseFloat(
                  (element.product.msrp * element.quantity).toFixed(2)
                );
            } else if (
              element.margin.method === this.constants.PricingMethods.Markup
            ) {
              const val: any =
                element.product.cost * (1 + element.margin.value / 100);
              let amount = Math.round((val + Number.EPSILON) * 100) / 100;
              productTotal = productTotal + amount * element.quantity;
            } else if (
              element.margin.method === this.constants.PricingMethods.Margin
            ) {
              const val: any =
                element.product.cost / (1 - element.margin.value / 100);
              let amount = Math.round((val + Number.EPSILON) * 100) / 100;
              productTotal = productTotal + amount * element.quantity;
            } else {
              const val: any =
                element.product.msrp -
                (element.product.msrp * element.margin.value) / 100;
              let amount = Math.round((val + Number.EPSILON) * 100) / 100;
              productTotal = productTotal + amount * element.quantity;
            }
          } else {
            let amount =
              Math.round((element.product.cost + Number.EPSILON) * 100) / 100;
            // productTotal =
            //   productTotal + element.product.cost.toFixed(2) * element.quantity;
            productTotal = productTotal + amount * element.quantity;
          }
        }
      });
    }
    if (this.kitList.customItem != null) {
      this.kitList.customItem.forEach((element) => {
        let amount = Math.round((element.price + Number.EPSILON) * 100) / 100;
        customTotal = customTotal + amount * element.quantity;
      });
    }
    this.kitList.total = customTotal + productTotal;
    // this.kitList.total = this.kitList.total.toFixed(2);
    this.kitList.total =
      Math.round((this.kitList.total + Number.EPSILON) * 100) / 100;
    return this.kitList.total;
  }

  copyKit(newKit) {
    this.kitList = newKit;
    this.kitList.id = null;
    this.loading = true;
    this.isEditKit = false;
    this.isKitTitleEdit = false;
    this.kitList.name = this.kitList.name + "_Copy";
    this._KitsService.AddKit(this.kitList).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            if (this.kitList.products !== null) {
              this.kitList.products.forEach((element) => {
                element.product.isUsed = element.product.isUsed + 1;
                // this.updateProduct(element.product);
                element.product["flag"] = 1;
                this.updateProductCount(element.product);
              });
            }
            this.getKitDetails(res.id);
            this.kitList = res;
            this.KitNameFormDetails = res;
            this.loading = false;
            this.router.navigate(["/kits/kits-detail"], {
              queryParams: { id: btoa(res.id) },
            });
          }
        });
      },
      (err) => {
        // this._toastrService.error(
        //   'Please provide valid details.',
        //   '',
        //   this.constants.toastrConfig
        // );
        this.loading = false;
        this.isKitTitleEdit = false;
      }
    );
  }

  getPricing() {
    this.subscriptions.push(
      this.productsService.getData("Configuration/GetPricingMethod").subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.masterMethodList = res;
          });
        },
        (err) => { }
      )
    );
  }

  onKitEdit(isvalid: boolean, KitFormDetails) {
    if (isvalid && !this.isKitNovalid) {
      this._KitsService.UpdateKit(this.KitFormDetails).subscribe(
        (res) => {
          this.ngZone.run(() => {
            // this._toastrService.success(
            //   'Kit updated successfully!',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.isEditKit = false;
            this.isKitNovalid = false;
            this.getKitDetails(this.KitFormDetails.id);
          });
        },
        (err) => {
          // console.log(err);
          if (err.status === 403) {
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      );
    } else {
      this.isEditKit = true;
      this.isKitNovalid = true;
    }
  }

  updateProduct(item) {
    this.productsService
      .postElement("Products/productUsed", item)
      .subscribe((Response) => { });
  }

  updateProductCount(item) {
    this.productsService
      .postElement("Products/UpdateProductCount", item)
      .subscribe((Response) => { });
  }

  removeProduct(i, item) {
    if (item) {
      this.kitList.products.splice(i, 1);
      item.product.isUsed = parseInt(item.product.isUsed, 0) - 1;

      this.updateKit(this.kitList);
      // this.updateProduct(item.product);
      item.product["flag"] = 0;
      this.updateProductCount(item.product);

      // this._toastrService.error(
      //   'Product removed successfully!',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.isProductDelete = false;
    } else {
      // this._toastrService.warning(
      //   'Something went wrong please try again!',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.show = true;
      this.popupOpen((this.popupFor = "went wrong"));
      this.show = true;
    }
  }

  removeCustomItem(i) {
    this.kitList.customItem.splice(i, 1);

    this.updateKit(this.kitList);
    this.isItemDelete = false;
  }

  gotoBrowse() {
    this.router.navigate(["/browse/browse"], {
      queryParams: { kid: btoa(this.kitId) },
    });
  }

  updateProdQuantity(item) {
    // const arr = item.quantity.split('.');
    // item.quantity = arr.length >= 2 ?
    //   arr[0] + '.' + arr[1].substring(0, 2) : item.quantity;
    item.quantity =
      item.product.productType.description === "Material"
        ? parseInt(item.quantity, 0) > 0
          ? parseInt(item.quantity, 0)
          : 1
        : item.quantity > 0
          ? item.quantity
          : 1;

    const arr = item.quantity.toString().split(".");
    item.quantity =
      arr.length >= 2
        ? +(arr[0] + "." + arr[1].substring(0, 2))
        : item.quantity;

    this.updateKitOnDBsite(this.kitList);

    // item.quantity = parseInt(item.quantity, 0);
    // if (item.quantity > 0) {
    //   this.updateKit(this.kitList);
    // } else {
    //   item.quantity = 1;
    //   this.updateKit(this.kitList);
    // }
  }

  updateItemQuantity(item) {
    item.quantity =
      item.product.productType.description === "Material"
        ? parseInt(item.quantity, 0) > 0
          ? parseInt(item.quantity, 0)
          : 1
        : item.quantity > 0
          ? item.quantity
          : 1;
    this.updateKit(this.kitList);
    // const val = parseInt(item.quantity, 0);
    // if (val > 0) {
    //   this.updateKit(this.kitList);
    // } else {
    //   item.quantity = 1;
    //   this.updateKit(this.kitList);
    // }
  }

  deleteKit() {
    this.loading = true;
    this.kitList.isActive = false;
    this._KitsService.DeleteKit(this.kitList.id).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            if (this.kitList.products !== null) {
              this.kitList.products.forEach((element) => {
                element.product.isUsed = element.product.isUsed - 1;
                // this.updateProduct(element.product);
                element.product["flag"] = 0;
                this.updateProductCount(element.product);
              });
            }
            // this._toastrService.error(
            //   'Kit deleted successfully',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.isKitDelete = false;
            this.router.navigate(["/kits/kits-list"]);
          }
          this.loading = false;
        });
      },
      (err) => {
        // console.log(err);
        this.loading = false;
        if (err.status === 403) {
          // tslint:disable-next-line:one-line
          // this._toastrService.warning(
          //   'Access denied',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.show = true;
          this.popupOpen((this.popupFor = "Access denied"));
        }
      }
    );
  }

  CalculateCost(element) {
    if (element.margin && element.margin.id) {
      if (element.margin.method === this.constants.PricingMethods.List) {
        element.price = parseFloat(element.product.msrp);
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;

        return element.price;
      } else if (
        element.margin.method === this.constants.PricingMethods.Markup
      ) {
        element.price = element.product.cost * (1 + element.margin.value / 100);
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return element.price;
      } else if (
        element.margin.method === this.constants.PricingMethods.Margin
      ) {
        element.price = element.product.cost / (1 - element.margin.value / 100);
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return element.price;
      } else if (
        element.margin.method === this.constants.PricingMethods.Discount
      ) {
        element.price =
          element.product.msrp -
          (element.product.msrp * element.margin.value) / 100;
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return element.price;
      }
    }
    // element.price = element.product.cost.toFixed(2);
    element.price = Math.round((element.price + Number.EPSILON) * 100) / 100;
    return element.price;
  }

  CalculatePrice(element) {
    if (element.margin && element.margin.id) {
      if (element.margin.method === this.constants.PricingMethods.List) {
        let amount =
          Math.round((element.product.msrp + Number.EPSILON) * 100) / 100;
        return amount * element.quantity;
      } else if (
        element.margin.method === this.constants.PricingMethods.Markup
      ) {
        const val: any =
          element.product.cost * (1 + element.margin.value / 100);
        let amount = Math.round((val + Number.EPSILON) * 100) / 100;
        return amount * element.quantity;
      } else if (
        element.margin.method === this.constants.PricingMethods.Margin
      ) {
        const val: any =
          element.product.cost / (1 - element.margin.value / 100);
        let amount = Math.round((val + Number.EPSILON) * 100) / 100;
        return amount * element.quantity;
      } else {
        const val: any =
          element.product.msrp -
          (element.product.msrp * element.margin.value) / 100;
        let amount = Math.round((val + Number.EPSILON) * 100) / 100;
        return amount * element.quantity;
      }
    }
    let amount =
      Math.round((element.product.cost + Number.EPSILON) * 100) / 100;
    return amount * element.quantity;
  }

  checkCost(item) {
    const val = parseFloat(item.cost);
    if (val > 0.0) {
      this.isCostValid = true;
      this.customItem.cost = item.cost;
    } else {
      this.isCostValid = false;
      // item.cost = null;
    }
  }

  updateCustomItemPrice(item) {
    const val = parseFloat(item.price);
    if (val > 0.0) {
      this.isPriceValid = true;
    } else {
      this.isPriceValid = false;
      // item.price = null;
    }
  }

  checkQuantity(item) {
    const val = parseInt(item.quantity, 0);
    if (val > 0) {
      this.customItem.quantity = item.quantity;
    } else {
      item.quantity = null;
    }
  }

  onProductSearch(body) {
    if (body.length) {
      this.searched = [];
      this.showSearch = true;
      this.search.productName = body;
      this.search.manufacturerPartNumber = body;
      // // console.log('Request Search:', body , ' Time: ', new Date());
      this.callSequenceForSearchProduct.push(
        body + this.callSequenceForSearchProduct.length
      );
      this.dataService
        .getElementThroughAnyParam("Products", this.search)
        .subscribe((res) => {
          // tslint:disable-next-line:max-line-length
          if (
            body.toString() + (this.callSequenceForSearchProduct.length - 1) ===
            this.callSequenceForSearchProduct[
            this.callSequenceForSearchProduct.length - 1
            ]
          ) {
            // // console.log(body + ' *************', this.callSequenceForSearchProduct);
            let data = [];
            data = res;
            this.searched = [];
            if (data.length) {
              this.searched.push({
                collectionName: this.collection,
                collectionData: data,
              });
              this.showSearchDropdown = true;
            }
          }
          // // console.log('Request Search:', body , ' After this.searched: ', this.searched);
        });
    }
  }

  // method to check if method property absent, if so then update method
  checkMethods(collection) {
    let change = false;
    this.kitList.products = JSON.parse(JSON.stringify(collection.products));
    if (collection.products) {
      collection.products.forEach((prod) => {
        if (prod.product.pricingMethod) {
          prod.product.pricingMethod.forEach((pricingMethods) => {
            if (
              !pricingMethods["method"] ||
              pricingMethods["method"] === null
            ) {
              this.masterMethodList.forEach((masterMethod) => {
                if (masterMethod.name === pricingMethods.name) {
                  pricingMethods.method = pricingMethods.name;
                  pricingMethods.name =
                    pricingMethods.value + pricingMethods.name;
                  change = true;
                } else {
                  change = false;
                }
              });
            } else if (pricingMethods["method"] !== null) {
              change = false;
              // this.kit = JSON.parse(JSON.stringify(collection));
              // this.kitList = JSON.parse(JSON.stringify(collection));
              // this.KitNameFormDetails = JSON.parse(JSON.stringify(collection));
              // this.KitFormDetails = JSON.parse(JSON.stringify(collection));
            }
          });
        }
        if (prod.margin) {
          if (!prod.margin["method"] || prod.margin["method"] === null) {
            prod.margin.method = prod.margin.name;
            prod.margin.name = prod.margin.value + prod.margin.name;
            change = true;
          } else {
            change = false;
          }
        }
      });
    }
    if (change) {
      this.kitList.products = JSON.parse(JSON.stringify(collection.products));

      this.updateKit(this.kitList);
    }
  }

  discardChanges() {
    this.KitNameFormDetails = JSON.parse(JSON.stringify(this.kit));
    this.KitFormDetails = JSON.parse(JSON.stringify(this.kit));
  }

  onEditorCreated(quill) {
    this.quill = quill;
    this.quill.disable();
  }

  // messages display
  popupOpen(popupFor) {
    if (this.popupFor === "copyKit") {
      this.typeofMessage = "Hello";
      this.messageToDisplay = "Would you like to copy the kit:";
      this.noteToDisplay = null;
      this.dataToDisplay = this.kit.name + "?";
      this.showButtons = true;
      this.notification = null;
    } else if (this.popupFor === "deleteKit") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this kit?";
    } else if (this.popupFor === "deleteproduct") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this product?";
    } else if (this.popupFor === "deleteCustompPoduct") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this customproduct?";
    } else if (this.popupFor === "Cannot Add Item") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Cannot Add Item";
    } else if (this.popupFor === "Access denied") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Access denied";
    } else if (this.popupFor === "went wrong") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Something went wrong please try again!";
    } else if (this.popupFor === "verify product cost") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Please verify product cost and try again";
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === "copyKit") {
      this.copyKit(this.kitList);
      ConfirmModal.hide();
    } else if (this.popupFor === "deleteKit") {
      this.deleteKit();
      ConfirmModal.hide();
    } else if (this.popupFor === "deleteproduct") {
      this.removeProduct(this.productIndex, this.product);
      ConfirmModal.hide();
    } else if (this.popupFor === "deleteCustompPoduct") {
      this.removeCustomItem(this.itemIndex);
      ConfirmModal.hide();
    } else if (this.popupFor === "Cannot Add Item") {
      this.show = false;
    } else if (this.popupFor === "Access denied") {
      this.show = false;
    } else if (this.popupFor === "went wrong") {
      this.show = false;
    }
  }

  no(ConfirmModal) {
    if (this.popupFor === "copyKit") {
      ConfirmModal.hide();
      this.popupFor = "none";
    } else if (this.popupFor === "deleteKit") {
      ConfirmModal.hide();
      this.popupFor = "none";
    } else if (this.popupFor === "deleteproduct") {
      ConfirmModal.hide();
      this.popupFor = "none";
    } else if (this.popupFor === "deleteCustompPoduct") {
      ConfirmModal.hide();
      this.popupFor = "none";
    }
    this.show = false;
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.show = false;
  }

  updateKitOnDBsite(newValue) {
    // this.loading = true;
    this._KitsService.UpdateKit(newValue).subscribe(
      () => {
        this.ngZone.run(() => { });
      },
      (err) => {
        // console.log(err);
        if (err.status === 403) {
          // this._toastrService.warning(
          //   'Access denied',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.show = true;
          this.popupOpen((this.popupFor = "Access denied"));
        }
      }
    );
  }

  onBlur() {
    setTimeout(() => {
      this.showSearchDropdown = false;
      this.searchText = null;
    }, 300);
  }

  validateLaborQuantity(evt) {
    evt.target.value.split(".").length >= 2
      ? // tslint:disable-next-line:no-unused-expression
      (evt.target.value =
        evt.target.value.split(".")[0] +
        "." +
        evt.target.value.split(".")[1].substring(0, 2))
      : null;
  }

  goToProductDetails(id) {
    this.productsService.getData("Products/" + id).subscribe((res) => {
      if (res !== null) {
        if (!res.isActive) {
          this.typeofMessage = "Error";
          this.messageToDisplay = null;
          this.noteToDisplay = null;
          this.dataToDisplay = null;
          this.showButtons = false;
          this.notification = "This product is no longer active.";
          this.confirmModal.show();
          return;
        }
        this.router.navigate(["/products/products-detail"], {
          queryParams: { id: btoa(id) },
        });
      }
    });
  }
}
