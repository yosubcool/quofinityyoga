import { HttpClient } from "@angular/common/http";
import { Injectable, NgZone } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { map } from "rxjs";
import { DataService } from "../_core/data.service";
import { Kit } from '../_shared/model/kit-model';
import { Pagination } from "../_shared/model/pagination-model";
import { Search } from "../_shared/model/search-model";

@Injectable()
export class KitsService {
  currentUser: any;
  account: Search = new Search();
  pagination = new Pagination();
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService
  ) {
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    this.pagination = new Pagination();
  }

  // To get all kits through paginantion - Suryakant
  GetKits(pagination: Pagination) {
    return this.dataService
      .getElementThroughAnyObject('Kit/GetKitsPagination', pagination)
      .pipe(map(response => {
        return response;
      }));
  }

  // To add new kit - Suryakant
  AddKit(kit: Kit) {
    kit.isActive = true;
    kit.createdOn = new Date();
    kit.createdBy = this.currentUser.id;
    return this.dataService.postElement('Kit', kit).pipe(map(Response => {
      return Response;
    }));
  }

  // To get kit through id - Suryakant
  GetKitDetails(Id: string) {
    return this.dataService.getData('Kit/' + Id).pipe(map(Response => {
      return Response;
    }));
  }

  // To Update kit data - Suryakant
  UpdateKit(data) {
    data.updatedOn = new Date();
    data.updatedBy = this.currentUser.id;
    return this.dataService.updateElement('Kit', data).pipe(map(Response => {
      return Response;
    }));
  }

  // gets all categories - Suryakant
  GetCategories() {
    this.pagination.alphabet = 'All';
    return this.dataService
      .getElementThroughAnyObject(
        'TenantMaster/GetAllCategories',
        this.pagination
      )
      .pipe(map(Response => {
        return Response.records;
      }));
  }

  GetAllCategories() {
    this.pagination.alphabet = 'All';
    return this.dataService
      .getElementThroughAnyObject(
        'TenantMaster/GetAllCategories',
        this.pagination
      )
      .pipe(map(response => {
        return response.records;
      }));
  }

  // GetAllSubCategories() {
  //   return this.dataService
  //     .getData('TenantMaster/GetAllSubCategories')
  //     .pipe(map(Response => {
  //       return Response;
  //     });
  // }

  // gets all categories - Suryakant
  GetSubCategories(id) {
    return this.dataService
      .getElementThroughId('TenantMaster/GetSubCategories', id)
      .pipe(map(Response => {
        return Response;
      }));
  }

  GetAllProducts() {
    return this.dataService.getData('Products').pipe(map(Response => {
      return Response;
    }));
  }

  GetAllKits() {
    return this.dataService.getData('Kit').pipe(map(Response => {
      return Response;
    }));
  }

  FindProduct(Id) {
    return this.dataService
      .getElementThroughId('Products/Find', Id)
      .pipe(map(response => {
        return response;
      }));
  }

  GetPricingMethods() {
    return this.dataService
      .getData('TenantMaster/GetPricingMethods')
      .pipe(map(response => {
        return response;
      }));
  }

  GetAllTags() {
    return this.dataService.getData('TenantMaster/GetTags').pipe(map(response => {
      return response;
    }));
  }
  getKitForExport() {
    return this.dataService.getData('Kit').pipe(map(response => {
      return response;
    }));
  }

  getModuleAccess() {
    return this.dataService
      .getData('TenantMaster/GetAccessRoleMatrix/')
      .pipe(map(response => {
        return response;
      }));
  }

  DeleteKit(id) {
    return this.dataService.deleteElement('Kit', id).pipe(map(response => {
      return response;
    }));
  }
}
