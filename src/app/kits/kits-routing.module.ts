import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KitsListComponent } from './kits-list/kits-list.component';
import { KitDetailsComponent } from './kits-detail/kit-details.component';
import { RouteGaurd } from '../_core/RouteGaurd';

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      title: 'Kits',
      isLogin: true
    },
    children: [
      {
        path: 'kits-list',
        component: KitsListComponent,
        data: {
          title: 'Kits List'
        }
      },
      {
        path: 'kits-detail',
        component: KitDetailsComponent,
        data: {
          title: 'Kit Detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KitsRoutingModule { }
