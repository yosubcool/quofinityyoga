import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KitsRoutingModule } from './kits-routing.module';
import { KitsListComponent } from './kits-list/kits-list.component';
import { KitDetailsComponent } from './kits-detail/kit-details.component';
import { KitsService } from './kits-service';
import { NgxPaginationModule } from 'ngx-pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { CommonTemplateModule } from '../common/common-template.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SharedModule } from '../_shared/shared.module';
import { CommonTemplateService } from '../common/common-template.service';
import { BrowseService } from '../browse/browse-service';
import { QuotesService } from '../quotes/quotes-service';
import { BrowseModule } from '../browse/browse.module';
// import { DataTablesModule } from 'angular-datatables';
import { QuillModule } from 'ngx-quill';
import { ProductsService } from '../products/products-service';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
@NgModule({
  imports: [
    CommonModule,
    KitsRoutingModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    FormsModule,
    NgxLoadingModule,
    CommonTemplateModule,
    TypeaheadModule.forRoot(),
    SharedModule,
    BrowseModule,
    // DataTablesModule,
    QuillModule.forRoot(),
    TableModule,
    ButtonModule
  ],
  declarations: [KitsListComponent, KitDetailsComponent],
  providers: [
    KitsService,
    ProductsService,
    CommonTemplateService,
    BrowseService,
    QuotesService
  ]
})
export class KitsModule { }
