import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { KitsService } from '../kits-service';
import { Pagination } from '../../_shared/model/pagination-model';
import { Kit } from '../../_shared/model/kit-model';
import { Constants } from '../../_shared/constant';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ngxCsv } from 'ngx-csv';
import { Categories } from '../../_shared/model/tenantMaster-model';
import { CommonTemplateService } from '../../common/common-template.service';
import { Tags } from '../../_shared/model/Tags-model';
import { Actions } from '../../_shared/model/actions-model';
declare var $: any;
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-kits-list',
  templateUrl: './kits-list.component.html',
  styleUrls: ['./kits-list.component.scss']
})
export class KitsListComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  reverse = false;
  key: any;
  headers: any = [];
  kitforExport: any = [];
  kId: any = 0;
  constants: any;
  kitList: Pagination;
  pagesCount: number;
  kit: Kit;
  newKit: any = {};
  kitId = 0;
  selectedAlpha: string;
  public loading = false;
  searchCategory: string;
  category: any;
  categories: any;
  tags: any;
  tag: any;
  accessValid: Actions;
  kitsId = true;
  kitTitle = true;
  price = true;
  cost = true;
  pricingMethod = true;
  kitsnumber = true;
  public counter = 0;
  public oldCount = 0;
  subscriptions: Subscription[] = [];
  showClearButton = false;
  showTagClearButton = false;
  items = [];
  itemCount = 0;
  editProperty: string;
  columns = [];
  toBeEdit = 'none';
  showKitNoAsLink = true;
  isOpenPopup = false;
  constructor(
    private _KitsService: KitsService,
    private ngZone: NgZone,
    private router: Router,
    private _toastrService: ToastrService,
    private _CommonTemplateService: CommonTemplateService
  ) {
    this.accessValid = new Actions();
    this.kitList = new Pagination();
    this.kit = new Kit();
    this.constants = Constants;
    this.selectedAlpha = 'All';
    this.category = new Categories();
    this.categories = new Categories();
    this.tags = new Tags();
    this.tag = new Tags();
    this.kitList.pageSize = 10;
  }

  ngOnInit() {
    $(window).scrollTop(0);
    this.getKitList(0);
    this.getAllCategories();
    this.getAllTags();
    this.getModuleAccess();
    this.columns = [{ property: "uniqueId", header: "Kit ID" }, { property: "name", header: "Kit Title" }, { property: "number", header: "Kit Number" }, { property: "total", header: "Price" }]
    if (window.innerWidth <= 1024) {
      this.kitsId = false;
      this.price = false;
      this.showKitNoAsLink = true;
    } else {
      this.kitsId = true;
      this.price = true;
      this.showKitNoAsLink = false;

    }
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
      // console.log('Done');
    }, 500);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
    if (document.getElementById('kit') !== null) {
      document.getElementById('kit').classList.remove('active');
    }
  }

  getModuleAccess() {
    this.subscriptions.push(
      this._KitsService.getModuleAccess().subscribe(res => {
        this.accessValid = res.kit;
      })
    );
  }

  // To Add New Kit - Suryakant
  addKit(kitForm, model) {
    this.loading = true; // Loader Enable
    if (kitForm.valid) {
      model.hide();
      this._KitsService.AddKit(this.kit).subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.loading = false; // Loader Disable
              this.kit = new Kit();
              this.router.navigate(['/kits/kits-detail'], {
                queryParams: { id: btoa(res.id) }
              });
            }
          });
          this.loading = false; // Loader Disable
        },
        err => {
          if (err.status === 403) {
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false; // Loader Disable
        }
      );
    } else {
      this.loading = false; // Loader Disable
    }
  }

  // TO Get Kit Details Using Alphabate - Suryakant
  getKits(a) {
    this.loading = true; // LoaderEnable
    this.selectedAlpha = a;
    this.getKitList(0);
    this.loading = false; // Loader Disable
  }

  // To Get Page Number Wise/All Kit details Using Pagination/Default - Suryakant
  getKitList(pageNo) {
    let sortingData = pageNo;
    //Adding this logic to accomodate inclusion of PrimeNg. Event is sent by the p-table.
    if (isNaN(pageNo)) {
      pageNo = pageNo.first / pageNo.rows + 1;
    }
    this.loading = true; // Loader Enable
    this.kitList.sortBy = 'name';
    this.kitList.sortType = 'desc';
    if (sortingData != 0) {
      if (sortingData.sortOrder === 1) {
        this.kitList.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.kitList.sortType = 'asc';
      }
      else {
        this.kitList.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.kitList.sortType = 'desc';
      }
    }
    if (this.key) {
      this.kitList.sortBy = this.key;
    }
    if (this.reverse) {
      this.kitList.sortType = 'asc';
    } else {
      this.kitList.sortType = 'desc';
    }
    this.kitList.pageNumber = pageNo;

    this.kitList.alphabet =
      this.selectedAlpha === 'All' ? '' : this.selectedAlpha;
    this.subscriptions.push(
      this._KitsService.GetKits(this.kitList).subscribe(
        res => {
          this.ngZone.run(() => {
            this.kitList = res;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            this.items = JSON.parse(JSON.stringify(this.kitList.records));
            this.itemCount = this.kitList.totalCount;
          });
          this.loading = false; // Loader Disable
        },
        err => {
          // console.log(err);
          if (err.status === 403) {
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
            this.loading = false; // Loader Disable
          }
        }
      )
    );
  }

  //  To Reset Modal Dialogue - Suryakant
  onModelClose() {
    this.kit = new Kit();
  }
  resetForm(kitForm) {
    kitForm.Reset();
  }

  //  To Get Kit Details Using Id  - Suryakant
  getKitDetails(id) {
    this.loading = true; // Loader Enable
    this.router.navigate(['/kits/kits-detail'], {
      queryParams: { id: btoa(id) }
    });
    this.loading = false; // Loader Disable
  }

  saveTitle(form: NgForm, newValue) {
    if (form.valid && newValue.name) {
      this.updateKit(newValue);
      this.toBeEdit = 'none';
    } else {
      this.toBeEdit = 'name';
    }
  }
  // To Update Kit Title Inline - Suryakant
  updateKit(newValue) {
    this._KitsService.UpdateKit(newValue).subscribe(Response => {
      if (Response) {
        this.getKitList(this.kitList.pageNumber);
      }
    });
  }
  exportToCSV() {
    this.loading = true;
    this.subscriptions.push(
      this._KitsService.getKitForExport().subscribe(
        res => {
          this.kitforExport.push(res);

          if (this.kitforExport.length) {
            const data = [];
            this.kitforExport[0].forEach(element => {
              const obj: any = {};
              this.columns.forEach(col => {
                const prop = col.property;
                obj[prop] = element[prop];
                this.headers.push(col.header);
              });
              data.push(obj);
            });
            const header = this.headers.filter((v, i, a) => a.indexOf(v) === i);
            const options = {
              fieldSeparator: ',',
              quoteStrings: '"',
              decimalseparator: '.',
              showLabels: true,
              showTitle: true,
              headers: header
            };
            this.headers = [];
            new ngxCsv(data, 'Kits', options);
            this.loading = false;
          } else {
            // this._toastrService.warning(
            //   'No data available to export!',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'No data available to export!'));
            this.loading = false;
          }
        },
        err => {
          // console.log(err);
          if (err.status === 403) {
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
          this.loading = false;
        }
      )
    );
  }

  // To Get All Categories - Suryakant
  getAllCategories() {
    this.subscriptions.push(
      this._KitsService.GetAllCategories().subscribe(
        Response => {
          this.ngZone.run(() => {
            this.categories = Response;
          });
        },
        err => { }
      )
    );
  }
  // To Get All Tags - Suryakant
  getAllTags() {
    this.subscriptions.push(
      this._KitsService.GetAllTags().subscribe(
        Response => {
          this.ngZone.run(() => {
            this.tags = Response;
          });
        },
        err => { }
      )
    );
  }
  // To Reset filters - Suryakant
  onKeyDown() {
    if (this.kitList.category === '' && this.kitList.category.length === 0) {
      this.getKitList(0);
    }
    if (this.kitList.tags === '' && this.kitList.tags.length === 0) {
      this.getKitList(0);
    }
  }
  // To Sort grid colums - Suryakant
  sort(key) {
    this.key = key.sortBy;
    this.reverse = key.sortAsc;
    this.getKitList(0);
  }

  editItem(item, edit) {
    this.editProperty = item.id;
    this.toBeEdit = edit;
    this.items = JSON.parse(JSON.stringify(this.kitList.records));
  }

  // Discard Inline Edit - Suryakant
  discardEdit() {
    this.toBeEdit = 'none';
    this.items = JSON.parse(JSON.stringify(this.kitList.records));
  }
  saveNumber(form: NgForm, newValue) {
    if (form.valid && (newValue.number !== null || newValue.number !== '')) {
      this.updateKit(newValue);
      this.toBeEdit = 'none';
    } else {
      this.toBeEdit = 'number';
    }
  }

  getChangedColumns(columns) {
    this.columns = columns.datatable.columns._results;
    this.toBeEdit = 'none';
    if (this.kitList.records) {
      this.items = JSON.parse(JSON.stringify(this.kitList.records));
    }
  }

  // messages display
  popupOpen(popupFor) {
    if (this.popupFor === 'Access denied') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Access denied';
    } else if (this.popupFor === 'No data available to export!') {
      this.typeofMessage = 'Error';
      this.notification = 'No data available to export!';
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.messageToDisplay = null;
    }
  }

  yes(ConfirmModal) {
    ConfirmModal.hide();
    this.show = false;
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }

  clonedKits: { [s: string]: any; } = {};

  onRowEditInit(item: any) {
    this.clonedKits[item.id] = JSON.parse(JSON.stringify(item));
    console.log('Initial Value Before Edit');
    console.log(this.clonedKits[item.id]);
  }

  onRowEditSave(item: any) {
    delete this.clonedKits[item.id];
    this.updateKit(item);
  }

  onRowEditCancel(item: any, index: number) {
    this.items[index] = this.clonedKits[item.id];
    delete this.clonedKits[item.id];
  }
}
