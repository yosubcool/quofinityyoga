import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs';
import { DataService } from '../_core/data.service';
import { Pagination } from '../_shared/model/pagination-model';
import { User } from '../_shared/model/user-model';
// import { Pagination } from 'app/_shared/model/pagination-model';

@Injectable()
export class ProductsService {
  pagination = new Pagination();
  user: User;
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService
  ) {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.pagination = new Pagination();
  }

  findSupplier(description) {
    return this.dataService
      .getElementThroughId('Products/FindSupplier', description)
      .pipe(map(response => {
        return response;
      }));
  }


  getData(method) {
    return this.dataService.getData(method).pipe(map(response => {
      return response;
    }));
  }

  postElement(method, model) {
    model.createdBy = this.user.id;
    model.createdOn = new Date();
    return this.dataService.postElement(method, model).pipe(map(response => {
      return response;
    }));
  }

  updateElement(method, model) {
    model.updatedBy = this.user.id;
    model.updatedOn = new Date();
    return this.dataService.updateElement(method, model).pipe(map(response => {
      return response;
    }));
  }
 
  
  UpdateProduct(data) {
    data.updatedOn = new Date();
    return this.dataService.updateElement('Products', data).pipe(map(response => {
      return response;
    }));
  }
  getElementThroughAnyObject(method, obj) {
    obj.isActive = true;
    return this.dataService
      .getElementThroughAnyObject(method, obj)
      .pipe(map(response => {
        return response;
      }));
  }

  findProductByPartNo(product) {
    // product.isActive = true;
    return this.dataService
      .getElementThroughAnyObject('Products/FindProductByPartNo', product)
      .pipe(map(response => {
        return response;
      }));
  }

}
