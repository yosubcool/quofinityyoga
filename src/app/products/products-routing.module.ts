import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteGaurd } from '../_core/RouteGaurd';
import { ProductsDetailComponent } from './products-detail/products-detail.component';
import { ProductsListComponent } from './products-list/products-list.component';


const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      title: 'Products',
      isLogin: true
    },
    children: [
      {
        path: 'products-detail',
        component: ProductsDetailComponent,
        data: {
          title: 'Product Detail'
        }
      },
      {
        path: 'products-list',
        component: ProductsListComponent,
        data: {
          title: 'Product List'
        }
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }

