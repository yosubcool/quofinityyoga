// import { element } from 'protractor/globals';
import { Jsonp } from "@angular/http/src/http";
// import { debug } from "util";
// import { debounce } from "rxjs/operator/debounce";
// import { Observable } from "rxjs/Rx";
// import { Subscription } from "./../../_shared/model/subscription-model";
// import { TagsComponent } from "app/common/tags/tags.component";
import { ToastrService } from "ngx-toastr";
import {
  Component,
  OnInit,
  NgZone,
  OnDestroy,
  ViewChild,
  ElementRef,
} from "@angular/core";
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError,
  ActivatedRoute,
} from "@angular/router";
import { ProductsService } from "../products-service";
import { Product, Image } from "../../_shared/model/product-model";
import {
  Suppliers,
  Categories,
  SubCategories,
  ProductType,
} from "../../_shared/model/tenantMaster-model";
import { Constants } from "../../_shared/constant";
// import { CategoriesComponent } from "app/common/categories/categories.component";
import { ISlimScrollOptions } from "ngx-slimscroll";
import { NgForm } from "@angular/forms";
import { Subscription } from "rxjs";
import { PricingMethod } from "src/app/_shared/model/pricing-model";
import { Pagination } from "src/app/_shared/model/pagination-model";
import { SettingsService } from "src/app/settings/settings-service";
import { Actions } from "src/app/_shared/model/actions-model";

declare var $: any;
@Component({
  selector: "app-products-detail",
  templateUrl: "./products-detail.component.html",
  styles: [],
})
export class ProductsDetailComponent implements OnInit, OnDestroy {
  config: {
    removeButtons: string;
    format_tags: string;
    addButton: string;
    target: string;
  };
  @ViewChild("ConfirmationModel") confirmationModel: ElementRef;
  @ViewChild("pricingForm") pricingForm: NgForm;
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  editConfirmshow = false;
  validImg = false;
  show = false;
  subscriptions: Subscription[] = [];
  index = 0;
  start = null;
  popUpOpen = false;
  focusTextBox = true;
  imageSource: string;
  public loading = false;
  constants: any;
  // for supplier
  searchText: string;
  supplierList: any = [];
  supplier: Suppliers;
  filteredListSub = [];
  supplierPresent = false;

  // check non zero
  isCostValid = false;
  isMsrpValid = false;
  isPartNovalid = false;
  isUPCNovalid = false;
  // for product Type
  productTypeList: any = [];
  productType: ProductType;
  productTypeId: string;

  // for title
  productTitleCopy = false;
  productTitleEdit = false;
  prodName: string;
  duplicatePartNo = false;
  formProductDetails: Product;
  pricingMethod: PricingMethod;
  presentMethod = [];
  productTypes: any = [];
  productId: string;
  masterMethodList: any = [];
  methodList: any = [];
  tenantMethodList = new Pagination();
  description: any;
  tobeDelete: string;
  tobeEdit: string;
  tobeEditPrevious: string;
  tobeEditFrom: string;
  productTypeDropdown = false;
  // methodDropdown = false;
  isImageSizeLarge = false;
  isEditTab = false;
  product: Product;
  productDetails: Product;
  image: Image;
  images = [];
  opts: ISlimScrollOptions;
  accessValid: Actions;
  quill: any;
  isProduectUsed = false;
  methodUsed = false;
  methodNotExist = false;
  messageForDeleteProduct =
    "Product cannot be deleted because it’s part of a kit.";
  isOpenPopup = false;
  urls: string;

  manufacturers: any[];
  unitOfMeasures: any[];
  Cost_REXEXP: any;
  costMaxLength: number;
  constructor(
    private route: ActivatedRoute,
    private _productsService: ProductsService,
    private router: Router,
    private ngZone: NgZone,
    private settingsService: SettingsService,
    private _toastrService: ToastrService
  ) {
    this.unitOfMeasures = new Array<any>();
    this.popupFor = "none";
    this.accessValid = new Actions();
    this.supplierList = new Array<Suppliers>();
    this.productTypeList = new Array<ProductType>();
    this.constants = Constants;
    this.Cost_REXEXP = Constants.Cost_REXEXP;
    this.costMaxLength = Constants.MaxLengthConstants.Cost;
    this.productDetails = new Product();
    this.image = new Image();
    this.formProductDetails = new Product();
    this.supplier = new Suppliers();
    this.productType = new ProductType();
    this.tenantMethodList = new Pagination();
    this.pricingMethod = new PricingMethod();
    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        let id = this.route.snapshot.queryParams["id"];
        id = atob(id);
        if (this.productId && this.productId !== id) {
          this.loading = true;
          if (!id) {
            this.router.navigate(["/products/products-list"]);
          }
          this.productId = id;
          this.getPricing();
          this.getMethodList();
          this.getProductType();
          this.getModuleAccess();
          this.getProduct(this.productId);
          $(window).scrollTop(0);
          if (document.getElementById("products") !== null) {
            document.getElementById("products").classList.add("active");
          }
          this.loading = false;
        }
      }
    });

    this.config = {
      removeButtons:
        "About,Cut,Copy,Paste,PasteText,PasteFromWord,Source,Anchor,",
      format_tags: "p;pre",
      addButton: "Save",
      target: "_blank",
    };
    this.manufacturers = [];
  }

  ngOnInit() {
    $(window).scrollTop(0);
    const id = this.route.snapshot.queryParams["id"];
    if (!id) {
      this.router.navigate(["/products/products-list"]);
    }
    this.productId = atob(id);
    this.getPricing();
    this.getMethodList();
    this.getProductType();
    this.getModuleAccess();
    this.getAllManufacturers();
    this.getMeasureList();
    this.getProduct(this.productId);
    if (document.getElementById("products") !== null) {
      document.getElementById("products").classList.add("active");
    }
    this.tobeEdit = "nun";
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
      // // console.log('Done');
    }, 800);
  }
  ngOnDestroy() {
    if (document.getElementById("products") !== null) {
      document.getElementById("products").classList.remove("active");
    }
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  getProduct(id) {
    this.subscriptions.push(
      this._productsService.getData("Products/" + id).subscribe(
        (res) => {
          this.checkMethods(res);
          this.ngZone.run(() => {
            this.product = JSON.parse(JSON.stringify(res));
            this.productDetails = JSON.parse(JSON.stringify(res));
            this.prodName = res.name;
            if (!this.product.productType) {
              this.productTypeId = this.start;
            }
            if (this.productDetails.images === null) {
              this.images = [];
            } else {
              this.images = this.productDetails.images;
            }
            this.description = this.productDetails.description;
            if (this.product.category !== null) {
            }
            this.getProductType();
            this.formProductDetails = JSON.parse(
              JSON.stringify(this.productDetails)
            );
          });
        },
        (err) => { }
      )
    );
  }

  getModuleAccess() {
    this.subscriptions.push(
      this._productsService
        .getData("TenantMaster/GetAccessRoleMatrix/")
        .subscribe((res) => {
          this.accessValid = res.products;
        })
    );
  }

  CheckPartNo(item) {
    if (item.match("^[0]+$")) {
      const val = parseInt(item, 0);
      if (val === 0) {
        // tslint:disable-next-line:one-line
        this.isPartNovalid = true;
      } else {
        this.isPartNovalid = false;
      }
    } else {
      this.isPartNovalid = false;
    }
  }

  removeLeadSpace(from, partNo) {
    if (from === "PartNumber") {
      this.formProductDetails.partNo = partNo.replace(/^\s+|\s+$/g, "");
    } else if (from === "ManufacturerPartNumber") {
      this.formProductDetails.manufacturerPartNumber = partNo.replace(
        /^\s+|\s+$/g,
        ""
      );
    } else if (from === "Manufacturer") {
      this.formProductDetails.manufacturer = partNo.replace(/^\s+|\s+$/g, "");
    }
  }

  CheckUPC(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isUPCNovalid = true;
    } else {
      this.isUPCNovalid = false;
    }
  }

  getProductType() {
    this.subscriptions.push(
      this._productsService.getData("Configuration/GetProductType").subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.productTypeList = res;
            this.productTypes = res;
          });
        },
        (err) => { }
      )
    );
  }

  uploadPic(evt) {
    this.changeToByte(evt.target);
    evt.target.value = "";
  }

  changeToByte(inputValue) {
    this.validImg = false;
    this.isImageSizeLarge = false;
    const file: File = inputValue.files[0];
    const myReader: FileReader = new FileReader();
    if (
      file.type === "image/jpeg" ||
      file.type === "image/png" ||
      file.type === "image/jpg"
    ) {
      if (file.size < 1051341) {
        myReader.onloadend = (e) => {
          this.image.string = myReader.result;
        };
        this.image.name = file.name;
        this.image.type = file.type;
        myReader.readAsDataURL(file);
        if (this.images.length < 3 || this.images === null) {
          setTimeout(() => {
            this.images.push(this.image);
            this.formProductDetails.images = this.images;
            this.changeDetail(true, this.formProductDetails);
            this.image = new Image();
          }, 1000);
        } else {
          // this._toastrService.error(
          //   'Can not add more than three images.',
          //   '',
          //   this.constants.toastrConfig
          // );
          // this.validImg = false;
          // this.show = true;

          this.show = true;
          this.popupOpen(
            (this.popupFor =
              "The maximum number of images per product is three")
          );
        }
      } else {
        this.isImageSizeLarge = true;
      }
    } else {
      // this._toastrService.error(
      //   'Please provide valid details.',
      //   '',
      //   this.constants.toastrConfig
      // );

      this.show = true;
      this.popupOpen(
        (this.popupFor = "The maximum number of images per product is three")
      );

      // this.show = true;
      // this.popupOpen('The maximum number of images per product is three');
      // this.validImg = true;
    }
    this.tobeEditPrevious = undefined;
  }

  onDeleteImage(id) {
    this.isImageSizeLarge = false;
    let i = 0;
    if (this.formProductDetails.images) {
      const img = JSON.parse(JSON.stringify(this.formProductDetails.images));
      img.forEach((sub) => {
        if (sub.id === id) {
          this.formProductDetails.images.splice(i, 1);
        } else {
          i = i + 1;
        }
      });
    }
    this.productDetails.images = this.formProductDetails.images;
    this.changeDetail(true, this.productDetails);
  }

  tagDelete(index) {
    this.tobeEdit = "nun";
    this.productTitleCopy = false;
    this.formProductDetails.tags.splice(index, 1);
    this.productDetails.tags = this.formProductDetails.tags;
    this.changeDetail(true, this.productDetails);
  }

  categoryDelete(index) {
    this.tobeEdit = "nun";
    this.productTitleCopy = false;
    const cate = this.formProductDetails.category[index];
    let i = 0;
    if (this.formProductDetails.subCategory) {
      const dSubcategory = JSON.parse(
        JSON.stringify(this.formProductDetails.subCategory)
      );
      dSubcategory.forEach((sub) => {
        if (
          sub.categories.id === cate.id ||
          sub.categories.description === cate.description
        ) {
          this.formProductDetails.subCategory.splice(i, 1);
        } else {
          i = i + 1;
        }
      });
    }
    this.formProductDetails.category.splice(index, 1);
    this.productDetails.category = this.formProductDetails.category;
    this.productDetails.subCategory = this.formProductDetails.subCategory;
    this.changeDetail(true, this.productDetails);
  }

  subcategoryDelete(index) {
    this.tobeEdit = "nun";
    this.productTitleCopy = false;
    this.formProductDetails.subCategory.splice(index, 1);
    this.productDetails.subCategory = this.formProductDetails.subCategory;
    this.changeDetail(true, this.productDetails);
  }

  changeDetail(isvalid: boolean, formProductDetails) {
    if (isvalid) {
      this._productsService
        .updateElement("Products", formProductDetails)
        .subscribe(
          (res) => {
            this.ngZone.run(() => {
              if (res) {
                this.tobeEdit = "nun";
                // this.tobeEdit = '';
                this.getProduct(this.formProductDetails.id);
                this.getMethodList();
                this.pricingMethod = null;
              }
            });
          },
          (err) => {
            if (err.status === 403) {
              // tslint:disable-next-line:one-line
              // this._toastrService.warning(
              //   'Access denied',
              //   '',
              //   this.constants.toastrConfig
              // );

              this.show = true;
              // console.log(err);
              this.popupOpen((this.popupFor = "Access denied"));
            }
          }
        );
    }
  }

  deleteProduct(formProductDetails, model) {
    this.isProduectUsed = false;
    if (formProductDetails.isUsed === 0) {
      formProductDetails.isActive = false;
      this._productsService
        .updateElement("Products/Delete", formProductDetails)
        .subscribe(
          (res) => {
            this.ngZone.run(() => {
              if (res) {
                // this._toastrService.error(
                //   'Product Deleted.',
                //   '',
                //   this.constants.toastrConfig
                // );
                model.hide();
                this.loading = false;
                this.router.navigate(["/products/products-list"]);
              }
            });
          },
          (err) => {
            // console.log(err);
            if (err.status === 403) {
              // tslint:disable-next-line:one-line
              // this._toastrService.warning(
              //   'Access denied',
              //   '',
              //   this.constants.toastrConfig
              // );

              this.show = true;
              // console.log(err);
              this.popupOpen((this.popupFor = "Access denied"));
            }
          }
        );
    } else {
      // this._toastrService.error(
      //   'Product is already used in kit / quote',
      //   '',
      //   this.constants.toastrConfig
      // );

      // this.show = true;
      // this.popupOpen(this.popupFor = 'Product is already used in kit / quote');
      this.isProduectUsed = true;

      this.show = true;
      this.popupOpen((this.popupFor = this.messageForDeleteProduct));
      this.loading = false;
    }
  }

  // Add tags array
  tagsArray(event) {
    this.tobeEdit = "nun";
    this.productTitleCopy = false;
    if (this.formProductDetails.tags === null) {
      const arr = [];
      arr.push(event);
      this.formProductDetails.tags = arr;
    } else {
      this.formProductDetails.tags.push(event);
    }
    this.changeDetail(true, this.formProductDetails);
  }

  // edit subcategory
  subCategories(event) {
    this.tobeEdit = "nun";
    this.productTitleCopy = false;
    if (this.formProductDetails.subCategory === null) {
      this.formProductDetails.subCategory = event;
    } else {
      this.formProductDetails.subCategory.push(event[0]);
    }
    this.changeDetail(true, this.formProductDetails);
  }

  // edit subcategory
  categories(event) {
    this.tobeEdit = "nun";
    this.productTitleCopy = false;
    if (this.formProductDetails.category === null) {
      this.formProductDetails.category = event;
    } else {
      this.formProductDetails.category.push(event[0]);
    }
    this.changeDetail(true, this.formProductDetails);
  }

  // Edit Title After Copy
  addTile(isvalid: boolean, formProductDetails) {
    if (isvalid) {
      this.productTitleEdit = true;
      this.productTitleCopy = false;
      formProductDetails.name = formProductDetails.name.replace(
        /^\s+|\s+$/g,
        ""
      );
      this.changeDetail(true, this.formProductDetails);
    } else {
      this.productTitleEdit = false;
      this.productTitleCopy = true;
    }
  }

  // Edit Title After Copy
  editdetails(isvalid: boolean, formProductDetails, supplier) {
    if (supplier.description) {
      this.addSupplier(supplier);
    }
    if (formProductDetails.manufacturer) {
      formProductDetails.manufacturer = formProductDetails.manufacturer.replace(
        /^\s+|\s+$/g,
        ""
      );
    }
    if (formProductDetails.manufacturerPartNumber) {
      formProductDetails.manufacturerPartNumber =
        formProductDetails.manufacturerPartNumber.replace(/^\s+|\s+$/g, "");
    }
    if (formProductDetails.partNo) {
      formProductDetails.partNo = formProductDetails.partNo.replace(
        /^\s+|\s+$/g,
        ""
      );
    }
    if (formProductDetails.name) {
      formProductDetails.name = formProductDetails.name.replace(
        /^\s+|\s+$/g,
        ""
      );
    }
    if (formProductDetails.suppliers.length !== 0) {
      formProductDetails.suppliers[0].description =
        formProductDetails.suppliers[0].description.replace(/^\s+|\s+$/g, "");
    }
    const prod = new Product();
    prod.id = formProductDetails.id;
    prod.partNo = formProductDetails.partNo;
    prod.isActive = formProductDetails.isActive;
    prod.name = formProductDetails.name;
    this._productsService.findProductByPartNo(prod).subscribe(
      (partduplicate) => {
        this.ngZone.run(() => {
          if (partduplicate && partduplicate.id !== formProductDetails.id) {
            this.duplicatePartNo = true;
          } else {
            this.duplicatePartNo = false;
            if (isvalid && !this.isPartNovalid) {
              this.changeDetail(true, this.formProductDetails);
            }
          }
        });
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  // Add product
  addProduct(model) {
    this.productDetails = model;
    this.productDetails.id = null;
    this.productDetails.isUsed = 0;
    this.loading = true;
    this.productTitleCopy = false;
    this.productDetails.name = this.productDetails.name + "_Copy";
    this.productDetails.partNo = this.productDetails.partNo + "_Copy";
    this._productsService
      .postElement("Products", this.productDetails)
      .subscribe(
        (result) => {
          this.ngZone.run(() => {
            if (result) {
              // this._toastrService.success(
              //   'Product Copied Successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.productDetails = result;
              this.formProductDetails = result;
              this.tobeEdit = "nun";
              this.router.navigate(["/products/products-detail"], {
                queryParams: { id: btoa(result.id) },
              });
              this.loading = false;
            }
          });
        },
        (err) => {
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );

            this.show = true;
            // console.log(err);
            this.popupOpen((this.popupFor = "Access denied"));
          }
          // this._toastrService.error(
          //   'Please provide valid details.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.loading = false;
        }
      );
  }

  discard(id) {
    this.getProduct(id);
    // this._toastrService.info(
    //   'Change Discarded.',
    //   '',
    //   this.constants.toastrConfig
    // );
  }

  imageToShow(img) {
    this.imageSource = img.string;
    this.tobeEditPrevious = undefined;
  }

  // pricing
  // Gets all methods
  getPricing() {
    this.subscriptions.push(
      this._productsService.getData("Configuration/GetPricingMethod").subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.masterMethodList = res;
          });
        },
        (err) => { }
      )
    );
  }

  // for type heads for method
  findPricingMethod(searchText) {
    this.pricingMethod = new PricingMethod();
    this.pricingMethod.name = searchText;
    this._productsService
      .getElementThroughAnyObject(
        "TenantMaster/SearchPricingMethod",
        this.pricingMethod
      )
      .subscribe((res) => {
        this.ngZone.run(() => {
          this.methodList = res;
        });
      });
    // this.methodList = this.tenantMethodList.records.filter(
    //   item => item.name.indexOf(searchText) !== -1
    // );
  }

  editpricing(form: NgForm, isvalid: boolean, formProductDetails) {
    formProductDetails.msrp =
      formProductDetails.msrp === "" ? null : +formProductDetails.msrp;
    if (
      isvalid &&
      formProductDetails.cost !== 0 &&
      this.productTypeId !== null
    ) {
      this.productTypeDropdown = false;
      const producttype = this.productTypes.filter(
        (item) => item.id.indexOf(this.productTypeId) !== -1
      );
      if (this.formProductDetails.pricingMethod) {
        this.formProductDetails.pricingMethod.forEach((pMethod) => {
          if (pMethod.method === this.masterMethodList[0].name) {
            pMethod.value = this.formProductDetails.msrp;
          }
        });
      }
      this.formProductDetails.productType = producttype[0];
      this.changeDetail(true, this.formProductDetails);
      this.tobeEdit = "";
    } else {
      this.productTypeDropdown = true;
      if (formProductDetails.cost === 0 && formProductDetails.msrp === 0) {
        this.isMsrpValid = true;
        this.isCostValid = true;
      } else if (formProductDetails.cost === 0) {
        this.isCostValid = true;
      } else if (formProductDetails.msrp === 0) {
        this.isMsrpValid = true;
      }
    }
  }

  addPricingMethod(Model, methodForm) {
    this.methodUsed = false;
    this.methodNotExist = false;
    if (this.pricingMethod !== undefined) {
      if (this.formProductDetails.pricingMethod === null) {
        const arr = [];
        arr.push(this.pricingMethod);
        this.formProductDetails.pricingMethod = arr;
        Model.hide();
        this.changeDetail(true, this.formProductDetails);
      } else {
        this.product.pricingMethod.forEach((element) => {
          let valueToCheck: any;
          valueToCheck = +this.pricingMethod.value;
          if (
            element.name === this.pricingMethod.name &&
            element.value === valueToCheck
          ) {
            this.presentMethod.push(element);
          } else if (
            element.name === this.pricingMethod.name &&
            this.pricingMethod.method === this.masterMethodList[0].name
          ) {
            this.presentMethod.push(element);
          }
        });
        if (this.presentMethod.length === 0) {
          this.formProductDetails.pricingMethod.push(this.pricingMethod);
          this.presentMethod = [];
          Model.hide();
          this.changeDetail(true, this.formProductDetails);
        } else {
          this.presentMethod = [];
          // this._toastrService.warning(
          //   'Method already present.',
          //   '',
          //   this.constants.toastrConfig
          // );
          // methodForm.valid = true;
          this.methodUsed = true;
          this.loading = false;
        }
      }
    }
  }

  onSelectPricingMethod(obj) {
    this.pricingMethod = obj.item;
    if (this.pricingMethod.method === this.masterMethodList[0].name) {
      this.pricingMethod.value = this.formProductDetails.msrp;
      this.searchText =
        this.pricingMethod.name +
        " " +
        (this.pricingMethod.method + " - $" + this.pricingMethod.value);
    } else if (this.pricingMethod.method === this.masterMethodList[1].name) {
      this.searchText =
        this.pricingMethod.name +
        " " +
        (this.pricingMethod.method + " - " + this.pricingMethod.value + "%");
    } else if (
      this.pricingMethod.method === this.masterMethodList[2].name ||
      this.pricingMethod.method === this.masterMethodList[3].name
    ) {
      this.searchText =
        this.pricingMethod.name +
        " " +
        (this.pricingMethod.method + " - " + this.pricingMethod.value + "%");
    }
  }

  pricingMethodArray(isvalid, methodForm, Model, id) {
    this.methodUsed = false;
    this.methodNotExist = false;
    if (isvalid) {
      if (this.pricingMethod.id !== null) {
        let valueToCheck: any;
        valueToCheck = +this.pricingMethod.value;
        if (
          methodForm.valid &&
          this.pricingMethod.name === this.masterMethodList[0].name
        ) {
          this.pricingMethod.value = this.formProductDetails.msrp;
          this.addPricingMethod(Model, methodForm);
        } else {
          this.addPricingMethod(Model, methodForm);
          this.loading = false;
        }
      } else {
        // this.methodDropdown = true;
        // this._toastrService.warning(
        //   'Pricing method does not exists.',
        //   '',
        //   this.constants.toastrConfig
        // );
        this.methodNotExist = true;
        this.loading = false;
      }
    }
  }

  deleteMethod(index) {
    this.formProductDetails.pricingMethod.splice(index, 1);
    this.productDetails.pricingMethod = this.formProductDetails.pricingMethod;
    this.changeDetail(true, this.productDetails);
  }
  CheckCost(item) {
    const val = parseFloat(item);
    if (val < 0.0) {
      // tslint:disable-next-line:one-line
      this.isCostValid = true;
    } else {
      this.isCostValid = false;
    }
  }

  CheckMsrp(item) {
    const val = parseFloat(item);
    if (val < 0.0) {
      // tslint:disable-next-line:one-line
      this.isMsrpValid = true;
    } else {
      this.isMsrpValid = false;
    }
  }
  // get methodlist from tenant
  getMethodList() {
    this.loading = true; // Loader Enable
    this.tenantMethodList.pageNumber = 1.2;
    this.tenantMethodList.alphabet = "All";
    this.settingsService.getMethods(this.methodList).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.tenantMethodList = JSON.parse(JSON.stringify(res));
          this.loading = false; // Loader Disable
        });
      },
      (err) => {
        // console.log(err);
        this.loading = false; // Loader Disable
      }
    );
  }

  // supplier
  // Signature changed from  addSupplier(form, model, supplier) - Suryakant
  addSupplier(supplier) {
    let isPresentInTagIdList = [];
    const arr = [];
    let searchText: string;
    searchText = supplier.description;
    // if (form.valid) {
    this.supplier.description = supplier.description;
    if (
      this.product.suppliers === null ||
      this.product.suppliers.length === 0
    ) {
      isPresentInTagIdList = [];
    } else {
      isPresentInTagIdList = this.product.suppliers.filter(
        (item) => item.description === supplier.description
      );
    }
    this.supplier.isActive = true;
    this._productsService
      .getElementThroughAnyObject(
        "TenantMaster/FindSupplierByName",
        this.supplier
      )
      .subscribe((res) => {
        this.ngZone.run(() => {
          if (res) {
            if (isPresentInTagIdList.length !== 0) {
              isPresentInTagIdList = [];
              // this._toastrService.warning(
              //   'Supplier already present.',
              //   '',
              //   this.constants.toastrConfig
              // );
              // this.supplierPresent = true;
              // this.show = true;
              // this.popupOpen(this.popupFor = 'Supplier already present.');

              // this.resetforms(form);
              this.loading = false;
            } else {
              if (this.formProductDetails.suppliers === null) {
                arr.push(res);
                this.formProductDetails.suppliers = arr;
              } else {
                this.formProductDetails.suppliers = new Array<Suppliers>();
                this.formProductDetails.suppliers.push(res);
              }
              // this._toastrService.success(
              //   'Supplier Added Successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              // model.hide();
              this.changeDetail(true, this.formProductDetails);
              // this.supplier = new Suppliers();
            }
          } else {
            this.supplier.description = searchText;
            this.supplier.id = null;
            this.postSupplier(this.supplier);
          }
        });
      });
    // }
  }
  // Signature changed from  postSupplier(form, supplier, model) - Suryakant
  postSupplier(supplier) {
    const arr = [];
    this._productsService
      .postElement("TenantMaster/PostSupplier", supplier)
      .subscribe(
        (res) => {
          this.ngZone.run(() => {
            if (res) {
              if (this.formProductDetails.suppliers === null) {
                arr.push(res);
                this.formProductDetails.suppliers = arr;
              } else {
                this.formProductDetails.suppliers = new Array<Suppliers>();
                this.formProductDetails.suppliers.push(res);
              }

              // this.resetforms(form);
              // model.hide();
              this.changeDetail(true, this.formProductDetails);
              this.supplier = new Suppliers();
            }
          });
        },
        (err) => {
          // this._toastrService.error(
          //   'Please provide valid details.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.loading = false;
        }
      );
  }

  onDeleteSupplier(index) {
    this.formProductDetails.suppliers.splice(index, 1);
    this.productDetails.suppliers = this.formProductDetails.suppliers;
    this.tobeDelete = "";
    this.changeDetail(true, this.productDetails);
  }

  // for type heads for supplier
  findSupplier(searchText) {
    this._productsService.findSupplier(searchText).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.supplierList = res;
        });
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  resetforms(form) {
    form.resetForm();
  }

  focusToInput() {
    setTimeout(() => {
      this.focusTextBox = !this.focusTextBox;
    }, 500);
  }

  // method to check if method property absent, if so then update method

  checkMethods(detail) {
    if (detail.pricingMethod) {
      const collection = detail.pricingMethod;
      let change = false;
      if (collection) {
        collection.forEach((pricingMethods) => {
          if (!pricingMethods["method"] || pricingMethods["method"] === null) {
            this.masterMethodList.forEach((masterMethod) => {
              if (masterMethod.name === pricingMethods.name) {
                pricingMethods.method = pricingMethods.name;
                pricingMethods.name =
                  pricingMethods.value + pricingMethods.name;
                change = true;
              }
            });
          } else if (pricingMethods["method"] !== null) {
            this.product = JSON.parse(JSON.stringify(detail));
            this.productDetails = JSON.parse(JSON.stringify(detail));
            this.prodName = detail.name;
          }
        });
        if (change) {
          this.formProductDetails.pricingMethod = JSON.parse(
            JSON.stringify(collection)
          );
          this.changeDetail(true, this.formProductDetails);
        }
      }
    }
  }
  close(quill) {
    if (!this.isEditTab) {
      this.quill = quill;
      this.isEditTab = true;
      this.tobeEdit = "description";
      this.quill.editor.enable(true);
    }
  }

  setEditorFocus(quill) {
    quill.focus();
    quill.enable();
    var selection = quill.getSelection(true);
    quill.insertText(selection.index, "");
  }

  startQuill(quill) {
    this.quill = quill;
    this.isEditTab = true;
    this.tobeEdit = "description";
    this.quill.editor.enable(true);
  }

  // closeEdit1(part, from?) {
  //   //not used
  //   if (
  //     this.tobeEdit !== null &&
  //     this.tobeEdit !== undefined &&
  //     this.tobeEdit !== "" &&
  //     this.tobeEditPrevious !== part
  //   ) {
  //     this.typeofMessage = "Error";
  //     this.messageToDisplay = null;
  //     this.noteToDisplay = null;
  //     this.dataToDisplay = null;
  //     this.showButtons = true;
  //     if (this.tobeEditPrevious === "productDetails") {
  //       // this.notification =
  //       //   "The changes made in product details may loss, are you sure you want to save them?";
  //       // this.popupFor = "updateProduct";
  //       // this.editConfirmshow = true;
  //     } else if (this.tobeEditPrevious === "pricing") {
  //       this.pricingForm.ngSubmit.emit(true);
  //       this.pricingForm.onSubmit(undefined);
  //       if (this.pricingForm.valid) {
  //         // this.notification =
  //         //   "The changes made in pricing details may loss, are you sure you want to save them?";
  //         // this.popupFor = "updatepricing";
  //         // this.editConfirmshow = true;
  //       } else {
  //         this.tobeEdit = this.tobeEditPrevious;
  //         this.tobeEditPrevious = this.tobeEdit;
  //         this.productTitleCopy = false;
  //         return;
  //       }
  //     } else if (this.tobeEditPrevious === "description") {
  //       // this.notification ="The changes made in description may loss, are you sure you want to save them?";
  //       // this.popupFor = "updateDescription";
  //       // this.editConfirmshow = true;
  //     } else if (this.tobeEditPrevious === "prodName") {
  //       // this.notification =
  //       //   "The changes made for product title may loss, are you sure you want to save them?";
  //       // this.popupFor = "updateProductTitle";
  //       // this.editConfirmshow = true;
  //     }
  //     this.tobeEditFrom = from === undefined ? part : from;
  //     this.supplier = new Suppliers();
  //     // this.formProductDetails = JSON.parse(JSON.stringify(this.product));
  //     if (
  //       this.formProductDetails.suppliers &&
  //       this.formProductDetails.suppliers.length !== 0
  //     ) {
  //       this.supplier = this.formProductDetails.suppliers[0];
  //     } else {
  //       this.supplier = new Suppliers();
  //     }

  //     this.tobeEdit = part;
  //     this.tobeEditPrevious = this.tobeEdit;
  //     this.productTitleCopy = false;
  //   } else {
  //     this.tobeEditFrom = from;
  //     this.supplier = new Suppliers();
  //     // this.formProductDetails = JSON.parse(JSON.stringify(this.product));
  //     if (
  //       this.formProductDetails.suppliers &&
  //       this.formProductDetails.suppliers.length !== 0
  //     ) {
  //       this.supplier = this.formProductDetails.suppliers[0];
  //     } else {
  //       this.supplier = new Suppliers();
  //     }
  //     this.tobeEdit = part;
  //     this.tobeEditPrevious = this.tobeEdit;
  //     this.productTitleCopy = false;
  //   }
  // }

  closeEdit(part, from?) {
    if (this.isEditTab) {
      return;
    }
    this.isEditTab = true;
    console.log("tobeEdit " + this.tobeEdit);
    if (this.tobeEditPrevious === undefined) {
      this.tobeEditFrom = from;
      this.supplier = new Suppliers();
      if (
        this.formProductDetails.suppliers &&
        this.formProductDetails.suppliers.length !== 0
      ) {
        this.supplier = this.formProductDetails.suppliers[0];
      } else {
        this.supplier = new Suppliers();
      }
      this.tobeEdit = part;
      this.tobeEditPrevious = this.tobeEdit;
      this.productTitleCopy = false;
    } else if (from !== "ALL" && part === this.tobeEditPrevious) {
      this.tobeEditFrom = from;
      this.supplier = new Suppliers();
      if (
        this.formProductDetails.suppliers &&
        this.formProductDetails.suppliers.length !== 0
      ) {
        this.supplier = this.formProductDetails.suppliers[0];
      } else {
        this.supplier = new Suppliers();
      }
      this.tobeEdit = part;
      this.tobeEditPrevious = this.tobeEdit;
      this.productTitleCopy = false;
    } else if (from !== "ALL" && part !== this.tobeEditPrevious) {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
    }
    this.tobeEditPrevious =
      this.tobeEditPrevious === "imageUpload"
        ? undefined
        : this.tobeEditPrevious;
    this.tobeEditPrevious =
      this.tobeEditPrevious === "deleteImage"
        ? undefined
        : this.tobeEditPrevious;
    if (part === "description") {
      this.quill.enable();
      this.quill.focus();
      let selection = this.quill.getSelection(true);
      this.quill.insertText(selection.index, "");
    }
  }

  onEditorCreated(quill) {
    this.quill = quill;
    this.quill.disable();
  }

  // messages display
  popupOpen(popupFor) {
    if (this.popupFor === "copyProduct") {
      this.typeofMessage = "Hello";
      this.messageToDisplay = "Would you like to copy the product:";
      this.noteToDisplay = null;
      this.dataToDisplay = this.productDetails.name + "?";
      this.showButtons = true;
      this.notification = null;
    } else if (this.popupFor === "deleteProduct") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this product?";
    } else if (this.popupFor === "deleteSupplier") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this supplier?";
    } else if (this.popupFor === "deleteImage") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this image?";
      this.tobeEditPrevious = undefined;
    } else if (this.popupFor === "deleteMethod") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification =
        "Are you sure you want to delete this pricing method?";
    } else if (this.popupFor === "Access denied") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Access denied";
    } else if (this.popupFor === this.messageForDeleteProduct) {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = this.messageForDeleteProduct;
    } else if (
      this.popupFor === "The maximum number of images per product is three"
    ) {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "The maximum number of images per product is three";
    }
  }

  yes(ConfirmationModel) {
    if (this.popupFor === "copyProduct") {
      this.addProduct(this.formProductDetails);
      ConfirmationModel.hide();
    } else if (this.popupFor === "deleteProduct") {
      if (this.isProduectUsed) {
        this.show = true;

        this.popupOpen((this.popupFor = this.messageForDeleteProduct));
      } else {
        this.deleteProduct(this.formProductDetails, ConfirmationModel);
      }
    } else if (this.popupFor === "deleteSupplier") {
      this.onDeleteSupplier(this.index);
      this.isProduectUsed = false;
      ConfirmationModel.hide();
    } else if (this.popupFor === "deleteImage") {
      this.onDeleteImage(this.index);
      this.isProduectUsed = false;
      ConfirmationModel.hide();
    } else if (this.popupFor === "deleteMethod") {
      this.deleteMethod(this.index);
      this.isProduectUsed = false;
      ConfirmationModel.hide();
    } else if (this.messageForDeleteProduct) {
      this.show = true;
      this.popupOpen((this.popupFor = this.messageForDeleteProduct));
    }
  }

  no(ConfirmationModel) {
    if (this.popupFor === "copyProduct") {
      ConfirmationModel.hide();
      this.popupFor = "none";
    } else if (this.popupFor === "deleteProduct") {
      ConfirmationModel.hide();
      this.isProduectUsed = false;
      this.popupFor = "none";
    } else if (this.popupFor === "deleteSupplier") {
      ConfirmationModel.hide();
      this.isProduectUsed = false;
      this.popupFor = "none";
    } else if (this.popupFor === "deleteImage") {
      ConfirmationModel.hide();
      this.isProduectUsed = false;
      this.popupFor = "none";
    } else if (this.popupFor === "deleteMethod") {
      ConfirmationModel.hide();
      this.isProduectUsed = false;
      this.popupFor = "none";
    } else {
      ConfirmationModel.hide();
      this.popupFor = "none";
      this.show = false;
    }
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }

  goToUrlReferenceUrl(url) {
    const proto = url.match(/^([a-z0-9]+):\/\//);
    if (proto === null) {
      const website = "https://" + url;
      window.open(website, "_blank");
    } else {
      window.open(url, "_blank").focus();
    }
  }

  getAllManufacturers() {
    this.subscriptions.push(
      this._productsService.getData("Products/GetAllManufacturers").subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.manufacturers = Response;
          });
        },
        (err) => { }
      )
    );
  }

  getMeasureList() {
    this.settingsService.GetAllUoms().subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.unitOfMeasures = res;
            this.loading = false;
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  yesSave(modal, currEdit, prevEdit) {
    if (this.popupFor === "updateProduct") {
      this.editdetails(true, this.formProductDetails, this.supplier);
    } else if (this.popupFor === "updatepricing") {
      this.editpricing(
        this.pricingForm,
        this.pricingForm.valid,
        this.formProductDetails
      );
    } else if (this.popupFor === "updateDescription") {
      this.changeDetail(true, this.formProductDetails);
    } else if (this.popupFor === "updateProductTitle") {
      this.addTile(true, this.formProductDetails);
    }
    modal.hide();
    this.editConfirmshow = false;
    this.tobeEdit = currEdit;
    this.tobeEditPrevious = prevEdit;
    this.popupFor = "none";
  }
  noSave(modal, currEdit, prevEdit) {
    this.editConfirmshow = false;
    modal.hide();
    this.tobeEdit = "nun";
    this.tobeEditPrevious = this.tobeEdit;
    this.popupFor = "none";
  }
  hideSave(modal, currEdit, prevEdit) {
    this.editConfirmshow = false;
    modal.hide();
    this.tobeEdit = "nun";
    this.tobeEditPrevious = this.tobeEdit;
    this.popupFor = "none";
  }
}
