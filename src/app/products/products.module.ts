import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsRoutingModule } from './products-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ProductsDetailComponent } from './products-detail/products-detail.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxLoadingModule } from 'ngx-loading';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { CommonTemplateModule } from '../common/common-template.module';
import { SettingsService } from '../settings/settings-service';
import { DataService } from '../_core/data.service';
import { SharedModule } from '../_shared/shared.module';
import { ProductsService } from './products-service';
// import { DataTablesModule } from 'angular-datatables';
//import { QuillEditorModule } from 'ngx-quill-editor';
import { QuillModule } from 'ngx-quill';
import { DashboardService } from '../dashboard/dashboard.service';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    FormsModule,
    NgxLoadingModule,
    CommonTemplateModule,
    ReactiveFormsModule,
    TypeaheadModule.forRoot(),
    NgSlimScrollModule,
    SharedModule,
    // DataTablesModule,
    QuillModule,
    TableModule,
    ButtonModule,
    //QuillEditorModule
  ],
  declarations: [ProductsDetailComponent, ProductsListComponent],
  providers: [ProductsService, DataService, SettingsService, DashboardService],
})
export class ProductsModule { }
