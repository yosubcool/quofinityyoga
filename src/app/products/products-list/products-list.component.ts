// import { debug } from 'util';
// import { debounce } from 'rxjs/operator/debounce';
import { Component, OnInit, NgZone, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { Pagination } from '../../_shared/model/pagination-model';
// import { PaginatePipe, PaginationService } from 'ng2-pagination';
// import { Ng2PaginationModule } from 'ng2-pagination';
import { Constants } from '../../_shared/constant';
import { ToastrService } from 'ngx-toastr';
import { ngxCsv } from 'ngx-csv';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
declare var $: any;
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder,
  NgForm,
} from '@angular/forms';
import {
  Categories,
  Tags,
  Suppliers,
} from '../../_shared/model/tenantMaster-model';
import { map, Subscription } from 'rxjs';
import { ProductsService } from '../products-service';
import { Actions } from 'src/app/_shared/model/actions-model';
import { Product } from 'src/app/_shared/model/product-model';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
import { HttpClient } from '@angular/common/http';
// import { json } from 'd3';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;
  validPoduct: any;
  totalProduct: any;
  showData = false;
  invalidProduct: any;
  showWarning = false;
  tobeEdit: string;
  isNameEdit = false;
  isProductRepated = false;
  reverse = false;
  showFile = false;
  myFilename: string;
  uplodedData: any;
  key: string;
  sampleHeaders: string[];
  pageNo = 0;
  productTypes: any = [];
  subscriptions: Subscription[] = [];
  headers: any = [];
  productforExport: any = [];
  prod: any;
  isFormSubmitted = false;
  PTitleForm: FormGroup;
  controlName = [];
  duplicatePartNo = false;
  product: Product;
  productAdd = new Product();
  isProductTitleEdit = false;
  name: any;
  public loading = false;
  selectedAlpha: string;
  constants: any;
  productsList: Pagination;
  productCount: any;
  quoteStats: any;
  quotesInfo = new Pagination();
  pagination: Pagination;
  pagesCount: any;
  categories: any;
  tags: any;
  isCostValid = false;
  isMsrpValid = false;
  start = null;
  allowedFiles = ['csv'];
  manufacturers: any;

  @ViewChild('fileImportInput') fileImportInput: any;
  accessValid: any;

  showCatClearButton = false;
  showManClearButton = false;
  showTagClearButton = false;
  isDevice = false;
  // New Edit grid
  items = [];
  itemCount = 0;
  editProperty: string;
  isPartNovalid = false;
  columns = [];
  toBeEdit = 'none';
  uniqueShow = true;
  msrpShow = true;
  costShow = true;
  manufacturerShow = true;
  productTypeShow = true;
  uomShow = true;
  isOpenPopup = false;
  isProductSelectedForDelete = 0;
  selectAllForDelete = false;
  messageForDeleteProducts =
    'Deleted products are unrecoverable. Do you want to delete the selected product(s)?';
  suppliers: Suppliers[];
  suppliersList: Suppliers[];
  showSupplierClearButton: Boolean = false;
  supplier: Suppliers;
  isPartNo: boolean;
  isProductTitle: boolean;
  isMsrp: boolean;
  isCost: boolean;
  isUPC: boolean;
  isSupplier: boolean;
  isManufacturer: boolean;
  isManufacturerPartNo: boolean;
  isProductType: boolean;
  isTaxable: boolean;
  isProductID: boolean;
  isUnit: boolean;
  checkBoxDataList = [];
  selectedItemsList = [];
  checkedIDs = [];
  modalRef: BsModalRef;
  constructor(
    private _productsService: ProductsService,
    private _dashboardService: DashboardService,
    private ngZone: NgZone,
    public builder: FormBuilder,
    private _toastrService: ToastrService,
    private router: Router,
    private http: HttpClient,
    private modalService: BsModalService
  ) {
    this.accessValid = new Actions();
    this.constants = Constants;
    this.product = new Product();
    this.productsList = new Pagination();
    this.selectedAlpha = 'All';
    this.PTitleForm = this.builder.group({});
    this.categories = new Categories();
    this.tags = new Tags();
    this.prod = new Pagination();
    this.pagination = new Pagination();
    this.productsList.pageSize = 10;
    this.suppliers = new Array<Suppliers>();
    this.suppliersList = new Array<Suppliers>();
    this.supplier = new Suppliers();
    this.sampleHeaders = [
      'Part Number',
      'Product Title',
      'MSRP/List',
      'Cost',
      'UPC',
      'Supplier',
      'Manufacturer',
      'Manufacturer Part Number',
      'Product Type',
      'Taxable',
    ];

    const intervalForAddCheckbox = setInterval(() => {
      const ele: any = document.getElementsByClassName('data-table-header');
      // if (ele) { //#Revisit_PreviousCode
      if (ele && ele.length > 0) {
        const div = document.createElement('DIV');
        div.className = 'ml-3';

        const checkbox = document.createElement('input');
        checkbox.type = 'CheckBox';
        checkbox.id = 'selectProductsForDelete';
        checkbox.className = 'cursor-pointer-checkbox';
        div.appendChild(checkbox);

        ele[0].appendChild(div);
        clearInterval(intervalForAddCheckbox);
        this.addEventToSelectAllProductForDelete();
      }
    }, 50);
  }

  ngOnInit() {
    $(window).scrollTop(0);
    document.getElementById('products').classList.add('active');
    this.getProductList(0);
    this.getAllCategories();
    this.validateForm(0);
    this.getModuleAccess();
    this.getAllTags();
    this.getAllManufacturers();
    this.getProductType();
    this.getSuppliers();
    this.isPartNo = true;
    this.isProductType = false;
    this.isCost = true;
    this.isMsrp = true;
    this.isSupplier = false;
    this.isUPC = false;
    this.isManufacturer = false;
    this.isManufacturerPartNo = true;
    this.isProductTitle = true;
    this.isTaxable = true;
    this.isProductID = true;
    this.isUnit = true;
    // { property: "unitOfMeasure", header: "Unit Of Measure" }, { property: 'isTaxable', header: 'Taxable' } -- Removing As David Suggested
    this.columns = [{ property: 'uniqueId', header: 'Product ID' }, { property: 'partNo', header: 'partNo' }, { property: 'name', header: 'Product Title' }, { property: 'msrp', header: 'MSRP/List' },
    { property: 'cost', header: 'Cost' },
    { property: 'manufacturerPartNumber', header: 'Manufacturer Part No' }];

    this.checkBoxDataList = [{ property: 'uniqueId', header: 'Product ID', ischecked: true }, { property: 'partNo', header: 'partNo', ischecked: true }, { property: 'name', header: 'Product Title', ischecked: true }, { property: 'msrp', header: 'MSRP/List', ischecked: true },
    { property: 'cost', header: 'Cost', ischecked: true }, { property: "unitOfMeasure", header: "Unit Of Measure", ischecked: true }, { property: 'upc', header: 'UPC', ischecked: false }, { property: 'suppliers', header: 'Supplier', ischecked: false }, { property: 'manufacturer', header: 'Manufacturer', ischecked: false },
    { property: 'manufacturerPartNumber', header: 'Manufacturer Part No', ischecked: true }, { property: 'productType', header: 'Product Type', ischecked: false }, { property: 'isTaxable', header: 'Taxable', ischecked: true }];
    if (window.innerWidth <= 1024) {
      this.uniqueShow = false;
      this.msrpShow = false;
      this.manufacturerShow = false;
      this.productTypeShow = false;
      this.costShow = false;
      this.isDevice = true;
      this.uomShow = true;
    } else {
      this.uniqueShow = true;
      this.msrpShow = true;
      this.manufacturerShow = true;
      this.productTypeShow = true;
      this.costShow = true;
      this.isDevice = false;
      this.uomShow = true;
    }
  }

  addEventToSelectAllProductForDelete() {
    document
      .getElementById('selectProductsForDelete')
      .addEventListener('click', (event) =>
        this.selectAllProductForDelete(event)
      );
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
    }, 500);
  }
  fetchSelectedItems() {
    this.selectedItemsList = this.checkBoxDataList.filter((value, index) => {
      return value.ischecked
    });
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      animated: true,
      // backdrop: 'static',
      class: 'modal-sm  modal-right'
    });
  }

  changeSelection() {
    this.fetchSelectedItems();
    this.columns = this.selectedItemsList;
    this.isProductType = false;
    this.isCost = false;
    this.isMsrp = false;
    this.isSupplier = false;
    this.isUPC = false;
    this.isManufacturer = false;
    this.isManufacturerPartNo = false;
    this.isProductTitle = false;
    this.isTaxable = false;
    this.isProductID = false;
    this.isUnit = false;
    this.columns.forEach(item => {

      if (item.property === "uniqueId") {
        this.isProductID = true;
      }
      if (item.property === "partNo") {
        this.isPartNo = true;

      }
      else if (item.property === "name") {
        this.isProductTitle = true;

      }
      else if (item.property === "msrp") {
        this.isMsrp = true;

      }
      else if (item.property === "manufacturer") {
        this.isManufacturer = true;

      }
      else if (item.property === "unitOfMeasure") {
        this.isUnit = true;
      }
      else if (item.property === "manufacturerPartNumber") {
        this.isManufacturerPartNo = true;

      }
      else if (item.property === "isTaxable") {
        this.isTaxable = true;

      }
      else if (item.property === "suppliers") {
        this.isSupplier = true;

      }
      else if (item.property === "upc") {
        this.isUPC = true;

      }
      else if (item.property === "cost") {
        this.isCost = true;
      }
      else if (item.property === "productType") {
        this.isProductType = true;
      }
    })
  }
  fetchCheckedIDs() {
    this.checkedIDs = []
    this.checkBoxDataList.forEach((value, index) => {
      if (value.ischecked) {
        this.checkedIDs.push(value.id);
      }
    });
  }
  getModuleAccess() {
    this.subscriptions.push(
      this._productsService
        .getData('TenantMaster/GetAccessRoleMatrix/')
        .subscribe((res) => {
          this.accessValid = res.products;
        })
    );
  }

  validateForm(i: number) {
    const action = 'addControl';
    this.PTitleForm[action](
      'item_name_' + i,
      new FormControl('', [Validators.required])
    );
  }

  goToDetails(id) {
    this.router.navigate(['/products/products-detail'], {
      queryParams: { id: btoa(id) },
    });
  }

  searchProducts(a) {
    this.selectedAlpha = a;
    this.getProductList(0);
  }
  getProductType() {
    this.subscriptions.push(
      this._productsService.getData('Configuration/GetProductType').subscribe(
        (res) => {
          this.ngZone.run(() => {
            //  this.productTypeList = res;
            this.productTypes = res;
          });
        },
        (err) => { }
      )
    );
  }

  getProductList(pageNo) {
    let sortingData = pageNo;
    if (isNaN(pageNo)) {
      pageNo = pageNo.first / pageNo.rows + 1;
    }
    this.loading = true;
    this.productsList.sortBy = 'CreatedOn';
    this.productsList.sortType = 'asc';
    this.productsList.sortBy = 'uniqueId';
    if (pageNo != 0) {
      if (sortingData.sortOrder === 1) {
        this.productsList.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.productsList.sortType = 'asc';
      }
      else {
        this.productsList.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.productsList.sortType = 'desc';
      }
    }
    // if (this.key) {
    //   this.productsList.sortBy = this.key;
    // }
    // if (this.reverse) {
    //   this.productsList.sortType = 'asc';
    // } else {
    //   this.productsList.sortType = 'desc';
    // }

    this.productsList.pageNumber = pageNo;
    this.productsList.alphabet =
      this.selectedAlpha === 'All' ? '' : this.selectedAlpha;
    this._productsService
      .getElementThroughAnyObject(
        'Products/GetProductsPagination',
        this.productsList
      )
      .subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.productsList = res;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            this.pageNo = res.pageNumber;
            this.items = this.productsList.records;
            this.itemCount = this.productsList.totalCount;
            this.getAllManufacturers();
            this.getSuppliers();
            this.loading = false; // Loader Disable

            this.isProductSelectedForDelete = 0;
            this.selectAllForDelete = false;
            this.items.forEach((element) => {
              element['isSelectedForDelete'] = false;
            });
            this.mmakeUncheckedCheckboxByDefault();
          });
        },
        (err) => {
          // console.log(err);
          if (err.status === 403) {
            this.show = true;
            // console.log(err);
            this.popupOpen((this.popupFor = 'Access denied'));
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
          this.loading = false; // Loader Disable
        }
      );
  }
  // To Update Product Title Inline
  updateProduct(isvalid: boolean, newValue) {
    if (isvalid) {
      if (newValue.manufacturer) {
        newValue.manufacturer = newValue.manufacturer.replace(/^\s+|\s+$/g, '');
      }
      if (newValue.manufacturerPartNumber) {
        newValue.manufacturerPartNumber = newValue.manufacturerPartNumber.replace(
          /^\s+|\s+$/g,
          ''
        );
      }
      if (newValue.partNo) {
        newValue.partNo = newValue.partNo.replace(/^\s+|\s+$/g, '');
      }
      if (newValue.name) {
        newValue.name = newValue.name.replace(/^\s+|\s+$/g, '');
      }
      if (newValue.suppliers.length !== 0) {
        newValue.suppliers[0].description = newValue.suppliers[0].description.replace(
          /^\s+|\s+$/g,
          ''
        );
      }
      this._productsService.updateElement('Products', newValue).subscribe(
        (Response) => {
          if (Response) {
            this.getProductList(this.productsList.pageNumber);
            this.getAllManufacturers();
          }
        },
        (err) => {
          // console.log(err);
          if (err.status === 403) {
            this.show = true;
            // console.log(err);
            this.popupOpen((this.popupFor = 'Access denied'));
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
        }
      );
    }
  }
  CheckCost(item) {
    const val = parseFloat(item);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isCostValid = true;
    } else {
      this.isCostValid = false;
    }
  }
  CheckMsrp(item) {
    const val = parseFloat(item);
    if (val === 0) {
      this.isMsrpValid = true;
    } else {
      this.isMsrpValid = false;
    }
  }

  CheckPartNo(item) {
    this.product.partNo = item.replace(/^\s+|\s+$/g, '');
    const val = parseInt(item.partNo, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isPartNovalid = true;
    } else {
      this.isPartNovalid = false;
    }

    this._productsService.findProductByPartNo(item).subscribe(
      (partduplicate) => {
        this.ngZone.run(() => {
          if (partduplicate && partduplicate.id !== item.id) {
            this.duplicatePartNo = true;
          } else {
            this.duplicatePartNo = false;
          }
        });
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  removeLeadSpace(partNo) {
    this.product.partNo = partNo.replace(/^\s+|\s+$/g, '');
  }

  addProduct(isValid: boolean, model) {
    this.product.partNo = this.product.partNo.replace(/^\s+|\s+$/g, '');
    this.product.name = this.product.name.replace(/^\s+|\s+$/g, '');
    this.product;
    this._productsService.findProductByPartNo(this.product).subscribe(
      (partduplicate) => {
        this.ngZone.run(() => {
          if (partduplicate) {
            this.duplicatePartNo = true;
          } else {
            this.duplicatePartNo = false;
            this.loading = true;
            if (isValid && !this.isPartNovalid && !this.duplicatePartNo) {
              model.hide();
              this._productsService
                .postElement('Products', this.product)
                .subscribe(
                  (res) => {
                    this.ngZone.run(() => {
                      if (res) {
                        this.router.navigate(['/products/products-detail'], {
                          queryParams: { id: btoa(res.id) },
                        });
                        this.loading = false;
                      }
                    });
                  },
                  (err) => {
                    this.loading = false;
                  }
                );
            } else {
              this.loading = false;
            }
          }
        });
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  onModelClose() {
    this.product = new Product();
  }

  resetforms(form) {
    form.reset();
  }

  getAllCategories() {
    this.pagination.alphabet = 'All';
    this._productsService
      .getElementThroughAnyObject(
        'TenantMaster/GetAllCategories',
        this.pagination
      )
      .subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.categories = Response.records;
          });
        },
        (err) => { }
      );
  }

  getAllTags() {
    this.subscriptions.push(
      this._productsService.getData('TenantMaster/GetTags').subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.tags = Response;
          });
        },
        (err) => { }
      )
    );
  }

  getAllManufacturers() {
    this.subscriptions.push(
      this._productsService.getData('Products/GetAllManufacturers').subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.manufacturers = Response;
          });
        },
        (err) => { }
      )
    );
  }
  onKeyDown() {
    if (
      this.productsList.manufacturer === '' &&
      this.productsList.manufacturer.length === 0
    ) {
      this.getProductList(0);
    }
    if (
      this.productsList.category === '' &&
      this.productsList.category.length === 0
    ) {
      this.getProductList(0);
    }
    if (this.productsList.tags === '' && this.productsList.tags.length === 0) {
      this.getProductList(0);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  sort(key) {
    this.key = key.sortBy;
    this.reverse = key.sortAsc;
    this.getProductList(0);
  }

  DownloadTemplate() {
    const parsedResponse = this.sampleHeaders;
    // if (navigator.appVersion.toString().indexOf('.NET') > 0) {
    //   const bb = new MSBlobBuilder();
    //   bb.append(parsedResponse);
    //   const blob1 = bb.getBlob('csv');
    //   window.navigator.msSaveBlob(blob1, 'Product.csv');
    //   const url = window.URL.createObjectURL(blob1);
    //   window.URL.revokeObjectURL(url);
    // } else {
    const blob = new Blob([parsedResponse.toString()], { type: 'csv' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'product.csv';
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
    // }
  }

  isInArray(array, word) {
    return array.indexOf(word.toLowerCase()) > -1;
  }
  fileChangeListener(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.loading = true;
      const file1: File = fileList[0];
      const filename = file1.name;
      const Extension = filename
        .substring(filename.lastIndexOf('.') + 1)
        .toLowerCase();
      if (this.isInArray(this.allowedFiles, Extension)) {
        this.myFilename = filename;
        const formData: FormData = new FormData();
        formData.append('uploadFile', file1, file1.name);
        const headers = new Headers();
        headers.append('Content-Type', 'multipart/form-data');
        const file = new FormData();

        file.append('file', file1);
        file.append(
          'Authorization',
          'Bearer ' + sessionStorage.getItem('AccessToken')
        );
        this.loading = true;
        this.http
          .post(this.constants.BASE_URL + 'Products/UploadFile', file)
          .pipe(map((res) => res))
          .subscribe(
            {
              next: (response: any) => {
                this.showData = true;
                this.showWarning = false;
                this.loading = false;
                this.invalidProduct = response.failedProducts.length;
                this.validPoduct = response.successfullProducts.length;
                this.totalProduct = this.validPoduct + this.invalidProduct;
                this.getSuppliers();
                if (this.validPoduct || this.invalidProduct) {
                  this.showFile = true;
                  this.loading = false;
                  event.target.value = '';
                  this.uplodedData = response;
                  this.validPoduct = response.successfullProducts.length;
                } else {
                  this.showFile = false;
                  this.showWarning = true;
                  this.showData = false;
                }
              },
              error: (error) => {
                this.showWarning = true;
                this.loading = false;
              }
            }
            // (
            //   (this.loading = false),
            //   this._toastrService.error(
            //     'Please select valid file', '', this.constants.toastrConfig)
            // )
          );
      } else {
        this.loading = false;
        event.target.value = '';
        this.showWarning = true;
        this.showData = false;
        this.fileImportInput.nativeElement.value = '';
        this.uplodedData = null;

        return;
      }
    }
  }
  CancelUpload() {
    this.fileImportInput.nativeElement.value = '';
    this.myFilename = '';
    this.uplodedData = null;
  }

  addProductFile() {
    if (this.uplodedData.successfulAccounts.length) {
      this.uplodedData.successfulAccounts.forEach((element) => {
        element.createdOn = new Date();
        element.updatedOn = new Date();
        if (element.productType != null) {
          if (
            element.productType.description !== 'Material' &&
            element.productType.description !== 'Labor'
          ) {
            element.productType = {};
          }
          if (element.productType.description === 'Material') {
            element.productType.id = '5aaf989567115b4dac9b841a';
            element.productType.description = 'Material';
          }
          if (element.productType.description === 'Labor') {
            element.productType.id = '5aaf98b667115b4dac9b84bf';
            element.productType.description = 'Labor';
          }
        }
        this._productsService
          .postElement('Products', element)
          .subscribe((res) => {
            this.ngZone.run(() => {
              this.getProductList(0);
            });
          });
      });
    }
  }

  private handleError(error: any) {
    const errMsg = error.message
      ? error.message
      : error.status
        ? `${error.status} - ${error.statusText}`
        : 'Server error';
    console.error(errMsg);
    return errMsg;
  }
  // Saving Cost - Suryakant
  saveGridCost(item, form: NgForm) {
    if (form.valid && (item !== null || !item)) {
      this.CheckCost(item.cost);
      if (!this.isCostValid) {
        this.isCostValid = false;
        this.toBeEdit = 'none';
        this.updateProduct(true, item);
      } else {
        this.isCostValid = true;
        this.toBeEdit = 'cost';
      }
    }
  }
  // Saving Part number - Suryakant
  saveGridPartNo(item, form: NgForm) {
    if (form.valid && (item !== null || !item)) {
      this.CheckPartNo(item);
      if (!this.isPartNovalid && !this.duplicatePartNo) {
        this.updateProduct(true, item);
        this.isPartNovalid = false;
        this.duplicatePartNo = false;
        this.toBeEdit = 'none';
      }
    }
  }
  // Saving Msrp - Suryakant
  saveGridMsrp(item, form: NgForm) {
    if (form.valid && (item !== null || !item)) {
      this.CheckMsrp(item.msrp);
      if (!this.isMsrpValid) {
        this.isMsrpValid = false;
        this.toBeEdit = 'none';
        this.updateProduct(true, item);
      } else {
        this.isMsrpValid = true;
        this.toBeEdit = 'msrp';
      }
    }
  }
  editItem(item, edit) {
    if (edit === 'supplier') {
      if (item.suppliers) {
        this.supplier.description = item.suppliers[0].description;
      } else {
        this.supplier = new Suppliers();
      }
    }
    this.editProperty = item.id;
    this.toBeEdit = edit;
    this.items = JSON.parse(JSON.stringify(this.productsList.records));
    this.onSave(true, this.items);
  }
  // Discard Inline Edit - Suryakant
  discardEdit() {
    this.toBeEdit = 'none';
    this.items = JSON.parse(JSON.stringify(this.productsList.records));
  }
  //  Saving inline edit data - Suryakant
  saveChanges(item, form: NgForm) {
    if (form.valid && (item !== null || !item)) {
      this.updateProduct(true, item);
      this.toBeEdit = 'none';
    }
  }
  //  To get list of columns headers - Suryakant
  getChangedColumns(columns) {
    this.columns = columns.datatable.columns._results;
    if (this.productsList.records) {
      this.items = JSON.parse(JSON.stringify(this.productsList.records));
    }
  }
  // export To CSV
  exportToCSV() {
    this.loading = true;
    this.subscriptions.push(
      this._productsService.getData('Products').subscribe(
        (res) => {
          this.productforExport.push(res);
          if (this.productforExport.length) {
            const data = [];
            this.productforExport[0].forEach((element) => {
              const obj: any = {};
              this.columns.forEach((col) => {
                if (col.visible === true) {
                  const prop = col.property;
                  if (prop === 'productType' && element.productType !== null) {
                    obj[prop] = element.productType.description;
                    this.headers.push(col.header);
                  } else {
                    obj[prop] = element[prop];
                    this.headers.push(col.header);
                  }
                }
              });
              data.push(obj);
            });
            const header = this.headers.filter((v, i, a) => a.indexOf(v) === i);
            const options = {
              fieldSeparator: ',',
              quoteStrings: '"',
              decimalseparator: '.',
              showLabels: true,
              showTitle: true,
              headers: header,
            };
            this.headers = [];
            // tslint:disable-next-line:no-unused-expression
            new ngxCsv(data, 'Products', options);
            this.loading = false;
          } else {
            this.show = true;
            this.popupOpen((this.popupFor = 'No data available to export!'));

            // this._toastrService.warning(
            //   'No data available to export!',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.loading = false;
          }
        },
        (err) => {
          if (err.status === 403) {
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            // console.log(err);
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false;
        }
      )
    );
  }

  openNewTab(event, url) {
    const proto = url.match(/^([a-z0-9]+):\/\//);
    if (proto === null) {
      const website = 'https://' + url;
      window.open(website, '_blank');
    } else {
      window.open(url, '_blank');
    }
  }
  // messages display
  popupOpen(popupFor) {
    // this.titletoClose();
    if (this.popupFor === 'Access denied') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Access denied';
    } else if (this.popupFor === 'No data available to export!') {
      this.typeofMessage = 'Error';
      this.notification = 'No data available to export!';
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.messageToDisplay = null;
    } else if (this.popupFor === this.messageForDeleteProducts) {
      this.notification = this.messageForDeleteProducts;
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
    }
  }

  yes(ConfirmModal) {
    // if (this.popupFor === 'deleteSubcategory') {
    //   this.onDeleteSubcategory(this.index);
    //   ConfirmModal.hide();
    // } else if (this.popupFor === 'deleteCategory') {
    //   this.onDeleteCategory(this.index);
    //   ConfirmModal.hide();
    // }
    if (this.popupFor === this.messageForDeleteProducts) {
      this.deleteProducts();
      ConfirmModal.hide();
    }
    this.show = false;
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }

  selectAllProductForDelete(event) {
    this.selectAllForDelete = !this.selectAllForDelete;
    this.items.forEach((element) => {
      element['isSelectedForDelete'] =
        // element.isUsed > 0 ? false : this.selectAllForDelete;
        element.isUsed > 0
          ? false
          : (this.selectAllForDelete = event.target.checked);
    });
    this.productSelectedForDelete(event);
  }

  productSelectedForDelete(event = null) {
    this.isProductSelectedForDelete = this.items.filter(
      (item) => item['isSelectedForDelete'] === true
    ).length;
    // tslint:disable-next-line:no-unused-expression
    if (this.isProductSelectedForDelete === 0) {
      this.selectAllForDelete = false;
      this.mmakeUncheckedCheckboxByDefault();
    }
  }

  deleteProducts() {
    const productSelectedForDelete = this.items.filter(
      (item) => item['isSelectedForDelete'] === true
    );
    productSelectedForDelete.forEach((item) => {
      item.isActive = false;
      this._productsService.updateElement('Products/Delete', item).subscribe(
        (res) => {
          this.ngZone.run(() => {
            if (res) {
              this.loading = false;
              this.getProductList(this.productsList.pageNumber);
            }
          });
        },
        (err) => {
          // console.log(err);
          if (err.status === 403) {
            this.popupOpen((this.popupFor = 'Access denied'));
          }
        }
      );
    });
  }

  mmakeUncheckedCheckboxByDefault() {
    const ele: any = document.getElementById('selectProductsForDelete');
    if (ele) {
      return (ele.checked = false);
    } else {
      return false;
    }
  }
  // To get All unique suppliers from product - Suryakant
  getSuppliers() {
    this.subscriptions.push(
      this._productsService.getData('Products/GetAllSuppliers').subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.suppliersList = Response;
          });
        },
        (err) => { }
      )
    );
  }

  // for type heads for supplier
  findSupplier(searchText) {
    this._productsService.findSupplier(searchText).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.suppliers = res;
        });
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  saveSupplierChanges(item, supplier, form: NgForm) {
    if (form.valid && supplier.description) {
      this.addSupplier(item, supplier);
      this.toBeEdit = 'none';
    } else {
    }
  }

  addSupplier(item, supplier) {
    let isPresentInTagIdList = [];
    let searchText: string;
    searchText = supplier.description;
    this.supplier.description = supplier.description;
    // if (this.product.suppliers === null || this.product.suppliers.length === 0) {
    //   isPresentInTagIdList = [];
    // } else {
    //   isPresentInTagIdList = this.product.suppliers.filter(item => item.description === supplier.description);
    // }
    this.supplier.isActive = true;
    this._productsService
      .getElementThroughAnyObject(
        'TenantMaster/FindSupplierByName',
        this.supplier
      )
      .subscribe((res) => {
        this.ngZone.run(() => {
          if (res) {
            if (item.suppliers) {
              item.suppliers[0] = res;
            } else {
              item.suppliers = new Array<Suppliers>();
              item.suppliers.push(res);
            }
            this.updateProduct(true, item);
            this.getProductList(this.productsList.pageNumber);
          } else {
            this.supplier.description = searchText;
            this.supplier.id = null;
            this.postSupplier(item, this.supplier);
          }
        });
      });
    // }
  }
  // Signature changed from  postSupplier(form, supplier, model) - Suryakant
  postSupplier(item, supplier) {
    const arr = [];
    this._productsService
      .postElement('TenantMaster/PostSupplier', supplier)
      .subscribe(
        (res) => {
          this.ngZone.run(() => {
            if (res) {
              if (item.suppliers) {
                item.suppliers[0] = res;
              } else {
                item.suppliers = new Array<Suppliers>();
                item.suppliers.push(res);
              }
              this.updateProduct(true, item);
              this.getProductList(this.productsList.pageNumber);
              this.getSuppliers();
              this.supplier = new Suppliers();
            }
          });
        },
        (err) => {
          this.loading = false;
        }
      );
  }
  clonedProducts: { [s: string]: any; } = {};
  onRowEditInit(item: any) {
    if (item.suppliers.length > 0) {
      this.supplier.description = item.suppliers[0].description;
    } else {
      this.supplier = new Suppliers();
    }
    this.clonedProducts[item.id] = JSON.parse(JSON.stringify(item));
    console.log(this.clonedProducts[item.id]);
  }

  onRowEditSave(item: any) {
    delete this.clonedProducts[item.id];
    this.onSave(true, item);
  }

  onRowEditCancel(item: any, index: number) {
    this.items[index] = this.clonedProducts[item.id];
    delete this.clonedProducts[item.id];
  }
  discard() {
    this.tobeEdit = 'nun';
    this.items = JSON.parse(JSON.stringify(this.productsList.records));
    this.isProductRepated = false;
  }
  toEdit(item, edit) {
    this.editProperty = item.id;
    this.tobeEdit = edit;
    if (this.productsList.records) {
      this.items = JSON.parse(JSON.stringify(this.productsList.records));
    }
  }
  onSave(valid, myavalue) {
    const arr = this.productsList.records.filter(x => x.name === myavalue.name);
    const flag = arr.length > 1 ? true : false;
    if (valid) {
      if (!flag) {

        // this.CheckCost(myavalue.cost);
        // this.CheckPartNo(myavalue);
        // this.CheckMsrp(myavalue.msrp);

        this.productAdd = myavalue;
        this.productAdd.updatedOn = new Date();
        this.productAdd.name = this.productAdd.name.trim();
        if (this.supplier.description?.length > 0) {
          this.productAdd.suppliers = [];
          this.productAdd.suppliers.push(this.supplier);
        }
        this._productsService.UpdateProduct(this.productAdd).subscribe(
          response => {
            if (response) {
              this.tobeEdit = 'nun';
              this.getProductList(this.productsList.pageNumber);
            }
          },
          err => {
            // // console.log(err);
            this.isNameEdit = true;
            if (err.status === 403) {
              this.show = true;
              this.popupOpen((this.popupFor = 'Access denied'));
            }
          }
        );
      } else {
        this.isProductRepated = true;
      }
    }
  }
}
