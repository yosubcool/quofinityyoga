import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BreadcrumbsComponent } from './_shared/breadcrumb.component';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SettingsService } from './settings/settings-service';
import { UserService } from './user-account/user-service';
import { AutoLogoutService } from './_core/autoLogoutService';
import { DataService } from './_core/data.service';
import { RouteGaurd } from './_core/RouteGaurd';
import { NgxLoadingModule } from 'ngx-loading';
import { CommonTemplateModule } from './common/common-template.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { NgxMaskModule } from 'ngx-mask';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { QuoteReportComponent } from './quotes/quote-report/quote-report.component';
import { RoleGaurd } from './_core/RoleGuard';
import { QuotesService } from './quotes/quotes-service';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AdminLayoutComponent } from './layouts/admin-layout.component';
import { SingleLayoutComponent } from './layouts/single-layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AsideToggleDirective } from './_shared/aside.directive';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './_shared/sidebar.directive';
import { NgChartsModule } from 'ng2-charts'
import { NAV_DROPDOWN_DIRECTIVES } from './_shared/nav-dropdown.directive';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { InjectionToken } from '@angular/core';
@NgModule({
  declarations: [
    AppComponent,
    BreadcrumbsComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    QuoteReportComponent,
    AdminLayoutComponent,
    SingleLayoutComponent,
    AsideToggleDirective,
    SIDEBAR_TOGGLE_DIRECTIVES,
    NAV_DROPDOWN_DIRECTIVES
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    NgxLoadingModule.forRoot({}),
    CommonTemplateModule,
    ModalModule,
    ToastrModule.forRoot(),
    NgSlimScrollModule,
    NgxMaskModule,
    NgxMaskModule.forRoot(),
    BrowserAnimationsModule,
    NgChartsModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
  providers: [
    [DataService, SettingsService, QuotesService],
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },

    UserService,
    RouteGaurd,
    RoleGaurd,
    AutoLogoutService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
