import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-toaster",
  templateUrl: "./toaster.component.html",
  styleUrls: ["./toaster.component.scss"],
})
export class ToasterComponent implements OnInit {
  @Input() typeofMessage: string;
  @Input() message: string;
  @Input() note: string;
  @Input() data: string;
  @Input() notification: string;
  @Input() showButtons: boolean;
  @Output() yes: EventEmitter<any> = new EventEmitter<any>();
  @Output() Ok: EventEmitter<any> = new EventEmitter<any>();
  @Output() no: EventEmitter<any> = new EventEmitter<any>();
  @Output() hide: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() { }

  popupClose(closeB) {

    this.no.emit({ yes: closeB });
    // this.hide.emit({ hide: closeB });
  }

  yesButton(yesB) {

    this.yes.emit({ yes: yesB });
  }

  noButton(noB) {

    this.no.emit({ yes: noB });
  }
}
