import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit, OnDestroy,
  NgZone
} from '@angular/core';
import { SubCategories, Categories } from 'src/app/_shared/model/tenantMaster-model';
import { CommonTemplateService } from '../common-template.service';
import { ToastrService } from 'ngx-toastr';
import { Constants } from '../../_shared/constant';
import { Subscription } from 'rxjs';
// import * as sub from 'rxjs/Subscription';
// import { Actions } from 'src/app/_shared/model/actions-model';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit, OnDestroy {

  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;

  presentCategory = false;
  presentSubCategory = false;
  deletedcategory = false;
  index: any;
  subscriptions: Subscription[] = [];
  constants: any;
  isFormSubmitted = false
  @Input() subCategory = [];
  @Input() category = [];
  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  @Output() categories: EventEmitter<any> = new EventEmitter<any>();
  @Output() subCategories: EventEmitter<any> = new EventEmitter<any>();
  @Output() categoryToDelete: EventEmitter<any> = new EventEmitter<any>();
  @Output() subCategoryToDelete: EventEmitter<any> = new EventEmitter<any>();
  @Input() isEdit: boolean;
  noData = false;
  public loading = false;
  categoryToSave = new Categories();
  subCategoryToSave: SubCategories;
  formCategory = new Categories();
  formSubCategory: SubCategories;
  cate: string;
  categoryDropdown = false;
  subcate: string;
  allCategories = [];
  categoryList = [];
  subCategoryList = [];
  start = null;
  validSubCateForm = false;
  validCateForm = false;
  tobeDelete: string;
  // accessValid: Actions;
  isOpenPopup = false;
  constructor(
    private _CommonTemplateService: CommonTemplateService,
    private ngZone: NgZone,
    private _toastrService: ToastrService
  ) {
    // this.accessValid = new Actions();
    this.categoryToSave = new Categories();
    this.formCategory = new Categories();
    this.formSubCategory = new SubCategories();
    this.constants = Constants;
  }

  ngOnInit() {
    this.getAllCategories();
    // this.getModuleAccess();
  }

  // getModuleAccess() {
  //   this.subscriptions.push(
  //     this._CommonTemplateService.getData('TenantMaster/GetAccessRoleMatrix/').subscribe(res => {
  //       this.accessValid = res.categories;
  //     }));
  // }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
    }, 500);
  }

  addCategory(categoryForm, model, categoriesForm) {
    this.isFormSubmitted = true;
    const arr = [];
    let searchText: string;
    let isPresentInCategoryList: any;

    searchText = (JSON.parse(JSON.stringify(this.formCategory.description)));
    // modal.hide();
    if (categoryForm.valid) {

      this._CommonTemplateService.FindCategoryByName(this.formCategory).subscribe(res => {
        this.ngZone.run(() => {
          this.isFormSubmitted = false;
          if (res) {
            this.presentCategory = false;
            if (this.category !== null) {
              isPresentInCategoryList = this.category.filter(item => ((item.description) === searchText));
            } else {
              isPresentInCategoryList = [];
            }
            if (isPresentInCategoryList.length) {
              // this._toastrService.warning(
              //   'Category already present.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.presentCategory = true;
            } else {
              arr.push(res);
              this.loading = false;
              model.hide();
              this.categories.emit({ category: arr });
            }
            categoryForm.valid = true;
            this.loading = false;
          } else {
            const isPresentInwholeList = [];
            if (this.category !== null) {
              this.category.forEach(allCate => {
                if (((allCate.description)) === searchText) {
                  isPresentInwholeList[0] = allCate;
                }
              });
            }
            if (isPresentInwholeList.length) {
              // this._toastrService.warning(
              //   'Category already present.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.presentCategory = true;
            } else {
              model.hide();
              this.postCategory(this.formCategory, categoriesForm);
            }
          }
        });
      });
    }
  }

  postCategory(formCategory, categoriesForm) {
    const arr = [];
    this.isFormSubmitted = true;
    this._CommonTemplateService.postElement('TenantMaster/AddCategory', formCategory).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            this.isFormSubmitted = false;
            this.resetforms(categoriesForm);
            arr.push(res);
            this.loading = false;
            this.categories.emit({ category: arr });
            // this.getAllCategories();
            // this._toastrService.success(
            //   'Category Added Successfully.',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.getAllCategories();
            this.categoryToSave = new Categories();
            this.formSubCategory = new SubCategories();
            this.formCategory = new Categories()
            this.formSubCategory.description = null;
            this.loading = false;
          }
        });
      },
      err => {
        // this._toastrService.warning(
        //   'Access denied',
        //   '',
        //   this.constants.toastrConfig
        // );
        this.loading = false;
      }
    );
  }

  getAllCategories() {
    this.subscriptions.push(
      this._CommonTemplateService.getAllCategories().subscribe(
        res => {
          this.ngZone.run(() => {
            this.allCategories = res.records;
          });
        },
        err => { }
      ));
  }

  findCategory(searchText) {
    this.formCategory = new Categories();
    this.formCategory.description = searchText;
    this._CommonTemplateService.findCategory(this.formCategory).subscribe(
      res => {
        this.ngZone.run(() => {
          this.categoryList = res;
        });
      },
      err => {
        // // console.log(err)
      }
    );
  }

  resetforms(form) {
    this.categoryToSave = new Categories();
    this.formCategory = new Categories();
    this.formSubCategory = new SubCategories();
    this.isFormSubmitted = false;
    form.resetForm();
  }

  getSubcatForCategory(id) {

    if (id === null || id === undefined) {

      // this.subscriptions.push(
      //   this._CommonTemplateService.getElementThroughId('TenantMaster/GetCategoriesByName', description).subscribe(
      //     res => {
      //       this.ngZone.run(() => {
      //         if (res) {
      //           this.categoryToSave = res;
      //         } else {
      //           this.categoryToSave = null;
      //         }
      //       });
      //     },
      //     err => { }
      //   ));
    } else {

      const isPresentInCategoryList = this.allCategories.filter(item => (item.id === id));
      if (isPresentInCategoryList.length !== 0) {
        this.categoryToSave = isPresentInCategoryList[0];
        this.subscriptions.push(
          this._CommonTemplateService.getElementThroughId('TenantMaster/GetSubCategories', id).subscribe(
            res => {
              this.ngZone.run(() => {
                this.subCategoryList = res;
              });
            },
            err => { }
          ));
      } else {

        const categoryInProduct = this.category.filter(item => ((item.id) === id));
        this.subscriptions.push(
          this._CommonTemplateService.getElementThroughId('TenantMaster/GetCategoriesByName', categoryInProduct[0].description).subscribe(
            res => {
              this.ngZone.run(() => {
                if (res) {
                  this.categoryToSave = res;
                } else {
                  this.categoryToSave = null;
                }
              });
            },
            err => { }
          ));
        // this._toastrService.warning(
        //   'This category is discontinued, sub-category cannot be added.',
        //   '',
        //   this.constants.toastrConfig
        // );
      }
    }
  }

  addSubCategory(isValid, form, model) {
    this.deletedcategory = false;
    let isPresentInSubCategoryList: any;
    const arr = [];
    this.categoryDropdown = true;
    if (isValid) {
      this.formSubCategory.categories = this.categoryToSave;
      if (this.formSubCategory.categories) {
        this.categoryDropdown = false;
        let searchText: string;
        searchText = (JSON.parse(JSON.stringify(this.formSubCategory.description)));
        this._CommonTemplateService.postElement('TenantMaster/FindSubCategoryByName', this.formSubCategory).subscribe(res => {
          this.ngZone.run(() => {
            if (res) {
              this.presentSubCategory = false;
              if (this.subCategory !== null) {
                isPresentInSubCategoryList = this.subCategory.filter(item => ((item.description) === searchText));
              } else {
                isPresentInSubCategoryList = [];
              }

              if (isPresentInSubCategoryList.length) {
                this.presentSubCategory = true;
              } else {
                // arr.push(this.formSubCategory);
                arr.push(res);
                this.loading = false;
                model.hide();
                this.subCategories.emit({ subcategory: arr });
                form.resetForm();
                this.categoryToSave = new Categories();
                this.formSubCategory = new SubCategories();
                this.formCategory = new Categories();
              }
              this.loading = false;
            } else {
              model.hide();
              this.postSubCategory(form, this.formSubCategory);
            }
          });
        });
      } else {
        this.deletedcategory = true;
      }
    }
  }

  postSubCategory(form, formSubCategory) {
    this._CommonTemplateService.postElement('TenantMaster/AddSubCategory', formSubCategory).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            const arr = [];
            arr.push(res);
            this.subCategories.emit({ subcategory: arr });
            form.resetForm();
            this.categoryToSave = new Categories();
            this.formSubCategory = new SubCategories();
            this.formCategory = new Categories();
            this.loading = false;
          }
        });
      },
      err => {
        this.loading = false;
      }
    );
  }

  findSubCategory(searchText) {
    if (!this.categoryToSave.id) {
      this.categoryDropdown = true;
    } else {
      this.formSubCategory.categories = this.categoryToSave;
      this.formSubCategory.description = searchText;
      this._CommonTemplateService.findSubCategory(this.formSubCategory).subscribe(
        res => {
          this.ngZone.run(() => {
            this.subCategoryList = res;
          });
        },
        err => {
          // // console.log(err)
        }
      );
    }
  }

  onDeleteCategory(index) {
    const arr = [];
    arr.push(index);
    this.tobeDelete = '';
    this.categoryToDelete.emit({ arr: arr });
  }

  onDeleteSubcategory(index) {
    const arr = [];
    arr.push(index);
    this.tobeDelete = '';
    this.subCategoryToDelete.emit({ arr: arr });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
    console.clear();
  }

  titletoClose() {
    let titleclose: boolean;
    titleclose = false;
    this.close.emit({ close: titleclose });
  }

  // messages display
  popupOpen(popupFor) {
    this.titletoClose();
    if (this.popupFor === 'deleteSubcategory') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = 'Are you sure you want to delete this subcategory?';
    } else if (this.popupFor === 'deleteCategory') {
      this.typeofMessage = 'Error';
      this.notification = 'Are you sure you want to delete this category?';
      this.noteToDisplay = '(Note: Subcategories under this category will also get deleted)';
      this.dataToDisplay = null;
      this.showButtons = true;
      this.messageToDisplay = null;
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === 'deleteSubcategory') {
      this.onDeleteSubcategory(this.index);
      ConfirmModal.hide();
    } else if (this.popupFor === 'deleteCategory') {
      this.onDeleteCategory(this.index);
      ConfirmModal.hide();
    }
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none'
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }
}
