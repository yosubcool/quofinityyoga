import { SubCategories } from './../_shared/model/tenantMaster-model';
import { Router } from '@angular/router';
import { Injectable, NgZone, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpParamsOptions } from '@angular/common/http';
import { DataService } from 'src/app/_core/data.service';
import { Categories } from '../_shared/model/tenantMaster-model';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { User } from 'src/app/_shared/model/user-model';
import { map } from 'rxjs/operators';

@Injectable()
export class CommonTemplateService implements OnInit {
  currentUser: any;
  user: User;
  pagination = new Pagination();
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router
  ) {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.pagination = new Pagination();
  }

  ngOnInit() { }


  getData(method) {
    return this.dataService.getData(method).pipe(map(response => {
      return response;
    }));
  }

  postElement(method, model) {
    model.createdBy = this.user.id;
    model.isActive = true;
    return this.dataService.postElement(method, model).pipe(map(response => {
      return response;
    }));
  }

  getElementThroughId(method, id) {
    return this.dataService.getElementThroughId(method, id).pipe(map(response => {
      return response;
    }))
  }


  findTag(tag) {
    tag.isActive = true;
    return this.dataService
      .getElementThroughAnyObject('TenantMaster/FindTag', tag)
      .pipe(map(response => {
        return response;
      }));
  }


  // gets all categories - Tejaswini
  getAllCategories() {
    this.pagination.alphabet = 'All';
    return this.dataService.getElementThroughAnyObject('TenantMaster/GetAllCategories', this.pagination).pipe(map(response => {
      return response;
    }));
  }

  // unique category
  FindCategoryByName(category) {
    category.isActive = true;
    return this.dataService
      .getElementThroughAnyObject('TenantMaster/FindCategoryByName', category)
      .pipe(map(response => {
        return response;
      }));
  }

  // unique subcategory
  FindTagByName(tag) {
    tag.isActive = true;
    return this.dataService
      .getElementThroughAnyObject('TenantMaster/FindTagByName', tag)
      .pipe(map(response => {
        return response;
      }));
  }

  // category for typeHead
  findCategory(description) {
    return this.dataService.getElementThroughAnyObject('TenantMaster/FindCategory', description).pipe(map(response => {
      return response;
    }));
  }

  // sub category for typeHead
  findSubCategory(description) {
    return this.dataService.getElementThroughAnyObject('TenantMaster/FindSubCategory', description).pipe(map(response => {
      return response;
    }));
  }

  deleteCategory(category) {
    return this.dataService.deleteElement('TenantMaster/DeleteCategory', category.id).pipe(map(response => {
      return response;
    }));
  }

  deleteSubcategory(subcategory) {
    return this.dataService.deleteElement('TenantMaster/DeleteSubcategory', subcategory.id).pipe(map(response => {
      return response;
    }));
  }
}
