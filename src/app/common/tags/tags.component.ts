import { Component, Input, Output, EventEmitter, OnDestroy, OnInit, NgZone } from '@angular/core';
import { Tags } from 'src/app/_shared/model/tenantMaster-model';
import { FormsModule } from '@angular/forms';
import { CommonTemplateService } from 'src/app/common/common-template.service';
import { ToastrService } from 'ngx-toastr';
import { Constants } from '../../_shared/constant';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit, OnDestroy {
  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  @Input() tagShowList = [];
  @Input() isEdit: boolean;
  @Output() tagID: EventEmitter<any> = new EventEmitter<any>();
  @Output() tagToDelete: EventEmitter<any> = new EventEmitter<any>();
  tag: Tags;
  index = 0;
  subscriptions: Subscription[] = [];
  tagLimit: any;
  isFormSubmitted = false;
  constants: any;
  tagListToSelect: any = [];
  public loading = false;
  maxTagLimit = false;
  tagPresent = false;

  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  isOpenPopup = false;
  constructor(private _CommonTemplateService: CommonTemplateService, private ngZone: NgZone,
    private _toastrService: ToastrService) {
    this.constants = Constants;
    this.tagListToSelect = new Array<Tags>();
    this.tag = new Tags();
  }

  ngOnInit() {
    this.subscriptions.push(
      this._CommonTemplateService.getData('Configuration').subscribe(res => {
        this.tagLimit = res.tagLimit

      }));
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
      // // console.log('Done');
    }, 500);
  }

  addTag(productForm, model, result, Form) {

    this.isFormSubmitted = true;
    let isPresentInTagIdList = [];
    const arr = [];
    let searchText: string;
    searchText = result.description;
    if (productForm.valid) {
      this.tag.description = result.description;
      if (this.tagShowList.length === 0) {
        isPresentInTagIdList = [];
      } else {
        let checkText: string;
        checkText = (JSON.parse(JSON.stringify(result.description))).toLowerCase();
        isPresentInTagIdList = this.tagShowList.filter(item => (item.toLowerCase() === checkText));
      }
      this._CommonTemplateService.FindTagByName(this.tag).subscribe(res => {
        this.ngZone.run(() => {
          this.isFormSubmitted = false;
          this.tagPresent = false;
          if (res) {
            if (isPresentInTagIdList.length !== 0) {
              // this._toastrService.warning(
              //   'Tag already present.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.tagPresent = true;
              // this.resetforms(Form);
              this.loading = false;
              productForm.valid = true;
            } else {
              // this._toastrService.success(
              //   'Tag Added Successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              arr.push(res.description);
              this.loading = false;
              model.hide();
              this.tagID.emit({ arr: arr });
              // this.tag = new Tags();
            }
          } else {
            this.tag.description = searchText;
            this.postTag(this.tag, model, productForm);
          }
        });
      });
    }
  }

  postTag(tag, model, Form) {
    this.maxTagLimit = false;
    this.isFormSubmitted = false;
    if (this.tagShowList.length < this.tagLimit) {
      const arr = [];
      this._CommonTemplateService.postElement('TenantMaster/PostTag', tag).subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.resetforms(Form);
              // this._toastrService.success(
              //   'Tag Added Successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              arr.push(res.description);
              this.loading = false;
              model.hide();
              this.tagID.emit({ arr: arr });
              this.tag = new Tags();
            }
          });
        },
        err => {
          // this._toastrService.error(
          //   'Please provide valid details.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.loading = false;
        }
      );
    } else {
      // this._toastrService.error(
      //   'You have reached maximun limit.',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.maxTagLimit = true;
      // model.hide();
      this.loading = false;
    }
  }

  findTag(searchText) {
    this.tag.description = searchText;
    this._CommonTemplateService.findTag(this.tag).subscribe(
      res => {
        this.ngZone.run(() => {
          this.tagListToSelect = res;
        });
      },
      err => {
        // // console.log(err)
      }
    );
  }

  resetforms(form) {
    this.isFormSubmitted = false;
    form.resetForm();
  }

  onDeleteTag(id) {
    const arr = [];
    arr.push(id);
    this.tagToDelete.emit({ arr: arr });
    this.tag = new Tags();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  titletoClose() {
    let titleclose: boolean;
    titleclose = false;
    this.close.emit({ close: titleclose });
  }

  // messages display
  popupOpen(popupFor) {
    this.titletoClose();
    if (this.popupFor === 'deleteTag') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = 'Are you sure you want to delete this tag?';
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === 'deleteTag') {
      this.onDeleteTag(this.index)
      ConfirmModal.hide();
    }
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none'
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }
}
