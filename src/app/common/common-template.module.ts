import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesComponent } from 'src/app/common/categories/categories.component';
import { TagsComponent } from 'src/app/common/tags/tags.component';
import { CommonTemplateService } from 'src/app/common/common-template.service';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { ToasterComponent } from 'src/app/common/toaster/toaster.component';

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    ModalModule,
    ReactiveFormsModule,
    FormsModule,
    NgSlimScrollModule,
    TypeaheadModule.forRoot()
  ],
  declarations: [CategoriesComponent, TagsComponent,
    ToasterComponent],
  exports: [CategoriesComponent, TagsComponent, ToasterComponent],
  providers: [CommonTemplateService]
})
export class CommonTemplateModule { }
