import { Injectable, NgZone, OnInit } from '@angular/core';
import { DataService } from '../_core/data.service';
import { Router } from '@angular/router';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { User } from 'src/app/_shared/model/user-model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';

@Injectable()
export class SettingsService implements OnInit {
  constants: any;
  user: User;
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router
  ) {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
  }

  ngOnInit() { }

  postElement(method, model) {
    model.createdBy = this.user.id;
    return this.dataService.postElement(method, model).pipe(map((response) => {
      return response;
    }));
  }

  // getAllUsers(pagination: Pagination, id: any) {
  //   return this.dataService.postElement(pagination,id).pipe(map(response => {
  //     return response;
  //   });

  getAllUsers(pagination: Pagination) {
    return this.dataService
      .getElementThroughAnyObject('User/getAllUsers', pagination)
      .pipe(map((response) => {
        return response;
      }));
  }

  getTaxes(pagination: Pagination) {
    return this.dataService
      .getElementThroughAnyObject('TenantMaster/GetTaxes', pagination)
      .pipe(map((response) => {
        return response;
      }));
  }
  getMethods(pagination: Pagination) {
    return this.dataService
      .getElementThroughAnyObject('TenantMaster/GetPricingMethods', pagination)
      .pipe(map((response) => {
        return response;
      }));
  }

  // checkMethods(collection) {
  //   let masterMethodList = [];
  //   // let methodList = [];
  //   this.dataService.getData('Configuration/GetPricingMethod').subscribe(
  //     res => {
  //       this.ngZone.run(() => {
  //         masterMethodList = res;
  //         collection.records.forEach(pricingMethods => {
  //           if (!pricingMethods['method'] || pricingMethods['method'] === null) {
  //             masterMethodList.forEach(masterMethod => {
  //               if (masterMethod.name === pricingMethods.name) {
  //                 pricingMethods.method = pricingMethods.name;
  //                 pricingMethods.name = pricingMethods.value + pricingMethods.name;
  //                 // this.updateMethod(pricingMethods, true, pageNo);
  //                 pricingMethods.updatedBy = this.user.id;
  //                 this.dataService.updateElement('TenantMaster/UpdateMethod', pricingMethods).pipe(map(response => {
  //                   // console.log(response);
  //                 });
  //               }
  //             });
  //           } else if (pricingMethods['method'] !== null) {
  //             return this.dataService.getElementThroughAnyObject('TenantMaster/GetPricingMethods', collection).pipe(map(response => {
  //               // console.log(response);
  //             });
  //           }
  //         });
  //       });
  //     },
  //     err => { }
  //   );
  //   return this.dataService.getElementThroughAnyObject('TenantMaster/GetPricingMethods', collection).pipe(map(response => {
  //     return response;
  //   });
  // }
  // postTax(tax) {
  //   tax.createdBy = this.user.id;
  //   return this.dataService.postElement('TenantMaster/PostTax', tax).pipe(map(response => {
  //     return response;
  //   });
  // }

  // postMethod(method) {
  //   method.createdBy = this.user.id;
  //   return this.dataService.postElement('TenantMaster/PostPricingMethod', method).pipe(map(response => {
  //     return response;
  //   });
  // }

  findTax(tax) {
    tax.isActive = true;
    return this.dataService
      .getElementThroughAnyObject('TenantMaster/FindTax', tax)
      .pipe(map((response) => {
        return response;
      }));
  }

  findTaxByName(tax) {
    tax.isActive = true;
    return this.dataService
      .getElementThroughAnyObject('TenantMaster/FindTaxByName', tax)
      .pipe(map((response) => {
        return response;
      }));
  }

  update(method, object) {
    object.updatedBy = this.user.id;
    return this.dataService.updateElement(method, object).pipe(map((response) => {
      return response;
    }));
  }

  updateMethod(method) {
    method.updatedBy = this.user.id;
    return this.dataService
      .updateElement('TenantMaster/UpdateMethod', method)
      .pipe(map((response) => {
        return response;
      }));
  }

  getConfiguration() {
    return this.dataService.getData('Configuration').pipe(map((response) => {
      return response;
    }));
  }

  // postConfig(configuration) {
  //   configuration.createdBy = this.user.id;
  //   return this.dataService.postElement('Configuration', configuration).pipe(map(response => {
  //     return response;
  //   });
  // }

  DeletePricingMethod(method) {
    return this.dataService
      .deleteElement('TenantMaster/DeletePricingMethod', method.id)
      .pipe(map((response) => {
        return response;
      }));
  }
  DeleteMeasure(method) {
    return this.dataService
      .deleteElement('TenantMaster/DeleteUom', method.id)
      .pipe(map((response) => {
        return response;
      }));
  }
  getRoles() {
    return this.dataService
      .getData('Configuration/GetRoles')
      .pipe(map((response) => {
        return response;
      }));
  }

  getAccessMatrix(id) {
    return this.dataService
      .getElementThroughId('TenantMaster/GetAccessMatrix', id)
      .pipe(map((response) => {
        return response;
      }));
  }

  // postAccessMartix(accessMartix) {
  //   accessMartix.createdBy = this.user.id;
  //   return this.dataService.postElement('TenantMaster/AddAccessMartix', accessMartix).pipe(map(response => {
  //     return response;
  //   });
  // }

  findMethod(pricingMethod) {
    pricingMethod.isActive = true;
    return this.dataService
      .getElementThroughAnyObject(
        'TenantMaster/FindPricingMethod',
        pricingMethod
      )
      .pipe(map((response) => {
        return response;
      }));
  }

  findEmail(email) {
    return this.dataService
      .getElementThroughId('User/FindEmail', email)
      .pipe(map((response) => {
        return response;
      }));
  }

  // gets all categories () - Tejaswini
  getAllCategories(pagination: Pagination) {
    return this.dataService
      .getElementThroughAnyObject('TenantMaster/GetAllCategories', pagination)
      .pipe(map((response) => {
        return response;
      }));
  }

  // gets all Subcategories () - Tejaswini
  GetAllSubcategories(pagination: Pagination) {
    return this.dataService
      .getElementThroughAnyObject(
        'TenantMaster/GetAllSubcategories',
        pagination
      )
      .pipe(map((response) => {
        return response;
      }));
  }

  getModuleAccess() {
    return this.dataService
      .getData('TenantMaster/GetAccessRoleMatrix/')
      .pipe(map((response) => {
        return response;
      }));
  }

  getLoginDetailsofUser(id) {
    return this.dataService
      .getElementThroughId('Configuration/GetLoginDetails', id)
      .pipe(map((response) => {
        return response;
      }));
  }

  GetAllUoms() {
    return this.dataService
      .getData('TenantMaster/GetAllUom/')
      .pipe(map((response) => {
        return response;
      }));
  }
}
