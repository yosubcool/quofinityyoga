/*******************************************Confidential*****************************************
 * <copyright file = "settings.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from 'src/app/settings/settings-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { CompairValidator } from 'src/app/_shared/compair-validator.directive';
import { ProfileComponent } from 'src/app/settings/profile/profile.component';
import { ChangePassword } from 'src/app/settings/changePassword/change-password.component';
import { SettingComponent } from 'src/app/settings/setting/setting.component';
import { AccessMatrixComponent } from './setting/access-matrix/access-matrix.component';
import { TaxMatrixComponent } from './setting/tax-matrix/tax-matrix.component';
import { SettingsService } from 'src/app/settings/settings-service';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgxPaginationModule } from 'ngx-pagination';
import { ManageUserComponent } from './setting/manage-user/manage-user.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MyPlansComponent } from './my-plans/my-plans.component';
import { PricingMethodComponent } from './setting/pricing-method/pricing-method.component';
import { CategorySubcategoryComponent } from './setting/category-subcategory/category-subcategory.component';
// import { ProductsService } from 'src/app/products/products-service';
import { CommonTemplateService } from 'src/app/common/common-template.service';
import { SharedModule } from 'src/app/_shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { ConfigurationComponent } from 'src/app/settings/setting/configuration/configuration.component';
import { CommonTemplateModule } from '../common/common-template.module';
import { UnitOfMeasuresComponent } from './setting/unit-of-measures/unit-of-measures.component';
import { ProductsService } from '../products/products-service';
import { AlertModule } from 'ngx-bootstrap/alert';
// import { NgxStripeModule } from 'ngx-stripe';

@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMaskModule,
    NgxMaskModule.forRoot(),
    NgxMaskModule.forChild(),
    CommonTemplateModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    NgxPaginationModule,
    NgxLoadingModule,
    NgxLoadingModule.forRoot({}),
    TypeaheadModule.forRoot(),
    AlertModule.forRoot(),
    // NgxStripeModule.forRoot('pk_test_4TSepYhwTpSnCeTma7Iu9lnA')
  ],
  declarations: [
    ProfileComponent,
    ChangePassword,
    CompairValidator,
    SettingComponent,
    AccessMatrixComponent,
    TaxMatrixComponent,
    ManageUserComponent,
    MyPlansComponent,
    PricingMethodComponent,
    CategorySubcategoryComponent,
    ConfigurationComponent,
    UnitOfMeasuresComponent
  ],
  providers: [SettingsService
    , ProductsService
    , CommonTemplateService]
})
export class SettingsModule { }
