/*******************************************Confidential*****************************************
 * <copyright file = "change-password.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component, NgZone } from '@angular/core';
import { UserService } from 'src/app/user-account/user-service';
import { Router } from '@angular/router';
import { Constants } from 'src/app/_shared/constant';
import { User } from 'src/app/_shared/model/user-model';
import { ToastrService } from 'ngx-toastr';
@Component({
  templateUrl: './change-password.component.html'
})
// tslint:disable-next-line:component-class-suffix
export class ChangePassword {
  changePassword: any = {};
  public loading = false;
  currentpwd = false;
  constants: any;
  currentUser: any;
  isOpenPopup = false;
  constructor(
    private _UserService: UserService,
    private router: Router,
    private ngZone: NgZone,
    private toastrService: ToastrService
  ) {
    this.constants = Constants;
    toastrService.toastrConfig.preventDuplicates = true;
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
    }, 500);
  }

  Changepassword(isValid: boolean) {
    this.loading = true;
    this.currentpwd = false;
    if (isValid) {
      this._UserService.Changepassword(this.changePassword).subscribe(
        res => {
          this.loading = false;
          this.ngZone.run(() => {
            if (res) {
              // this.toastrService.success(
              //   'Password changed successfully!',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.LogOut();
            } else {
              this.loading = false;
              // this.toastrService.error(
              //   'Something went wrong! please try again.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.currentpwd = true;
            }
          });
        },
        err => { }
      );
    } else {
      this.loading = false;
      // this.toastrService.error(
      //   'Please Provide all Details.',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  LogOut() {
    this.loading = true;
    this._UserService.LogoutUser(this.currentUser.id).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res !== null) {
            this.loading = false;
            this.currentUser = null;
            sessionStorage.clear();
            this.router.navigate(['pages/app-home']);
          }
        });
      },
      err => {
        this.loading = false;
      }
    );
    // sessionStorage.clear();
    // this.router.navigate(['pages/app-home']);
  }
}
