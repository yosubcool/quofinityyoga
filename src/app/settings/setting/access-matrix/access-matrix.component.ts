import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { SettingsService } from 'src/app/settings/settings-service';
import { AccessMatrix } from 'src/app/_shared/model/access-matrix-model';
import { Constants } from 'src/app/_shared/constant';
import { Subscription } from 'rxjs';
declare var $: any;
@Component({
  selector: 'app-access-matrix',
  templateUrl: './access-matrix.component.html',
  styleUrls: ['./access-matrix.component.scss']
})
export class AccessMatrixComponent implements OnInit, OnDestroy {
  userRoles = [];
  accessMatrix: AccessMatrix;
  roleID: string;
  subscriptions: Subscription[] = [];
  constants: any;
  isAdmin = true;
  public loading = false;
  constructor(private _settingsService: SettingsService, private toastrService: ToastrService, private ngZone: NgZone) {
    this.constants = Constants;
  }
  ngOnInit() {
    this.GetRoles();
    $(window).scrollTop(0);
  }

  GetRoles() {
    this.subscriptions.push(
      this._settingsService.getRoles().subscribe(
        response => {
          this.ngZone.run(() => {
            const index = response.findIndex(x => x.description === 'Super Admin');
            if (index > -1) {
              response.splice(index, 1);
            }
            this.userRoles = response;
            this.roleID = response[0].id;
            this.roleSelected(this.roleID);
          });
        },
        err => { }
      )
    );
  }

  GetAccessMatrix(id) {
    this.subscriptions.push(
      this._settingsService.getAccessMatrix(id).subscribe(
        response => {
          this.ngZone.run(() => {
            this.accessMatrix = response;
          });
        },
        err => { }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  roleSelected(roleId) {
    if (roleId === this.userRoles[0].id) {
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
    this.GetAccessMatrix(roleId);
  }

  postAccessMartix(accessMartix) {
    if (this.isAdmin === false) {
      this._settingsService.postElement('TenantMaster/AddAccessMartix', accessMartix).subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.loading = false;
            }
          });
        },
        err => {
          this.loading = false;
        }
      );
    }
  }
}
