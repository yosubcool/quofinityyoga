import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricingMethodComponent } from './pricing-method.component';

describe('PricingMethodComponent', () => {
  let component: PricingMethodComponent;
  let fixture: ComponentFixture<PricingMethodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricingMethodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricingMethodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
