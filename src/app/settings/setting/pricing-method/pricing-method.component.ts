import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { Component, OnInit, OnDestroy, NgZone } from "@angular/core";
import { PricingMethod } from "src/app/_shared/model/pricing-model";
import { SettingsService } from "src/app/settings/settings-service";
import { Constants } from "src/app/_shared/constant";
import { PaginatePipe, PaginationService } from "ngx-pagination";
import { NgxPaginationModule } from "ngx-pagination";
import { Pagination } from "src/app/_shared/model/pagination-model";
declare var $: any;
import { Actions } from "src/app/_shared/model/actions-model";
import { Subscription } from "rxjs";
import { ProductsService } from "src/app/products/products-service";
@Component({
  selector: "app-pricing-method",
  templateUrl: "./pricing-method.component.html",
  styleUrls: ["./pricing-method.component.scss"],
})
export class PricingMethodComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  clearMName = false;
  clearMPercent = false;
  constants: any;
  page: any;
  masterMethodList = [];
  start = null;
  methodID: any;
  methodValue: number;
  subscriptions: Subscription[] = [];
  showbtn = false;
  PmethodFormSubmitted = false;
  methodList: Pagination;
  public loading = false;
  isAddMaximum = false;
  isUpdateMaximum = false;
  editMethod = false;
  pagesCount: any;
  selectedAlpha: string;
  methodName: string;
  mathod: string;
  pricingMethod: PricingMethod;
  addPricingMethod: PricingMethod;
  isMethodPrecent = false;
  isFormMethodPrecent = false;
  accessValid: any;
  isOpenPopup = true;
  constructor(
    private _settingsService: SettingsService,
    private productsService: ProductsService,//#revisit_tempRemovalUntilFolderMove
    private router: Router,
    private toastrService: ToastrService,
    private ngZone: NgZone
  ) {
    this.accessValid = new Actions();
    this.constants = Constants;
    this.methodList = new Pagination();
    this.pricingMethod = new PricingMethod();
    this.addPricingMethod = new PricingMethod();
    this.showbtn = false;
    this.pricingMethod.method = this.start;
    this.addPricingMethod.method = this.start;
    this.getModuleAccess();
  }
  ngOnInit() {
    this.getModuleAccess();
    this.getPricing();
    this.getMethodList(0);
    this.pricingMethod.value = 0;
    this.addPricingMethod.value = 0;
    this.selectedAlpha = "All";
    this.pricingMethod.method = null;
    this.addPricingMethod.method = null;
    this.showbtn = false;
    this.pricingMethod = new PricingMethod();
    this.addPricingMethod = new PricingMethod();
    this.pricingMethod.method = this.start;
    this.addPricingMethod.method = this.start;
    $(window).scrollTop(0);
    this.setFocus();
  }
  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      setTimeout(() => {
        this.isOpenPopup = false;
      }, 500);
    }, 500);
  }
  getModuleAccess() {
    this.subscriptions.push(
      this._settingsService.getModuleAccess().subscribe((res) => {
        this.accessValid = res.pricingMethods;
      })
    );
  }

  // Gets all methods
  getPricing() {

    //#revisit_tempRemovalUntilFolderMove
    this.subscriptions.push(
      this.productsService.getData("Configuration/GetPricingMethod").subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.masterMethodList = res;
          });
        },
        (err) => {
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.getMethodList(0);
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  deleteMethod(obj, page, ConfirmModal) {
    if (obj.isActive === false) {
      this._settingsService.DeletePricingMethod(obj).subscribe(
        (res) => {
          ConfirmModal.hide();
          this.pricingMethod.method = this.start;
          this.pricingMethod.value = null;
          this.pricingMethod.name = null;
          this.getMethodList(page);
        },
        (err) => {
          // console.log(err);
          if (err.status === 403) {
            this.show = true;
            this.getMethodList(0);
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      );
      this.editMethod = false;
      this.methodID = 0;
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.methodID = obj.id;
    }
  }
  getMethodList(pageNo) {
    this.pricingMethod = new PricingMethod();
    this.addPricingMethod = new PricingMethod();
    this.editMethod = false;
    this.loading = true; // Loader Enable
    this.methodList.pageNumber = pageNo;
    this.methodList.pageSize = 10;
    this.subscriptions.push(
      this._settingsService.getMethods(this.methodList).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.checkMethods(res);
            this.loading = false; // Loader Disable
          });
        },
        (err) => {
          // console.log(err);
          this.loading = false; // Loader Disable
          if (err.status === 403) {
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      )
    );
  }
  resetforms(form) {
    form.resetForm();
    this.PmethodFormSubmitted = false;
    this.addPricingMethod.method = this.start;
  }
  addMethod(methodForm) {
    this.pricingMethod = this.addPricingMethod;
    let isPresent: any;
    this.PmethodFormSubmitted = true;
    if (this.pricingMethod.method && this.pricingMethod.method) {
      if (methodForm.valid) {
        const valueToCheck = parseFloat(this.pricingMethod.value);
        this.pricingMethod.value = isNaN(valueToCheck)
          ? ""
          : valueToCheck.toFixed(2);
        if (
          (this.masterMethodList[0].name === this.pricingMethod.method ||
            this.masterMethodList[3].name === this.pricingMethod.method ||
            this.masterMethodList[2].name === this.pricingMethod.method ||
            this.masterMethodList[1].name === this.pricingMethod.method) &&
          valueToCheck !== 0 &&
          valueToCheck < 100.1
        ) {
          this._settingsService.findMethod(this.pricingMethod).subscribe(
            (res) => {
              this.ngZone.run(() => {
                isPresent = res;
                if (isPresent === null || isPresent === undefined) {
                  this.showbtn = true;
                  this.isMethodPrecent = false;
                  this.postMethod(methodForm, this.pricingMethod);
                } else {
                  this.showbtn = true;
                  // this.toastrService.warning(
                  //   'Method already present.',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                  this.isMethodPrecent = true;
                  this.loading = false;
                }
              });
            },
            (err) => {
              if (err.status === 403) {
                this.show = true;
                this.getMethodList(0);
                this.popupOpen((this.popupFor = "Access denied"));
              }
              // console.log(err);
            }
          );
        }
      } else {
        this.showbtn = true;
        // this.loading = false;
      }
    } else {
      this.showbtn = true;
      // this.toastrService.error(
      //   'Select method.',
      //   '',
      //   this.constants.toastrConfig
      // );
      //  this.loading = false;
    }
  }
  postMethod(methodForm, method) {
    // if (method.method === this.masterMethodList[0].name) {
    //   method.value = 0;
    // }
    this._settingsService
      .postElement("TenantMaster/PostPricingMethod", method)
      .subscribe(
        (res) => {
          this.ngZone.run(() => {
            if (res) {
              this.PmethodFormSubmitted = false;
              this.resetforms(methodForm);
              this.pricingMethod = new PricingMethod();
              this.addPricingMethod = new PricingMethod();
              this.getMethodList(0);
              this.showbtn = false;
              // this.toastrService.success(
              //   'Method Added Successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.loading = false;
            }
          });
        },
        (err) => {
          // this.showbtn = false;
          if (err.status === 403) {
            this.show = true;
            this.getMethodList(0);
            this.PmethodFormSubmitted = false;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      );
  }
  updateMethod(pricingMethod: PricingMethod, isValid, pageNumber) {
    if (isValid) {
      const valueToCheck = parseFloat(pricingMethod.value);
      pricingMethod.value = isNaN(valueToCheck) ? "" : valueToCheck.toFixed(2);

      if (
        this.masterMethodList[0].name === pricingMethod.method ||
        ((this.masterMethodList[3].name === pricingMethod.method ||
          this.masterMethodList[2].name === pricingMethod.method ||
          this.masterMethodList[1].name === pricingMethod.method) &&
          valueToCheck !== 0 &&
          valueToCheck < 100.1)
      ) {
        let isPresent: any;
        this._settingsService.findMethod(pricingMethod).subscribe(
          (res) => {
            this.ngZone.run(() => {
              isPresent = res;
              if (isPresent === null || isPresent === undefined) {
                // this.postMethod(true, pricingMethod);
                if (
                  (pricingMethod.name &&
                    pricingMethod.method &&
                    pricingMethod.value) ||
                  (pricingMethod.name &&
                    pricingMethod.method === this.masterMethodList[0].name)
                ) {
                  this.changeMethod(pricingMethod, pageNumber);
                  this.editMethod = false;
                  this.methodID = 0;
                  // this.toastrService.success(
                  //   'Method Updated Successfully.',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                  this.loading = false;
                } else {
                  this.editMethod = true;
                  this.methodID = pricingMethod.id;
                  // this.toastrService.warning(
                  //   'Fields should not be empty!',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                }
              } else {
                this.isFormMethodPrecent = false;
                if (res.id === pricingMethod.id) {
                  if (
                    valueToCheck === parseFloat(res.value) &&
                    pricingMethod.method === res.method &&
                    pricingMethod.name === res.name
                  ) {
                    this.editMethod = false;
                  } else if (
                    valueToCheck !== parseFloat(res.value) &&
                    pricingMethod.method === res.method
                  ) {
                    this.changeMethod(pricingMethod, pageNumber);
                    this.editMethod = false;
                    this.methodID = 0;
                  } else if (
                    pricingMethod.method !== res.method &&
                    valueToCheck === parseFloat(res.value) &&
                    pricingMethod.name === res.name
                  ) {
                    this.changeMethod(pricingMethod, pageNumber);
                    this.editMethod = false;
                    this.methodID = 0;
                  }
                } else {
                  // this.toastrService.warning(
                  //   'Method already present.',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                  this.isFormMethodPrecent = true;
                  this.loading = false;
                }
              }
            });
          },
          (err) => {
            if (err.status === 403) {
              this.show = true;
              this.getMethodList(0);
              this.popupOpen((this.popupFor = "Access denied"));
            }
            // console.log(err);
          }
        );
      }
    }
  }
  changeMethod(method, pageNumber) {
    this._settingsService.updateMethod(method).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.pricingMethod.method = this.start;
            this.pricingMethod.value = null;
            this.getMethodList(pageNumber);
          }
        });
      },
      (err) => {
        // this.toastrService.error(
        //   'Please provide valid details.',
        //   '',
        //   this.constants.toastrConfig
        // );
        if (err.status === 403) {
          this.show = true;
          this.getMethodList(0);
          this.popupOpen((this.popupFor = "Access denied"));
        }
        this.loading = false;
      }
    );
  }
  checkForMax(value, method, callfrom) {
    let isMax: boolean;
    const valuetoCompare = parseFloat(value);
    if (method === this.masterMethodList[2].name) {
      if (valuetoCompare > 100 || valuetoCompare === 0) {
        isMax = true;
      } else {
        isMax = false;
      }
    } else if (method === this.masterMethodList[1].name) {
      if (valuetoCompare > 100 || valuetoCompare === 0) {
        isMax = true;
      } else {
        isMax = false;
      }
    } else if (method === this.masterMethodList[3].name) {
      if (valuetoCompare > 100 || valuetoCompare === 0) {
        isMax = true;
      } else {
        isMax = false;
      }
    }

    if (callfrom === "Add") {
      this.isAddMaximum = isMax;
    } else if (callfrom === "Update") {
      this.isUpdateMaximum = isMax;
    }
  }

  // method to check if method property absent, if so then update method
  checkMethods(collection) {
    collection.records.forEach((pricingMethods) => {
      if (!pricingMethods["method"] || pricingMethods["method"] === null) {
        this.masterMethodList.forEach((masterMethod) => {
          if (masterMethod.name === pricingMethods.name) {
            pricingMethods.method = pricingMethods.name;
            pricingMethods.name = pricingMethods.value + pricingMethods.name;
            this.updateMethod(pricingMethods, true, collection.pageNumber);
          }
        });
      } else if (pricingMethods["method"] !== null) {
        this.methodList = collection;
      }
    });
  }

  // messages display
  popupOpen(popupFor) {
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.messageToDisplay = null;
    this.typeofMessage = "Error";
    if (this.popupFor === "deletepricingMethod") {
      this.typeofMessage = "Error";
      this.notification = "Are you sure you want to delete this method?";
      this.showButtons = true;
    } else if (this.popupFor === "Access denied") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Access denied";
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === "deletepricingMethod") {
      this.deleteMethod(this.pricingMethod, this.page, ConfirmModal);
    }
  }

  no(ConfirmModal) {
    this.show = false;
    ConfirmModal.hide();
    this.popupFor = "none";
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }
}
