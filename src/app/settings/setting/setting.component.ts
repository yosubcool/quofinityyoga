import { Component, OnInit, NgZone } from '@angular/core';
// import { UserService } from 'src/app/account/User-service';
import { Constants } from 'src/app/_shared/constant';
import { User } from 'src/app/_shared/model/user-model';
// import { RegistrationVM } from 'src/app/_shared/model/RegistrationVM';
// import { ToastrService } from 'toastr-ng2';
declare var $: any;
@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html'
})
export class SettingComponent {
  tabString: string;
  constants: any;
  currentUser: User;
  constructor() {
    this.tabString = 'manageUser';
    this.constants = Constants;
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    if (this.currentUser.role.description === this.constants.GeneralUser) {
      this.tabString = 'pricicngMethods';
    }
  }

  showTab(tab) {
    tab = true;
  }

}
