import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxMatrixComponent } from './tax-matrix.component';

describe('TaxMatrixComponent', () => {
  let component: TaxMatrixComponent;
  let fixture: ComponentFixture<TaxMatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxMatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
