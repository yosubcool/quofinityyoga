import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { SettingsService } from 'src/app/settings/settings-service';
import { Constants } from 'src/app/_shared/constant';
import { Taxes } from 'src/app/_shared/model/tenantMaster-model';
import { PaginatePipe, PaginationService } from 'ngx-pagination';
import { NgxPaginationModule } from 'ngx-pagination';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { Subscription } from 'rxjs';
// import * as sub from 'rxjs/Subscription';
declare var $: any;
@Component({
  selector: 'app-tax-matrix',
  templateUrl: './tax-matrix.component.html',
  styleUrls: ['./tax-matrix.component.scss']
})
export class TaxMatrixComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  clearTName = false;
  clearTPercent = false;
  isTaxPresent = false;
  page: any;
  showbtn = false;
  constants: any;
  tax: Taxes;
  taxInAddForm: Taxes;
  pagesCount: number;
  taxListToSelect = [];
  isFormSubmitted = false;
  taxList = new Pagination();
  editTax = false;
  taxID: any;
  taxName: string;
  subscriptions: Subscription[] = [];
  isAddMaximum = false;
  isUpdateMaximum = false;
  isValueValid = false;
  taxValue: number;
  taxUsed = false;
  public loading = false;
  isOpenPopup = false;
  constructor(
    private _settingsService: SettingsService,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService
  ) {
    this.constants = Constants;
    this.tax = new Taxes();
    this.taxInAddForm = new Taxes();
    this.taxListToSelect = new Array<Taxes>();
    this.taxID = 0;
    // this.setFocus();
  }

  ngOnInit() {
    this.getTaxList(0);
    $(window).scrollTop(0);
    setTimeout(() => this.setFocus(), 500);
    // this.setFocus();
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
    }, 500);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }
  getTaxList(pageNo) {
    this.loading = true; // Loader Enable
    this.taxList.pageNumber = pageNo;
    this.taxList.pageSize = 10;
    this.subscriptions.push(
      this._settingsService.getTaxes(this.taxList).subscribe(
        res => {
          this.ngZone.run(() => {
            this.taxList = res;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            this.loading = false; // Loader Disable
          });
        },
        err => {
          // console.log(err);
          this.loading = false; // Loader Disable
        }
      )
    );
  }

  taxpageChanged(no) {
    this.getTaxList(no);
  }

  addTax(taxForm) {
    let isPresent: any;
    this.isFormSubmitted = true;
    this.loading = true;
    if (taxForm.valid && !this.isValueValid) {
      this._settingsService.findTaxByName(this.taxInAddForm).subscribe(
        res => {
          this.ngZone.run(() => {
            isPresent = res;
            if (isPresent === null || isPresent === undefined) {
              this.postTax(taxForm, this.taxInAddForm);
              this.isTaxPresent = false;
            } else {
              // this.toastrService.warning(
              //   'Tax already present.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.isTaxPresent = true;
              this.loading = false;
            }
          });
        },
        err => {
          // console.log(err);
        }
      );
    }
  }

  findTax(searchText) {
    this.tax.name = searchText;
    this._settingsService.findTax(this.tax).subscribe(
      res => {
        this.ngZone.run(() => {
          this.taxListToSelect = res;
        });
      },
      err => {
        // console.log(err);
      }
    );
  }
  resetforms(form) {
    this.isFormSubmitted = false;
    this.editTax = false;
    this.tax = new Taxes();
    this.taxValue = null;
    this.taxName = null;
    if (form !== undefined) {
      form.resetForm();
    }
  }
  postTax(taxForm, tax) {
    const valueToCheck = parseFloat(tax.value);
    tax.value = isNaN(valueToCheck) ? '' : valueToCheck.toFixed(2);
    if (valueToCheck >= 0 && valueToCheck < 101) {
      this._settingsService.postElement('TenantMaster/PostTax', tax).subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.isFormSubmitted = false;
              this.resetforms(taxForm);
              this.tax = new Taxes();
              this.taxInAddForm = new Taxes();
              this.getTaxList(0);
              // this.toastrService.success(
              //   'Tax Added Successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.loading = false;
            }
          });
        },
        err => {
          // this.toastrService.error(
          //   'Please provide valid details.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.loading = false;
        }
      );
    }
  }
  updateTax(tax: Taxes, nameForm, valueForm, pageNumber) {
    this.taxUsed = false;
    if (
      nameForm.valid === true &&
      valueForm.valid === true &&
      !this.isValueValid &&
      !this.isUpdateMaximum
    ) {
      let isPresent: any;
      const valueToCheck = parseFloat(tax.value);
      tax.value = isNaN(valueToCheck) ? '' : valueToCheck.toFixed(2);
      this._settingsService.findTaxByName(tax).subscribe(
        res => {
          this.ngZone.run(() => {
            isPresent = res;
            if (isPresent === null || isPresent === undefined) {
              if (tax.name && tax.value) {
                this.changeTax(tax, pageNumber);
                this.editTax = false;
                this.taxID = 0;
                // this.toastrService.success(
                //   'Tax Updated Successfully.',
                //   '',
                //   this.constants.toastrConfig
                // );
                this.loading = false;
              } else {
                // this.toastrService.warning(
                //   'Fields should not be empty!',
                //   '',
                //   this.constants.toastrConfig
                // );
                this.editTax = true;
                this.taxID = tax.id;
              }
            } else {
              if (res.id === tax.id) {
                if (valueToCheck === parseFloat(res.value)) {
                  this.editTax = false;
                } else {
                  this.changeTax(tax, pageNumber);
                  this.editTax = false;
                  this.taxID = 0;
                  // this.toastrService.success(
                  //   'Tax Updated Successfully.',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                  this.loading = false;
                }
              } else {
                // this.toastrService.warning(
                //   'Tax already present.',
                //   '',
                //   this.constants.toastrConfig
                // );
                this.taxUsed = true;
                this.loading = false;
              }
            }
          });
        },
        err => {
          // console.log(err);
        }
      );
    }
  }

  deleteTax(tax: Taxes, page) {
    if (tax.isActive === false) {
      this.changeTax(tax, page);
      this.taxID = 0;
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.taxID = tax.id;
    }
  }

  changeTax(tax, page) {
    this._settingsService.update('TenantMaster/UpdateTax', tax).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            this.getTaxList(page);
          }
        });
      },
      err => {
        // this.toastrService.error(
        //   'Please provide valid details.',
        //   '',
        //   this.constants.toastrConfig
        // );
        this.loading = false;
      }
    );
  }
  checkForMax(value, callfrom) {
    let isMax: boolean;
    if (parseFloat(value) > 100 || parseFloat(value) < 0) {
      isMax = true;
    } else {
      isMax = false;
    }
    if (callfrom === 'Add') {
      this.isAddMaximum = isMax;
    } else if (callfrom === 'Update') {
      this.isUpdateMaximum = isMax;
    }
  }

  // messages display
  popupOpen(popupFor) {
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.messageToDisplay = null;
    this.typeofMessage = 'Error';
    if (this.popupFor === 'tax') {
      this.typeofMessage = 'Error';
      this.notification = 'Are you sure you want to delete this tax?';
      this.noteToDisplay = null;
      this.showButtons = true;
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === 'tax') {
      this.deleteTax(this.tax, this.page);
      ConfirmModal.hide();
    }
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }
}
