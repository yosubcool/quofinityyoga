import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitOfMeasuresComponent } from './unit-of-measures.component';

describe('UnitOfMeasuresComponent', () => {
  let component: UnitOfMeasuresComponent;
  let fixture: ComponentFixture<UnitOfMeasuresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitOfMeasuresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitOfMeasuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
