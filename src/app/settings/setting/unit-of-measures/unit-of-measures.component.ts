import { Component, OnInit, NgZone } from '@angular/core';
import { Constants } from 'src/app/_shared/constant';
import { SettingsService } from 'src/app/settings/settings-service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-unit-of-measures',
  templateUrl: './unit-of-measures.component.html',
  styleUrls: ['./unit-of-measures.component.scss'],
})
export class UnitOfMeasuresComponent implements OnInit {
  subscriptions: Subscription[] = [];
  constants: any;

  unitOfMeasure: {
    id: string;
    name: string;
    isActive: boolean;
    createdOn: Date;
    createdBy: number;
    updatedOn: Date;
    updatedBy: number;
  };
  unitOfMeasures: {
    id: string;
    name: string;
    isActive: boolean;
    createdOn: Date;
    createdBy: number;
    updatedOn: Date;
    updatedBy: number;
  }[];
  clearMName = false;
  accessValid: any;
  isOpenPopup = true;
  noteToDisplay: any;
  dataToDisplay: any;
  messageToDisplay: any;
  typeofMessage: string;
  popupFor: string;
  notification: string;
  showButtons: boolean;
  measure: any;
  page: any;
  show: boolean;
  isEdit = false;
  public loading = false;
  uomTodelete: any;
  constructor(
    private _settingsService: SettingsService,
    private ngZone: NgZone
  ) {
    this.constants = Constants;
    $(window).scrollTop(0);
    this.setFocus();
    this.unitOfMeasure = {
      id: null,
      name: null,
      isActive: true,
      createdOn: new Date(),
      createdBy: null,
      updatedOn: new Date(),
      updatedBy: null,
    };
    this.unitOfMeasures = new Array<{
      id: null;
      name: null;
      isActive: true;
      createdOn: null;
      createdBy: null;
      updatedOn: null;
      updatedBy: null;
    }>();
  }

  ngOnInit() {
    this.getMeasureList();
  }

  getModuleAccess() {
    this.subscriptions.push(
      this._settingsService.getModuleAccess().subscribe((res) => {
        this.accessValid = res.pricingMethods;
      })
    );
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      setTimeout(() => {
        this.isOpenPopup = false;
      }, 500);
    }, 500);
  }

  addMeasures(form: NgForm) {
    this.loading = true;
    if (form.valid) {
      this._settingsService
        .postElement('TenantMaster/PostUom', this.unitOfMeasure)
        .subscribe(
          (res) => {
            this.ngZone.run(() => {
              if (res) {
                this.loading = false;
                this.unitOfMeasure = {
                  id: null,
                  name: null,
                  isActive: true,
                  createdOn: new Date(),
                  createdBy: null,
                  updatedOn: new Date(),
                  updatedBy: null,
                };
                form.resetForm();
                this.getMeasureList();
              }
            });
          },
          (err) => {
            this.loading = false;
          }
        );
    }
  }

  getMeasureList() {
    this._settingsService.GetAllUoms().subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.unitOfMeasures = res;
            this.loading = false;
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  // messages display
  popupOpen(popupFor, uom) {
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.messageToDisplay = null;
    this.typeofMessage = 'Error';
    if (this.popupFor === 'deleteMeasure') {
      this.typeofMessage = 'Error';
      this.notification =
        'Are you sure you want to delete this unit of measure?';
      this.showButtons = true;
      this.uomTodelete = uom;
    } else if (this.popupFor === 'Access denied') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Access denied';
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === 'deleteMeasure') {
      this.deleteMeasure(this.uomTodelete, ConfirmModal);
    }
  }

  no(ConfirmModal) {
    this.show = false;
    ConfirmModal.hide();
    this.popupFor = 'none';
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }

  deleteMeasure(obj, ConfirmModal) {
    this.loading = true;
    if (obj.isActive === true) {
      this._settingsService.DeleteMeasure(obj).subscribe(
        (res) => {
          ConfirmModal.hide();
          this.getMeasureList();
        },
        (err) => {
          if (err.status === 403) {
            this.show = true;
            this.getMeasureList();
            this.popupOpen((this.popupFor = 'Access denied'), obj);
          }
        }
      );
    } else {
    }
  }

  edit(data) {
    this.isEdit = true;
    this.clearMName = true;
    this.unitOfMeasure = {
      id: data.id,
      name: data.name,
      isActive: data.isActive,
      createdOn: data.createdOn,
      createdBy: data.createdBy,
      updatedBy: data.updatedBy,
      updatedOn: data.updatedOn,
    };
  }
  updateMeasure(form: NgForm) {
    this.loading = true;
    if (form.valid) {
      this._settingsService
        .update('TenantMaster/UpdateUom', this.unitOfMeasure)
        .subscribe(
          (res) => {
            this.ngZone.run(() => {
              if (res) {
                this.isEdit = false;
                this.loading = false;
                form.resetForm();
                this.unitOfMeasure = {
                  id: null,
                  name: null,
                  isActive: true,
                  createdOn: new Date(),
                  createdBy: null,
                  updatedOn: new Date(),
                  updatedBy: null,
                };
                this.getMeasureList();
              }
            });
          },
          (err) => {
            this.loading = false;
          }
        );
    }
  }
  cancel(form: NgForm) {
    form.reset();
    this.isEdit = false;
    this.unitOfMeasure = {
      id: null,
      name: null,
      isActive: true,
      createdOn: new Date(),
      createdBy: null,
      updatedOn: new Date(),
      updatedBy: null,
    };
  }
}
