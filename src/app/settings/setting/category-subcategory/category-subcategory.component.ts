// import { debounce } from 'rxjs/operator/debounce'; //#Revisit_IgnoredAsNotUsed
import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { SettingsService } from 'src/app/settings/settings-service';
import { CommonTemplateService } from 'src/app/common/common-template.service';
import {
  Categories,
  SubCategories
} from 'src/app/_shared/model/tenantMaster-model';
import { Constants } from 'src/app/_shared/constant';
import { PaginatePipe, PaginationService } from 'ngx-pagination';
import { NgxPaginationModule } from 'ngx-pagination';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { Actions } from '../../../_shared/model/actions-model';
import { Subscription } from 'rxjs';
declare var $: any;
@Component({
  selector: 'app-category-subcategory',
  templateUrl: './category-subcategory.component.html',
  styleUrls: ['./category-subcategory.component.scss']
})
export class CategorySubcategoryComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  clearCategory = false;
  clearSubcategory = false;
  clearCategorySubcategory = false;
  public isCategoryClicked: boolean;
  public isSubCategoryClicked: boolean;
  subCategoryPresent = false;
  updateSubCategoryPresent = false;
  categoryPresnet = false;
  mainCategory = false;
  showbtn = false;
  public loading = false;
  body: any;
  start = null;
  editcategory = false;
  editSubcategory = false;
  categoryID: any;
  categoryName: string;
  subcategoryName: string;
  subcategoryID: any;
  isCategoryDelete = false;
  isSubcategoryDelete = false;
  subcategories: SubCategories;
  subcategoryForm = false;
  category: Categories;
  categoryList = [];
  page: any;
  categoryToSave: Categories;
  formCategory: Categories;
  formSubCategory: SubCategories;
  subcategoryList = [];
  allCategory: Pagination;
  subscriptions: Subscription[] = [];
  allCategoryForDropdown = [];
  constants: any;
  allSubcategory: any;
  accessValid: Actions;
  updatedCategory = false;
  isOpenPopup = false;
  isFormSubmitted = false;

  isMaximum: boolean; //#revisit_AddedToRectifyError
  constructor(
    private _settingsService: SettingsService,
    private ngZone: NgZone,
    private _toastrService: ToastrService,
    private commonTemplateService: CommonTemplateService
  ) {
    this.accessValid = new Actions();
    this.category = new Categories();
    this.subcategories = new SubCategories();
    this.formCategory = new Categories();
    this.formSubCategory = new SubCategories();
    this.constants = Constants;
    this.editcategory = false;
    this.allCategory = new Pagination();
    this.allSubcategory = new Pagination();
  }

  ngOnInit() {
    $(window).scrollTop(0);
    this.getAllCategories(0);
    this.GetAllSubcategories(0);
    this.getAllCategoriesForDropdown();
    this.getModuleAccess();
    // this.setFocus();
  }

  setFocus() {
    // console.log('SetFocuss called');
    this.isOpenPopup = true;
    setTimeout(
      () =>
        setTimeout(() => {
          this.isOpenPopup = false;
        }, 500),
      500
    );
    // setTimeout(() => {
    //   this.isOpenPopup = false;
    // }, 500);
  }

  getModuleAccess() {
    this.subscriptions.push(
      this._settingsService.getModuleAccess().subscribe(res => {
        this.accessValid = res.categories;
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  getAllCategories(pageNo) {
    this.allCategory.pageNumber = pageNo;
    this.allCategory.pageSize = 10;
    this.subscriptions.push(
      this._settingsService.getAllCategories(this.allCategory).subscribe(
        res => {
          this.ngZone.run(() => {
            // // console.log(res);
            this.allCategory = res;
            this.loading = false; // Loader Disable
          });
        },
        err => {
          // // console.log(err);
          this.loading = false; // Loader Disable
        }
      )
    );
  }

  getAllCategoriesForDropdown() {
    this.allCategory.alphabet = 'All';
    this.allCategory.pageSize = 10;
    this.subscriptions.push(
      this._settingsService.getAllCategories(this.allCategory).subscribe(
        res => {
          this.ngZone.run(() => {
            // // console.log(res);
            this.allCategoryForDropdown = res.records;
            this.loading = false; // Loader Disable
          });
        },
        err => {
          // // console.log(err);
          this.loading = false; // Loader Disable
        }
      )
    );
  }

  GetAllSubcategories(pageNo) {
    this.allSubcategory.pageNumber = pageNo;
    this.allSubcategory.pageSize = 10;
    this.subscriptions.push(
      this._settingsService.GetAllSubcategories(this.allSubcategory).subscribe(
        res => {
          this.ngZone.run(() => {
            // // console.log(res);
            this.allSubcategory = res;
            this.loading = false; // Loader Disable
          });
        },
        err => {
          // // console.log(err);
          this.loading = false; // Loader Disable
        }
      )
    );
  }

  onSearchCategory(category: Categories) {
    this.categoryList = [];
    if (category.description === null || category.description === undefined) {
      this.categoryList = [];
    } else {
      category.description = category.description;
      this.commonTemplateService.findCategory(category).subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.categoryList = res;
              // // console.log(res);
              this.loading = false; // Loader Disable
            }
          });
        },
        err => {
          // // console.log(err);
          this.loading = false; // Loader Disable
        }
      );
    }
  }

  onSearchSubcategory(subcategories: SubCategories) {
    this.subcategoryList = [];
    if (
      this.subcategories.description === null ||
      this.subcategories.description === undefined
    ) {
      this.subcategoryList = [];
    } else {
      this.subcategories.description = this.subcategories.description;
      this.commonTemplateService
        .postElement('TenantMaster/FindSubCategoryByName', this.subcategories)
        .subscribe(
          res => {
            this.ngZone.run(() => {
              if (res) {
                this.subcategoryList.push(res);
                // // console.log(res);
                this.loading = false; // Loader Disable
              }
            });
          },
          err => {
            // // console.log(err);
            this.loading = false; // Loader Disable
          }
        );
    }
  }

  searchUniqueCategory(body) {
    this.categoryList = [];
    if (body === null || body === undefined) {
      this.categoryList = [];
    } else {
      this.category.description = body;
      this.commonTemplateService.FindCategoryByName(this.category).subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.categoryList.push(res);
              // // console.log(res);
              this.loading = false; // Loader Disable
            }
          });
        },
        err => {
          // // console.log(err);
          this.loading = false; // Loader Disable
        }
      );
    }
  }

  searchUniqueSubcategory(body) {
    this.subcategoryList = [];
    if (body === null || body === undefined) {
      this.subcategoryList = [];
    } else {
      this.subcategories.description = body;
      this.commonTemplateService
        .postElement('TenantMaster/FindSubCategoryByName', this.subcategories)
        .subscribe(
          res => {
            this.ngZone.run(() => {
              if (res) {
                this.subcategoryList.push(res);
                // // console.log(res);
                this.loading = false; // Loader Disable
              }
            });
          },
          err => {
            // // console.log(err);
            this.loading = false; // Loader Disable
          }
        );
    }
  }

  deleteSubcategory(subcategory: SubCategories, page) {
    this.commonTemplateService.deleteSubcategory(subcategory).subscribe(
      res => {
        this.ngZone.run(() => {
          // // console.log(res);
          // this._toastrService.error(
          //   'Subcategory deleted Successfully.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.getAllCategories(0);
          this.GetAllSubcategories(page);
          this.page = null;
          this.getAllCategoriesForDropdown();
          this.category.description = subcategory.description;
          this.onSearchCategory(this.category);
          this.onSearchSubcategory(subcategory);
          this.loading = false; // Loader Disable
        });
      },
      err => {
        // this._toastrService.warning(
        //   'Access denied',
        //   '',
        //   this.constants.toastrConfig
        // );
        // // console.log(err);
        if (err.status === 403) {
          this.show = true;
          this.popupOpen((this.popupFor = 'Access denied'));
        }
        this.loading = false; // Loader Disable
      }
    );
  }

  deleteCategory(categoryTobeDeleted: Categories, page) {
    this.subscriptions.push(
      this.commonTemplateService
        .getElementThroughId(
          'TenantMaster/GetSubCategories',
          categoryTobeDeleted.id
        )
        .subscribe(
          res => {
            this.ngZone.run(() => {
              res.forEach(element => {
                this.commonTemplateService.deleteSubcategory(element).subscribe(
                  sub => {
                    this.ngZone.run(() => {
                      this.category.description = element.description;
                      this.onSearchCategory(this.category);
                      this.onSearchSubcategory(element);
                      this.loading = false; // Loader Disable
                    });
                  },
                  err => {
                    // this._toastrService.warning(
                    //   'Access denied',
                    //   '',
                    //   this.constants.toastrConfig
                    // );
                    if (err.status === 403) {
                      this.show = true;
                      this.popupOpen((this.popupFor = 'Access denied'));
                    }
                    // // console.log(err);
                    this.loading = false; // Loader Disable
                  }
                );
              });
            });
          },
          err => {
            // // console.log(err);
            this.loading = false;
          }
        )
    );
    categoryTobeDeleted.isActive = false;
    this.commonTemplateService.deleteCategory(categoryTobeDeleted).subscribe(
      res => {
        this.ngZone.run(() => {
          // // console.log(res);
          // this._toastrService.error(
          //   'Category and its all subcategories are deleted successfully.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.onSearchCategory(categoryTobeDeleted);
          this.subcategories.description = categoryTobeDeleted.description;
          this.onSearchSubcategory(this.subcategories);
          this.getAllCategories(page);
          this.page = null;
          this.GetAllSubcategories(0);
          this.getAllCategoriesForDropdown();
          this.loading = false; // Loader Disable
        });
      },
      err => {
        // // console.log(err);
        this.loading = false; // Loader Disable
      }
    );
  }
  resetforms(form) {
    form.resetForm();
    this.isFormSubmitted = false;
    form = false;
  }
  addSubCategory(form) {
    const arr = [];
    this.subcategoryForm = true;
    if (this.categoryToSave !== undefined) {
      this.formSubCategory.categories = this.categoryToSave;
    }

    if (form.valid) {
      if (this.categoryToSave !== undefined) {
        this.subCategoryPresent = false;
        this.categoryPresnet = false;
        if (this.formSubCategory.categories) {
          this.commonTemplateService
            .postElement(
              'TenantMaster/FindSubCategoryByName',
              this.formSubCategory
            )
            .subscribe(res => {
              this.subCategoryPresent = false;
              this.ngZone.run(() => {
                if (res) {
                  // this._toastrService.warning(
                  //   'Subcategory already present.',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                  // this.subCategoryPresent = true;
                  this.show = true;
                  this.popupOpen((this.popupFor = 'subCategoryPresent'));
                  this.categoryPresnet = false;
                  this.loading = false;
                } else {
                  this.subCategoryPresent = false;
                  this.categoryPresnet = false;
                  this.postSubCategory(form, this.formSubCategory);
                  this.resetforms(form);
                  this.subcategoryForm = false;
                  this.formSubCategory.categories = new Categories();
                  this.GetAllSubcategories(0);
                  this.getAllCategories(0);
                  this.getAllCategoriesForDropdown();
                }
              });
            });
        } else {
          this.categoryPresnet = true;
        }
      } else {
        this.categoryPresnet = true;
      }
    } else {
      this.categoryPresnet = false;
    }
  }

  findSubCategory(searchText) {
    this.formSubCategory.categories = this.categoryToSave;
    this.formSubCategory.description = searchText;
    this.commonTemplateService.findSubCategory(this.formSubCategory).subscribe(
      res => {
        this.ngZone.run(() => {
          this.subcategoryList = res;
        });
      },
      err => {
        // // console.log(err);
      }
    );
  }

  getSubcatForCategory(obj) {
    const isPresentInCategoryList = this.allCategoryForDropdown.filter(
      item => item.id === obj.item.id
    );
    this.categoryToSave = isPresentInCategoryList[0];
    this.subscriptions.push(
      this.commonTemplateService
        .getElementThroughId('TenantMaster/GetSubCategories', obj.item.id)
        .subscribe(
          res => {
            this.ngZone.run(() => {
              this.subcategoryList = res;
            });
          },
          err => { }
        )
    );
  }

  addCategory(categoryForm) {
    const arr = [];
    this.isFormSubmitted = true;
    if (categoryForm.valid) {
      this.commonTemplateService
        .FindCategoryByName(this.formCategory)
        .subscribe(res => {
          this.ngZone.run(() => {
            if (res) {
              // this._toastrService.warning(
              //   'Category already present.',
              //   '',
              //   this.constants.toastrConfig
              // );
              // categoryForm.valid = true;
              // this.mainCategory = true;
              this.show = true;
              this.popupOpen((this.popupFor = 'mainCategory'));
              this.loading = false;
            } else {
              this.mainCategory = false;
              this.postCategory(this.formCategory, categoryForm);
              this.resetforms(categoryForm);
              // this.GetAllSubcategories(0);
              // this.getAllCategories(0);
              // this.getAllCategoriesForDropdown();
            }
          });
        });
    }
  }

  findCategory(searchText) {
    this.formCategory = new Categories();
    this.formCategory.description = searchText;
    this.commonTemplateService.findCategory(this.formCategory).subscribe(
      res => {
        this.ngZone.run(() => {
          this.categoryList = res;
        });
      },
      err => {
        // // console.log(err);
      }
    );
  }

  postCategory(formCategory, categoryForm) {
    const arr = [];
    this.commonTemplateService
      .postElement('TenantMaster/AddCategory', formCategory)
      .subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              arr.push(res);
              // categoryForm.valid = true;
              this.getAllCategories(0);
              this.loading = false;
              this.categoryToSave = res;
              this.formCategory = new Categories();
              this.formSubCategory = new SubCategories();
              this.GetAllSubcategories(0);
              this.getAllCategoriesForDropdown();
              // this._toastrService.success(
              //   'Category added successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.onSearchCategory(formCategory.description);
              this.category.description = res.description;
              this.onSearchCategory(res);
              this.subcategories.categories.id = res.id;
              this.onSearchSubcategory(this.subcategories);
              this.formCategory = new Categories();
              this.formSubCategory.description = null;
              this.formCategory.description = null;
              this.loading = false;
            }
          });
        },
        err => {
          // this._toastrService.warning(
          //   'Access denied',
          //   '',
          //   this.constants.toastrConfig
          // );
          if (err.status === 403) {
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false;
        }
      );
  }

  postSubCategory(form, formSubCategory) {
    this.commonTemplateService
      .postElement('TenantMaster/AddSubCategory', formSubCategory)
      .subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.formCategory = new Categories();
              this.formSubCategory = new SubCategories();
              // this._toastrService.success(
              //   'Subcategory added successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.getAllCategories(0);
              this.GetAllSubcategories(0);
              this.getAllCategoriesForDropdown();
              this.category = res.categories;
              this.onSearchCategory(this.category);
              this.subcategories.categories = this.category;
              this.subscriptions.push(
                this.commonTemplateService
                  .getElementThroughId(
                    'TenantMaster/SubCategories',
                    this.category.id
                  )
                  .subscribe(sub => {
                    this.subcategoryList = sub;
                  })
              );
              this.formCategory.description = null;
              this.formSubCategory = new SubCategories();
              this.loading = false;
              this.resetforms(form);
            }
          });
        },
        err => {
          // this._toastrService.warning(
          //   'Access denied',
          //   '',
          //   this.constants.toastrConfig
          // );
          if (err.status === 403) {
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false;
        }
      );
  }

  updateCategory(uCategory: Categories, nameForm, pageNumber) {
    if (nameForm.valid === true) {
      let isPresent: any;
      this.commonTemplateService.FindCategoryByName(uCategory).subscribe(
        res => {
          this.ngZone.run(() => {
            isPresent = res;
            if (isPresent === null || isPresent === undefined) {
              if (uCategory.description) {
                uCategory.description = uCategory.description;
                this.changeCategory(
                  'TenantMaster/UpdateCategory',
                  uCategory,
                  pageNumber
                );
                this.editcategory = false;
                this.categoryID = 0;
                this.loading = false;
                this.updatedCategory = false;
              } else {
                // this._toastrService.warning(
                //   'Fields should not be empty!',
                //   '',
                //   this.constants.toastrConfig
                // );
                this.editcategory = true;
                this.categoryID = uCategory.id;
              }
            } else {
              if (res.id === uCategory.id) {
                this.editcategory = false;
              } else {
                // this._toastrService.warning(
                //   'Category already present.',
                //   '',
                //   this.constants.toastrConfig
                // );
                //  this.updatedCategory = true;
                this.show = true;
                this.popupOpen((this.popupFor = 'mainCategory'));
                this.loading = false;
              }
            }
          });
        },
        err => {
          // this._toastrService.warning(
          //   'Access denied',
          //   '',
          //   this.constants.toastrConfig
          // );
          if (err.status === 403) {
            this.show = true;
            this.getAllCategories(0);
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          // // console.log(err);
        }
      );
    }
  }

  changeCategory(method, object, pageNumber) {
    this._settingsService.update(method, object).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            // this._toastrService.success(
            //   'Category Updated Successfully.',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.getAllCategories(pageNumber);
          }
        });
      },
      err => {
        // this._toastrService.warning(
        //   'Access denied',
        //   '',
        //   this.constants.toastrConfig
        // );
        if (err.status === 403) {
          this.show = true;
          this.getAllCategories(0);
          this.popupOpen((this.popupFor = 'Access denied'));
        }
        this.loading = false;
      }
    );
  }

  updateSubcategory(uSubcategory: SubCategories, nameForm, pageNumber) {
    this.updateSubCategoryPresent = false;
    if (nameForm.valid === true) {
      let isPresent: any;
      this.commonTemplateService
        .postElement('TenantMaster/FindSubCategoryByName', uSubcategory)
        .subscribe(
          res => {
            this.ngZone.run(() => {
              isPresent = res;
              if (isPresent === null || isPresent === undefined) {
                if (uSubcategory.description) {
                  uSubcategory.description = uSubcategory.description;
                  this.changeSubcategory(
                    'TenantMaster/UpdateSubcategory',
                    uSubcategory,
                    pageNumber
                  );
                  this.editSubcategory = false;
                  this.subcategoryID = 0;

                  this.loading = false;
                } else {
                  // this._toastrService.warning(
                  //   'Fields should not be empty!',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                  this.editSubcategory = true;
                  this.subcategoryID = uSubcategory.id;
                }
              } else {
                if (res.id === uSubcategory.id) {
                  this.editSubcategory = false;
                } else {
                  // this._toastrService.warning(
                  //   'Subcategory already present',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                  //  this.updateSubCategoryPresent = true;
                  this.show = true;
                  this.popupOpen((this.popupFor = 'subCategoryPresent'));
                  this.loading = false;
                }
              }
            });
          },
          err => {
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            if (err.status === 403) {
              this.show = true;
              this.GetAllSubcategories(0);
              this.popupOpen((this.popupFor = 'Access denied'));
            }
            // // console.log(err);
          }
        );
    }
  }

  changeSubcategory(method, object, pageNumber) {
    this._settingsService.update(method, object).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            // this._toastrService.success(
            //   'Subcategory updated successfully.',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.GetAllSubcategories(pageNumber);
          }
        });
      },
      err => {
        // this._toastrService.warning(
        //   'Access denied',
        //   '',
        //   this.constants.toastrConfig
        // );

        if (err.status === 403) {
          this.show = true;
          this.GetAllSubcategories(0);
          this.popupOpen((this.popupFor = 'Access denied'));
        }
        this.loading = false;
      }
    );
  }

  // messages display
  popupOpen(popupFor) {
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.messageToDisplay = null;
    this.typeofMessage = 'Error';
    if (this.popupFor === 'category') {
      this.typeofMessage = 'Error';
      this.notification = 'Are you sure you want to delete this Category?';
      this.noteToDisplay =
        ' Note: Subcategories under this category will also get deleted';
      this.showButtons = true;
    } else if (this.popupFor === 'subcategory') {
      this.typeofMessage = 'Error';
      this.notification = 'Are you sure you want to delete this Subcategory?';
      this.noteToDisplay = null;
      this.showButtons = true;
    } else if (this.popupFor === 'mainCategory') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = ' Category already present';
    } else if (this.popupFor === 'subCategoryPresent') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Subcategory already present';
    } else if (this.popupFor === 'Access denied') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Access denied';
    }
  }
  yes(ConfirmModal) {
    if (this.popupFor === 'category') {
      this.deleteCategory(this.category, this.page);
      ConfirmModal.hide();
    } else if (this.popupFor === 'subcategory') {
      this.deleteSubcategory(this.subcategories, this.page);
      ConfirmModal.hide();
    } else {
      this.show = false;
    }
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
    this.show = false;
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }
}
