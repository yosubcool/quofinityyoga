// import { debounce } from "rxjs/operator/debounce"; #Revisit_IgnoredAsNotUsed
import { ToastrService } from "ngx-toastr";
/*******************************************Confidential*****************************************
 * <copyright file = "manage-user.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component, OnInit, OnDestroy, NgZone, Renderer2 } from "@angular/core";
// import { ModalDirective } from "ngx-bootstrap/modal/modal.component";  #Revisit_IgnoredAsNotUsed
import { User } from "src/app/_shared/model/user-model";
import { UserService } from "src/app/user-account/user-service";
import { Router } from "@angular/router";
import { Constants } from "src/app/_shared/constant";
// import { RegistrationVM } from "src/app/_shared/model/RegistrationVM"; #Revisit_IgnoredAsNotUsed
import { Company } from "src/app/_shared/model/company-model";
import { SettingsService } from "src/app/settings/settings-service";
import { Role } from "../../../_shared/model/role.model";
// import { window } from 'd3';
// import { Window } from 'selenium-webdriver';
import { PaymentInfo } from "../../../_shared/model/payment-model";
import { Pagination } from "../../../_shared/model/pagination-model";
import { Plans } from "../../../_shared/model/plans-model";
import { Subscription } from "rxjs";
declare var $: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: "app-manageUser",
  templateUrl: "./manage-user.component.html",
})
// tslint:disable-next-line:one-line
export class ManageUserComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;
  isChargeable: boolean;
  globalListener: any;
  obj: any;
  userNewRole: any;
  userRoless: any;
  userRole111: any;
  isTrial = false;
  myRoles: any = [];
  userRole: any[];
  userForm: any;
  subscriptions: Subscription[] = [];
  userName: any;
  loading: boolean;
  expairy: any;
  currentPlan: any;
  addUserPopUp: any;
  public myModal;
  public largeModal;
  public smallModal;
  public primaryModal;
  public successModal;
  public warningModal;
  public dangerModal;
  public infoModal;
  pagesCount: number;
  paymentInformation: PaymentInfo[];
  user: User;
  newMask: string = " (000) 000-0000";
  mask: any[] = [
    "+",
    "1",
    " ",
    "(",
    /[1-9]/,
    /\d/,
    /\d/,
    ")",
    " ",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];
  currentUser: User;
  active = true;
  personName: string;
  hidden = true;
  constants: any;
  isUserNameDuplicate: boolean;
  allowAddUser = false;
  company: Company;
  isdiabled: boolean;
  GenralUsers: any;
  isEdit: boolean;
  deleteId: string;
  isEmailRepated = false;
  userRoles = [];
  EmailUserList = [];
  usersList: Pagination;
  SubscriptionType = [];
  isOpenPopup = false;
  constructor(
    private _userService: UserService,
    private renderer: Renderer2,
    private _settingsService: SettingsService,
    private router: Router,
    private ngZone: NgZone
  ) {
    this.isChargeable = false;
    this.user = new User();
    this.user.role = new Role();
    this.isdiabled = false;
    this.constants = Constants;
    this.GenralUsers = new Array<User>();
    this.usersList = new Pagination();
    this.usersList.pageSize = 10;
    this.company = new Company();
    this.EmailUserList = [];
    this.currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    this.SubscriptionType = Array<Plans>();
    this.paymentInformation = new Array<PaymentInfo>();
  }
  ngOnInit() {
    this.getPlans();
    this.getUsersList(0);
    this.GetRoles();
    $(window).scrollTop(0);
    this.getCompany();
    this.GetCurrentPlan();
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
    }, 500);
  }

  getCompany() {
    this.subscriptions.push(
      this._userService.GetCompany().subscribe(
        (res) => {
          this.ngZone.run(() => {
            if (res) {
              this.company = res;
              this.GetGenralUser();
            }
          });
        },
        (err) => { }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  GetCurrentPlan() {
    this.loading = true;
    this.subscriptions.push(
      this._userService.GetCurrentPlan().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.currentPlan = res;
            this.loading = false;
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
  }

  getExpairy() {
    this.loading = true;
    this.subscriptions.push(
      this._userService.GetExpairy().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.loading = false;
            if (res) {
              this.paymentInformation = this.company.payment.filter(
                (res) => res.isActive === true
              );
              // this.paymentInformation=this.company.payment.filter(res=> {
              //   if(res.isActive === true) {
              //     return res.noOfUsers;
              //   }
              // });
              this.expairy = res;
              const length = this.GenralUsers.filter((_) => _.isActive).length;
              if (res.subscription === this.constants.SUBSCRIPTION_TRIAL) {
                this.isTrial = true;
                const a = this.company.payment.filter(
                  (entry: PaymentInfo) => entry.isActive === true
                )[0].noOfUsers;
                // if (length >= this.company.noOfUsers){ //-- user limit changed to 3 hardcode value -- datta
                if (length < this.paymentInformation[0].noOfUsers) {
                  if (res.expairyDays === 0) {
                    this.allowAddUser = false;
                  } else {
                    this.allowAddUser = true;
                  }
                } else {
                  this.allowAddUser = false;
                }
              } else {
                this.isTrial = false;
                // if (length >= this.company.noOfUsers){ //-- user limit changed to 3 hardcode value -- datta
                if (length >= this.paymentInformation[0].noOfUsers) {
                  this.isChargeable = true;
                } else {
                  this.isChargeable = false;
                }
                // if (length >= this.company.noOfUsers){ //-- user limit changed to 3 hardcode value -- datta
                if (length < this.paymentInformation[0].noOfUsers) {
                  if (res.expairyDays === 0) {
                    this.allowAddUser = false;
                  } else {
                    this.allowAddUser = true;
                  }
                } else {
                  if (res.expairyDays === 0) {
                    this.allowAddUser = false;
                  } else {
                    this.allowAddUser = true;
                  }
                }
              }
            }
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
  }

  alertToUser() {
    if (this.isTrial) {
      // this.toastrService.error(
      //   'You can not add more users in  trial period.',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.show = true;
      this.popupOpen((this.popupFor = "trial period"));
      this.loading = false;
    } else {
      // this.toastrService.error(
      //   'You can not add more uers. Please check plans.',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.show = true;
      this.popupOpen((this.popupFor = "You can not add more uers"));
      this.loading = false;
    }
  }
  addUserName(Id) {
    // this.userName = this.user.filter(
    //   item => item.name === <string>(<any>Id.target.value)
    // );
  }
  GetGenralUser() {
    this.loading = true;
    this.subscriptions.push(
      this._userService.GetGenralUser(this.company.id).subscribe(
        (res1) => {
          this.ngZone.run(() => {
            this.GenralUsers = res1;

            this.getExpairy();
            this.loading = false;
          });
          this.getUsersList(0);
        },
        (err) => {
          this.loading = false;
        }
      )
    );
  }
  pageChanged(no) {
    this.getUsersList(no);
  }

  getUsersList(pageNo) {
    this.loading = true;
    this.usersList.pageNumber = pageNo;
    this.usersList.companyId = this.company.id;
    this.subscriptions.push(
      this._settingsService.getAllUsers(this.usersList).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.usersList = res;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
          });
          this.loading = false;
        },
        (err) => {
          // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
          this.loading = false;
        }
      )
    );
  }

  GetRoles() {
    this.subscriptions.push(
      this._settingsService.getRoles().subscribe(
        (response) => {
          this.ngZone.run(() => {
            this.userRoles = response;
            this.userRoles.splice(0, 1);
            this.myRoles.push(response);
          });
        },
        (err) => { }
      )
    );
  }

  onEdit() {
    this.active = false;
  }

  getUserRole(event) {
    const selectedValue = event.target.value;

    this.userRoles.forEach((ele) => {
      if (ele.id === selectedValue) {
        // console.log('Value found');
        this.obj = ele;
      } else {
        // console.log('Value not found');
      }
    });
  }

  onSubmit(isValid: boolean, model, userForm) {
    if (isValid) {
      this.loading = true;
      this._settingsService.findEmail(this.user.email).subscribe(
        (mail) => {
          this.ngZone.run(() => {
            this.paymentInformation = this.company.payment.filter(function (
              res
            ) {
              if (res.isActive === true) {
                return res.noOfUsers;
              }
            });
            this.loading = false;
            if (mail) {
              this.isEmailRepated = true;
            } else {
              // this.getUsersList(0);
              model.hide();
              this.isdiabled = true;
              this.user.subscriberId = this.company.id;
              this.user.isActive = true;
              this.user.createdBy = this.currentUser.id;
              this.user.createdOn = new Date();
              this.user.role = this.obj;
              const length = this.GenralUsers.filter((_) => _.isActive).length;
              // if (length >= this.company.noOfUsers){ //-- user limit changed to 3 hardcode value -- datta
              if (length >= this.paymentInformation[0].noOfUsers) {
                if (
                  this.currentPlan.subscription.subscriptionType ===
                  "SUBSCRIPTION_ANNUAL"
                ) {
                  this.addPayment(
                    this.SubscriptionType[3].cost,
                    null,
                    model,
                    userForm
                  );
                } else if (
                  this.currentPlan.subscription.subscriptionType ===
                  "SUBSCRIPTION_MONTHLY"
                ) {
                  this.addPayment(
                    this.SubscriptionType[2].cost,
                    null,
                    model,
                    userForm
                  );
                }
                return;
              }
              this.AddUser(model, userForm);
            }
          });
        },
        (err) => {
          this.loading = false;
          // console.log(err);
        }
      );
    }
  }
  AddUser(model, userForm) {
    this.loading = true;
    this._userService.AddUser(this.user).subscribe(
      (res) => {
        if (res) {
          model.hide();
          this.ngZone.run(() => {
            this.getUsersList(0);
            this.loading = false;
            this.isEmailRepated = false;
            this.resetforms(userForm);
            this.getCompany();
            this.GetGenralUser();
          });
        } else {
          // this.isEmailRepated = true;
          this.loading = false;
        }
      },
      (err) => {
        this.isdiabled = false;
        this.loading = false;
      }
    );
  }
  resetforms(form) {
    //  this.isFormSubmitted = false;
    form.resetForm();
  }

  editUser(modal, usr, confirmModal) {
    this._settingsService.getLoginDetailsofUser(usr.id).subscribe((details) => {
      if (!details) {
        this.isEdit = true;
        this.isEmailRepated = false;
        modal.show();
        this.user = JSON.parse(JSON.stringify(usr));
      } else if (!details.isLoggedIn) {
        this.isEdit = true;
        this.isEmailRepated = false;
        modal.show();
        this.user = JSON.parse(JSON.stringify(usr));
      } else {
        this.popupOpen((this.popupFor = "User is login"));
        confirmModal.show();
      }
    });
  }

  activeDeactiveUser(popup, usr, ConfirmModal) {
    // this._settingsService
    //   .getLoginDetailsofUser(usr.id)
    //   .subscribe(details => {

    //     if (!details) { // IF user is new
    this.popupOpen((this.popupFor = popup));
    ConfirmModal.show();
    // } else if (!details.isLoggedIn) { // If user is not login
    //       this.popupOpen(this.popupFor = popup);
    //       ConfirmModal.show();
    //   } else {
    //     this.popupOpen(this.popupFor = 'User is login');
    //     ConfirmModal.show();
    //   }
    // });
  }

  updateUser(isValid: boolean, model, pageNo) {
    this._settingsService.findEmail(this.user.email).subscribe(
      (mail) => {
        this.ngZone.run(() => {
          if (mail && mail.id !== this.user.id) {
            this.isEmailRepated = true;
          } else {
            // this.isFormSubmitted = false;

            if (isValid && !this.isEmailRepated) {
              this.loading = true;
              model.hide();
              this.isdiabled = true;
              this.user.updatedOn = new Date();
              if (this.obj) {
                this.user.role = this.obj;
              }
              this._userService.UpdateUsers(this.user).subscribe(
                (res) => {
                  this.ngZone.run(() => {
                    this.loading = false;
                    this.GetGenralUser();
                    this.isdiabled = true;
                    this.loading = false;
                    this.getUsersList(pageNo);
                  });
                },
                (err) => {
                  this.isdiabled = false;
                  this.loading = false;
                }
              );
            }
          }
        });
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  DeleteUser(id: string) {
    this.loading = true;
    this._userService.DeleteUser(id).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.loading = false;
          this.GetGenralUser();
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  ActivateUser(id: string) {
    this.loading = true;
    this.paymentInformation = this.company.payment.filter(function (res) {
      if (res.isActive === true) {
        return res.noOfUsers;
      }
    });
    const length = this.GenralUsers.filter((_) => _.isActive).length;
    if (length >= this.paymentInformation[0].noOfUsers) {
      if (
        this.currentPlan.subscription.subscriptionType === "SUBSCRIPTION_ANNUAL"
      ) {
        this.addPayment(this.SubscriptionType[3].cost, id, null, null);
      } else if (
        this.currentPlan.subscription.subscriptionType ===
        "SUBSCRIPTION_MONTHLY"
      ) {
        this.addPayment(this.SubscriptionType[2].cost, id, null, null);
      }
      return;
    }
    this.ActiveUser(id);
  }

  ActiveUser(id) {
    this.loading = true;
    this._userService.ActivateUser(id).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.loading = false;
          if (res) {
            this.GetGenralUser();
            this.getCompany();
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  onModelClose() {
    //  this.isFormSubmitted = false;
    this.isEmailRepated = false;
    this.user = new User();
    this.user.role = new Role();
    this.user.role.id = null;
  }
  onModelopen(flag: boolean) {
    this.isEdit = flag;
    if (!flag) {
      this.user = new User();
      this.user.role = new Role();
    }
  }

  onEmailBlur() {
    if (!this.isEdit) {
      if (this.user.email) {
        this.userName = this.user.email;
      }
    }
  }

  findSupplier(searchText) {
    this.EmailUserList = [];
    this._settingsService.findEmail(searchText).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.EmailUserList = res;
            // this.isEmailRepated = true;
          }
        });
      },
      (err) => {
        // console.log(err);
      }
    );
  }

  addPayment(amount, id, model, userForm) {
    this.loading = true;
    const payment: PaymentInfo = new PaymentInfo();
    // tslint:disable-next-line:curly
    if (
      this.currentPlan.subscription.subscriptionType === "SUBSCRIPTION_ANNUAL"
    )
      payment.subscriptionType = "ANNUAL_TOPUP";
    // tslint:disable-next-line:curly
    else payment.subscriptionType = "MONTHLY_TOPUP";
    payment.isActive = true;
    payment.subscriptionId = this.currentPlan.subscription.subscriptionId;
    payment.planCost = amount;
    payment.subscriptionStartDate = new Date();
    payment.subscriptionStartDate = new Date(
      payment.subscriptionStartDate.getUTCFullYear(),
      payment.subscriptionStartDate.getUTCMonth(),
      payment.subscriptionStartDate.getUTCDate(),
      payment.subscriptionStartDate.getUTCHours(),
      payment.subscriptionStartDate.getUTCMinutes(),
      payment.subscriptionStartDate.getUTCSeconds()
    );
    payment.subscriptionEndDate =
      this.currentPlan.subscription.subscriptionEndDate;
    payment.planName = "EXTRA_USER_TOPUP";
    payment.totalCost = amount;
    this._userService.AddPayment(payment).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.loading = false;
          if (res) {
            if (id === null) {
              this.AddUser(model, userForm);
            } else {
              this.ActiveUser(id);
            }
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }
  getPlans() {
    this._userService.GetPlans().subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.SubscriptionType = res;
          }
        });
      },
      (err) => { }
    );
  }

  // messages display
  popupOpen(popupFor) {
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.messageToDisplay = null;
    this.typeofMessage = "Error";
    if (this.popupFor === "User is login") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification =
        "You can not perform this action.As user is logged in.";
    } else if (this.popupFor === "deactiveUser") {
      this.typeofMessage = "Error";
      this.notification = "Are you sure, you want to deactivate this user?";
      this.noteToDisplay = null;
      this.showButtons = true;
    } else if (this.popupFor === "activeUser") {
      this.typeofMessage = "Hello";
      this.messageToDisplay = "Are you sure, you want to activate this user?";
      if (this.isChargeable) {
        const usr = this.GenralUsers.filter((_) => _.isActive).length;
        if (
          this.currentPlan.subscription.subscriptionType ===
          "SUBSCRIPTION_ANNUAL"
        ) {
          this.messageToDisplay =
            "You presently have " +
            usr +
            " users. Activating a new user will increase your annual payment by $" +
            this.SubscriptionType[3].cost * 12 +
            ", would you like to proceed?";
        } else if (
          this.currentPlan.subscription.subscriptionType ===
          "SUBSCRIPTION_MONTHLY"
        ) {
          this.messageToDisplay =
            "You presently have " +
            usr +
            " users. Activating a new user will increase your monthly payment by $" +
            this.SubscriptionType[2].cost +
            ", would you like to proceed?";
        }
      }
      this.notification = null;
      this.noteToDisplay = null;
      this.showButtons = true;
    } else if (this.popupFor === "You can not add more uers") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "You can not add more uers. Please check plans";
    } else if (this.popupFor === "trial period") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "You can not add more users in  trial period";
    } else if (this.popupFor === "Access denied") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Access denied";
    } else if (this.popupFor === "Charge") {
      this.typeofMessage = "Chargeable User";
      // this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      const usr = this.GenralUsers.filter((_) => _.isActive).length;
      if (
        this.currentPlan.subscription.subscriptionType === "SUBSCRIPTION_ANNUAL"
      ) {
        this.messageToDisplay =
          "You presently have " +
          usr +
          " users. Adding a new user will increase your annual payment by $" +
          this.SubscriptionType[3].cost * 12 +
          ", would you like to proceed?";
      } else if (
        this.currentPlan.subscription.subscriptionType ===
        "SUBSCRIPTION_MONTHLY"
      ) {
        this.messageToDisplay =
          "You presently have " +
          usr +
          " users. Adding a new user will increase your monthly payment by $" +
          this.SubscriptionType[2].cost +
          ", would you like to proceed?";
      }
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === "deactiveUser") {
      this.DeleteUser(this.user.id);
      ConfirmModal.hide();
    } else if (this.popupFor === "activeUser") {
      this.ActivateUser(this.user.id);
      ConfirmModal.hide();
    } else if (this.popupFor === "You can not add more uers") {
      this.show = false;
      ConfirmModal.hide();
    } else if (this.popupFor === "Access denied") {
      this.show = false;
      ConfirmModal.hide();
    } else if (this.popupFor === "trial period") {
      this.show = false;
      ConfirmModal.hide();
    } else if (this.popupFor === "Charge") {
      this.show = false;
      ConfirmModal.hide();
      this.addUserPopUp.show();
    }
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }

  AddUserPopup(poupBox) {
    this.onModelopen(false);
    if (this.isChargeable) {
      this.addUserPopUp = poupBox;
      this.show = true;
      this.popupFor = "Charge";
      this.popupOpen("Charge");
    } else {
      poupBox.show();
    }
  }
}
