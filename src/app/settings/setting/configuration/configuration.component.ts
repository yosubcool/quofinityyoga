import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { SettingsService } from 'src/app/settings/settings-service';
import { Constants } from 'src/app/_shared/constant';
import { Plans } from 'src/app/_shared/model/plans-model';
import { Configuration } from 'src/app/_shared/model/Configuration-model';
import { Subscription } from 'rxjs';
declare var $: any;
@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit, OnDestroy {
  public isTPClicked: boolean;
  public isRQClicked: boolean;
  public isTLlicked: boolean;
  public isSBlicked: boolean;

  configuration: Configuration;
  formConfiguration: Configuration;
  plan: Plans;
  subscriptions: Subscription[] = [];
  constants: any;
  isTrialFormSubmitted = false;
  isQuoteFormSubmitted = false;
  isTagFormSubmitted = false;
  isSubscriptionFormSubmitted = false;
  public loading = false;
  constructor(
    private _settingsService: SettingsService,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService
  ) {
    this.configuration = new Configuration();
    this.formConfiguration = new Configuration();
    this.plan = new Plans();
    this.constants = Constants;
  }

  ngOnInit() {
    $(window).scrollTop(0);
    this.getConfiguration();
  }

  getConfiguration() {
    this.subscriptions.push(
      this._settingsService.getConfiguration().subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.configuration = res;
            } else {
              this.configuration = new Configuration();
            }
          });
        },
        err => { }
      ));
  }
  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }
  addTrial(trialForm) {
    this.configuration.trialPeriod = this.formConfiguration.trialPeriod;
    this.isTrialFormSubmitted = true;
    if (trialForm.valid) {
      this.changeConfig(this.configuration);
      this.isTrialFormSubmitted = false;
      this.resetforms(trialForm);
      // this.toastrService.success(
      //   'Trial Period Added Successfully.',
      //   '',
      //   this.constants.toastrConfig
      // );
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  addRecentQuote(trialForm) {
    this.configuration.recentQuote = this.formConfiguration.recentQuote;
    this.isQuoteFormSubmitted = true;
    if (trialForm.valid) {
      this.changeConfig(this.configuration);
      this.isQuoteFormSubmitted = false;
      this.resetforms(trialForm);
      // this.toastrService.success(
      //   'Quote Limit Added Successfully.',
      //   '',
      //   this.constants.toastrConfig
      // );
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  addRecentTagLimit(trialForm) {
    this.configuration.tagLimit = this.formConfiguration.tagLimit;
    this.isTagFormSubmitted = true;
    if (trialForm.valid) {
      this.changeConfig(this.configuration);
      this.isTagFormSubmitted = false;
      this.resetforms(trialForm);
      // this.toastrService.success(
      //   'Tag Limit Added Successfully.',
      //   '',
      //   this.constants.toastrConfig
      // );
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  addSubsciption(trialForm) {
    if (this.configuration.subscriptionPlan) {
      this.configuration.subscriptionPlan.push(this.plan);
    } else {
      const arr = [];
      arr.push(this.plan);
      this.configuration.subscriptionPlan = arr;
    }
    this.isSubscriptionFormSubmitted = true;
    if (trialForm.valid) {
      this.changeConfig(this.configuration);
      this.isSubscriptionFormSubmitted = false;
      this.resetforms(trialForm);
      // this.toastrService.success(
      //   'Subscription Plan Added Successfully.',
      //   '',
      //   this.constants.toastrConfig
      // );
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  changeConfig(configuration) {
    this._settingsService.postElement('Configuration', configuration).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            this.isTrialFormSubmitted = false;
            this.isQuoteFormSubmitted = false;
            this.isTagFormSubmitted = false;
            this.isSubscriptionFormSubmitted = false;
          }
        });
      },
      err => {
        // this.toastrService.error(
        //   'Please provide valid details.',
        //   '',
        //   this.constants.toastrConfig
        // );
        this.loading = false;
      }
    );
  }

  resetforms(form) {
    form.resetForm();
    this.isTrialFormSubmitted = false;
    this.isQuoteFormSubmitted = false;
    this.isSubscriptionFormSubmitted = false;
    this.isTagFormSubmitted = false;
  }
}
