/*******************************************Confidential*****************************************
 * <copyright file = "settings-routing.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteGaurd } from 'src/app/_core/RouteGaurd';
import { ProfileComponent } from 'src/app/settings/profile/profile.component';
import { ChangePassword } from 'src/app/settings/changePassword/change-password.component';
import { SettingComponent } from 'src/app/settings/setting/setting.component';
import { MyPlansComponent } from './my-plans/my-plans.component';
// import { RoleGaurd } from 'src/app/_core/RoleGuard'; //#Revisit_IgnoredAsNotUsed

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      // title: 'Setting'
      isLogin: true
    },
    children: [
      {
        path: 'setting',
        component: SettingComponent,
        data: {
          isLogin: true,
          title: 'Setting'
        }
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [RouteGaurd],
        data: {
          isLogin: true,
          title: 'My Profile'
        }
      },
      {
        path: 'change-password',
        component: ChangePassword,
        canActivate: [RouteGaurd],
        data: {
          title: 'Change Password',
          isLogin: true,
        }
      },
      {
        path: 'my-plans',
        component: MyPlansComponent,
        canActivate: [RouteGaurd],
        data: {
          title: 'My Subscription Plan',
          isLogin: true,
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
