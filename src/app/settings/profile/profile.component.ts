/*******************************************Confidential*****************************************
 * <copyright file = "profile.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component, OnInit, OnDestroy, NgZone, ViewChild } from "@angular/core";
import {
  Router,
} from "@angular/router";
import { ModalDirective } from "ngx-bootstrap/modal";
import { Subscription } from "rxjs";
import { UserService } from "src/app/user-account/user-service";
import { Constants } from "src/app/_shared/constant";
import { User } from "src/app/_shared/model/user-model";
declare var $: any;
@Component({
  selector: "app-settings",
  templateUrl: "./profile.component.html",
})
export class ProfileComponent implements OnInit, OnDestroy {
  @ViewChild("ConfirmModal")
  confirmModal: ModalDirective;
  isEmail = false;
  public loading = false;
  isNotImage = false;
  imgUploaded = false;
  isEdit = false;
  subscriptions: Subscription[] = [];
  constants: Constants;
  isImageSizeLarge = false;
  user: User;
  show: boolean = false;
  showButton = false;
  OrignalUser: User;
  newMask: string = " (000) 000-0000";
  mask: any[] = [
    "+",
    "1",
    " ",
    "(",
    /[1-9]/,
    /\d/,
    /\d/,
    ")",
    " ",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/,
  ];
  Taxjar: any = {};
  TaxData: any = {};
  constructor(
    private _UserService: UserService,
    private ngZone: NgZone,
    private router: Router
  ) {
    this.constants = new Constants();
    this.subscriptions.push(
      this._UserService.GetUser().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.user = JSON.parse(JSON.stringify(res));
            this.OrignalUser = JSON.parse(JSON.stringify(res));
          });
        },
        (err) => { }
      )
    );
  }

  ngOnInit() {
    $(window).scrollTop(0);
  }
  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  uploadDoc(evt) {
    const files = evt.target.files;
    const file = files[0];
    if (files && file) {
      const reader = new FileReader();
      reader.onload = this._handleReaderLoadeds.bind(this, file);
      reader.readAsArrayBuffer(file);
    }
  }
  _handleReaderLoadeds(file, readerEvt) {
    this.isNotImage = false;
    this.isImageSizeLarge = false;
    if (file.type === "image/png" || file.type === "image/jpeg") {
      if (file.size < 1051341) {
        const buffer = readerEvt.target.result;
        file.Base64 = this.arrayBufferToBase64(buffer);
        this.isEdit = true;
        this.imgUploaded = true;
        setTimeout(() => {
          this.user.profilePicture =
            "data:" + file.type + ";base64," + file.Base64;
        }, 1000);
      } else {
        this.isImageSizeLarge = true;
      }
    } else {
      this.imgUploaded = false;
      this.isNotImage = true;
      return;
    }
  }
  arrayBufferToBase64(buffer) {
    let binary = "";
    const bytes = new Uint8Array(buffer);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  }

  onEdit() {
    this.isEdit = true;
  }
  Ok(ConfirmModal) {
    ConfirmModal.hide();
    this.loading = true;
    this.subscriptions.push(
      this._UserService.LogoutUser(this.user.id).subscribe(
        (res) => {
          this.ngZone.run(() => {
            if (res !== null) {
              this.loading = false;
              sessionStorage.clear();
              this.router.navigate(["pages/app-home"]);
            }
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
    $(".sidebar-fixed").removeClass("sidebar-hidden");
  }

  onSubmit(isvalid) {
    if (isvalid) {
      this.isImageSizeLarge = false;
      this.loading = true;
      this.user.updatedOn = new Date();
      this._UserService.UpdateUser(this.user).subscribe(
        (res) => {
          this.isEmail = false;
          this.loading = false;
          if (res) {
            this.isEdit = false;
            if (this.OrignalUser.email !== this.user.email) {
              this.confirmModal.show();
              // this.LogOut();
              // this.router.navigate(["pages/app-home"]);
              // sessionStorage.clear();
            } else {
              this.OrignalUser = JSON.parse(JSON.stringify(this.user));
              sessionStorage.setItem("currentUser", JSON.stringify(this.user));
              this._UserService.updateInitials();
              this.ngZone.run(() => { });
            }
          } else {
            // this._toastrService.warning(
            //   'Email already exists!',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.isEmail = true;
          }
        },
        (err) => {
          this.loading = false;
        }
      );
    }
  }
  onDiscard() {
    this.user.profilePicture = this.OrignalUser.profilePicture;
    this.loading = false;
    this.isEdit = false;
    this.user = JSON.parse(JSON.stringify(this.OrignalUser));
  }

  // SubmitTaxJar() {
  //   const Taxjar = require('taxjar');
  //   const client = new Taxjar({
  //     apiKey: 'a5ce9cdbc974893b84d8b24e24f7044f'
  //   });
  //   client
  //     .taxForOrder({
  //       from_country: 'US',
  //       from_zip: this.Taxjar.from_zip,
  //       from_state: this.Taxjar.from_state,
  //       to_country: 'US',
  //       to_zip: this.Taxjar.to_zip,
  //       to_state: this.Taxjar.to_state,
  //       amount: this.Taxjar.amount,
  //       shipping: 0,
  //       line_items: [
  //         {
  //           quantity: 1,
  //           unit_price: this.Taxjar.amount,
  //           product_tax_code: 30070
  //         }
  //       ]
  //     })
  //     .then(value => {
  //       this.TaxData = value.tax;
  //     });
  // }
}
