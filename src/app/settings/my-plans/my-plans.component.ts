import { Role } from "src/app/_shared/model/role.model";
import { PaymentInfo } from "./../../_shared/model/payment-model";
import {
  Component,
  OnInit,
  Renderer2,
  NgZone,
  AfterViewInit,
  AfterViewChecked,
} from "@angular/core";
import { Company } from "src/app/_shared/model/company-model";
import { UserService } from "src/app/user-account/user-service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Constants } from "../../_shared/constant";
import { Plans } from "../../_shared/model/plans-model";
declare var $: any;
import { ViewChild, ElementRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
// import {
//   StripeService,
//   Elements,
//   Element as StripeElement,
//   ElementsOptions
// } from 'ngx-stripe';
@Component({
  selector: "app-my-plans",
  templateUrl: "./my-plans.component.html",
  styleUrls: ["./my-plans.component.scss"],
})
export class MyPlansComponent implements OnInit, AfterViewChecked {
  globalListener: any;
  company: Company;
  payment: PaymentInfo;
  paymentInfo: any;
  SubscriptionType: any;
  userCount: number;
  extraUserCount: number;
  companyId: any;
  extraAmount: any;
  constants: any;
  subscriptionResult: any;
  currentPlan: PaymentInfo;
  expiryDays: number;
  currentRole: Role;
  expairyDays: number;
  loading: boolean;

  constructor(
    private ngZone: NgZone,
    private router: Router,
    private _userService: UserService
  ) {
    this.constants = Constants;
    this.company = new Company();
    this.payment = new PaymentInfo();
    this.SubscriptionType = Array<Plans>();
    this.loading = false;
  }

  ngOnInit() {
    $(window).scrollTop(0);
    this.getPlans();
    this.getCompany();
    this.getCurrentPlan();
    this.getCurrentRole();
  }

  ngAfterViewChecked() {
    window["Chargebee"].registerAgain();
    const chargebee = window["Chargebee"].getInstance();
    chargebee.setCheckoutCallbacks((n) => {
      const prod = n.products[0];
      if (this.extraUserCount > 0) {
        if (prod.planId === this.constants.plan_annual) {
          prod.addons.push({
            id: "additional-annual-user",
            quantity: this.extraUserCount,
          });
        } else if (prod.planId === this.constants.plan_monthly) {
          prod.addons.push({
            id: "additional-monthly-user",
            quantity: this.extraUserCount,
          });
        }
      }
      // // console.log(prod.planId);
      // // console.log(prod.addons);
      return {
        loaded: () => {
          // console.log('checkout opened');
        },
        close: () => {
          // console.log('checkout closed');
        },
        success: (res) => {

          // // console.log(res);
          if (prod.planId === this.constants.plan_annual) {
            this.addPayment(
              res,
              this.SubscriptionType[1].subscriptionType,
              this.SubscriptionType[1].planName,
              this.SubscriptionType[1].cost,
              this.SubscriptionType[1].userLimit
            );
          } else if (prod.planId === this.constants.plan_monthly) {
            this.addPayment(
              res,
              this.SubscriptionType[0].subscriptionType,
              this.SubscriptionType[0].planName,
              this.SubscriptionType[0].cost,
              this.SubscriptionType[0].userLimit
            );
          }
        },
        step: (res) => {
          // console.log(res);
        },
      };
    });
  }

  getCompany() {
    this._userService.GetCompany().subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            // this.userCount = res.noOfUsers;
            this.paymentInfo = res.payment;
            this.getActiveUsers(res.id);
          }
        });
      },
      (err) => { }
    );
  }

  getActiveUsers(id) {
    this._userService.getActiveUsers(id).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.userCount = res;
            this.extraUserCount = this.userCount - 3;
          }
        });
      },
      (err) => { }
    );
  }

  getCurrentPlan() {
    this._userService.GetCurrentPlan().subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.currentPlan = res.subscription;
            this.expairyDays = res.expairyDays;
          }
        });
      },
      (err) => { }
    );
  }
  getCurrentRole() {
    this._userService.GetCurrentRole().subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.currentRole = res;
            // // console.log(this.currentRole);
          }
        });
      },
      (err) => { }
    );
  }

  CancelSubscription() {
    this._userService.CancelSubscription().subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.getCurrentPlan();
          }
        });
        this.loading = false;
      },
      (err) => { }
    );
  }

  addPayment(token, description, name, amount, userLimit) {
    this.loading = true;
    this.payment.subscriptionType = description;
    this.payment.isActive = true;
    this.payment.planCost = amount;
    this.payment.transcationId = token;
    this.payment.noOfUsers = userLimit;
    this.payment.subscriptionStartDate = new Date();
    this.payment.subscriptionStartDate = new Date(
      this.payment.subscriptionStartDate.getUTCFullYear(),
      this.payment.subscriptionStartDate.getUTCMonth(),
      this.payment.subscriptionStartDate.getUTCDate(),
      this.payment.subscriptionStartDate.getUTCHours(),
      this.payment.subscriptionStartDate.getUTCMinutes(),
      this.payment.subscriptionStartDate.getUTCSeconds()
    );
    this.payment.isRenewed = true;
    if (description === "SUBSCRIPTION_ANNUAL") {
      const tempDate = new Date();
      tempDate.setFullYear(
        this.payment.subscriptionStartDate.getFullYear() + 1
      );
      this.payment.subscriptionEndDate = tempDate;
    }
    if (description === "SUBSCRIPTION_MONTHLY") {
      const tempDate = new Date();
      tempDate.setMonth(this.payment.subscriptionStartDate.getMonth() + 1);
      this.payment.subscriptionEndDate = tempDate;
    }
    this.payment.planName = name;
    this.payment.totalCost = amount;
    this.payment.subscriptionEndDate = new Date(
      this.payment.subscriptionEndDate.getUTCFullYear(),
      this.payment.subscriptionEndDate.getUTCMonth(),
      this.payment.subscriptionEndDate.getUTCDate(),
      this.payment.subscriptionEndDate.getUTCHours(),
      this.payment.subscriptionEndDate.getUTCMinutes(),
      this.payment.subscriptionEndDate.getUTCSeconds()
    );
    this._userService.AddPayment(this.payment).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.loading = false;
            this.company = res.company;
            this.company.noOfUsers = this.userCount;
            this.UpdateCompany(this.company);
            this.router.navigate(["dashboard"]);
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  getPlans() {
    this._userService.GetPlans().subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.SubscriptionType = res;
            this.loading = false;
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  GetTotalCost() {
    let cost = 0;
    if (this.currentPlan.subscriptionType === "SUBSCRIPTION_ANNUAL") {
      cost = this.SubscriptionType[1].cost * 12;
      if (this.userCount > this.SubscriptionType[1].userLimit) {
        cost =
          cost +
          (this.userCount - this.SubscriptionType[1].userLimit) *
          this.SubscriptionType[3].cost *
          12;
      }
    } else if (this.currentPlan.subscriptionType === "SUBSCRIPTION_MONTHLY") {
      cost = this.SubscriptionType[0].cost;
      if (this.userCount > this.SubscriptionType[0].userLimit) {
        cost =
          cost +
          (this.userCount - this.SubscriptionType[0].userLimit) *
          this.SubscriptionType[2].cost;
      }
    }
    return cost;
  }
  UpdateCompany(company) {

    company.updatedOn = new Date();
    this._userService.UpdateCompany(company).subscribe(
      (res) => {
        this.ngZone.run(() => { });
      },
      (err) => { }
    );
    this.loading = false;
  }
}
