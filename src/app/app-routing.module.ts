import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesModule } from './home/pages.module';
import { AdminLayoutComponent } from './layouts/admin-layout.component';
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { SingleLayoutComponent } from './layouts/single-layout.component';
import { QuoteReportComponent } from './quotes/quote-report/quote-report.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "pages/app-home",
    pathMatch: "full",
  },
  {
    path: "",
    component: FullLayoutComponent,
    data: {
      // title: 'Home'
    },
    children: [
      {
        path: "dashboard",
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
        data: {
          title: "Dashboard",
        },
      },
      {
        path: "settings",
        loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule),
      },
      {
        path: "admin",
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
      },
      {
        path: "accounts",
        loadChildren: () => import('./accounts/accounts.module').then(m => m.AccountsModule),
      },
      {
        path: "contacts",
        loadChildren: () => import('./contacts/contacts.module').then(m => m.ContactsModule),
      },
      {
        path: "products",
        loadChildren: () => import('./products/products.module').then(m => m.ProductsModule),
      },
      {
        path: "kits",
        loadChildren: () => import('./kits/kits.module').then(m => m.KitsModule),
      },
      {
        path: "quotes",
        loadChildren: () => import('./quotes/quotes.module').then(m => m.QuotesModule),
      },
      {
        path: "browse",
        loadChildren: () => import('./browse/browse.module').then(m => m.BrowseModule),
      },
      {
        path: "reports",
        loadChildren: () => import('./report/report.module').then(m => m.ReportModule),
        data: {
          title: "Report",
        },
      },
    ],
  },
  {
    path: "",
    component: SimpleLayoutComponent,
    data: {
      title: "",
    },
    children: [
      {
        path: "pages",
        loadChildren: () => import('./home/pages.module').then(m => m.PagesModule),
      },
      {
        path: "account",
        loadChildren: () => import('./user-account/user-account.module').then(m => m.UserAccountModule),
      }, //
    ],
  },
  {
    path: "",
    component: SingleLayoutComponent,
    data: {
      title: "",
    },
    children: [
      {
        path: "confirmation",
        loadChildren: () => import('./confirmation/confirmation.module').then(m => m.ConfirmationModule),
      },
      {
        path: "quotes-report/:id/:companyId",
        component: QuoteReportComponent,
        data: {
          title: "Quotes Report",
        },
      },
    ],
  },
  {
    path: "",
    component: AdminLayoutComponent,
    data: {
      title: "",
    },
    children: [
      {
        path: "superadmin",
        loadChildren: () => import('./superadmin/superadmin.module').then(m => m.SuperadminModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
