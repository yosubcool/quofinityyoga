import { Component, OnDestroy, OnInit, NgZone } from '@angular/core';
import { Plans } from '../../_shared/model/plans-model';
import { Constants } from '../../_shared/constant';
import { Router } from '@angular/router';
import { SuperadminService } from '../superadmin-service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Configuration } from 'src/app/_shared/model/Configuration-model';
declare var $: any;
@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class SuperAdminConfigurationComponent implements OnInit, OnDestroy {
  formDisclaimer: string;
  isTPClicked: boolean;
  isRQClicked: boolean;
  isTLlicked: boolean;
  isSBlicked: boolean;
  isDiscClicked: boolean;
  isExtendClicked: boolean;
  istermsAndConditionsClicked: boolean;
  subscriptions: Subscription[] = [];
  plan: Plans;
  isCostValid = false;
  isUserLimitValid = false;
  isTimeSpanValid = false;
  isDaysValid = false;
  isNumberValid = false;
  isTrialValid = false;
  isRQuoteValid = false;
  isTagValid = false;
  configuration: Configuration;
  formConfiguration: Configuration;
  constants: any;
  isTrialFormSubmitted = false;
  isQuoteFormSubmitted = false;
  isExtendFormSubmitted = false;
  isTagFormSubmitted = false;
  isDisclaimerFormSubmitted = false;
  planName: string;
  isPlanEditDisabled = true;
  isSubscriptionFormSubmitted = false;
  public loading = false;
  Companies: any = [];
  Extend: Configuration;
  subscriptionPlans = [];
  public editorOptions = {
    placeholder: 'insert content...'
  };
  constructor(
    private superadminService: SuperadminService,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService
  ) {
    this.configuration = new Configuration();
    this.formConfiguration = new Configuration();
    this.plan = new Plans();
    this.constants = Constants;
    this.Extend = new Configuration();
  }

  ngOnInit() {
    this.getConfiguration();
    this.getCompany();
    $(window).scrollTop(0);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  getConfiguration() {
    this.subscriptions.push(
      this.superadminService.getConfiguration().subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.configuration = res;
              this.subscriptionPlans = JSON.parse(
                JSON.stringify(this.configuration.subscriptionPlan)
              );
              // // console.log(res);
            } else {
              this.configuration = new Configuration();
            }
          });
        },
        err => { }
      )
    );
  }

  getCompany() {
    this.subscriptions.push(
      this.superadminService.getCompany().subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.Companies = res;
            }
          });
        },
        err => { }
      )
    );
  }

  GetDate(id) {
    if (id && this.Companies) {
      const data = this.Companies.filter(_ => _.id === id)[0];
      return data.createdOn;
    }
  }

  GetNoOfUsers(id) {
    if (id && this.Companies) {
      const data = this.Companies.filter(_ => _.id === id)[0];
      this.Extend.noOfUsers = data.noOfUsers;
      return data.noOfUsers;
    } else {
      return null;
    }
  }

  addTrial(trialForm, trial) {
    this.configuration.trialPeriod = this.formConfiguration.trialPeriod;
    this.isTrialFormSubmitted = true;
    const val = parseInt(trial, 0);
    if (trialForm.valid && val !== 0) {
      this.changeConfig(this.configuration);
      this.isTrialFormSubmitted = false;
      this.resetforms(trialForm);
    } else {
    }
  }
  addDisclaimer(discForm) {
    this.configuration.disclaimer = this.formConfiguration.disclaimer;
    this.isDisclaimerFormSubmitted = true;
    if (discForm.valid && this.configuration.disclaimer !== null) {
      this.changeConfig(this.configuration);
      this.isDisclaimerFormSubmitted = false;
      // this.resetforms(discForm);
      this.formConfiguration.disclaimer = this.configuration.disclaimer;
    } else {
      this.formConfiguration.disclaimer = this.configuration.disclaimer;
    }
  }

  addRecentQuote(trialForm, recentQuote) {
    this.configuration.recentQuote = this.formConfiguration.recentQuote;
    this.isQuoteFormSubmitted = true;
    const val = parseInt(recentQuote, 0);
    if (trialForm.valid && val !== 0) {
      this.changeConfig(this.configuration);
      this.isQuoteFormSubmitted = false;
      this.resetforms(trialForm);
      // this.toastrService.success(
      //   'Quote Limit Added Successfully.',
      //   '',
      //   this.constants.toastrConfig
      // );
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  addRecentTagLimit(trialForm, tagLimit) {
    this.configuration.tagLimit = this.formConfiguration.tagLimit;
    this.isTagFormSubmitted = true;
    const val = parseInt(tagLimit, 0);
    if (trialForm.valid && val !== 0) {
      this.changeConfig(this.configuration);
      this.isTagFormSubmitted = false;
      this.resetforms(trialForm);
      // this.toastrService.success(
      //   'Tag Limit Added Successfully.',
      //   '',
      //   this.constants.toastrConfig
      // );
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }
  getPlan(id) {
    if (id === null) {
      this.isPlanEditDisabled = true;
    } else {
      this.isPlanEditDisabled = false;
      this.plan = this.configuration.subscriptionPlan.filter(
        _ => _.id === id
      )[0];
    }
  }
  addSubsciption(subscriptionPlanForm, cost, userLimit, timeSpan) {
    const Cost = parseFloat(cost);
    const UserLimit = parseInt(userLimit, 0);
    const TimeSpan = parseInt(timeSpan, 0);
    this.plan.cost = parseFloat(this.plan.cost).toFixed(2);
    this.configuration.subscriptionPlan.forEach(sPlan => {
      if (sPlan.id === this.plan.id) {
        sPlan = this.plan;
      }
    });
    this.isSubscriptionFormSubmitted = true;
    if (
      subscriptionPlanForm.valid &&
      Cost !== 0 &&
      UserLimit !== 0 &&
      UserLimit !== 0
    ) {
      this.changeConfig(this.configuration);
      this.isSubscriptionFormSubmitted = false;
      this.isPlanEditDisabled = true;
      this.planName = null;
      this.plan = new Plans();
      this.resetforms(subscriptionPlanForm);
    } else {
      // this.toastrService.warning(
      //   'Fields should not be empty!',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  addTermsAndConditions(termsAndConditionForm, termsAndConditions) {
    if (termsAndConditionForm.valid) {
      this.configuration.termsAndConditions = termsAndConditions;
      this.changeConfig(this.configuration);
    }
  }

  changeConfig(configuration) {
    this.superadminService
      .postElement('Configuration', configuration)
      .subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.getConfiguration();
              this.isTrialFormSubmitted = false;
              this.isDisclaimerFormSubmitted = false;
              this.isQuoteFormSubmitted = false;
              this.isExtendFormSubmitted = false;
              this.isTagFormSubmitted = false;
              this.isSubscriptionFormSubmitted = false;
              this.formConfiguration.disclaimer = this.configuration.disclaimer;
            }
          });
        },
        err => {
          // console.log(err);
        }
      );
  }

  resetforms(form) {
    form.resetForm();
    this.isTrialFormSubmitted = false;
    this.isDisclaimerFormSubmitted = false;
    this.isQuoteFormSubmitted = false;
    this.isExtendFormSubmitted = false;
    this.isSubscriptionFormSubmitted = false;
    this.isTagFormSubmitted = false;
    this.isExtendClicked = false;
    this.istermsAndConditionsClicked = false;
    this.plan = new Plans();
    this.isPlanEditDisabled = true;
    this.planName = null;
    this.formConfiguration.disclaimer = this.configuration.disclaimer;
  }

  ExtendCompanyTrail(Form) {
    this.isExtendFormSubmitted = true;

    if (!this.Extend.noOfUsers) {
      const data = this.Companies.filter(_ => _.id === this.Extend.id)[0];
      this.Extend.noOfUsers = data.noOfUsers;
    }

    if (!this.Extend.trialPeriod) {
      const data = this.Companies.filter(_ => _.id === this.Extend.id)[0];
      this.Extend.trialPeriod = data.trialPeriod;
    }

    if (Form.valid && !this.isDaysValid && !this.isNumberValid) {
      this.Extend.createdOn = new Date();
      this.Extend.updatedOn = new Date();
      this.superadminService.ExtendCompanyTrail(this.Extend).subscribe(
        res => {
          this.ngZone.run(() => {
            if (res) {
              this.getCompany();
              this.resetforms(Form);
              this.isExtendFormSubmitted = false;
              // this.toastrService.success(
              //   'Subscription trial extended successfully.',
              //   '',
              //   this.constants.toastrConfig
              // );
            }
          });
        },
        err => {
          // console.log(err);
        }
      );
    }
  }

  CheckCost(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isCostValid = true;
    } else {
      this.isCostValid = false;
    }
  }

  CheckUserLimit(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isUserLimitValid = true;
    } else {
      this.isUserLimitValid = false;
    }
  }

  CheckTimeSpanLimit(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isTimeSpanValid = true;
    } else {
      this.isTimeSpanValid = false;
    }
  }

  CheckDaysLimit(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isDaysValid = true;
    } else {
      this.isDaysValid = false;
    }
  }

  CheckNumberLimit(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isNumberValid = true;
    } else {
      this.isNumberValid = false;
    }
  }

  CheckTrialLimit(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isTrialValid = true;
    } else {
      this.isTrialValid = false;
    }
  }

  CheckRQuoteLimit(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isRQuoteValid = true;
    } else {
      this.isRQuoteValid = false;
    }
  }

  CheckTagLimit(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isTagValid = true;
    } else {
      this.isTagValid = false;
    }
  }
  resetForms() {
    this.isPlanEditDisabled = true;
    this.isDaysValid = false;
    this.isNumberValid = false;
    this.isCostValid = false;
    this.isTagValid = false;
    this.isTagFormSubmitted = false;
    this.isQuoteFormSubmitted = false;
    this.isExtendFormSubmitted = false;
    this.isUserLimitValid = false;
    this.isTimeSpanValid = false;
    this.isTrialValid = false;
    this.isRQuoteValid = false;
    this.isTrialFormSubmitted = false;
    this.isDisclaimerFormSubmitted = false;
    this.isSubscriptionFormSubmitted = false;
    this.configuration = new Configuration();
    this.formConfiguration = new Configuration();
    this.plan = new Plans();
    this.planName = null;
    this.constants = Constants;
    this.Extend = new Configuration();
    this.getConfiguration();
    this.formConfiguration.disclaimer = this.configuration.disclaimer;
  }

  // On company select bind values to the no. of users - Suryakant
  onSelectCompany(id) {
    if (id && this.Companies) {
      const data = this.Companies.filter(_ => _.id === id)[0];
      this.Extend.noOfUsers = data.noOfUsers;
    }
  }
}
