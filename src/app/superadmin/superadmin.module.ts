import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { SuperadminRoutingModule } from './superadmin-routing.module';
import { SuperAdminConfigurationComponent } from './configuration/configuration.component';
import { SupaerAdminAccessMatrixComponent } from './access-matrix/access-matrix.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../_shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { SuperadminService } from './superadmin-service';
import { AdminLoginComponent } from './login/login.component';
import { CommonTemplateModule } from '../common/common-template.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { QuillModule } from 'ngx-quill';
import { UsersInTrialReportComponent } from './users-in-trial-report/users-in-trial-report.component';

@NgModule({
  imports: [
    CommonModule,
    SuperadminRoutingModule,
    FormsModule,
    SharedModule,
    NgxLoadingModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    CommonTemplateModule,
    QuillModule
  ],
  declarations: [SuperAdminConfigurationComponent, UsersInTrialReportComponent, SupaerAdminAccessMatrixComponent, AdminLoginComponent],
  providers: [SuperadminService]
})
export class SuperadminModule { }
