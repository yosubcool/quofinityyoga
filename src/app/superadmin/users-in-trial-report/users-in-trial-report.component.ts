import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Subscription } from 'rxjs';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { SuperadminService } from '../superadmin-service';

@Component({
  selector: 'app-users-in-trial-report',
  templateUrl: './users-in-trial-report.component.html',
  styleUrls: ['./users-in-trial-report.component.scss']
})
export class UsersInTrialReportComponent implements OnInit, OnDestroy {

  page = new Pagination();
  allCompanies = new Pagination();
  public loading = false;
  status: any;
  subscriptions: Subscription[] = [];
  keyall: any;
  reverse = false;
  pagesCount: any;

  constructor(private ngZone: NgZone, private _superadminService: SuperadminService) {
    this.allCompanies = new Pagination();
  }

  ngOnInit() {

    this.keyall = 'Newest';
    this.getAllQuotes(0);
  }


  sortQuotes(key) {

    this.keyall = key;
    this.reverse = !this.reverse;
    this.getAllQuotes(0);
  }

  ngOnDestroy() {

    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  pageChanged(no) {
    this.getAllQuotes(no);
  }

  getAllQuotes(pageNo) {
    this.page.sortType = 'desc';
    this.loading = true;
    if (this.keyall) {
      this.page.sortBy = this.keyall;
    } else {
      // this.page.sortBy = 'Newest';
      this.page.sortType = 'Newest';
    }
    if (this.reverse) {
      this.page.sortType = 'asc';
    } else {
      this.page.sortType = 'desc';
    }
    this.page.sortBy = this.keyall;
    this.page.pageNumber = pageNo;
    this.page.pageSize = 10;
    this.page.status = 'All';
    this.page.alphabet = 'All';
    this.page.records = null;
    this._superadminService.getCompaniesInTrial(this.page).subscribe(
      res => {
        this.allCompanies = res;
        this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
        this.loading = false;
      });
  }
}
