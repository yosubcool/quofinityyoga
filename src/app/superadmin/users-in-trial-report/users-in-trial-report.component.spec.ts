import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersInTrialReportComponent } from './users-in-trial-report.component';

describe('UsersInTrialReportComponent', () => {
  let component: UsersInTrialReportComponent;
  let fixture: ComponentFixture<UsersInTrialReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersInTrialReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersInTrialReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
