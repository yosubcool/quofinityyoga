import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Constants } from '../../_shared/constant';
import { SuperadminService } from '../superadmin-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class AdminLoginComponent implements OnInit {

  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  public loading = false;
  [x: string]: any;
  email: string;
  password: string;
  constants: any;
  constructor(
    private _route: Router,
    private superadminService: SuperadminService,
    private _toastrService: ToastrService,
    private ngZone: NgZone
  ) {
    this.constants = Constants;
    window.scrollTo(0, 0);
  }

  ngOnInit() {
    const token = sessionStorage.getItem('AccessToken');
    if (token) {
      this._route.navigate(['/superadmin/configuration']);
    }
  }


  login(isValid: boolean) {
    if (isValid) {
      this.loading = true;
      this.superadminService.Login(this.email, this.password).subscribe(
        res => {

          this.loading = false;
          this.ngZone.run(() => {
            if (res) {
              this._route.navigate(['/superadmin/configuration']);
            }
          });
        },
        err => {
          this.loading = false;
          // this._toastrService.error(
          //   'Please provide valid details.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.show = true;
          this.popupOpen(this.popupFor = 'Invalid details');
        }
      );
    } else {
      this.loading = false;
      // this._toastrService.error(
      //   'Please provide valid details.',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.show = true;
      this.popupOpen(this.popupFor = 'Invalid details');
    }
  }


  // messages display
  popupOpen(popupFor) {
    // this.titletoClose();

    if (this.popupFor === 'Invalid details') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Please provide valid details.';
    }
  }

  yes(ConfirmModal) {
    this.show = false;
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }
}
