import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SuperAdminConfigurationComponent } from './configuration/configuration.component';
import { SupaerAdminAccessMatrixComponent } from './access-matrix/access-matrix.component';
import { AdminLoginComponent } from './login/login.component';
import { RouteGaurd } from '../_core/RouteGaurd';
import { UsersInTrialReportComponent } from './users-in-trial-report/users-in-trial-report.component';
import { RoleGaurd } from '../_core/RoleGuard';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Super Admin'
    },
    children: [
      {
        path: 'configuration',
        canActivate: [RoleGaurd],
        component: SuperAdminConfigurationComponent,
        // isLogin: true,
        data: {
          title: 'Configuration page',
          expectedRole: 'Super Admin'
        }
      },
      {
        path: 'users-in-trial-report',
        canActivate: [RoleGaurd],
        component: UsersInTrialReportComponent,
        // isLogin: true,
        data: {
          title: 'Users in Trial Period',
          expectedRole: 'Super Admin'
        }
      },
      {
        path: 'access-matrix',
        canActivate: [RoleGaurd],
        component: SupaerAdminAccessMatrixComponent,
        // isLogin: true,
        data: {
          title: 'Access Matrix Page ',
          expectedRole: 'Super Admin'
        }
      },
      {
        path: 'login',
        component: AdminLoginComponent,
        data: {
          title: 'Login'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperadminRoutingModule { }
