import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { AccessMatrix } from '../../_shared/model/access-matrix-model';
import { SuperadminService } from '../superadmin-service';
declare var $: any;
@Component({
  selector: 'app-access-matrix',
  templateUrl: './access-matrix.component.html',
  styleUrls: ['./access-matrix.component.scss']
})
export class SupaerAdminAccessMatrixComponent implements OnInit, OnDestroy {
  userRoles = [];
  accessMatrix: AccessMatrix;
  subscriptions: Subscription[] = [];
  roleID: string;
  constants: any;
  public loading = false;
  constructor(private superadminService: SuperadminService, private toastrService: ToastrService, private ngZone: NgZone) { }

  ngOnInit() {
    this.GetRoles();
    $(window).scrollTop(0);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  GetRoles() {
    this.loading = true;
    this.subscriptions.push(
      this.superadminService.getRoles().subscribe(
        response => {
          this.ngZone.run(() => {
            this.roleSelected(response[0].id);
            const index = response.findIndex(x => x.description === 'Super Admin');
            if (index > -1) {
              response.splice(index, 1);
            }
            this.roleID = response[0].id;
            this.userRoles = response;
            this.loading = false;
          });
        },
        err => {
          this.loading = false;
        }
      )
    );
  }

  GetAccessMatrix(id) {
    this.loading = true;
    this.subscriptions.push(
      this.superadminService.getAccessMatrix(id).subscribe(
        response => {
          this.ngZone.run(() => {
            this.accessMatrix = response;
            this.loading = false;
          });
        },
        err => {
          this.loading = false;
        }
      )
    );
  }

  roleSelected(roleId) {
    this.GetAccessMatrix(roleId);
  }

  postAccessMartix(accessMartix) {
    this.superadminService.postElement('SuperAdmin/AddAccessMartix', accessMartix).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
          }
        });
      },
      err => {
        // console.log(err);
        this.loading = false;
      }
    );
  }
}
