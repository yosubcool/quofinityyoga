import { Injectable, NgZone, OnInit } from "@angular/core";
import { DataService } from "../_core/data.service";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { map, Observable } from "rxjs";
import { User } from "../_shared/model/user-model";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class SuperadminService implements OnInit {
  constants: any;
  user: User;
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService
  ) {
    this.user = JSON.parse(sessionStorage.getItem("currentUser"));
  }

  ngOnInit() { }

  postElement(method, model) {
    return this.dataService.postElement(method, model).pipe(map((response) => {
      return response;
    }));
  }

  getConfiguration() {
    return this.dataService.getData("Configuration").pipe(map((response) => {
      return response;
    }));
  }

  getCompany() {
    return this.dataService
      .getData("Company/GetExpairedCompanies")
      .pipe(map((response) => {
        return response;
      }));
  }

  getAccessMatrix(id) {
    return this.dataService
      .getElementThroughId("SuperAdmin/GetAccessMatrix", id)
      .pipe(map((response) => {
        return response;
      }));
  }

  getRoles() {
    return this.dataService
      .getData("Configuration/GetRoles")
      .pipe(map((response) => {
        return response;
      }));
  }

  ExtendCompanyTrail(model) {
    return this.dataService
      .postElement("Company/ExtendCompanyTrail", model)
      .pipe(map((response) => {
        return response;
      }));
  }

  Login(userName: string, password: string): Observable<any> {

    const user: User = new User();
    user.email = userName;
    user.password = password;
    return this.dataService
      .postElementForAllowAnonymous("SuperAdmin/Login", user)
      .pipe(map((res) => {
        const data = res;
        if (data) {
          sessionStorage.setItem("AccessToken", data.token.result.value);
          return data;
        } else {
          this.toastrService.error(
            "Invalid login credentials",
            "",
            this.constants.toastrConfig
          );
          return false;
        }
      }));
  }

  getCompaniesInTrial(data) {
    return this.dataService
      .getElementThroughAnyObject("Report/CompaniesInTrial", data)
      .pipe(map((response) => {
        return response;
      }));
  }
}
