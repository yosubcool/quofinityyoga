import { Injectable, NgZone } from '@angular/core';
import { DataService } from '../_core/data.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Pagination } from '../_shared/model/pagination-model';
import { User } from '../_shared/model/user-model';
import { map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ContactsService {
  pagination = new Pagination();
  user: User;
  constructor(private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService) {
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
  }



  getContacts(pagination: Pagination) {
    return this.dataService.getElementThroughAnyObject('Contact/GetContacts', pagination).pipe(map(response => {
      return response;
    }));
  }

  FindAccount(Id) {
    return this.dataService.getElementThroughId('Accounts/Find', Id).pipe(map(response => {
      return response;
    }));
  }

  getModuleAccess() {
    return this.dataService.getData('TenantMaster/GetAccessRoleMatrix/').pipe(map(response => {
      return response;
    }));
  }

  GetAllContact() {
    return this.dataService.getData('Contact').pipe(map(response => {
      return response;
    }));
  }
  GetAccounts() {
    return this.dataService.getData('Accounts').pipe(map(response => {
      return response;
    }));
  }
  getContactById(Id: string) {
    return this.dataService.getData('Contact/' + Id).pipe(map(response => {
      return response;
    }));
  }


  addContacts(Contact) {
    Contact.isActive = true;
    Contact.createdOn = new Date();
    Contact.updatedOn = new Date();
    Contact.createdBy = this.user.id;
    Contact.updatedBy = this.user.id;
    return this.dataService.postElement('Contact', Contact).pipe(map(response => {
      return response;
    }));
  }

  UpdateContacts(data) {
    return this.dataService.updateElement('Contact', data).pipe(map(response => {
      return response;
    }));
  }
  deleteContact(data) {
    return this.dataService.updateElement('Contact/DeleteContact', data).pipe(map(response => {
      return response;
    }));
  }


  UpdateContactData(data) {
    return this.dataService.updateElement('Contact/UpdateContactData', data).pipe(map(response => {
      return response;
    }));
  }

  GetContactsForExport() {
    return this.dataService.getData('Contact/GetContactsForExport').pipe(map(response => {
      return response;
    }));
  }
}
