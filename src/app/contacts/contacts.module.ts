import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsRoutingModule } from './contacts-routing.module';
import { ContactsListComponent } from './contacts-list/contacts-list.component';
import { ContactsService } from './contacts-service';
import { ContactsDetailComponent } from './contacts-detail/contacts-detail.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgxMaskModule } from 'ngx-mask';
import { NgxLoadingModule } from 'ngx-loading';
// import { DataTablesModule } from 'angular-datatables';
import { CommonTemplateModule } from '../common/common-template.module';
import { AccountsService } from '../accounts/accounts-service';
import { DashboardService } from '../dashboard/dashboard.service';
import { QuotesService } from '../quotes/quotes-service';
import { DataService } from '../_core/data.service';
import { SharedModule } from '../_shared/shared.module';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';

@NgModule({
  imports: [

    CommonModule,
    FormsModule,
    ContactsRoutingModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    NgSlimScrollModule,
    TypeaheadModule.forRoot(),
    CommonTemplateModule,
    SharedModule,
    NgxLoadingModule,
    NgxMaskModule,
    TableModule,
    ButtonModule
    // DataTablesModule
  ],
  providers: [AccountsService, DataService, DashboardService, QuotesService, ContactsService],
  declarations: [ContactsListComponent, ContactsDetailComponent]
})
export class ContactsModule { }
