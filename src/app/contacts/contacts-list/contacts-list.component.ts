import {
  Component,
  OnInit,
  NgZone,
  OnDestroy,
  ViewChild,
  DoCheck
} from '@angular/core';
import { Contact } from '../../_shared/model/contact-model';
import { ContactsService } from '../contacts-service';
import { Pagination } from '../../_shared/model/pagination-model';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ngxCsv } from 'ngx-csv';
import { map, Subscription } from 'rxjs';
import { QuotesService } from 'src/app/quotes/quotes-service';
import { Constants } from 'src/app/_shared/constant';
import { Actions } from 'src/app/_shared/model/actions-model';
import { User } from 'src/app/_shared/model/user-model';
import { HttpClient } from '@angular/common/http';
// import { PaginatePipe, PaginationService } from 'ng2-pagination';//#Revisit_IgnoredAsNotUsed
declare var $: any;
@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;
  validContacts: any;
  totalContact: any;
  showData = false;
  invalidContact: any;
  showWarning = false;
  reverse = false;
  key: any;
  validContact = false;
  sampleHeaders: string[];
  subscriptions: Subscription[] = [];
  uplodedData: any;
  myFilename: string;
  cntforExport: any;
  myheaders: any;
  isNameEdit = false;
  accountCntlist: any[];
  contactEmailArray: any[];
  pagesCount: number;
  selectedAlpha: string;
  contactcount: any;
  contact = new Contact();
  contactAdd = new Contact();
  contactsList: Pagination;
  isFormSubmitted = false;
  constants: any;
  allowedFiles = ['csv'];
  data: any;
  editEmail = 0;
  textEmail = 0;
  editCntName = 0;
  textCntName = 0;
  textPhone = 0;
  editPhone = 0;
  editHome = 0;
  textHome = 0;
  editFax = 0;
  textFax = 0;
  isShowTextBox = false;
  textIdc = 0;
  cntName = true;
  pattern = '[a-zA-Z]+';
  currentUser: User;
  contactName1 = true;
  account1 = true;
  email1 = true;
  phone1 = true;
  home1 = true;
  fax1 = false;
  quotes1 = true;
  extShow = false;
  isEmailRepated = false;
  loading: boolean;
  showFile = false;
  accessValid: any;
  isExtensionNovalid = false;
  compId: any;
  // tslint:disable-next-line:max-line-length
  phonePattern = /^\s*(?!0{10})[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,9}\s*$/;
  EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  NAME_REGEXP = /[a-zA-Z\s]+/;
  mask: any[] = [
    '+',
    '1',
    ' ',
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  // New Grid
  items = [];
  itemCount = 0;
  editProperty: string;
  columns = [];
  isContactNameEdit = false;
  isContactPhoneEdit = false;
  iisContactFaxEdit = false;
  isContactHomeEdit = false;
  isContactEmailEdit = false;
  isContactFaxEdit = false;
  toBeEdit = 'none';
  //primeNg for Migration
  newMask: string = " (000) 000-0000";
  first: number = 0;

  @ViewChild('fileImportInput') fileImportInput: any;
  isOpenPopup = false;
  constructor(
    private _contactsService: ContactsService,
    private quoteService: QuotesService,
    private router: Router,
    private _toastrService: ToastrService,
    private ngZone: NgZone,
    private http: HttpClient
  ) {
    this.accessValid = new Actions();
    this.contactsList = new Pagination();
    this.contactEmailArray = [];
    this.myheaders = [];
    this.constants = Constants;
    this.selectedAlpha = 'All';
    this.currentUser = new User();
    this.cntforExport = [];
    this.sampleHeaders = [
      'Name',
      'Email',
      'Work Number',
      'Mobile Number',
      'Fax Number',
      'Account Name'
    ];
    this.contactsList.pageSize = 10;
  }

  ngOnInit() {
    $(window).scrollTop(0);
    this.getModuleAccess();
    this.getContactsList(0);
    this.getAccounts();
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    // {property:"faxNo",header:"Fax Number"}, {property:"extensionNo",header:"Extension No"} -- Removing As David Suggested
    this.columns = [{ property: "firstName", header: "Name" }, { property: "accounts", header: "Account Name" }, { property: "email", header: "Email" }, { property: "phone", header: "Mobile Number" }, { property: "homeNo", header: "Work Number" }, { property: "quotes", header: "Quotes" }]
    if (window.innerWidth <= 1024) {
      this.account1 = false;
      this.email1 = false;
      this.phone1 = false;
      this.home1 = false;
      this.quotes1 = false;
    } else {
      this.account1 = true;
      this.email1 = true;
      this.phone1 = true;
      this.home1 = true;
      this.quotes1 = true;
    }
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
      // // console.log('Done');
    }, 500);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
    if (document.getElementById('contacts') !== null) {
      document.getElementById('contacts').classList.remove('active');
    }
  }

  getModuleAccess() {
    this.subscriptions.push(
      this._contactsService.getModuleAccess().subscribe(res => {
        this.accessValid = res.contact;
      })
    );
  }
  SearchAccount(e) {
    this.selectedAlpha = e;
    this.getContactsList(0);
  }

  goToDetails(id) {
    this.router.navigate(['/contacts/contact-detail'], {
      queryParams: { id: btoa(id) }
    });
  }
  pageChanged(no) {
    this.getContactsList(no);
  }
  isUniqueAccountName(name) {
    if (name === this.pattern) {
      this.cntName = true;
    } else {
      this.cntName = false;
    }
  }
  getAccounts() {
    this.subscriptions.push(
      this._contactsService.GetAccounts().subscribe(res => {
        this.accountCntlist = res;
      })
    );
  }

  DownloadTemplate() {
    const parsedResponse = this.sampleHeaders;
    //#revisit_CommentForV1
    // if (navigator.appVersion.toString().indexOf('.NET') > 0) {
    //   const bb = new MSBlobBuilder();
    //   bb.append(parsedResponse);
    //   const blob1 = bb.getBlob('csv');
    //   window.navigator.msSaveBlob(blob1, 'Contact.csv');
    //   const url = window.URL.createObjectURL(blob1);
    //   window.URL.revokeObjectURL(url);
    // } else {
    // new Blob([parsedResponse] //#Revisit_PreviousCode
    const blob = new Blob([parsedResponse.toString()], { type: 'csv' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'Contact.csv';
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
    // }
  }

  exportToCSV() {
    this.subscriptions.push(
      this._contactsService.GetContactsForExport().subscribe(
        res => {
          this.cntforExport.push(res);

          const data: any = [];
          if (this.cntforExport.length) {
            this.cntforExport[0].forEach(element => {
              const obj: any = {};
              // tslint:disable-next-line:curly
              this.columns.forEach(col => {
                const prop = col.property;
                if (prop === 'firstName') {
                  obj[col.header] = element.contacts.firstName;
                  this.myheaders.push(col.header);
                } else if (prop === 'accounts') {
                  obj[col.header] = element.contacts.accounts.name;
                  this.myheaders.push(col.header);
                } else if (prop === 'email') {
                  obj[col.header] = element.contacts.email;
                  this.myheaders.push(col.header);
                } else if (prop === 'phone') {
                  obj[col.header] = element.contacts.phone;
                  this.myheaders.push(col.header);
                } else if (prop === 'homeNo') {
                  obj[col.header] = element.contacts.homeNo;
                  this.myheaders.push(col.header);
                } else if (prop === 'faxNo') {
                  obj[col.header] = element.contacts.faxNo;
                  this.myheaders.push(col.header);
                } else if (prop === 'quotes') {
                  obj[col.header] = element.quotes;
                  this.myheaders.push(col.header);
                } else if (prop === 'extensionNo') {
                  obj[col.header] = element.contacts.extensionNo;
                  this.myheaders.push(col.header);
                }
              });
              data.push(obj);
            });
            const uniqueheader = this.myheaders.filter(
              (v, i, a) => a.indexOf(v) === i
            );
            const options = {
              fieldSeparator: ',',
              quoteStrings: '"',
              decimalseparator: '.',
              showLabels: true,
              showTitle: true,
              headers: uniqueheader
            };
            this.myheaders = [];

            // tslint:disable-next-line:no-unused-expression
            new ngxCsv(data, 'Contacts', options);
          } else {
            // this._toastrService.warning(
            //   'No data available to export!',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'No data available to export!'));
            this.loading = false;
          }
        },
        err => {
          // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
        }
      )
    );
  }

  isUniqueContactEmail() {
    const arr = this.contactEmailArray.filter(
      x =>
        x.contacts.email === this.contact.email && x.contacts.isActive === true
    );
    this.isEmailRepated = arr.length === 0 ? false : true;
  }

  addContactDetailModal(valid, abc, contactform) {
    this.isFormSubmitted = true;
    if (valid && !this.isEmailRepated) {
      this.contact.isActive = true;
      this.contact.createdOn = new Date();
      this.contact.updatedOn = new Date();
      this.contact.createdBy = this.currentUser.id;
      this.contact.updatedBy = this.currentUser.id;
      if (this.contact.accounts.id) {
        this.contact.accounts = this.accountCntlist.filter(
          x => x.id === this.contact.accounts.id
        )[0];
      } else {
        this.contact.accounts = {};
      }

      this._contactsService.addContacts(this.contact).subscribe(
        response => {
          if (response === false) {
            this.isEmailRepated = true;
          } else {
            this.isEmailRepated = false;
            abc.hide();
            this.ngZone.run(() => {
              this.router.navigate(['/contacts/contact-detail'], {
                queryParams: { id: btoa(response.id) }
              });

              this.cancelContactDetail(contactform, abc);
              // this._toastrService.success(
              //   'Contact added successfully',
              //   '',
              //   this.constants.toastrConfig
              // );
            });
            this.loading = false;
          }
        },

        err => {
          // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false;
        }
      );
    }
  }
  cancelContactDetail(form, popup) {
    form.reset();
    this.contact = new Contact();
    //  this.isFormSubmitted = false;
    this.isEmailRepated = false;
    popup.hide();
  }

  getContactsList(pageNo) {
    let sortingData = pageNo;
    //Adding this logic to accomodate inclusion of PrimeNg. Event is sent by the p-table.
    if (isNaN(pageNo)) {
      pageNo = pageNo.first / pageNo.rows + 1;
    }
    this.contactsList.sortBy = 'firstName';
    this.contactsList.sortType = 'asc';
    this.loading = true;
    if (sortingData != 0) {
      if (sortingData.sortOrder === 1) {
        this.contactsList.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.contactsList.sortType = 'asc';
      }
      else {
        this.contactsList.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.contactsList.sortType = 'desc';
      }
    }
    this.contactsList.pageNumber = pageNo;

    this.contactsList.alphabet =
      this.selectedAlpha === 'All' ? '' : this.selectedAlpha;
    this.subscriptions.push(
      this._contactsService.getContacts(this.contactsList).subscribe(
        res => {
          this.ngZone.run(() => {
            this.contactsList = res;
            this.contactEmailArray = res.records;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            this.items = JSON.parse(JSON.stringify(this.contactsList.records));
            this.itemCount = this.contactsList.totalCount;
          });
          this.loading = false;
        },
        err => {
          // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false;
        }
      )
    );
  }
  getContact() {
    this.subscriptions.push(
      this._contactsService.GetContactsForExport().subscribe(
        res => {
          this.cntforExport.push(res);
        },
        err => {
          // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false;
        }
      )
    );
  }

  isInArray(array, word) {
    return array.indexOf(word.toLowerCase()) > -1;
  }

  fileChangeListener(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.loading = true;
      const file1: File = fileList[0];
      const filename = file1.name;
      const Extension = filename
        .substring(filename.lastIndexOf('.') + 1)
        .toLowerCase();
      if (this.isInArray(this.allowedFiles, Extension)) {
        this.myFilename = filename;
        const formData: FormData = new FormData();
        formData.append('uploadFile', file1, file1.name);
        const headers = new Headers();
        headers.append('Content-Type', 'multipart/form-data');
        const file = new FormData();

        file.append('file', file1);
        file.append(
          'Authorization',
          'Bearer ' + sessionStorage.getItem('AccessToken')
        );
        this.compId = this.currentUser.subscriberId;
        this.http
          .post(
            this.constants.BASE_URL + 'Contact/UploadFile/' + this.compId,
            file
          )
          .pipe(map(res => res))
          .subscribe({
            next: (response: any) => {
              this.showData = true;
              this.showWarning = false;
              this.loading = false;
              this.totalContact = response.totalCount;
              this.invalidContact = response.failedContacts.length;
              this.validContacts = this.totalContact - this.invalidContact;

              if (this.validContacts || this.invalidContact) {
                this.showFile = true;
                this.loading = false;
                event.target.value = '';
                this.uplodedData = response;
                this.validContacts = response.successfullContacts.length;
              } else {
                this.showFile = false;
                this.showWarning = true;
                this.showData = false;
              }
            },
            error: error => (this.loading = false)
          }
            // this._toastrService.error('Please select valid file', '', this.constants.toastrConfig))
          );
      } else {
        this.loading = false;
        event.target.value = '';
        this.showWarning = true;
        this.showData = false;
        this.fileImportInput.nativeElement.value = '';
        this.uplodedData = null;

        return;
      }
    }
  }

  CancelUpload() {
    this.fileImportInput.nativeElement.value = '';
    this.myFilename = '';
    this.uplodedData = null;
  }

  addContactFile() {
    if (this.uplodedData.successfulAccounts.length) {
      this.uplodedData.successfulAccounts.forEach(element => {
        element.createdOn = new Date();
        element.updatedOn = new Date();
        element.notes = [];
        element.accounts = {};

        if (element.phone != null) {
          let workNo = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
          const digits = element.phone.replace(/\D/g, '');
          if (digits.length === 10) {
            if (element.phone.length <= 3) {
              workNo = element.phone.replace(/^(\d{0,3})/, '($1)');
            } else if (element.phone.length <= 6) {
              workNo = element.phone.replace(/^(\d{0,3})(\d{0,3})/, '($1) $2');
            } else {
              workNo = element.phone.replace(
                /^(\d{0,3})(\d{0,3})(.*)/,
                '($1) $2-$3'
              );
            }
            element.phone = '+1' + ' ' + workNo;
            this.validContact = true;
          }
        }

        if (element.homeNo != null && element.homeNo.length === 10) {
          let homeNo = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
          const digits = element.homeNo.replace(/\D/g, '');
          if (digits.length === 10) {
            if (element.homeNo.length <= 3) {
              homeNo = element.homeNo.replace(/^(\d{0,3})/, '($1)');
            } else if (element.homeNo.length <= 6) {
              homeNo = element.homeNo.replace(/^(\d{0,3})(\d{0,3})/, '($1) $2');
            } else {
              homeNo = element.homeNo.replace(
                /^(\d{0,3})(\d{0,3})(.*)/,
                '($1) $2-$3'
              );
            }
            element.homeNo = '+1' + ' ' + homeNo;
          }
        }
        if (element.faxNo != null && element.faxNo.length === 10) {
          let faxNo = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
          const digits = element.faxNo.replace(/\D/g, '');
          if (digits.length === 10) {
            if (element.faxNo.length <= 3) {
              faxNo = element.faxNo.replace(/^(\d{0,3})/, '($1)');
            } else if (element.faxNo.length <= 6) {
              faxNo = element.faxNo.replace(/^(\d{0,3})(\d{0,3})/, '($1) $2');
            } else {
              faxNo = element.faxNo.replace(
                /^(\d{0,3})(\d{0,3})(.*)/,
                '($1) $2-$3'
              );
            }
            element.faxNo = '+1' + ' ' + faxNo;
          }
        }
        this._contactsService.addContacts(element).subscribe(res => {
          this.ngZone.run(() => {
            // this._toastrService.success(
            //   'Contact added successfully ',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.getContactsList(0);
          });
        });
      });
    }
  }

  private handleError(error: any) {
    const errMsg = error.message
      ? error.message
      : error.status
        ? `${error.status} - ${error.statusText}`
        : 'Server error';
    console.error(errMsg);
    return errMsg;
  }

  sort(key) {
    this.key = key.sortBy;
    this.reverse = key.sortAsc;
    this.getContactsList(0);
    this.toBeEdit = 'none';
  }

  getChangedColumns(columns) {
    this.columns = columns.datatable.columns._results;
    this.toBeEdit = 'none';
    if (this.contactsList.records) {
      this.items = JSON.parse(JSON.stringify(this.contactsList.records));
    }
  }

  onSave(valid, myavalue, callFrom = 'name') {//#revisitPradeep
    if (valid && !this.isExtensionNovalid) {
      this.contactAdd = myavalue.contacts;
      this.contactAdd.email = myavalue.contacts.email.trim();
      this._contactsService.UpdateContactData(this.contactAdd).subscribe(
        response => {
          if (response === false || this.contactAdd.id !== response.id) {
            this.isEmailRepated = true;
            this.toBeEdit = 'email';
          } else {
            this.toBeEdit = 'none';
            this.getContactsList(this.contactsList.pageNumber);
          }
        },
        err => {
          // console.log(err);
          if (err.status === 403) {
            this.isNameEdit = true;
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
        }
      );
    } else {
      this.toBeEdit = callFrom;
    }
  }

  discardEdit() {
    this.toBeEdit = 'none';
    this.isExtensionNovalid = false;
    this.items = JSON.parse(JSON.stringify(this.contactsList.records));
  }

  isEdit(item, callFrom) {
    this.editProperty = item.contacts.id;
    this.toBeEdit = callFrom;
    this.items = JSON.parse(JSON.stringify(this.contactsList.records));
  }
  CheckExtensionNo(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isExtensionNovalid = true;
    } else {
      this.isExtensionNovalid = false;
    }
  }

  // messages display
  popupOpen(popupFor) {
    // this.titletoClose();
    if (this.popupFor === 'Access denied') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Access denied';
    } else if (this.popupFor === 'No data available to export!') {
      this.typeofMessage = 'Error';
      this.notification = 'No data available to export!';
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.messageToDisplay = null;
    }
  }

  yes(ConfirmModal) {
    // if (this.popupFor === 'deleteSubcategory') {
    //   this.onDeleteSubcategory(this.index);
    //   ConfirmModal.hide();
    // } else if (this.popupFor === 'deleteCategory') {
    //   this.onDeleteCategory(this.index);
    //   ConfirmModal.hide();
    // }
    this.show = false;
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }

  clonedContacts: { [s: string]: any; } = {};

  onRowEditInit(item: any) {
    this.clonedContacts[item.contacts.id] = JSON.parse(JSON.stringify(item));
    console.log('Initial Value Before Edit');
    console.log(this.clonedContacts[item.contacts.id]);
  }

  onRowEditSave(item: any) {
    console.log('Event Triggered');
    console.log(item);
    delete this.clonedContacts[item.contacts.id];
    this.onSave(true, item);
  }

  onRowEditCancel(item: any, index: number) {
    this.items[index] = this.clonedContacts[item.contacts.id];
    delete this.clonedContacts[item.contacts.id];
  }
}
