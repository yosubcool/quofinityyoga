import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactsListComponent } from './contacts-list/contacts-list.component';
import { ContactsDetailComponent } from './contacts-detail/contacts-detail.component';
import { RouteGaurd } from '../_core/RouteGaurd';

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      title: 'Contacts',
      isLogin: true
    },
    children: [
      {
        path: 'contact-list',
        component: ContactsListComponent,
        data: {
          title: 'Contact List'
        }
      },
      {
        path: 'contact-detail',
        component: ContactsDetailComponent,
        data: {
          title: 'Contact Detail'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule { }
