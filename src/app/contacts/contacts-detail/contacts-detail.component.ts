import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { History } from '../../_shared/model/history-model';
import { ToastrService } from 'ngx-toastr';
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError,
  ActivatedRoute
} from '@angular/router';
import { ISlimScrollOptions } from 'ngx-slimscroll';
import { AccountsService } from 'src/app/accounts/accounts-service';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
import { QuotesService } from 'src/app/quotes/quotes-service';
import { UserService } from 'src/app/user-account/user-service';
import { DataService } from 'src/app/_core/data.service';
import { Constants } from 'src/app/_shared/constant';
import { Account } from 'src/app/_shared/model/account-model';
import { Actions } from 'src/app/_shared/model/actions-model';
import { Company } from 'src/app/_shared/model/company-model';
import { Contact } from 'src/app/_shared/model/contact-model';
import { Notes } from 'src/app/_shared/model/notes-model';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { User } from 'src/app/_shared/model/user-model';
import { ContactsService } from '../contacts-service';
import { Quote } from 'src/app/_shared/model/quote-model';
import { Subscription } from 'rxjs';
// import { PricePipe } from 'app/_shared/price_pipe'; //#Revisit_IgnoredAsNotUsed
declare var $: any;
@Component({
  selector: 'app-contacts-detail',
  templateUrl: './contacts-detail.component.html'
})
export class ContactsDetailComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;

  msg: string;
  flag = false;
  searchAccountForm = false;
  isAccountAlreadyPresent = false;
  presentAccount = false;
  reverse = false;
  key: any;
  accountsList: any[];
  QuoteNameArray: any;
  accountsName: any[];
  isExtensionNovalid = false;
  expired: any[];
  rejected: any[];
  accepted: any[];
  quoted: any[];
  status: string;
  contactName = false;
  inprogress: any[];
  quoteStatusList: any;
  inProgressRecords: any;
  quoteRecords: any[];
  totalCount: number;
  statusCount: any[];
  loading: boolean;
  allStatusCount: any = {};
  selectedAccount = new Account();
  notes: any;
  showtext = false;
  statusList: any;
  accounName: any;
  accountData: any;
  accountsData: any[];
  accountId: string;
  constants: typeof Constants;
  quoteStats = [];
  // isFormSubmitted = false;
  // isFormSubmitted1 = false;
  contactDetailsForm = false;
  contactId: string;
  edit = false;
  editacc = false;
  editbtn = true;
  quotes: any[];
  accountName: any;
  start = null;
  accountIdForContact: any;
  editaccbtn = true;
  log: History = new History();
  activity: History[];
  show = true;
  historyData: any[];
  oldnotes: any[];
  oldHistoryData: any[];
  quote: Quote;
  newMask: string = " (000) 000-0000";
  mask: any[] = [
    '+',
    '1',
    ' ',
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  phonepattern = false;
  note: Notes;
  extNo = false;
  data: any;
  opts: ISlimScrollOptions;
  QouteCount: any = {};
  accountDataPresent = false;
  account = new Account();
  newAccount = new Account();
  SearchText: string;
  user: User;
  notenew: any[];
  quotesInfo: Pagination;
  showallphone = false;
  isEmailRepated = false;
  contact: Contact;
  oldcontact: Contact;
  accessValid: any;
  addQuote: boolean;
  accName = false;
  alldata: any[];
  subscriptions: Subscription[] = [];
  pagesCount: number;
  noteIndex: any;
  isAccount = false;
  noAccount = false;
  showBtn = true;
  notealldata: any;
  isOpenPopup = false;
  company: Company;
  constructor(
    private accountsService: AccountsService,
    private _UserService: UserService,
    private dashboardService: DashboardService,
    private dataService: DataService,
    private contactService: ContactsService,
    private route: ActivatedRoute,
    private _quotesService: QuotesService,
    private toastrService: ToastrService,
    private _route: Router,
    private ngZone: NgZone
  ) {
    this.company = new Company();
    this.quotesInfo = new Pagination();
    this.accessValid = new Actions();
    this.accountIdForContact = new Account();
    this.accountsList = new Array<Contact>();
    this.contact = new Contact();
    this.oldcontact = new Contact();
    this.log = new History();
    this.quote = new Quote();
    this.constants = Constants;
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.note = new Notes();
    this.statusCount = [];
    this.QuoteNameArray = [];
    this.quoteRecords = [];
    this.quoteStatusList = [];
    this.start = null;
    this.accountsData = [];
    this.alldata = [];
    this.notealldata = [];

    _route.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        let id = this.route.snapshot.queryParams['id'];
        id = atob(id);
        if (this.contactId && this.contactId !== id) {
          this.loading = true;
          if (!id) {
            this._route.navigate(['/contacts/contact-list']);
          }
          this.contactId = id;
          this.getModuleAccess();
          this.getContactById(this.contactId);
          //  this.getAccounts();
          this.getQuotes(0);
          $(window).scrollTop(0);
          this.opts = {
            position: 'right',
            barBackground: '#000000'
          };
          this.log.userId = this.user.id;
          if (document.getElementById('contacts') !== null) {
            document.getElementById('contacts').classList.add('active');
          }
        }
      }
    });
  }

  ngOnInit() {
    $(window).scrollTop(0);
    const id = this.route.snapshot.queryParams['id'];
    if (!id) {
      this._route.navigate(['/contacts/contact-list']);
    }
    this.contactId = atob(id);
    this.getModuleAccess();
    this.GetCompanys();
    this.getContactById(this.contactId);
    // this.getAccounts();
    //  this.getQuotes(0);
    this.opts = {
      position: 'right',
      barBackground: '#000000'
    };
    this.log.userId = this.user.id;
    if (document.getElementById('contacts') !== null) {
      document.getElementById('contacts').classList.add('active');
    }
  }
  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
      // // console.log('Done');
    }, 500);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
    if (document.getElementById('contacts') !== null) {
      document.getElementById('contacts').classList.remove('active');
    }
  }

  GetCompanys() {
    this.loading = true;
    this.subscriptions.push(
      this._UserService.GetCompany().subscribe(
        res => {
          this.ngZone.run(() => {
            this.company = res;
            this.loading = false;
          });
        },
        err => {
          this.loading = false;
        }
      )
    );
  }

  getModuleAccess() {
    this.subscriptions.push(
      this.accountsService.getModuleAccess().subscribe(res => {
        this.accessValid = res.contact;
        this.addQuote = res.quote.create;
        if (!this.accessValid.view) {
          this._route.navigate(['/contacts/contact-list']);
        }
      })
    );
  }

  getContactById(id) {
    this.loading = true;
    this.subscriptions.push(
      this.contactService.getContactById(id).subscribe(response => {
        this.loading = false;
        this.contact = response.contacts;
        this.oldcontact = JSON.parse(JSON.stringify(response.contacts));
        this.oldHistoryData = JSON.parse(JSON.stringify(response.history));
        this.quotes = response.quote;
        this.historyData = response.history;
        this.alldata = [];
        this.notealldata = [];
        this.notes = response.contacts.notes;

        this.historyData.forEach(element => {
          this.alldata.push(element);
        });

        this.notes.forEach(element => {
          this.alldata.push(element);
        });
        this.sortNote();
        this.getAllData();
        this.quoteStats = response.stats;
        if (
          response.contacts.accounts &&
          response.contacts.accounts.id !== null
        ) {
          this.accountDataPresent = true;
          this.account = response.contacts.accounts;
        }
        if (
          this.contact.faxNo == null &&
          this.contact.homeNo == null &&
          this.contact.extensionNo == null
        ) {
          this.showallphone = false;
        } else {
          this.showallphone = true;
        }
      })
    );
  }
  sortNote() {
    this.notes.sort(function (a, b) {
      // tslint:disable-next-line:curly
      if (a.createdOn > b.createdOn) return -1;
      // tslint:disable-next-line:curly
      if (a.createdOn < b.createdOn) return 1;
      return 0;
    });
  }
  sortHistory() {
    this.historyData.sort(function (a, b) {
      // tslint:disable-next-line:curly
      if (a.createdOn > b.createdOn) return -1;
      // tslint:disable-next-line:curly
      if (a.createdOn < b.createdOn) return 1;
      return 0;
    });
  }
  pageChanged(no) {
    this.getQuotes(no);
  }
  SearchQuotes(e) {
    this.status = e;
    this.getQuotes(0);
  }

  getQuotes(pageNo) {
    this.quotesInfo.sortBy = 'CreatedOn';
    this.quotesInfo.sortType = 'desc';
    this.loading = true;
    if (this.key) {
      this.quotesInfo.sortBy = this.key;
    }
    if (this.reverse) {
      this.quotesInfo.sortType = 'asc';
    } else {
      this.quotesInfo.sortType = 'desc';
    }
    this.quotesInfo.pageNumber = pageNo;
    this.quotesInfo.pageSize = 10;
    this.quotesInfo.contactId = this.contactId;
    this.quotesInfo.status = this.status === 'All' ? '' : this.status;
    this.subscriptions.push(
      this.accountsService.getStatusWiseQuote(this.quotesInfo).subscribe({
        next: res => {
          this.ngZone.run(() => {
            this.quotesInfo = res;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            this.loading = false;
          });
        },
        error: err => {
          this.loading = false;
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
        }
      }
      )
    );
  }

  CheckExtensionNo(item) {
    const val = parseInt(item, 0);
    if (val === 0) {
      // tslint:disable-next-line:one-line
      this.isExtensionNovalid = true;
    } else {
      this.isExtensionNovalid = false;
    }
  }
  getAllData() {
    this.alldata.sort(function (a, b) {
      // tslint:disable-next-line:curly
      if (a.createdOn < b.createdOn) return -1;
      // tslint:disable-next-line:curly
      if (a.createdOn > b.createdOn) return 1;
      return 0;
    });
  }
  // AccountDetailsThroughId(evt) {
  //   this.accountsName = this.accountsData.filter(
  //     item => item.name === evt.target.value
  //   );
  // }

  deleteContact() {
    const Id = this.route.snapshot.queryParams['id'];
    this.contact.isActive = false;
    this.contact.id = this.contactId;

    this.contactService.deleteContact(this.contact).subscribe(
      response => {
        this._route.navigate(['/contacts/contact-list']);
        // this.toastrService.error(
        //   'Contact deleted successfully',
        //   '',
        //   this.constants.toastrConfig
        // );
        this.loading = false;
      },
      err => {
        // // console.log(err);
        if (err.status === 403) {
          // tslint:disable-next-line:one-line
          // this.toastrService.warning(
          //   'Access denied',
          //   '',
          //   this.constants.toastrConfig
          // );
        }
        this.loading = false;
      }
    );
  }

  // getAccounts() {
  //   this.subscriptions.push(
  //     this.accountsService.getAccountsData().subscribe(responce => {
  //       responce.forEach(element => {
  //         if (element.isActive) {
  //           this.accountsData.push(element);
  //         }
  //       });
  //     })
  //   );
  // }

  // goToDetails(id) {
  //   this._route.navigate(['/quotes/quotes-detail'], {
  //     queryParams: { id: btoa(id) }
  //   });
  // }

  goToDetails(id, ownerId, ConfirmModal) {
    if (ownerId === this.user.id) {
      this._route.navigate(['/quotes/quotes-detail'], {
        queryParams: { id: btoa(id) }
      });
    } else if (
      this.user.role.description === this.constants.Admin ||
      this.user.role.description === this.constants.SuperAdmin
    ) {
      this._route.navigate(['/quotes/quotes-detail'], {
        queryParams: { id: btoa(id) }
      });
    } else {
      this.popupOpen((this.popupFor = 'Can not access others quote'));
      ConfirmModal.show();
    }
  }

  AddQuotes(valid, modal, form) {
    if (valid) {
      if (this.contact.accounts.id) {
        this.isAccount = false;
        this.quote.contact = this.contact;
        this.log.contactId = this.contact.id;
        this.quote.createdOn = new Date();
        this.quote.createdOn = new Date(
          this.quote.createdOn.getUTCFullYear(),
          this.quote.createdOn.getUTCMonth(),
          this.quote.createdOn.getUTCDate(),
          this.quote.createdOn.getUTCHours(),
          this.quote.createdOn.getUTCMinutes(),
          this.quote.createdOn.getUTCSeconds()
        );
        this.quote.expirationDate = this.quote.createdOn;
        this.quote.updatedOn = this.quote.createdOn;
        this.log.name = 'Quote Added:' + ' ' + this.quote.name;
        this.quote.expirationDate.setDate(
          this.quote.expirationDate.getDate() + this.constants.Expairy[0]
        );
        this.quote.expairyDays = this.constants.Expairy[0];
        this.quote.ownerName = this.user.firstName + ' ' + this.user.lastName;
        if (this.company) {
          this.quote.termsandConditions = this.company.termsConditions;
        }
        this.accountsService.AddQuote(this.quote).subscribe(
          response => {
            this.cancelQuotesDetails(form, modal);
            // this.isFormSubmitted1 = false;
            this.ngZone.run(() => {
              // this.toastrService.success(
              //   'Quote added  successfully',
              //   '',
              //   this.constants.toastrConfig
              // );
              modal.hide();
              this.quotes.push(response);
              this._route.navigate(['/quotes/quotes-detail'], {
                queryParams: { id: btoa(response.id) }
              });
            });
            this.addHistory(this.log);
            this.loading = false;
          },
          err => {
            // // console.log(err);
            if (err.status === 403) {
              // tslint:disable-next-line:one-line
              // this.toastrService.warning(
              //   'Access denied',
              //   '',
              //   this.constants.toastrConfig
              // );
            }
            this.loading = false;
          }
        );
      } else {
        this.isAccount = true;
        // this.toastrService.warning(
        //   'Please add account first',
        //   '',
        //   this.constants.toastrConfig
        // );

        this.loading = false;
      }
    }
  }

  cancelQuotesDetails(form, popup) {
    // form.reset();
    // this.isFormSubmitted = false;
    //  this.quote = new Quote();
    popup.hide();
  }

  AddNote(isValid) {
    this.loading = true;
    if (isValid && this.note.description) {
      this.note.createdOn = new Date();
      this.note.updatedBy = this.user.id;
      this.note.updatedOn = new Date();
      this.note.isActive = true;
      // tslint:disable-next-line:curly
      if (this.contact.notes === undefined)
        this.contact.notes = new Array<Notes>();
      this.contact.notes.push(this.note);
      this.note = new Notes();
      this.contactService.UpdateContacts(this.contact).subscribe(response => {
        this.loading = false;
        // this.toastrService.success(
        //   'Note added successfully',
        //   '',
        //   this.constants.toastrConfig
        // );
        this.notes = response.notes;
        this.sortNote();

        this.alldata = [];
        this.notes.forEach(element => {
          this.alldata.push(element);
        });
        this.historyData.forEach(element => {
          this.alldata.push(element);
        });
        this.sortHistory();
        this.getAllData();
      });
    }
    this.loading = false;
  }
  CancelNote() {
    this.note = new Notes();
  }

  editContact() {
    this.edit = true;
    this.show = false;
    this.editbtn = false;
    if (
      (this.contact.faxNo != null && this.contact.faxNo !== '') ||
      (this.contact.homeNo != null && this.contact.homeNo !== '') ||
      (this.contact.extensionNo != null && this.contact.extensionNo !== '')
    ) {
      this.showtext = true;
      this.showallphone = true;
    }
  }

  discard(id) {
    if (
      (this.contact.faxNo != null && this.contact.faxNo !== '') ||
      (this.contact.homeNo != null && this.contact.homeNo !== '') ||
      (this.contact.extensionNo != null && this.contact.extensionNo !== '')
    ) {
      this.showtext = true;
    } else {
      this.showtext = false;
    }
    this.contact = this.oldcontact;
    this.edit = false;
    this.editbtn = true;
    this.show = true;
    this.isEmailRepated = false;
  }
  saveContactName(contact, valid) {
    this.contactDetailsForm = true;
    // this.isFormSubmitted = true;

    if (valid) {
      this.show = true;
      this.contactName = false;
      this.edit = false;
      this.editbtn = true;
      this.contactService.UpdateContactData(contact).subscribe(
        response => {
          if (response === false) {
            this.show = false;
            this.edit = true;
            this.editbtn = false;
            this.isEmailRepated = true;
          } else {
            this.log.contactId = contact.id;

            if (contact.firstName !== this.oldcontact.firstName) {
              this.log.name =
                'Name Modified: From ' +
                ' ' +
                this.oldcontact.firstName +
                ' ' +
                '  to' +
                ' ' +
                contact.firstName;

              this.addHistory(this.log);
            }
            this.isEmailRepated = false;
            this.contactDetailsForm = false;

            this.loading = false;
          }
          this.oldcontact = response;
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
          this.loading = false;
        }
      );
    }
    // this.isFormSubmitted = false;
  }
  saveContact(contact, valid) {
    this.contactDetailsForm = true;
    if (valid && !this.isExtensionNovalid) {
      this.show = true;
      this.phonepattern = false;
      this.edit = false;
      this.editbtn = true;
      this.contactService.UpdateContactData(contact).subscribe(
        response => {
          if (response === false) {
            this.show = false;
            this.edit = true;
            this.editbtn = false;
            this.isEmailRepated = true;
          } else {
            this.contact = response;
            if (contact.extensionNo != null) {
              this.extNo = true;
            }
            this.log.contactId = contact.id;

            if (contact.email !== this.oldcontact.email) {
              this.log.name =
                'Email Modified: From ' +
                ' ' +
                this.oldcontact.email +
                ' ' +
                ' to' +
                ' ' +
                contact.email;

              this.addHistory(this.log);
            }
            if (contact.phone !== this.oldcontact.phone) {
              this.log.name =
                'Work No Modified: From' +
                ' ' +
                this.oldcontact.phone +
                ' ' +
                ' to' +
                ' ' +
                contact.phone;

              this.addHistory(this.log);
            }
            if (contact.extensionNo !== this.oldcontact.extensionNo) {
              if (
                (this.oldcontact.extensionNo == null &&
                  contact.extensionNo != null) ||
                this.oldcontact.extensionNo == ''
              ) {
                this.log.name =
                  ' Extension No Added:  ' + '' + contact.extensionNo;

                this.addHistory(this.log);
                // tslint:disable-next-line:no-unused-expression
              } else if (
                this.oldcontact.extensionNo != null &&
                contact.extensionNo === ''
              ) {
                this.log.name =
                  ' Extension No  ' +
                  '' +
                  this.oldcontact.extensionNo +
                  ' ' +
                  'is Deleted';

                this.addHistory(this.log);
              } else {
                this.log.name =
                  'Extension No Modified: From' +
                  ' ' +
                  this.oldcontact.extensionNo +
                  ' ' +
                  '  to' +
                  ' ' +
                  contact.extensionNo;

                this.addHistory(this.log);
              }
            }
            if (contact.faxNo !== this.oldcontact.faxNo) {
              if (
                (this.oldcontact.faxNo === null && contact.faxNo !== null) ||
                this.oldcontact.faxNo === ''
              ) {
                this.log.name = ' Fax No Added:' + ' ' + contact.faxNo;

                this.addHistory(this.log);
              } else if (
                this.oldcontact.faxNo != null &&
                contact.faxNo === ''
              ) {
                this.log.name =
                  'Fax No' + '' + this.oldcontact.faxNo + ' ' + 'is Deleted';

                this.addHistory(this.log);
              } else {
                this.log.name =
                  'Fax No Modified: From' +
                  ' ' +
                  this.oldcontact.faxNo +
                  ' ' +
                  'to' +
                  ' ' +
                  contact.faxNo;

                this.addHistory(this.log);
              }
            }
            if (contact.homeNo !== this.oldcontact.homeNo) {
              if (
                (this.oldcontact.homeNo === null && contact.homeNo !== null) ||
                this.oldcontact.homeNo === ''
              ) {
                this.log.name = ' Mobile No Added:' + '' + contact.homeNo;

                this.addHistory(this.log);
              } else if (
                this.oldcontact.homeNo != null &&
                contact.homeNo === ''
              ) {
                this.log.name =
                  ' Mobile No' +
                  ' ' +
                  this.oldcontact.homeNo +
                  ' ' +
                  ' is Deleted';

                this.addHistory(this.log);
              } else {
                this.log.name =
                  'Mobile No Modified: From' +
                  ' ' +
                  this.oldcontact.homeNo +
                  ' ' +
                  ' to' +
                  ' ' +
                  contact.homeNo;

                this.addHistory(this.log);
              }
            }

            this.isEmailRepated = false;
            this.contactDetailsForm = false;

            this.loading = false;
          }
          this.oldcontact = contact;
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
          this.loading = false;
        }
      );
    }
  }
  deleteNote(note) {
    // this.notes.splice(index, 1);
    const noteData: number = this.notes.indexOf(note);
    if (noteData !== -1) {
      this.notes.splice(noteData, 1);
    }
    this.contact.notes = this.notes;
    this.accountsService.UpdateContact(this.contact).subscribe(response => {
      this.contact = response;
      this.notes = response.notes;
      this.sortNote();

      this.alldata = [];
      this.notes.forEach(element => {
        this.alldata.push(element);
      });
      this.historyData.forEach(element => {
        this.alldata.push(element);
      });
      this.sortHistory();
      this.getAllData();
    });
  }

  addAccountModal(valid, abc, account, form) {
    debugger
    // this.isFormSubmitted = true;
    // this.accountsData.forEach(element => {
    //   if (element.name === account.name) {
    //     this.accName = true;
    //   }
    // });

    this.accountsService.FindAccount(account.name).subscribe(name => {
      this.ngZone.run(() => {
        this.loading = false;
        if (name === null) {
          if (valid && !this.accName) {
            this.accountsService.addAccounts(account).subscribe(
              response => {
                this.ngZone.run(() => {
                  this.contact.accounts = response;
                  this.accountsService
                    .UpdateContact(this.contact)
                    .subscribe(res => {
                      this._route.navigate(['/accounts/accounts-detail'], {
                        queryParams: { id: btoa(response.id) }
                      });
                    });
                  abc.hide();
                  this.cancelAccoutDetail(form, abc);
                  // this.toastrService.success(
                  //   'Account added  Successfully',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                  this.log.contactId = this.contactId;
                  this.log.accountId = null;
                  this.log.name =
                    'Account Added:' + ' ' + this.contact.accounts.name;
                  this.addHistory(this.log);
                  //   this.isFormSubmitted = false;
                });
              },
              err => {
                // // console.log(err);
                if (err.status === 403) {
                  // tslint:disable-next-line:one-line
                  // this.toastrService.warning(
                  //   'Access denied',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                }
                this.loading = false;
              }
            );
          }
        } else {
          this.accName = true;
        }
      });
    });
  }
  saveAccountOnSelect(e) {
    this.selectedAccount = e.item;
  }

  AddAccount(model) {
    debugger
    this.flag = false;
    let isDuplicate = false;
    this.searchAccountForm = true;
    this.isAccountAlreadyPresent = false;
    // this.accountsData.forEach(element => {
    //   if (element.name === this.SearchText) {
    //     this.flag = true;
    //   }
    // });
    this.accountsService.FindAccount(this.SearchText).subscribe(name => {
      this.ngZone.run(() => {
        this.loading = false;
        if (name) {
          this.flag = true;
        }
        if (this.flag) {
          // this.accountsData.forEach(element => {
          //   if (
          //     element.name !== this.selectedAccount.name &&
          //     this.selectedAccount.id != null
          //   ) {
          //     this.presentAccount = false;
          //     this.isAccountAlreadyPresent = false;
          //   }
          // });
          if (
            this.account.name === this.selectedAccount.name &&
            this.selectedAccount.id != null
          ) {
            this.presentAccount = false;
            this.isAccountAlreadyPresent = false;
          }
          if (this.selectedAccount.name === this.account.name) {
            isDuplicate = true;
            this.ngZone.run(() => {
              this.isAccountAlreadyPresent = true;
              this.selectedAccount = null;
              this.loading = false;
            });
          }

          if (isDuplicate === false) {
            if (this.selectedAccount.name == null) {
              this.ngZone.run(() => {
                this.searchAccountForm = true;
              });
            } else {
              this.accountsService
                .UpdateAccount(this.selectedAccount)
                .subscribe(
                  response => {
                    this.ngZone.run(() => {
                      this.contact.accounts = response;
                      this.account = response;
                      this.accountDataPresent = true;
                      this.updateContact(this.contact);
                      model.hide();
                    });
                  },
                  err => {
                    // // console.log(err);
                    if (err.status === 403) {
                    }
                    this.loading = false;
                  }
                );
            }
          } else {
            this.ngZone.run(() => {
              this.isAccountAlreadyPresent = true;
              this.loading = false;
            });
          }
        } else {
          this.presentAccount = true;
        }
      });
    });
  }

  FindAccount() {
    debugger
    this.isAccountAlreadyPresent = false;
    this.selectedAccount = new Account();
    this.contactService.FindAccount(this.SearchText).subscribe(
      res => {
        this.ngZone.run(() => {
          this.accountsList = res;
        });
      },
      err => {
        // // console.log(err);
      }
    );
  }
  goToAccountDetails(id) {
    this._route.navigate(['/accounts/accounts-detail'], {
      queryParams: { id: btoa(id) }
    });
  }
  updateContact(contact) {
    this.loading = true;
    this.accountsService.UpdateContact(contact).subscribe(
      response => {
        this.loading = false;
        this.contact = response;
        this.account = response.accounts;
        this.accountDataPresent = true;
        this.log.contactId = this.contactId;
        this.log.name = 'Account Linked: ' + this.selectedAccount.name;
        this.addHistory(this.log);
        // model.hide();
        this.searchAccountForm = false;
        this.SearchText = null;
      },
      err => {
        // // console.log(err);
        if (err.status === 403) {
          // tslint:disable-next-line:one-line
          // this.toastrService.warning(
          //   'Access denied',
          //   '',
          //   this.constants.toastrConfig
          // );
        }
        this.loading = false;
      }
    );
  }
  cancelAccoutDetail(form, popup) {
    popup.hide();
    this.accName = false;
    //  form.reset();
    //  this.newAccount = new Account();
    // this.isFormSubmitted = false;
    // popup.hide();
  }
  addHistory(history) {
    this.loading = true;
    this.accountsService.postHistory(history).subscribe(activity => {
      this.loading = false;
      this.activity = activity;
      // this.notes = response.notes;
      this.oldHistoryData.push(activity);
      this.historyData = this.oldHistoryData;
      this.sortHistory();
      this.alldata = [];
      this.oldHistoryData.forEach(element => {
        this.alldata.push(element);
      });
      this.notes.forEach(element => {
        this.alldata.push(element);
      });
      this.getAllData();
    });
  }
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
    this.getQuotes(0);
  }
  openNewTab(event, url) {
    const proto = url.match(/^([a-z0-9]+):\/\//);
    if (proto === null) {
      const website = 'https://' + url;
      window.open(website, '_blank');
    } else {
      window.open(url, '_blank');
    }
  }
  showMsg(msg) {
    if (msg == 'account') {
      this.msg = ' Please add account first';
      this.showBtn = false;
    } else if (msg == 'note') {
      this.msg = 'Are you sure you want to delete this note?';
      this.showBtn = true;
    }
  }

  // messages display
  popupOpen(popupFor) {
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.messageToDisplay = null;
    this.typeofMessage = 'Error';
    if (this.popupFor === 'deleteContact') {
      this.notification = ' Are you sure you want to delete this contact?';
      this.showButtons = true;
    } else if (this.popupFor === 'deleteNote') {
      this.typeofMessage = 'Error';
      this.notification = 'Are you sure you want to delete this note?';
      this.showButtons = true;
    } else if (this.popupFor === 'quoteforaccount') {
      this.typeofMessage = 'Error';
      this.notification = '* Please add account first';
      this.showButtons = false;
    } else if (this.popupFor === 'contact') {
      this.typeofMessage = 'Error';
      this.notification = 'Are you sure you want to delete this contact?';
      this.showButtons = true;
    } else if (this.popupFor === 'Can not access others quote') {
      this.typeofMessage = 'Error';
      this.showButtons = false;
      this.notification =
        'Access Denied! You are not the quote owner. For access, please see your administrator.';
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === 'deleteContact') {
      this.deleteContact();
      ConfirmModal.hide();
    } else if (this.popupFor === 'deleteNote') {
      this.deleteNote(this.noteIndex);
      ConfirmModal.hide();
    } else if (this.popupFor === 'quoteforaccount') {
      //   this.RemoveContact(this.contactItem);
      ConfirmModal.hide();
    } else if (this.popupFor === 'contact') {
      // this.RemoveContact(this.contactItem);
      ConfirmModal.hide();
    }
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }

  goToQuoteList(status) {
    this._route.navigate(['/quotes/quotes-list'], {
      queryParams: { status: btoa(status), contactId: btoa(this.contactId) }
    });
  }
}
