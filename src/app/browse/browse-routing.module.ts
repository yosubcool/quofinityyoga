import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowseComponent } from './browse/browse.component';

const routes: Routes = [
  {
    path: '',
    // canActivate: [RouteGaurd],
    data: {
      title: 'Browse'
      // isLogin: false
    },
    children: [
      {
        path: 'browse',
        component: BrowseComponent,
        data: {
          title: 'browse product/kit'
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrowseRoutingModule {}
