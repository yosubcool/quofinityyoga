import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowseRoutingModule } from './browse-routing.module';
import { BrowseComponent } from './browse/browse.component';
import { BrowseService } from './browse-service';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxLoadingModule } from 'ngx-loading';
import { CommonTemplateModule } from '../common/common-template.module';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { QuotesService } from '../quotes/quotes-service';
// import { SlimScrollModule } from "ng2-slimscroll";
import { SharedModule } from '../_shared/shared.module';
import { KitsService } from '../kits/kits-service';
import { ProductsService } from '../products/products-service';

@NgModule({
  imports: [
    CommonModule,
    BrowseRoutingModule,
    FormsModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    FormsModule,
    NgxLoadingModule,
    CommonTemplateModule,
    TypeaheadModule.forRoot(),
    SharedModule
  ],
  declarations: [BrowseComponent],
  providers: [BrowseService, QuotesService, KitsService, ProductsService],
  exports: [BrowseComponent]
})
export class BrowseModule { }
