import {
  Component,
  OnInit,
  NgZone,
  ViewChild,
  Input,
  OnDestroy,
  Output,
  EventEmitter,
} from "@angular/core";
import { BrowseService } from "../browse-service";
import { ToastrService } from "ngx-toastr";
import {
  Categories,
  Tags,
  SubCategories,
} from "../../_shared/model/tenantMaster-model";
import { CommonTemplateService } from "../../common/common-template.service";
import { ActivatedRoute, Route, Router } from "@angular/router";

import { Kit } from "../../_shared/model/kit-model";
import { Product } from "../../_shared/model/product-model";
import { KitsService } from "src/app/kits/kits-service";
import { ProductsService } from "src/app/products/products-service";
import { QuotesService } from "src/app/quotes/quotes-service";
import { Constants } from "src/app/_shared/constant";
import { Pagination } from "src/app/_shared/model/pagination-model";
import { ProductDetail, KitDetail } from "src/app/_shared/model/productDetail-model";
import { Quote } from "src/app/_shared/model/quote-model";
import { Subscription } from "rxjs";
declare var $: any;
@Component({
  selector: "app-browse",
  templateUrl: "./browse.component.html",
  styleUrls: ["./browse.component.scss"],
})
export class BrowseComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;
  arrr = [];
  // @ViewChild('myModal')
  // bsModal: BsModalComponent;
  shoProduct = false;
  @Input("quote") quote = new Quote();
  @Input("kit") kit = new Kit();
  @Input() isEdit: boolean;
  @Output() quoteData: EventEmitter<any> = new EventEmitter<any>();
  @Output() quoteTitle: EventEmitter<any> = new EventEmitter<any>();
  clearManufacturer = false;
  clearFindKit = false;
  clearFindProduct = false;
  ClearTag = false;
  subscriptions: Subscription[] = [];
  showKitDetail = false;
  isMobileView = false;
  showProductDetail = false;
  kitCount: any;
  quoteCount: any;
  formProductDetails: any;
  description: any;
  images: any[];
  productDetails: any;
  kitDetails: any;
  product: any;
  kits: Pagination;
  products: Pagination;
  constants: any;
  categories: any = [];
  subcategories: any = [];
  kitCategories: any = [];
  kitSubCategories: any = [];
  tag: Tags;
  tagListToSelect: any = [];
  tagList: any = [];
  KitTagList: any = [];
  ManufacturerProduct: any = [];
  quoteId: string;
  kitId: string;
  quotesInfo: Quote;
  kitInfo: Kit;
  isProductSearch: boolean;
  loading: boolean;
  ProductQuantity: number;
  KitQuantity: number;
  urls: string;
  // mobKitProductItemisClicked = false;
  // mobKitisClicked = false;
  // isKitClicked = false;
  // isKitProductClicked = false;
  // isKitProductItemClicked = false;
  // mobKitProductisClicked = false;
  // isProductClicked = false;
  // mobProductisClicked = false;
  constructor(
    private _BrowseService: BrowseService,
    private _quotesService: QuotesService,
    private _KitsService: KitsService,
    private productsService: ProductsService,
    private _CommonTemplateService: CommonTemplateService,
    private route: ActivatedRoute,
    private ngZone: NgZone,
    private _toastrService: ToastrService,
    private router: Router
  ) {
    this.products = new Pagination();
    this.products.pageSize = 6;
    this.products.sortType = "Newest";
    this.kits = new Pagination();
    this.kits.pageSize = 6;
    this.kits.sortType = "Newest";
    this.constants = Constants;
    this.tag = new Tags();
    this.tagListToSelect = new Array<Tags>();
    this.tagList = new Array<Tags>();
    this.KitTagList = new Array<Tags>();
    this.quotesInfo = new Quote();
    this.isProductSearch = true;
    this.product = new Product();
    this.kitDetails = new Kit();
  }

  ngOnInit() {
    $(window).scrollTop(0);
    const screensize2 = $(window).width();
    if (screensize2 < 768) {
      this.isMobileView = true;
    } else {
      this.isMobileView = false;
    }
    this.getProducts(0);
    // tslint:disable-next-line:curly
    if (this.quoteId) this.getKits(0);
    this.getCategories();
    this.getKitCategories();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges() {
    if (this.kit !== undefined && this.kit.id) {
      this.kitId = this.kit.id;
      this.kitInfo = this.kit;
    } else if (this.quote !== undefined && this.quote.id) {
      this.quoteId = this.quote.id;
      this.quotesInfo = this.quote;
    }
  }

  clearSearchProduct(property) {
    this.products[property] = null;
    this.kits[property] = null;
    this.getProducts(0);
  }

  clearSearchKit(property) {
    this.kits[property] = null;
    this.getKits(0);
  }

  getQuotes(id) {
    this.subscriptions.push(
      this._quotesService.getQuoteThroughtId(id).subscribe({

        next: (res) => {
          this.ngZone.run(() => {
            this.quotesInfo = res.quote;
            this.quote = this.quotesInfo;
          });
        },
        error: (err) => {
          // // console.log(err);
        }
      }

      )
    );
  }

  getKitDetails(id) {
    this.subscriptions.push(
      this._KitsService.GetKitDetails(id).subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.kitInfo = Response;
            // this.kitCount = this.kitInfo.products.length + this.kitInfo.customItem.length;
          });
        },
        (err) => { }
      )
    );
  }

  UpdateQuote() {
    this.loading = true;
    this.quote = this.quotesInfo;
    this._quotesService.UpdateQuote(this.quotesInfo).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.loading = false;
          this.quoteData.emit(this.quotesInfo.id);
        });
      },
      (err) => {
        // // console.log(err);
      }
    );
  }

  updateKit() {
    this.loading = true;
    this._KitsService.UpdateKit(this.kitInfo).subscribe((Response) => {
      this.ngZone.run(() => {
        this.getKitDetails(Response.id);
        this.loading = false;
        this.quoteData.emit(this.kitInfo.id);
      });
    });
  }

  //AddProduct(modal) { //#Revisit_PreviousCode
  AddProduct() {
    if (this.productDetails.cost > 0) {
      if (this.quoteId) {
        this.loading = true;
        const Prod: ProductDetail = new ProductDetail();
        Prod.product = this.productDetails;
        Prod.quantity = this.ProductQuantity;
        if (this.quotesInfo.products === null) {
          this.quotesInfo.products = new Array<ProductDetail>();
        }
        // this.productDetails.isUsed = this.productDetails.isUsed + 1;
        this.productDetails.isActive = true;
        this.updateProduct(this.productDetails, true);
        this.quotesInfo.products.push(Prod);
        this.showProductDetail = false;
      } else if (this.kitId) {
        this.loading = true;
        const Prod: ProductDetail = new ProductDetail();
        Prod.product = this.productDetails;
        Prod.quantity = this.ProductQuantity;
        if (this.kitInfo.products === null) {
          this.kitInfo.products = new Array<ProductDetail>();
        }
        this.productDetails.isUsed = this.productDetails.isUsed + 1;
        this.updateProduct(this.productDetails, false);
        this.kitInfo.products.push(Prod);
        this.showProductDetail = false;
      } else {
        // this._toastrService.warning(
        //   'Something went wrong please try again!',
        //   '',
        //   this.constants.toastrConfig
        // );
        this.show = true;
        this.popupOpen((this.popupFor = "went wrong"));
      }
      this.ProductQuantity = 1;
      this.show = false;
      this.shoProduct = false;
    } else {
      this.show = true;
      this.shoProduct = true;
      // this.bsModal.open();
      // this._toastrService.warning(
      //   'Please verify product cost and try again.',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.show = true;
      this.popupOpen((this.popupFor = "verify product cost"));
    }
  }

  AddKit() {
    if (this.kitDetails.total > 0) {
      if (this.quoteId) {
        const kit: KitDetail = new KitDetail();
        kit.Kit = this.kitDetails;
        kit.quantity = this.KitQuantity;
        kit.total = this.kitDetails.total;
        if (this.quotesInfo.kits === null) {
          this.quotesInfo.kits = new Array<KitDetail>();
        }
        this.quotesInfo.kits.push(kit);
        this.UpdateQuote();
        this.showKitDetail = false;
        this.KitQuantity = 1;
      }
    } else {
      this.shoProduct = false;
      this.show = true;
      this.popupOpen((this.popupFor = "verify kit cost"));
    }
  }

  ProductSearch(e: boolean) {
    this.isProductSearch = e;
    this.products.alphabet = "";
    if (e) {
      this.getProducts(0);
    } else {
      this.getKits(0);
    }
  }

  getCategories() {
    this.subscriptions.push(
      this._BrowseService.getAllCategories().subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.categories = Response;
          });
        },
        (err) => { }
      )
    );
  }

  getKitCategories() {
    this.subscriptions.push(
      this._BrowseService.getAllCategories().subscribe(
        (Response) => {
          this.ngZone.run(() => {
            this.kitCategories = Response;
          });
        },
        (err) => { }
      )
    );
  }

  pageChangedProduct(e) {
    this.getProducts(e);
  }

  pageChangedKit(e) {
    this.getKits(e);
  }

  getProducts(pageNo: number) {
    this.loading = true;
    this.products.pageNumber = pageNo;
    // this.products.manufacturer = 'Apple';
    this._BrowseService.BrowseProduct(this.products).subscribe(
      (Response) => {
        this.ngZone.run(() => {
          Response.category = this.products.category;
          Response.subCategory = this.products.subCategory;
          Response.tags = this.products.tags;
          Response.alphabet = this.products.alphabet;
          this.products = Response;
          this.loading = false;
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  getKits(pageNo: number) {
    this.kits.pageNumber = pageNo;
    this._BrowseService.BrowseKits(this.kits).subscribe(
      (Response) => {
        this.ngZone.run(() => {
          Response.category = this.kits.category;
          Response.subCategory = this.kits.subCategory;
          Response.tags = this.kits.tags;
          Response.alphabet = this.kits.alphabet;
          this.kits = Response;
        });
      },
      (err) => { }
    );
  }

  viewDetail(id) {
    this.subscriptions.push(
      this._BrowseService.GetProductById(id).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.product = res;
            this.showProductDetail = true;
            this.productDetails = res;
            this.ProductQuantity = 1;
          });
        },
        (err) => { }
      )
    );
  }

  viewKitDetail(kit) {
    this.showKitDetail = true;
    this.kitDetails = kit;
    this.KitQuantity = 1;
  }

  onKitModelClose() {
    this.kitDetails = {};
  }

  KitCategoryCheck(e, cat) {
    if (e) {
      // tslint:disable-next-line:curly
      if (this.kits.category === null || this.kits.category === undefined)
        this.kits.category = new Array<string>();
      this.kits.category.push(cat);
    } else {
      // tslint:disable-next-line:one-line
      for (let index = 0; index < this.kits.category.length; index++) {
        const element = this.kits.category[index];
        if (element === cat) {
          this.kits.category.splice(index, 1);
          const sublist = this.kitSubCategories.filter(
            (ele) => ele.categories.id === cat
          );
          sublist.forEach((ele) => {
            if (this.kits.subCategory) {
              for (let j = 0; j < this.kits.subCategory.length; j++) {
                const sub = this.kits.subCategory[index];
                if (ele.id === sub) {
                  this.kits.subCategory.splice(j, 1);
                  j--;
                }
              }
            }
          });
        }
      }
    }
    const id = this.kits.category.join();
    // tslint:disable-next-line:curly
    if (id !== "") {
      this.GetSubCategoriesForKit(id);
    } else {
      this.kits.subCategory = new Array<string>();
      this.kitSubCategories = null;
    }
    this.getKits(0);
  }

  KitSubCategoryCheck(e, cat) {
    if (e) {
      // tslint:disable-next-line:curly
      if (this.kits.subCategory === null || this.kits.subCategory === undefined)
        this.kits.subCategory = new Array<string>();
      this.kits.subCategory.push(cat);
    } else {
      // tslint:disable-next-line:one-line
      for (let index = 0; index < this.kits.subCategory.length; index++) {
        const element = this.kits.subCategory[index];
        if (element === cat) {
          this.kits.subCategory.splice(index, 1);
        }
      }
    }
    this.getKits(0);
  }

  GetSubCategoriesForKit(categories) {
    this.subscriptions.push(
      this._CommonTemplateService
        .getElementThroughId("TenantMaster/SubCategories", categories)
        .subscribe(
          (res) => {
            this.ngZone.run(() => {
              this.kitSubCategories = res;
            });
          },
          (err) => {
            // // console.log(err);
          }
        )
    );
  }

  CategoryCheck(e, cat) {
    this.showKitDetail = false;
    this.showProductDetail = false;
    if (e) {
      // tslint:disable-next-line:curly
      if (
        this.products.category === null ||
        this.products.category === undefined
      )
        this.products.category = new Array<string>();
      this.products.category.push(cat);
    } else {
      // tslint:disable-next-line:one-line
      for (let index = 0; index < this.products.category.length; index++) {
        const element = this.products.category[index];
        if (element === cat) {
          this.products.category.splice(index, 1);
          const sublist = this.subcategories.filter(
            (ele) => ele.categories.id === cat
          );
          sublist.forEach((ele) => {
            if (this.products.subCategory) {
              for (let j = 0; j < this.products.subCategory.length; j++) {
                const sub = this.products.subCategory[index];
                if (ele.id === sub) {
                  this.products.subCategory.splice(j, 1);
                  j--;
                }
              }
            }
          });
        }
      }
    }
    const id = this.products.category.join();
    if (id !== "") {
      this.GetSubCategoriesForQuote(id);
    } else {
      this.products.subCategory = new Array<string>();
      this.subcategories = null;
    }
    this.getProducts(0);
  }

  SubCategoryCheck(e, cat) {
    if (e) {
      // tslint:disable-next-line:curly
      if (
        this.products.subCategory === null ||
        this.products.subCategory === undefined
      )
        this.products.subCategory = new Array<string>();
      this.products.subCategory.push(cat);
    } else {
      // tslint:disable-next-line:one-line
      for (let index = 0; index < this.products.subCategory.length; index++) {
        const element = this.products.subCategory[index];
        if (element === cat) {
          this.products.subCategory.splice(index, 1);
        }
      }
    }
    this.getProducts(0);
  }

  GetSubCategoriesForQuote(categories) {
    this.subscriptions.push(
      this._CommonTemplateService
        .getElementThroughId("TenantMaster/SubCategories", categories)
        .subscribe(
          (res) => {
            this.ngZone.run(() => {
              this.subcategories = res;
            });
          },
          (err) => {
            // // console.log(err);
          }
        )
    );
  }

  TagsAdd(e, tag) {
    if (e) {
      // tslint:disable-next-line:curly
      if (this.products.tags === null || this.products.tags === undefined)
        this.products.tags = new Array<string>();
      this.products.tags.push(tag.description);
      this.tagList.push(tag);
      this.tag = new Tags();
    } else {
      // tslint:disable-next-line:one-line
      for (let index = 0; index < this.products.tags.length; index++) {
        const element = this.products.tags[index];
        if (element === tag.description) {
          this.products.tags.splice(index, 1);
        }
      }
      for (let index = 0; index < this.tagList.length; index++) {
        const element = this.tagList[index];
        if (element.description === tag.description) {
          this.tagList.splice(index, 1);
        }
      }
    }
    this.getProducts(0);
  }

  KitsTagsAdd(e, tag) {
    if (e) {
      // tslint:disable-next-line:curly
      if (this.kits.tags === null || this.kits.tags === undefined)
        this.kits.tags = new Array<string>();
      this.kits.tags.push(tag.description);
      this.KitTagList.push(tag);
      this.tag = new Tags();
    } else {
      // tslint:disable-next-line:one-line
      for (let index = 0; index < this.kits.tags.length; index++) {
        const element = this.kits.tags[index];
        if (element === tag.description) {
          this.kits.tags.splice(index, 1);
        }
      }
      for (let index = 0; index < this.KitTagList.length; index++) {
        const element = this.KitTagList[index];
        if (element.description === tag.description) {
          this.KitTagList.splice(index, 1);
        }
      }
    }
    this.getKits(0);
  }

  findTag(searchText) {
    this.tag.description = searchText;
    this._CommonTemplateService.findTag(this.tag).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.tagListToSelect = res;
        });
      },
      (err) => {
        // // console.log(err);
      }
    );
  }

  FindManufacturer(searchText) {
    if (searchText !== null) {
      this._BrowseService.FindManufacturer(searchText).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.ManufacturerProduct = res;
          });
        },
        (err) => {
          // // console.log(err);
        }
      );
    } else {
      // tslint:disable-next-line:one-line
      this.getProducts(0);
    }
  }

  GetCategoryName(id) {
    let name = "";
    if (id) {
      const obj = this.categories.filter((x) => x.id === id)[0];
      name = obj.description;
    }
    return name;
  }

  GetSubCategoryName(id) {
    let name = "";
    if (id) {
      const obj = this.subcategories.filter((x) => x.id === id)[0];
      name = obj.description;
    }
    return name;
  }

  GetKitsCategoryName(id) {
    let name = "";
    if (id) {
      const obj = this.kitCategories.filter((x) => x.id === id)[0];
      name = obj.description;
    }
    return name;
  }

  GetKitsSubCategoryName(id) {
    let name = "";
    if (id) {
      const obj = this.kitSubCategories.filter((x) => x.id === id)[0];
      name = obj.description;
    }
    return name;
  }

  goToKit() {
    // this.loading = true; // Loader Enable
    this.router.navigate(["/kits/kits-detail"], {
      queryParams: { id: btoa(this.kitId) },
    });
    // this.loading = false; // Loader Disable
  }

  goToQuote() {
    this.router.navigate(["/quotes/quotes-detail"], {
      queryParams: { id: btoa(this.quoteId) },
    });
  }

  updateProduct(product, isQuote?) {
    this.loading = true;
    this.productsService.updateElement("Products", product).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            if (isQuote) {
              this.UpdateQuote();
            } else {
              this.updateKit();
            }
            // this._toastrService.success(
            //   'Product added successfully!',
            //   '',
            //   this.constants.toastrConfig
            // );
            // this.loading = false;
          }
        });
      },
      (err) => { }
    );
  }

  onModelClose() {
    this.productDetails = {};
    this.product = {};
    this.showKitDetail = false;
    this.showProductDetail = false;
    this.products.category = new Array<string>();
    this.products.subCategory = new Array<string>();
    this.products.tags = new Array<string>();
    this.products.manufacturer = "";
    this.products.alphabet = "";
    this.subcategories = Array<SubCategories>();
    this.kitSubCategories = Array<SubCategories>();
    this.kits.category = new Array<string>();
    this.kits.subCategory = new Array<string>();
    this.kits.tags = new Array<string>();
    this.kits.manufacturer = "";
    this.kits.alphabet = "";
    if (!this.isProductSearch) {
      document.getElementById("products-tab").classList.add("active");
      document.getElementById("kits-tab").classList.remove("active");
    }
    this.isProductSearch = false;
    this.isProductSearch = true;
    this.ngOnInit();
  }

  onChangeTab() {
    this.showKitDetail = false;
    this.showProductDetail = false;
    this.kitDetails = {};
    this.productDetails = {};
    this.products.category = new Array<string>();
    this.products.subCategory = new Array<string>();
    this.products.tags = new Array<string>();
    this.products.manufacturer = "";
    this.subcategories = Array<SubCategories>();
    this.kitSubCategories = Array<SubCategories>();
    this.kits.category = new Array<string>();
    this.kits.subCategory = new Array<string>();
    this.kits.tags = new Array<string>();
    this.kits.manufacturer = "";
  }

  categoriesChecked(val, cat) {
    if (val === "P" && this.products.category) {
      const data = this.products.category.filter((ele) => ele === cat.id)[0];
      // tslint:disable-next-line:curly
      if (data) return true;
      // tslint:disable-next-line:curly
      else return false;
    } else if (val === "K" && this.kits.category) {
      const data = this.kits.category.filter((ele) => ele === cat.id)[0];
      // tslint:disable-next-line:curly
      if (data) return true;
      // tslint:disable-next-line:curly
      else return false;
    }
    return false; //#revisit_CommentForV1
  }

  subCategoriesChecked(val, cat) {
    if (val === "P" && this.products.subCategory) {
      const data = this.products.subCategory.filter((ele) => ele === cat.id)[0];
      // tslint:disable-next-line:curly
      if (data) return true;
      // tslint:disable-next-line:curly
      else return false;
    } else if (val === "K" && this.kits.subCategory) {
      const data = this.kits.subCategory.filter((ele) => ele === cat.id)[0];
      // tslint:disable-next-line:curly
      if (data) return true;
      // tslint:disable-next-line:curly
      else return false;
    }
    return false; //#revisit_CommentForV1
  }

  updateQuantity(item, type) {
    if (type === "K") {
      this.KitQuantity = parseInt(item, 0) > 0 ? parseInt(item, 0) : 1;
    } else {
      this.ProductQuantity =
        this.productDetails.productType.description === "Material"
          ? parseInt(item, 0) > 0
            ? parseInt(item, 0)
            : 1
          : item > 0
            ? item
            : 1;
    }

    // const val = parseInt(item, 0);
    // if (val > 0) {
    // } else {
    //   // tslint:disable-next-line:curly
    //   if (type === 'K')
    //     this.KitQuantity = 1;
    //   // tslint:disable-next-line:curly
    //   else
    //     this.ProductQuantity = 1;
    // }
  }

  // messages displayfor toasters
  popupOpen(popupFor) {
    if (this.popupFor === "went wrong") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Something went wrong please try again!";
    } else if (this.popupFor === "verify product cost") {
      this.typeofMessage = "Error";
      this.notification = "Please verify product cost and try again";
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.messageToDisplay = null;
    } else if (this.popupFor === "verify kit cost") {
      this.typeofMessage = "Error";
      this.notification = "Please verify kit cost and try again";
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.messageToDisplay = null;
    }
  }

  yes(ConfirmModal) {
    ConfirmModal.hide();
    this.show = false;
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }

  getTotalKitCostByAllProducts() {
    let total = 0;
    this.kitDetails
      ? this.kitDetails.products
        ? this.kitDetails.products.forEach((element) => {
          total = total + element.product.cost * element.quantity;
          // tslint:disable-next-line:no-unused-expression
        })
        : null
      : null;
    return total;
  }
  validateLaborQuantity(evt) {
    if (evt.target.value.split(".").length >= 2) {
      evt.target.value =
        evt.target.value.split(".")[0] +
        "." +
        evt.target.value.split(".")[1].substring(0, 2);
      this.ProductQuantity = evt.target.value;
    }
  }

  goToUrlReferenceUrl(url) {
    this.urls = url;
  }

  openNewTab(event, url) {
    const proto = url.match(/^([a-z0-9]+):\/\//);
    if (proto === null) {
      const website = "https://" + url;
      window.open(website, "_blank");
    } else {
      window.open(url, "_blank");
    }
  }
}
