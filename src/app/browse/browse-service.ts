import { Injectable, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs';
import { DataService } from '../_core/data.service';
import { Kit } from '../_shared/model/kit-model';
import { Pagination } from '../_shared/model/pagination-model';

@Injectable()
export class BrowseService {
  pagination: Pagination
  constructor(private dataService: DataService) {
    this.pagination = new Pagination();
  }

  BrowseProduct(page) {
    return this.dataService.getElementThroughAnyObject('Products/BrowseProduct', page).pipe(map(Response => {
      return Response;
    }));
  }


  BrowseKits(page) {
    return this.dataService.getElementThroughAnyObject('Kit/BrowseKit', page).pipe(map(Response => {
      return Response;
    }));
  }

  getAllCategories() {
    this.pagination.alphabet = 'All';
    return this.dataService.getElementThroughAnyObject('TenantMaster/GetAllCategories', this.pagination).pipe(map(response => {
      return response.records;
    }));
  }

  GetProductById(Id: string) {
    return this.dataService.getData('Products/' + Id).pipe(map(response => {
      return response;
    }));
  }

  FindManufacturer(Id) {
    return this.dataService.getElementThroughId('Products/FindManufacturer', Id).pipe(map(response => {
      return response;
    }));
  }

}
