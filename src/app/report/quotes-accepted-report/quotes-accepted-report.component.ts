import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  ViewChild
} from '@angular/core';
import * as c3 from 'c3';
import * as moment from 'moment/moment';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerComponent } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
// import { Overlay } from 'toastr-ng2'; //#Revisit_IgnoredAsNotUsed
import { ReportService } from '../report-service';
declare var $: any;
@Component({
  selector: 'app-quotes-accepted-report',
  templateUrl: './quotes-accepted-report.component.html',
  styles: [
    `
      :host >>> #chart svg rect.overlay {
        width: 0 !important;
        height: 0 !important;
      }
      :host >>> #chart svg  .c3-grid line.c3-xgrid-focus {
        stroke: transparent;
      }
    `
  ]
})
export class QuotesAcceptedReportComponent implements OnInit {
  @Input('Users') Users: any;
  toDate = moment();
  fromDate = moment();
  filterData: any;
  allAcceptedQuotes: any;
  UserNames: Array<string>;
  subscriptions: any;
  loading: boolean;
  @ViewChild(DaterangepickerComponent) private picker: any;
  constructor(
    private reportService: ReportService,
    private daterangepickerOptions: DaterangepickerConfig
  ) { }

  ngOnInit() {
    $(window).scrollTop(0);
    $('rect').remove('.overlay');
    $('svg g rect').removeClass('selection');
    $('overlay').css({ width: '0', height: '0' });
    this.daterangepickerOptions.settings = {
      locale: { format: 'YYYY-MM-DD' },
      alwaysShowCalendars: false,
      startDate: this.fromDate.subtract(30, 'day'),
      endDate: this.toDate,
      maxDate: this.toDate,
      opens: 'left',
      ranges: {
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [
          moment()
            .subtract(moment().dates() - 1, 'day')
            .subtract(1, 'month'),
          moment().subtract(moment().dates(), 'day')
        ],
        // 'Last 3 Months': [moment().subtract(3, 'month'), moment()],
        Q1: [
          moment()
            .year(moment().year())
            .month(0)
            .date(1),
          moment()
            .year(moment().year())
            .month(2)
            .date(31)
        ],
        Q2: [
          moment()
            .year(moment().year())
            .month(3)
            .date(1),
          moment()
            .year(moment().year())
            .month(5)
            .date(30)
        ],
        Q3: [
          moment()
            .year(moment().year())
            .month(6)
            .date(1),
          moment()
            .year(moment().year())
            .month(8)
            .date(30)
        ],
        Q4: [
          moment()
            .year(moment().year())
            .month(9)
            .date(1),
          moment()
            .year(moment().year())
            .month(11)
            .date(31)
        ],
        'This Year': [
          moment()
            .subtract(moment().months(), 'month')
            .subtract(moment().dates() - 1, 'day'),
          moment()
        ],
        'Last Year': [
          moment()
            .subtract(moment().months(), 'month')
            .subtract(moment().dates() - 1, 'day')
            .subtract(1, 'year'),
          moment()
            .subtract(moment().months(), 'month')
            .subtract(moment().dates(), 'day')
        ]
      }
    };
    this.filterData = {
      Status: 'Accepted',
      FromDate: this.fromDate,
      ToDate: this.toDate
    };
    this.UserNames = new Array<string>();
    this.getData();
  }
  loadChart() {
    const chart = c3.generate({
      data: {
        json: this.allAcceptedQuotes,
        type: 'bar',
        keys: {
          value: ['Total Quotes Accepted']
        },
        colors: {
          'Total Quotes Accepted': '#00711a'
        }
      },

      grid: {
        x: {
          show: true
        },
        y: {
          show: true
        }
      },
      bar: {
        width: {
          ratio: 0.25 // this makes bar width 50% of length between ticks
        }
        // width : 30
      },
      axis: {
        x: {
          type: 'category',
          categories: this.UserNames
        },
        rotated: true
      }
    });
  }

  someFunction(event) {
    if (event.picker.chosenLabel !== 'Custom Range') {
      this.picker.input.nativeElement.value = event.picker.chosenLabel;
    }
  }

  getData(value?: any) {
    this.loading = true;
    this.filterData.FromDate = value
      ? value.start._d
      : this.filterData.FromDate;
    this.filterData.ToDate = value ? value.end._d : this.filterData.ToDate;
    this.subscriptions = this.reportService
      .GetAcceptedQuotesByUser(this.filterData)
      .subscribe(data => {
        const arr = [];
        data.forEach(element => {
          arr.push(JSON.parse(element));
        });
        this.allAcceptedQuotes = arr;
        this.assignUsers();
        this.loadChart();
        this.loading = false;
      });
  }

  assignUsers(): any {
    this.allAcceptedQuotes.forEach(element => {
      this.Users.filter(
        v => (v.id === element._id ? (element['UserName'] = v.FullName) : '')
      );
      this.UserNames.push(element.UserName);
    });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
