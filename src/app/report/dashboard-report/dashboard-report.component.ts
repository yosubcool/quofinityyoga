import { Component, OnInit, Input, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import * as moment from 'moment/moment';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerComponent } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { ReportService } from '../report-service';
declare var $: any;
@Component({
  selector: 'app-dashboard-report',
  templateUrl: './dashboard-report.component.html',
  styleUrls: ['./dashboard-report.component.scss']
})
export class DashboardReportComponent implements OnInit {
  subscriptions: Subscription[] = [];
  @Input('Users')
  Users: any;
  QuoteStatus = [];
  filterData: { UserId: number; FromDate: any; ToDate: any };
  MostQuotesSentByUserId: {
    UserId: number;
    UserName: string;
    NumberOfQuotesSent: number;
  };
  TotalQuotesSent: {
    TotalQuotesSent: number;
    Accepted: number;
    TotalRevenue: number;
    AcceptedRevenue: number;
  };
  toDate = moment();
  fromDate = moment().subtract(1, 'month');
  UserId: number = 0;
  TopPerformer: {
    TotalQuotesAccepted: number;
    TotalRevenue: number;
    UserId: number;
    UserName: any;
  };
  TopPerformerFilter: { Status: string; FromDate: any; ToDate: any };

  loading: boolean;
  @ViewChild(DaterangepickerComponent)
  private picker: any;

  constructor(
    private reportService: ReportService,
    private daterangepickerOptions: DaterangepickerConfig
  ) { }

  ngOnInit() {
    $(window).scrollTop(0);
    this.daterangepickerOptions.settings = {
      locale: { format: 'YYYY-MM-DD' },
      alwaysShowCalendars: false,
      startDate: this.fromDate,
      endDate: this.toDate,
      maxDate: this.toDate,
      opens: 'left',
      ranges: {
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [
          moment()
            .subtract(moment().date() - 1, 'day')
            .subtract(1, 'month'),
          moment().subtract(moment().date(), 'day')
        ],
        // 'Last 3 Months': [moment().subtract(3, 'month'), moment()],
        Q1: [
          moment()
            .year(moment().year())
            .month(0)
            .date(1),
          moment()
            .year(moment().year())
            .month(2)
            .date(31)
        ],
        Q2: [
          moment()
            .year(moment().year())
            .month(3)
            .date(1),
          moment()
            .year(moment().year())
            .month(5)
            .date(30)
        ],
        Q3: [
          moment()
            .year(moment().year())
            .month(6)
            .date(1),
          moment()
            .year(moment().year())
            .month(8)
            .date(30)
        ],
        Q4: [
          moment()
            .year(moment().year())
            .month(9)
            .date(1),
          moment()
            .year(moment().year())
            .month(11)
            .date(31)
        ],
        'This Year': [
          moment()
            .subtract(moment().month(), 'month')
            .subtract(moment().date() - 1, 'day'),
          moment()
        ],
        'Last Year': [
          moment()
            .subtract(moment().month(), 'month')
            .subtract(moment().date() - 1, 'day')
            .subtract(1, 'year'),
          moment()
            .subtract(moment().month(), 'month')
            .subtract(moment().date(), 'day')
        ]
      }
    };
    this.filterData = {
      UserId: this.UserId | 0,
      FromDate: this.fromDate,
      ToDate: this.toDate
    };
    this.TopPerformerFilter = {
      Status: 'Accepted',
      FromDate: this.fromDate,
      ToDate: this.toDate
    };
    this.getData();
  }

  initializeData() {
    this.MostQuotesSentByUserId = {
      UserId: null,
      UserName: '',
      NumberOfQuotesSent: null
    };
    this.TotalQuotesSent = {
      TotalQuotesSent: 0,
      Accepted: 0,
      TotalRevenue: 0,
      AcceptedRevenue: 0
    };
    this.TopPerformer = {
      TotalQuotesAccepted: 0,
      TotalRevenue: 0,
      UserId: 0,
      UserName: null
    };
  }

  someFunction(event) {
    if (event.picker.chosenLabel !== 'Custom Range')
      this.picker.input.nativeElement.value = event.picker.chosenLabel;
  }
  getData(value?: any) {
    this.loading = true;
    this.initializeData();
    this.filterData.UserId = this.UserId;
    this.TopPerformerFilter.FromDate = this.filterData.FromDate = value
      ? value.start._d
      : this.filterData.FromDate;
    this.TopPerformerFilter.ToDate = this.filterData.ToDate = value
      ? value.end._d
      : this.filterData.ToDate;
    this.subscriptions.push(
      this.reportService.GetQuotesByStatus(this.filterData).subscribe(data => {
        const arr = [];
        data.forEach(element => {
          arr.push(JSON.parse(element));
        });
        this.QuoteStatus = arr;
        if (this.QuoteStatus.filter(v => v._id == 'Rejected').length == 0)
          this.QuoteStatus.push({
            _id: 'Rejected',
            TotalQuoteValue: 0,
            AverageQuoteValue: 0,
            TotalQuotes: 0
          });
        if (this.QuoteStatus.filter(v => v._id == 'Expired').length == 0)
          this.QuoteStatus.push({
            _id: 'Expired',
            TotalQuoteValue: 0,
            AverageQuoteValue: 0,
            TotalQuotes: 0
          });
        this.fetchData();
        this.loading = false;
      })
    );

    this.subscriptions.push(
      this.reportService.GetTopPerformer(this.filterData).subscribe(data => {
        const arr = [];
        data.forEach(element => {
          arr.push(JSON.parse(element));
        });
        if (arr.length > 0) {
          this.MostQuotesSentByUserId.UserId = arr[0]._id;
          this.MostQuotesSentByUserId.UserName = this.getUserName(
            this.MostQuotesSentByUserId.UserId
          );
          this.MostQuotesSentByUserId.NumberOfQuotesSent = arr[0].TotalQuotes;
        }
        this.loading = false;
      })
    );
    this.subscriptions.push(
      this.reportService
        .GetQuotesByUser(this.TopPerformerFilter)
        .subscribe(data => {
          const arr = [];
          data.forEach(element => {
            arr.push(JSON.parse(element));
          });
          if (arr.length > 0) {
            this.TopPerformer.UserId = arr[0]._id;
            this.TopPerformer.UserName = this.getUserName(
              this.TopPerformer.UserId
            );
            this.TopPerformer.TotalQuotesAccepted = arr[0].TotalQuotes;
            this.TopPerformer.TotalRevenue = arr[0].TotalQuoteValue;
          }
          this.loading = false;
        })
    );
  }
  fetchData() {
    if (this.QuoteStatus.length > 0) {
      this.TotalQuotesSent.TotalQuotesSent = this.QuoteStatus.reduce(function (
        prev,
        cur
      ) {
        return prev + cur.TotalQuotes;
      },
        0);

      this.QuoteStatus.forEach(element => {
        this.TotalQuotesSent.TotalRevenue += element.TotalQuoteValue;
        if (element._id === 'Accepted') {
          this.TotalQuotesSent.Accepted = element.TotalQuotes;
          this.TotalQuotesSent.AcceptedRevenue = element.TotalQuoteValue;
        }
      });
    }
  }
  getUserName(userId: number): string {
    if (this.Users.find(function (element) {
      return element.id === userId;
    })
    ) {
      return this.Users.find(function (element) {
        return element.id === userId;
      }).FullName;
    }
    else {
      console.log("UserName not found");
      return "";
    }
  }
  // ngOnDestroy(){
  //   this.subscriptions.forEach(element => {
  //     element.unsubscribe();
  //   });
  // }
}
