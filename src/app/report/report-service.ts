

import { Injectable } from '@angular/core';
import { DataService } from '../_core/data.service';
import { map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ReportService {
  constructor(private _http: HttpClient, private dataService: DataService) {

  }

  getUsers() {
    return this.dataService.getData('Report/Get').pipe(map(response => {
      return response;
    }));
  }
  GetQuotesByStatus(data) {
    return this.dataService.postElement('Report/GetQuotesByStatus', data).pipe(map(response => {
      return response;
    }));
  }
  GetQuotesByUser(data) {
    return this.dataService.postElement('Report/GetQuotesByUser', data).pipe(map(response => {
      return response;
    }));
  }
  GetTopPerformer(data) {
    return this.dataService.postElement('Report/GetTopPerformer', data).pipe(map(response => {
      return response;
    }));
  }
  GetHistoryByUserId(userId: string) {
    return this.dataService.postElement('Report/GetHistoryByUserId', userId).pipe(map(response => {
      return response;
    }));
  }
  GetAccountsByUserId(userId: string) {
    return this.dataService.postElement('Report/GetAccountsByUserId', userId).pipe(map(response => {
      return response;
    }));
  }
  GetContactsByUserId(userId: string) {
    return this.dataService.postElement('Report/GetContactsByUserId', userId).pipe(map(response => {
      return response;
    }));
  }
  getModuleAccess() {
    return this.dataService.getData('TenantMaster/GetAccessRoleMatrix/').pipe(map(response => {
      return response;
    }));
  }

  GetRjectedQuotesByUser(data) {
    return this.dataService.postElement('Report/GetRejectedQuotesByUser', data).pipe(map(response => {
      return response;
    }));

  }
  GetAcceptedQuotesByUser(data) {
    return this.dataService.postElement('Report/GetAcceptedQuotesByUser', data).pipe(map(response => {
      return response;
    }));
  }

}
