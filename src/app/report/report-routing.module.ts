import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { RouteGaurd } from '../_core/RouteGaurd';
import { DashboardReportComponent } from './dashboard-report/dashboard-report.component';
import { QuotesAcceptedReportComponent } from './quotes-accepted-report/quotes-accepted-report.component';
import { QuotesRejectedReportComponent } from './quotes-rejected-report/quotes-rejected-report.component';
import { ReportComponent } from './report.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    component: ReportComponent,
    data: {
      title: 'Reports',
      isLogin: true
    },
    children: [
      {
        path: 'dashboard-report',
        component: DashboardReportComponent
      },
      {
        path: 'quotes-accepted-report',
        component: QuotesAcceptedReportComponent
      },
      {
        path: 'quotes-accepted-report',
        component: QuotesRejectedReportComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ReportRoutingModule { }
