import { Component, OnInit, OnDestroy, Input, ViewChild } from '@angular/core';
import * as c3 from 'c3';
declare var $: any;
import * as moment from 'moment/moment';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerComponent } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { ReportService } from '../report-service';

@Component({
  selector: 'app-quotes-rejected-report',
  templateUrl: './quotes-rejected-report.component.html',
  styles: [
    `
      :host >>> #chart svg rect.overlay {
        width: 0 !important;
        height: 0 !important;
      }
      :host >>> #chart svg  .c3-grid line.c3-xgrid-focus {
        stroke: transparent;
      }
    `
  ]
})
export class QuotesRejectedReportComponent implements OnInit, OnDestroy {

  @Input('Users') Users: any;
  toDate = moment();
  fromDate = moment();
  filterData: any;
  allRejectedQuotes: any;
  UserNames: Array<string>;
  subscriptions: any;
  loading: boolean;
  @ViewChild(DaterangepickerComponent)
  private picker: any;
  constructor(private reportService: ReportService,
    private daterangepickerOptions: DaterangepickerConfig) { }

  ngOnInit() {
    $(window).scrollTop(0);
    this.daterangepickerOptions.settings = {
      locale: { format: 'YYYY-MM-DD' },
      alwaysShowCalendars: false,
      startDate: this.fromDate.subtract(30, 'day'),
      endDate: this.toDate,
      maxDate: this.toDate,
      opens: 'left',
      ranges: {
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(moment().dates() - 1, 'day').subtract(1, 'month'), moment().subtract(moment().dates(), 'day')],
        // 'Last 3 Months': [moment().subtract(3, 'month'), moment()],
        'Q1': [moment().year(moment().year()).month(0).date(1), moment().year(moment().year()).month(2).date(31)],
        'Q2': [moment().year(moment().year()).month(3).date(1), moment().year(moment().year()).month(5).date(30)],
        'Q3': [moment().year(moment().year()).month(6).date(1), moment().year(moment().year()).month(8).date(30)],
        'Q4': [moment().year(moment().year()).month(9).date(1), moment().year(moment().year()).month(11).date(31)],
        'This Year': [moment().subtract(moment().months(), 'month').subtract(moment().dates() - 1, 'day'), moment()],
        'Last Year': [moment().subtract(moment().months(), 'month').subtract(moment().dates() - 1, 'day').subtract(1, 'year'),
        moment().subtract(moment().months(), 'month').subtract(moment().dates(), 'day')],
      }
    };
    this.filterData = {
      'Status': 'Rejected',
      'FromDate': this.fromDate,
      'ToDate': this.toDate
    };
    this.UserNames = new Array<string>();
    this.getData();
  }
  someFunction(event) {
    if (event.picker.chosenLabel !== 'Custom Range') {
      this.picker.input.nativeElement.value = event.picker.chosenLabel;
    }
  }

  getData(value?: any) {
    this.loading = true;
    this.filterData.FromDate = value ? value.start._d : this.filterData.FromDate;
    this.filterData.ToDate = value ? value.end._d : this.filterData.ToDate;
    this.subscriptions = this.reportService.GetRjectedQuotesByUser(this.filterData).subscribe(
      (data) => {
        const arr = [];
        data.forEach(element => {
          arr.push(JSON.parse(element));
        });
        this.allRejectedQuotes = arr;
        this.assignUsers();
        this.loadChart();
        this.loading = false;
      }
    );
  }

  assignUsers(): any {
    this.allRejectedQuotes.forEach(element => {
      this.Users.filter(v => v.id === element._id ? (element['UserName'] = v.FullName) : '');
      this.UserNames.push(element.UserName);
    });
  }
  loadChart() {
    let chart = c3.generate({
      data: {
        json: this.allRejectedQuotes,
        type: 'bar',
        keys: {
          value: ['Total Quotes Rejected']
        },
        colors: {
          'Total Quotes Rejected': '#be1e06'
        },

      },
      bar: {
        width: {
          ratio: 0.25 // this makes bar width 50% of length between ticks
        }
        // width : 20
      },
      axis: {
        x: {
          type: 'category',
          categories: this.UserNames
        },
        rotated: true
      },
      grid: {
        // y: {
        //   lines: [
        //       {value: 0.5}
        //   ]
        // }
        x: {
          show: true
        },
        y: {
          show: true
        }
      }
    });
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
