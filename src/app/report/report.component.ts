import { Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Constants } from '../_shared/constant';
import { ReportService } from './report-service';
@Component({
  selector: 'app-report',
  templateUrl: './report.component.html'
})
export class ReportComponent implements OnInit {
  rp: any;
  Report: any;
  selectedReport: number;
  Users: any;
  reportName: any;
  subscriptions: any;
  constants: any;
  dropdownForm: any;
  loading: boolean;
  constructor(private reportService: ReportService,
    private _route: Router,
    private _toastrService: ToastrService) {
    this.constants = Constants;

  }

  ngOnInit() {
    this.loading = true;
    this.getModuleAccess();
    this.selectedReport = 1;
    this.Report = [
      { name: 'Dashboard', id: 1 },
      { name: 'Accepted', id: 2 },
      { name: 'Rejected', id: 3 },
      { name: 'Notes and History', id: 4 }
    ];
    this.reportName = this.Report[0].id;
    this.subscriptions = this.reportService.getUsers().subscribe(data => {
      this.Users = data;
      this.Users.forEach(element => {
        element['FullName'] = element.firstName + ' ' + element.lastName
      });
    });
    this.loading = false;
  }

  getModuleAccess() {
    this.reportService.getModuleAccess().subscribe(res => {
      if (!res.reports.view) {
        this._route.navigate(['/dashboard']);
        // this._toastrService.warning(
        //   'Access denied',
        //   '',
        //   this.constants.toastrConfig
        // );
      }
    });
  }

  reportSelected(event) {
    this.selectedReport = event;
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
