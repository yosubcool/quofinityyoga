import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Notes } from '../../_shared/model/notes-model';
import { ReportService } from '../report-service';
import { ContactsService } from 'src/app/contacts/contacts-service';
import { Contact } from 'src/app/_shared/model/contact-model';
import { Subscription } from 'rxjs';
import { Account } from 'src/app/_shared/model/account-model';
declare var $: any;
@Component({
  selector: 'app-notes-and-history',
  templateUrl: './notes-and-history.component.html'
})
export class NotesAndHistoryComponent implements OnInit, OnDestroy {
  subscriptions: Subscription;
  @Input('Users') Users: any;
  Accounts: Account[];
  Contacts: Contact[];
  Notes: Notes[];
  History: any[];
  alldata: any[];
  timer = 0;
  SelectedUser: any;
  loading: boolean;
  currentUser: any;

  constructor(private reportService: ReportService, private contactService: ContactsService) {
    this.SelectedUser = JSON.parse(sessionStorage.getItem('currentUser'));
    this.Contacts = new Array<Contact>();
    this.History = new Array<History>();
    this.Accounts = new Array<Account>();
    this.Notes = new Array<Notes>();
    this.alldata = [];
  }


  ngOnInit() {
    $(window).scrollTop(0);
    this.getData();
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
  }

  getData() {
    this.loading = true;
    this.Notes = [];
    this.alldata = [];
    this.History = [];
    this.subscriptions = this.reportService.GetHistoryByUserId(this.SelectedUser.id).subscribe(
      data => {
        this.History = data;
        this.reportService.GetAccountsByUserId(this.SelectedUser.id).subscribe(
          accounts => {
            this.Accounts = accounts;
            this.BindNameInHistory(this.Accounts);
            this.loading = false;
          }
        );
        this.reportService.GetContactsByUserId(this.SelectedUser.id).subscribe(
          contacts => {
            this.Contacts = contacts;
            this.BindNameInHistory(this.Contacts);
            this.loading = false;
          }
        );
      }
    );
  }

  BindNameInHistory(accounts) {
    accounts.forEach(element => {
      const account = element;
      this.History.filter(v => (v.accountId === element.id || v.contactId === element.id) ? (v['Name'] = element.name) : '');
      element.notes.forEach(note => {
        note.createdBy = account.name;
        this.Notes.push(note);
      });
    });
    if (this.timer > 0) {
      this.alldata = this.alldata.concat(this.History, this.Notes);
      // this.alldata.sort(function(a, b) {return (a.createdOn > b.createdOn) ? 1 : ((b.createdOn > a.createdOn) ? -1 : 0); } );
      this.alldata.sort(function (a, b) {
        if (a.createdOn < b.createdOn) { return -1; }
        if (a.createdOn > b.createdOn) { return 1; }
        return 0;
      });
    }
    this.timer++;
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
