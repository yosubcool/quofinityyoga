import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgModel, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReportComponent } from './report.component';
import { DashboardReportComponent } from './dashboard-report/dashboard-report.component';
import { QuotesAcceptedReportComponent } from './quotes-accepted-report/quotes-accepted-report.component';
import { QuotesRejectedReportComponent } from './quotes-rejected-report/quotes-rejected-report.component';
import { NotesAndHistoryComponent } from './notes-and-history/notes-and-history.component';
import { Daterangepicker } from 'ng2-daterangepicker';
// import { MomentModule } from 'angular2-moment/moment.module'; 
import { NgxLoadingModule } from 'ngx-loading';


import { NgxPaginationModule } from 'ngx-pagination'; // importing ng2-pagination
import { OrderModule } from 'ngx-order-pipe'; // importing the module
import { ContactsService } from '../contacts/contacts-service';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { ReportRoutingModule } from './report-routing.module';
import { ReportService } from './report-service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    // MomentModule,
    ReactiveFormsModule,
    ReportRoutingModule,
    Daterangepicker,
    NgxLoadingModule,
    NgxPaginationModule,
    OrderModule,
  ],
  declarations: [ReportComponent,
    DashboardReportComponent,
    QuotesAcceptedReportComponent,
    QuotesRejectedReportComponent,
    NotesAndHistoryComponent],
  providers: [ReportService, ContactsService]
})
export class ReportModule { }
