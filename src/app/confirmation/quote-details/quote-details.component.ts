import {
  Component,
  OnInit,
  OnDestroy,
  ElementRef,
  ViewChild,
  NgZone,
  AfterViewInit,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
declare var $: any;
// import * as sub from "rxjs/Subscription";
import { Quote, SendQuoteDetail } from "src/app/_shared/model/quote-model";
import { Company } from "src/app/_shared/model/company-model";
import { QuotesService } from "src/app/quotes/quotes-service";
import { UserService } from "src/app/user-account/user-service";
import { ToastrService } from "ngx-toastr";
import { Constants } from "src/app/_shared/constant";
import { Subscription } from "rxjs";
@Component({
  selector: "app-quote-details",
  templateUrl: "./quote-details.component.html",
  styleUrls: ["./quote-details.component.scss"],
})
export class QuoteDetailsComponent implements OnInit, OnDestroy {
  @ViewChild("content") content: ElementRef;
  quotesInfo: Quote;
  company: Company;
  loading: boolean;
  constants: any;
  subscriptions: Subscription[] = [];
  quoteId: string;
  companyId: string;
  quotePreparerMail: any;
  constructor(
    private _quotesService: QuotesService,
    private _UserService: UserService,
    private ngZone: NgZone,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private _route: Router
  ) {
    this.constants = Constants;
  }
  ngOnInit(): void {
    $(window).scrollTop(0);
    this.quoteId = this.route.snapshot.queryParams["id"];
    this.companyId = this.route.snapshot.queryParams["cid"];
    this.getQuotes(this.quoteId, this.companyId);
  }
  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
    if (document.getElementById("contacts") !== null) {
      document.getElementById("contacts").classList.remove("active");
    }
  }
  getPreparerMail(ownerId) {
    this._UserService.getUserDetails(ownerId).subscribe((response) => {
      if (response) {
        this.quotePreparerMail = response.email;
      }
    });
  }
  getQuotes(id, company) {
    this.subscriptions.push(
      this._quotesService.QuoteInfo(id, company).subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            this.quotesInfo = res.quote;
            this.getPreparerMail(this.quotesInfo.ownerId);
            this.company = res.company;
          });
        },
        error: (err) => {
          // // console.log(err);
        }
      }
      )
    );
  }
  Accept() {
    this._route.navigate(["/confirmation/quote-confirmation"], {
      queryParams: { id: this.quoteId, cid: this.companyId, val: true },
    });
  }
  Reject() {
    this._route.navigate(["/confirmation/quote-confirmation"], {
      queryParams: { id: this.quoteId, cid: this.companyId, val: false },
    });
  }
  validateLaborQuantity(evt) {
    evt.target.value.split(".").length >= 2
      ? // tslint:disable-next-line:no-unused-expression
      (evt.target.value =
        evt.target.value.split(".")[0] +
        "." +
        evt.target.value.split(".")[1].substring(0, 2))
      : null;
  }
  print() {
    var printContents = document.getElementById("printId")!.innerHTML;
    var originalContents = printContents;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }
  // print() {
  //   var divToPrint = document.getElementById('var divToPrint = document.getElementById('print-index-invoice');
  //   var newWin = window.open('', 'Print-Window');
  //   newWin.document.open();
  //   newWin.document.write('<html><link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" media="print"/><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
  //   newWin.document.close();
  //   setTimeout(function() {
  //     newWin.close();
  //   }, 10);');
  // var newWin = window.open('', 'Print-Window');
  // newWin.document.open();
  // newWin.document.write('<html><link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" media="print"/><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
  // newWin.document.close();
  // setTimeout(function() {
  //   newWin.close();
  // }, 10);
  //   // 
  //   // const printContent = document.getElementById("printId");
  //   // const WindowPrt = window.open(
  //   //   "",
  //   //   "",
  //   //   "left=0,top=0,width=900,height=900,toolbar=0,scrollbars=0,status=0"
  //   // );
  //   // WindowPrt.document.write(printContent.innerHTML);
  //   // WindowPrt.document.close();
  //   // WindowPrt.focus();
  //   // WindowPrt.print();
  //   // WindowPrt.close();
  // }
}
