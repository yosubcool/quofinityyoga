import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ConfirmationRoutingModule } from "./confirmation-routing.module";
import { QuoteDetailsComponent } from "./quote-details/quote-details.component";
import { QuoteConfirmationComponent } from "./quote-confirmation/quote-confirmation.component";
import { FormsModule } from "@angular/forms";
import { ModalModule } from "ngx-bootstrap/modal";
import { NgxLoadingModule } from 'ngx-loading';
import { ViewQuoteComponent } from "./view-quote/view-quote.component";
import { QuotesService } from "../quotes/quotes-service";
import { UserService } from "../user-account/user-service";
import { SharedModule } from "../_shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    ConfirmationRoutingModule,
    ModalModule.forRoot(),
    FormsModule,
    NgxLoadingModule,
    SharedModule,
  ],
  declarations: [
    QuoteDetailsComponent,
    ViewQuoteComponent,
    QuoteConfirmationComponent,
  ],
  providers: [QuotesService, UserService],
})
export class ConfirmationModule { }
