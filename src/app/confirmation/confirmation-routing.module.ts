import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { QuoteDetailsComponent } from "./quote-details/quote-details.component";
import { QuoteConfirmationComponent } from "./quote-confirmation/quote-confirmation.component";
import { ViewQuoteComponent } from "./view-quote/view-quote.component";
import { RouteGaurd } from "../_core/RouteGaurd";
const routes: Routes = [
  {
    path: "",
    canActivate: [RouteGaurd],
    data: {
      title: "Confirmation",
      isLogin: false,
    },
    children: [
      {
        path: "quote-detail",
        component: QuoteDetailsComponent,
        data: {
          title: "Quote Detail",
        },
      },
      {
        path: "quote-confirmation",
        component: QuoteConfirmationComponent,
        data: {
          title: "Quote Confirmation",
        },
      },
      {
        path: "quote-view",
        component: ViewQuoteComponent,
        data: {
          title: "Quote Confirmation",
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmationRoutingModule { }
