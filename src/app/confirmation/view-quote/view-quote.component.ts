import { Component, NgZone, OnInit } from "@angular/core";
// import { isArray } from "util"; //#Revisit_PreviousCode 
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { UserService } from "src/app/user-account/user-service";
import { Constants } from "src/app/_shared/constant";
import { Subscription } from "rxjs";
import { QuotesService } from "src/app/quotes/quotes-service";
import { Company } from "src/app/_shared/model/company-model";
import { Quote } from "src/app/_shared/model/quote-model";
@Component({
  selector: "app-view-quote",
  templateUrl: "./view-quote.component.html",
  styleUrls: ["./view-quote.component.scss"],
})
export class ViewQuoteComponent implements OnInit {
  quotesInfo: Quote;
  newExpiry: any;
  constants: any;
  start = null;
  isIteamizedLabor: any;
  loading: boolean;
  company: Company;
  quoteId: any;
  subscriptions: Subscription[] = [];
  companyId: any;
  quotePreparerMail: any;
  type: any;
  constructor(
    private _quotesService: QuotesService,
    private ngZone: NgZone,
    private _UserService: UserService,
    private _route: Router,
    private route: ActivatedRoute
  ) {

    this.quoteId = this.route.snapshot.queryParams["id"];
    this.companyId = this.route.snapshot.queryParams["cid"];
    this.getQuotes(this.quoteId, this.companyId);
    this.constants = Constants;
  }

  ngOnInit() {

    let id = this.route.snapshot.queryParams["id"];
    id = atob(id);
    this.companyId = this.route.snapshot.queryParams["cid"];
    this.type = atob(this.route.snapshot.queryParams["type"]);
    this.isIteamizedLabor = atob(
      this.route.snapshot.queryParams["isIteamizedLabor"]
    );
    this.getQuotes(this.quoteId, this.companyId);
  }
  getPreparerMail(ownerId) {
    this._UserService.getUserDetails(ownerId).subscribe((response) => {
      if (response) {
        this.quotePreparerMail = response.email;
      }
    });
  }
  addTotalCostWithoutLabor() {
    this.quotesInfo.kits.forEach((element) => {
      element.totalWithoutLabor = 0;
      if (Array.isArray(element.kit.products)) {
        element.kit.products.forEach((iItem) => {
          if (iItem.product.productType.description !== "Labor") {
            element.totalWithoutLabor =
              element.totalWithoutLabor + iItem.price * iItem.quantity;
          }
        });
      }
    });
  }
  // getPreparerMail(ownerId) {
  //   this._UserService.getUserDetails(ownerId).subscribe((response) => {
  //     if (response) {
  //       this.quotePreparerMail = response.email;
  //     }
  //   });
  // }
  getQuotes(id, company) {
    this.subscriptions.push(
      this._quotesService.QuoteInfo(id, company).subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            this.quotesInfo = res.quote;
            this.quotesInfo.details.type = atob(
              this.route.snapshot.queryParams["type"]
            );
            const sendDate = new Date(this.quotesInfo.sentDate);
            if (
              this.quotesInfo.status.description === this.constants.InProgress
            ) {
              this.newExpiry = new Date();
              this.newExpiry.setDate(
                sendDate.getDate() + this.quotesInfo.expairyDays
              );
            } else {
              this.newExpiry = new Date(this.quotesInfo.expirationDate);
            }
            this.quotesInfo.details.isIteamizedLabor = atob(
              this.route.snapshot.queryParams["isIteamizedLabor"]
            );
            this.getPreparerMail(this.quotesInfo.ownerId);
            this.company = res.company;
          });
        },
        error: (err) => {
          // // console.log(err);
        }
      }
      )
    );
  }
  Accept() {
    this._route.navigate(["/confirmation/quote-confirmation"], {
      queryParams: { id: this.quoteId, cid: this.companyId, val: true },
    });
  }
  Reject() {
    this._route.navigate(["/confirmation/quote-confirmation"], {
      queryParams: { id: this.quoteId, cid: this.companyId, val: false },
    });
  }
  validateLaborQuantity(evt) {
    evt.target.value.split(".").length >= 2
      ? // tslint:disable-next-line:no-unused-expression
      (evt.target.value =
        evt.target.value.split(".")[0] +
        "." +
        evt.target.value.split(".")[1].substring(0, 2))
      : null;
  }
  print() {
    var printContents = document.getElementById("printId")!.innerHTML;
    var originalContents = printContents;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    location.reload();
  }
}
