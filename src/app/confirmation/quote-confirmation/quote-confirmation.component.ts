import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { QuotesService } from 'src/app/quotes/quotes-service';
import { Constants } from 'src/app/_shared/constant';
import { Quote } from 'src/app/_shared/model/quote-model';
declare var $: any;
@Component({
  selector: 'app-quote-confirmation',
  templateUrl: './quote-confirmation.component.html',
  styleUrls: ['./quote-confirmation.component.scss']
})
export class QuoteConfirmationComponent implements OnInit {
  quoteDetail: Quote;
  constants: any;
  result: boolean;
  loading: boolean;
  constructor(private _quotesService: QuotesService,
    private ngZone: NgZone,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private _route: Router) {
    this.constants = Constants;
    this.loading = false;
  }

  ngOnInit() {
    $(window).scrollTop(0);
    const id = this.route.snapshot.queryParams['id'];
    const company = this.route.snapshot.queryParams['cid'];
    const isActive = this.route.snapshot.queryParams['val'];
    this.Confirmation(id, company, isActive);
  }


  Confirmation(id: string, com: string, isAccept: boolean) {
    this.loading = true;
    this._quotesService.QuoteConfirmation(id, com, isAccept).subscribe(res => {
      this.ngZone.run(() => {
        this.quoteDetail = res.quote;
        this.result = res.result;
        this.loading = false;
      });
    }, err => {
      this.loading = false;
    });
  }

}
