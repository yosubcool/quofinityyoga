import { Directive, ElementRef, HostListener, Input } from "@angular/core";
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: "[onlyNumber]"
})
export class OnlyNumberDirective {
  constructor(private el: ElementRef) {}

  // tslint:disable-next-line:member-ordering
  @Input() onlyNumber: boolean;
  @HostListener("keydown", ["$event"])
  onKeyDown(event) {
    const e = <KeyboardEvent>event;
    if (this.onlyNumber) {
      if (
        [46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode === 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode === 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode === 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)
      ) {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if (
        (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
        (e.keyCode < 96 || e.keyCode > 105)
      ) {
        e.preventDefault();
      }
    }
  }

  isNumberKey(event) {
    const charCode = event.which ? event.which : event.keyCode;
    if (
      charCode > 31 &&
      (charCode < 48 || charCode > 57) &&
      (charCode !== 109 || charCode !== 189 || charCode !== 173)
    ) {
      return false;
    }
    return true;
  }
}

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: "[onlyNumberWithWhiteSpces]"
})
// tslint:disable-next-line:class-name
export class onlyNumberWithWhiteSpcesDirective {
  // constructor() { }
  // @HostListener('keypress') onkeypress(e) {
  //     let event = e || window.event;
  //     if (event) {
  //         return this.isNumberKey(event);
  //     }
  // }

  constructor(private el: ElementRef) {}
  // tslint:disable-next-line:member-ordering
  @Input() onlyNumberWithWhiteSpces: boolean;
  @HostListener("keydown", ["$event"])
  onKeyDown(event) {
    const e = <KeyboardEvent>event;
    if (this.onlyNumberWithWhiteSpces) {
      if (
        [46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
        (e.keyCode === 65 && e.ctrlKey === true) ||
        (e.keyCode === 67 && e.ctrlKey === true) ||
        (e.keyCode === 88 && e.ctrlKey === true) ||
        (e.keyCode >= 35 && e.keyCode <= 39) ||
        e.keyCode === 32
      ) {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if (
        (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
        (e.keyCode < 96 || e.keyCode > 105)
      ) {
        e.preventDefault();
      }
    }
  }

  isNumberKey(event) {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 32) {
      return false;
    }
    return true;
  }
}
