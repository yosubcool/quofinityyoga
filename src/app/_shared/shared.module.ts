import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  OnlyNumberDirective,
  onlyNumberWithWhiteSpcesDirective
} from './OnlyNumber.directive';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AutofocusDirective } from 'src/app/_shared/autofocus.directive';
import { SafePipe } from './safe.pipe';
import { PricePipe } from './price_pipe';
import { TwoDigitDecimaNumberDirective } from './two-digit-decima-number.directive';

@NgModule({
  imports: [
    CommonModule,
    NgSlimScrollModule,
    PopoverModule.forRoot(),
    TypeaheadModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    OnlyNumberDirective,
    onlyNumberWithWhiteSpcesDirective,
    AutofocusDirective,
    SafePipe,
    PricePipe,
    TwoDigitDecimaNumberDirective
  ],
  providers: [],
  exports: [
    OnlyNumberDirective,
    onlyNumberWithWhiteSpcesDirective,
    NgSlimScrollModule,
    PopoverModule,
    AutofocusDirective,
    TypeaheadModule,
    TooltipModule,
    PricePipe,
    SafePipe,
    TwoDigitDecimaNumberDirective
  ]
})
export class SharedModule { }
