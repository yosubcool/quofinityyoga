import { Directive, Input, ElementRef, Renderer2, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appAutofocus]'
})
export class AutofocusDirective implements OnInit, OnChanges {
  @Input() appAutofocus;
  private el: any;
  constructor(
    private elementRef: ElementRef,
  ) {
    this.el = this.elementRef.nativeElement;
  }

  ngOnChanges(change: SimpleChanges) {
    this.el.focus();
  }
  ngOnInit() {
    this.el.focus();
  }
}
