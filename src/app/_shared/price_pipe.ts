import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pricePipe'
})
export class PricePipe implements PipeTransform {
  transform(value: any): any {
    let valueToConvert = parseFloat(value);
    if (value === 1000000 || value > 1000000) {
      valueToConvert = value / 1000000;
      return valueToConvert.toFixed(2) + 'M';
    } else {
      return valueToConvert.toFixed(2);
    }
  }
}
