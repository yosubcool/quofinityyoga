export class Subscription {
    id: string;
    subscription: string;
    price: string;
    isActive: boolean;

    User() {
        this.id = null;
        this.subscription = null;
        this.price = null;
        this.isActive = false;
    }
}
