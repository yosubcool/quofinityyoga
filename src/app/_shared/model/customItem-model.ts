export class CustomItem {
  id: string;
  name: string;
  cost: any;
  quantity: any;
  price: any;
  isTaxable: boolean;
  constructor() {
    this.id = null;
    this.name = null;
    this.cost = null;
    this.quantity = null;
    this.price = null;
    this.isTaxable = false;
  }
}
