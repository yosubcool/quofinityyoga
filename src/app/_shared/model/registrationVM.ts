/*******************************************Confidential*****************************************
 * <copyright file = "registrationVM.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */


import { User } from 'src/app/_shared/model/user-model';
import { Company } from 'src/app/_shared/model/company-model';

export class RegistrationVM {
    user: User;
    company: Company;
    RegistrationVM() {
        this.company = new Company();
        this.user = new User();
    }
}
