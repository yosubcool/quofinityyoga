import { PaymentInfo } from 'src/app/_shared/model/payment-model';

/*******************************************Confidential*****************************************
 * <copyright file = "company-model.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

export class Company {
  id: string;
  firstName: string;
  lastName: string;
  contactNo: string;
  email: string;
  name: string;
  street: string;
  city: string;
  state: string;
  zip: string;
  subscriptionPlanId: string;
  isPaymentDone: boolean;
  isActive: boolean;
  dbName: string;
  connectionString: string;
  createdBy: string;
  updatedOn: Date;
  createdOn: Date;
  updatedBy: string;
  logo: string;
  payment = [];
  noOfUsers: any;
  aboutUs: string;
  disclaimer: string;
  termsConditions: string;
  Company() {
    this.id = null;
    this.firstName = null;
    this.lastName = null;
    this.contactNo = null;
    this.email = null;
    this.name = null;
    this.street = null;
    this.city = null;
    this.state = null;
    this.zip = null;
    this.subscriptionPlanId = null;
    this.isPaymentDone = false;
    this.isActive = false;
    this.dbName = null;
    this.connectionString = null;
    this.createdBy = null;
    this.updatedOn = null;
    this.createdOn = null;
    this.updatedBy = null;
    this.logo = null;
    this.noOfUsers = null;
    this.payment = new Array<PaymentInfo>();
    this.aboutUs = null;
    this.disclaimer = null;
    this.termsConditions = null;
  }
}
