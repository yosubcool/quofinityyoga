import { Notes } from 'src/app/_shared/model/notes-model';

export class Account {
    id: string;
    uniqueId: string;
    name: string;
    address: string;
    accountId: string;
    website: string;
    contactNo: string;
    homeNo: string;
    faxNo: string;
    isActive: boolean;
    createdOn: Date;
    createdBy: string;
    updatedOn: Date;
    updatedBy: string;
    notes = []
    constructor() {
        this.id = null;
        this.uniqueId = null;
        this.name = null;
        this.homeNo = null;
        this.faxNo = null;
        this.address = null;
        this.accountId = null;
        this.website = null;
        this.contactNo = null;
        this.isActive = false;
        this.createdOn = null;
        this.updatedOn = null;
        this.createdBy = null;
        this.updatedBy = null;
        this.notes = new Array<Notes>();
    }
}


