export class Suppliers {
  id: string;
  description: string;
  isActive: boolean;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  Suppliers() {
    this.id = null;
    this.description = null;
    this.isActive = null;
    this.createdOn = null;
    this.updatedOn = null;
    this.createdBy = null;
    this.updatedBy = null;
  }
}

export class Tags {
  id: string;
  description: string;
  type: string;
  isActive: boolean;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  Tags() {
    this.id = null;
    this.type = null;
    this.description = null;
    this.isActive = null;
    this.createdOn = null;
    this.updatedOn = null;
    this.createdBy = null;
    this.updatedBy = null;
  }
}

export class Categories {
  id: string;
  description: string;
  isActive: boolean;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  Categories() {
    this.id = null;
    this.description = null;
    this.isActive = true;
    this.createdOn = null;
    this.updatedOn = null;
    this.createdBy = null;
    this.updatedBy = null;
  }
}

export class SubCategories {
  id: string;
  categories: Categories;
  description: string;
  isActive: boolean;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  SubCategories() {
    this.id = null;
    this.categories = new Categories();
    this.description = null;
    this.isActive = null;
    this.createdOn = null;
    this.updatedOn = null;
    this.createdBy = null;
    this.updatedBy = null;
  }
}

export class Taxes {
  id: string;
  name: string;
  value: string;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  isActive: boolean;
  Taxes() {
    this.id = null;
    this.name = null;
    this.value = null;
    this.createdOn = null;
    this.createdBy = null;
    this.updatedOn = null;
    this.updatedBy = null;
    this.isActive = true;
  }
}

export class ProductType {
  id: string;
  description: string;
  ProductType() {
    this.id = null;
    this.description = null;
  }
}


