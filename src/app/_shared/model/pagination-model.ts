export class Pagination {
  pageNumber: number;
  pageSize: number;
  totalCount: number;
  records: any;
  sortBy: string;
  sortType: string;
  alphabet: string;
  manufacturer: string;
  category: any;
  accountId: any;
  contactId: any;
  status: any;
  subCategory: any;
  tags: any;
  companyId: any;
  suppliers: any;
  Pagination() {
    this.pageNumber = 1;
    this.pageSize = null;
    this.totalCount = null;
    this.accountId = null;
    this.contactId = null;
    this.companyId = null;
    this.status = null;
    this.records = [];
    this.sortBy = null;
    this.sortType = null;
    this.alphabet = null;
    this.manufacturer = null;
    this.suppliers = null;
  }
}
