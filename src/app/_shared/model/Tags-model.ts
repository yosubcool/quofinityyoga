export class Tags {
  id: string;
  description: string;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  isActive: boolean;
  Tags() {
    this.id = null;
    this.description = null;
    this.createdOn = null;
    this.createdBy = null;
    this.updatedOn = null;
    this.updatedBy = null;
    this.isActive = true;
  }
}
