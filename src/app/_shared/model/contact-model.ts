import { Notes } from 'src/app/_shared/model/notes-model';
import { Account } from 'src/app/_shared/model/account-model';
export class Contact {
    id: string;
    uniqueId: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    homeNo: string;
    extensionNo: string;
    faxNo: string;
    isActive: boolean;
    createdOn: Date;
    createdBy: string;
    updatedOn: Date;
    updatedBy: string;
    notes = [];
    accounts: any;
    constructor() {
        this.id = null;
        this.uniqueId = null;
        this.firstName = null;
        this.extensionNo = null;
        this.lastName = null;
        this.email = null;
        this.phone = null;
        this.homeNo = null;
        this.faxNo = null;
        this.isActive = false;
        this.createdOn = null;
        this.updatedOn = null;
        this.createdBy = null;
        this.updatedBy = null;
        this.notes = new Array<Notes>();
        this.accounts = new Account();
    }
}
