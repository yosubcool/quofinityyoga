export class Role {
  id: string;
  description: string;
  isActive: boolean;
  constructor() {
    this.id = null;
    this.description = null;
    this.isActive = false;
  }
}
