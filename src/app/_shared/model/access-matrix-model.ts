import { Actions } from 'src/app/_shared/model/actions-model';
import { Role } from 'src/app/_shared/model/role.model';

export class AccessMatrix {
  id: string;
  role: any;
  accounts: any;
  contact: any;
  products: any;
  quote: any;
  kit: any;
  subscription: any;
  categories: any;
  suppliers: any;
  reports: any;
  pricingMethods: any;
  AccessMatrix() {
    this.id = null;
    this.role = new Role;
    this.accounts = new Actions();
    this.contact = new Actions();
    this.products = new Actions();
    this.quote = new Actions();
    this.kit = new Actions();
    this.subscription = new Actions();
    this.categories = new Actions();
    this.suppliers = new Actions();
    this.reports = new Actions();
    this.pricingMethods = new Actions();
  }
}
