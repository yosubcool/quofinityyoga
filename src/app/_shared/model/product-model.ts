import {
  ProductType,
  Categories,
  SubCategories,
  Suppliers,
} from 'src/app/_shared/model/tenantMaster-model';
import { PricingMethod } from 'src/app/_shared/model/pricing-model';

export class Product {
  id: string;
  uniqueId: string;
  description: string;
  partNo: string;
  upc: string;
  name: string;
  productType: any;
  pricingMethod: any;
  msrp: any;
  cost: any;
  category: any;
  subCategory: any;
  suppliers: any;
  tags: any;
  isTaxable: boolean;
  isActive: boolean;
  manufacturer: any;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  images: any;
  isUsed: number;
  // New field added as mentioned in CR
  // supplier: Suppliers;
  manufacturerPartNumber: string;
  productDetailHyperlink: string;
  productDatasheetHyperlink: string;
  unitOfMeasure: string;
  constructor() {
    this.id = null;
    this.uniqueId = null;
    this.manufacturer = null;
    this.description = null;
    this.partNo = null;
    this.upc = null;
    this.name = null;
    this.productType = new ProductType();
    this.pricingMethod = new Array<PricingMethod>();
    this.msrp = 0.0;
    this.cost = 0.0;
    this.category = new Array<Categories>();
    this.subCategory = new Array<SubCategories>();
    this.suppliers = new Array<Suppliers>();
    this.tags = [];
    this.isTaxable = false;
    this.isActive = true;
    this.createdOn = new Date();
    this.updatedOn = new Date();
    this.createdBy = null;
    this.updatedBy = null;
    this.images = [];
    this.isUsed = 0;
    this.manufacturerPartNumber = null;
    this.productDetailHyperlink = null;
    this.productDatasheetHyperlink = null;
    this.unitOfMeasure = null;
  }
}

export class Image {
  id: any;
  name: any;
  type: any;
  string: any; //string: string; //#Revisit_PreviousCode
  constructorS() {
    this.id = null;
    this.name = null;
    this.type = null;
    this.string = null;
  }
}
