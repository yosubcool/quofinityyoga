import { Plans } from 'src/app/_shared/model/plans-model';

export class Configuration {
  id: string;
  trialPeriod: number;
  noOfUsers: number;
  recentQuote: number;
  tagLimit: number;
  subscriptionPlan: any;
  isActive: boolean;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  disclaimer: string;
  termsAndConditions: string;
  constructor() {
    this.id = null;
    this.trialPeriod = null;
    this.recentQuote = null;
    this.tagLimit = null;
    this.noOfUsers = null;
    this.isActive = false;
    this.createdOn = null;
    this.updatedOn = null;
    this.createdBy = null;
    this.updatedBy = null;
    this.disclaimer = null;
    this.termsAndConditions = null;
    this.subscriptionPlan = new Array<Plans>();
  }
}
