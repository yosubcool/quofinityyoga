import { Kit } from "./kit-model";
import { Product } from "./product-model";
import { PricingMethod } from "./pricing-model";

export class ProductDetail {
  id: string;
  product: Product;
  quantity: any;
  margin: PricingMethod;
  price: any;
  constructor() {
    this.id = null;
    this.product = new Product();
    this.quantity = 0;
    this.margin = new PricingMethod();
    this.price = 0;
  }
}

export class KitDetail {
  id: string;
  Kit: Kit;
  quantity: Number;
  total: number;
  isActive: boolean;
  constructor() {
    this.id = null;
    this.Kit = new Kit();
    this.quantity = null;
    this.total = null;
    this.isActive = true;
  }
}
