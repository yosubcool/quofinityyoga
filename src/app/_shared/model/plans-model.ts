export class Plans {
  id: string;
  cost: any;
  userLimit: any;
  timeSpan: any;
  subscriptionType: string;
  planName: string;

  constructor() {
    this.id = null;
    this.cost = null;
    this.userLimit = null;
    this.timeSpan = null;
    this.subscriptionType = null;
    this.planName = null;
  }
}
