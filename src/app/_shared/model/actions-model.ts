export class Actions {
  create: boolean;
  update: boolean;
  delete: boolean;
  view: boolean;
  import: boolean;
  export: boolean;

  Actions() {
      this.create = false;
      this.update = false;
      this.delete = false;
      this.view = false;
      this.import = false;
      this.export = false;
  }
}
