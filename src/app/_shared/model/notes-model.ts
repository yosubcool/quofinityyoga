export class Notes {
    id: string;
    noteName: string;
    description: string;
    isActive: boolean;
    createdOn: Date;
    createdBy: string;
    updatedOn: Date;
    updatedBy: string;
    User() {
        this.id = null;
        this.noteName = null;
        this.description = null;
        this.isActive = false;
        this.createdOn = null;
        this.updatedOn = null;
        this.createdBy = null;
        this.updatedBy = null;
    }
}
