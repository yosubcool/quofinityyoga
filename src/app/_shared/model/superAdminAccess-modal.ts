export class SuperAdminAccess {
    id: string;
    companyId: string;
    accessId: string;
    isUsed: boolean;
    createdOn: Date;
    createdBy: string;

    constructor() {
        this.id = null;
        this.companyId = null;
        this.createdOn = null;
        this.createdBy = null;
        this.accessId = null;
        this.isUsed = false;
    }
}
