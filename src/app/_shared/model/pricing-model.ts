export class PricingMethod {
  id: string;
  name: string;
  method: string;
  value: any;
  constructor() {
    this.id = null;
    this.name = null;
    this.method = null;
    this.value = 0;
  }
}
