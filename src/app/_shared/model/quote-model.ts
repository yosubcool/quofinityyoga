import { CustomItem } from 'src/app/_shared/model/customItem-model';
import { ProductDetail, KitDetail } from './productDetail-model';
import { Account } from './account-model';
import { Contact } from './contact-model';
import { Taxes } from 'src/app/_shared/model/tenantMaster-model';

export class Quote {
  id: any;
  uniqueId: any;
  contact: Contact;
  expirationDate: Date;
  expairyDays: number;
  stage: any;
  status: any;
  name: any;
  quantity: any;
  msrp: any;
  discount: any;
  subtotal: any;
  saleTax: Taxes;
  taxAmount: any;
  total: any;
  laborCharge: any;
  scopeofWork: any;
  termsandConditions: any;
  cost: any;
  profitMargin: any;
  profit: any;
  customItem: any = [];
  products: ProductDetail[];
  kits: any = [];
  details: SendQuoteDetail;
  isActive: boolean;
  ownerId: string;
  ownerName: string;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  sentDate: Date;
  constructor() {
    this.id = null;
    this.uniqueId = null;
    this.contact = new Contact();
    this.expirationDate = null;
    this.expairyDays = null;
    this.stage = null;
    this.status = new QuoteStatus();
    this.name = null;
    this.quantity = 0;
    this.msrp = 0;
    this.discount = 0;
    this.subtotal = 0;
    this.saleTax = new Taxes();
    this.taxAmount = 0;
    this.total = 0;
    this.laborCharge = 0;
    this.scopeofWork = null;
    this.termsandConditions = null;
    this.cost = 0;
    this.profitMargin = 0;
    this.profit = 0;
    this.customItem = new Array<CustomItem>();
    this.products = new Array<ProductDetail>();
    this.kits = new Array<KitDetail>();
    this.details = new SendQuoteDetail();
    this.ownerId = null;
    this.ownerName = null;
    this.createdOn = null;
    this.createdBy = null;
    this.updatedOn = null;
    this.updatedBy = null;
    this.isActive = null;
    this.sentDate = null;
  }
}


export class QuoteStatus {
  id: any;
  description: Contact;
  constructor() {
    this.id = null;
    this.description = null;
  }
}

export class SendQuoteDetail {
  name: string;
  title: string;
  type: string;
  isIteamizedLabor: string;
  constructor() {
    this.title = null;
    this.title = null;
    this.type = null;
    this.isIteamizedLabor = null;
  }
}

