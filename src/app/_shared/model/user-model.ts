import { Role } from 'src/app/_shared/model/role.model';



export class User {
    id: string;
    subscriberId: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    contactNo: string;
    role: any;
    createdOn: Date;
    createdBy: string;
    updatedOn: Date;
    updatedBy: string;
    isActive: boolean;
    isLocked: boolean;
    profilePicture: string;
    otp: any;
    User() {
        this.id = null;
        this.subscriberId = null;
        this.firstName = null;
        this.lastName = null;
        this.email = null;
        this.password = null;
        this.contactNo = null;
        this.role = new Role();
        this.createdOn = null;
        this.updatedOn = null;
        this.createdBy = null;
        this.updatedBy = null;
        this.isActive = false;
        this.isLocked = false;
        this.profilePicture = null;
        this.otp = [];
    }
}
