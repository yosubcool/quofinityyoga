

export class History {
    id: string;
    name: string;
    isActive: boolean;
    createdOn: Date;
    createdBy: string;
    updatedOn: Date;
    updatedBy: string;
    accountId: string;
    contactId: string;
    userId: string;
    History() {
        this.id = null;
        this.name = null;
        this.isActive = false;
        this.createdOn = null;
        this.updatedOn = null;
        this.createdBy = null;
        this.updatedBy = null;
        this.accountId = null;
        this.contactId = null;
        this.userId=null;

    }
}


