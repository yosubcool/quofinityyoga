import { CustomItem } from './customItem-model';
import { ProductDetail } from './productDetail-model';
import { Categories, SubCategories } from './tenantMaster-model';

export class Kit {
  id: string;
  uniqueId: string;
  categoryId: string;
  description: string;
  name: string;
  number: string;
  products: any;
  customItem: any;
  category: any;
  subCategory: any;
  tags: any;
  total: any;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  isActive: Boolean;
  constructor() {
    this.id = null;
    this.uniqueId = null;
    this.categoryId = null;
    this.description = null;
    this.name = null;
    this.number = null;
    this.products = new Array<ProductDetail>();
    this.customItem = new Array<CustomItem>();
    this.category = new Array<Categories>();
    this.subCategory = new Array<SubCategories>();
    this.tags = [];
    this.total = null;
    this.createdOn = new Date();
    this.createdBy = null;
    this.updatedOn = new Date();
    this.updatedBy = null;
    this.isActive = true;
  }
}
