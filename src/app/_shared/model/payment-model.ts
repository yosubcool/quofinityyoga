export class PaymentInfo {
  id: string;
  transcationId: string;
  subscriptionId: string;
  subscriptionType: string;
  subscriptionStartDate: Date;
  subscriptionEndDate: Date;
  planPrice: string;
  tax: string;
  total: string;
  last4Digit: string;
  isActive: boolean;
  createdOn: Date;
  createdBy: string;
  updatedOn: Date;
  updatedBy: string;
  noOfUsers: number;
  planCost: any;
  UserCost: any;
  totalCost: any;
  planName: any;
  isRenewed: boolean;
  PaymentInfo() {
    this.id = null;
    this.transcationId = null;
    this.subscriptionType = null;
    this.subscriptionStartDate = null;
    this.subscriptionEndDate = null;
    this.planPrice = null;
    this.tax = null;
    this.total = null;
    this.last4Digit = null;
    this.noOfUsers = null;
    this.planCost = null;
    this.UserCost = null;
    this.totalCost = null;
    this.isActive = false;
    this.createdOn = null;
    this.updatedOn = null;
    this.createdBy = null;
    this.updatedBy = null;
    this.planName = null;
    this.isRenewed = null;
    this.subscriptionId = null;
  }
}
