import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appTwoDigitDecimaNumber]'
})
export class TwoDigitDecimaNumberDirective {
  // Allow decimal numbers and negative values
  private regex: RegExp = new RegExp(/^\d*\.?\d{0,2}$/g);
  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  private specialKeys: Array<string> = [
    'Backspace',
    'Tab',
    'End',
    'Home',
    '-',
    'ArrowLeft',
    'ArrowRight'
  ];

  constructor(private el: ElementRef) {}
  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    // // console.log(this.el.nativeElement.value, ' ', event.key);
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    // tslint:disable-next-line:max-line-length
    // tslint:disable-next-line:one-line
    // else if (
    //   event.key === '1' ||
    //   event.key === '2' ||
    //   event.key === '4' ||
    //   event.key === '5' ||
    //   event.key === '6' ||
    //   event.key === '7' ||
    //   event.key === '8' ||
    //   event.key === '9'
    // ) {
    //   return;
    // }
    const current: string = this.el.nativeElement.value;
    const next: string = current.concat(event.key);
    if (next && !String(next).match(this.regex)) {

      event.preventDefault();
    }
  }
}
