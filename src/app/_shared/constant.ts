/*******************************************Confidential*****************************************
 * <copyright file = "constant.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

export class Constants {

  public ConstantsAccess = Constants;
  // public static BASE_URL = 'http://14.142.236.6:9089/api/'; // ServerLink
  // public static BASE_URL = 'http://172.28.25.51:94/api/'; // DemoServerink
  // public static BASE_URL = "http://localhost:62694/api/"; // Local Server
  // public static BASE_URL = '/FlexAPI/api/';
  public static BASE_URL = "https://app.quofinity.com/API/api/";// Prod Server
  // public static BASE_URL = "http://137.117.95.36:8080/FlexAPI/api/";
  // public static BASE_URL = "http://137.117.95.36:8080/API/api/";
  // public static BASE_URL = "http://137.117.95.36:8080/API/api/"; //pre prod
  // public static BASE_URL = "http://localhost:62694/api/";
  // public static BASE_URL = '/api/';

  public static collections = [
    "Accounts",
    "Contact",
    "Quote",
    "Products",
    "Kit",
  ];

  public static Alphabets = [
    "All",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",

    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];

  public static SuperAdmin = "Super Admin";
  public static Admin = "Admin";
  public static GeneralUser = "General User";

  // Davids chargebee Account
  public static plan_monthly = "plan_monthly";
  public static plan_annual = "plan_annual";
  // Dhiraj Chargebee Account
  // public static plan_monthly = 'monthly';
  // public static plan_annual = 'annual';

  public static InProgress = "In Progress";
  public static Quoted = "Quoted";
  public static Accepted = "Accepted";
  public static Rejected = "Rejected";
  public static Expired = "Expired";
  public static Expairy = [30, 45, 60, 90];
  public static PageSize = [6, 12, 24, 48];
  public static PageViewSize = [10, 20, 30, 40];

  public static toastrConfig: any = {
    positionClass: "toast-top-center",
    closeButton: true,
    progressBar: true,
    timeOut: 2000,
  };
  public static MaxLengthConstants = {
    Name: 25,
    sendName: 50,
    CompanyName: 40,
    PropertyName: 25,
    Street: 250,
    City: 15,
    State: 15,
    Email: 254,
    Phone: 18,
    Zip: 10,
    Password: 16,
    OTP: 8,
    UserName: 20,
    Website: 100,
    LastName: 30,
    Fax: 50,
    Note: 50,
    ExtensionNo: 6,
    QuoteTitle: 50,
    AccountName: 50,
    KitTitle: 50,
    CustomItemTitle: 50,
    ProductTitle: 50,
    Supplier: 50,
    Cost: 10,
    PricingMethod: 5,
    ProductType: 15,
    Category: 22,
    SubCategory: 22,
    kitNumber: 30,
    Tag: 35,
    UPC: 10,
    PartNumber: 50,
    MethodName: 20,
  };
  public static ReservationMaxLengthonstants = {
    Name: 25,
    Email: 50,
    ZipCode: 10,
    Mobile: 15,
    City: 50,
    Address: 100,
    ReservationNo: 15,
  };
  public static USERNAME = /^\S*$/;
  public static DateFormat = "MM/dd/yyyy";
  // public static EMAIL_REGEXP = /^\s*[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)\s*$/i;
  public static EMAIL_REGEXP =
    "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z0-9]{2,})$";

  public static NAME_REGEXP = "^[a-zA-Z0-9]+[']*(s{0,1}[a-zA-Z0-9 ])*$";
  public static FULL_NAME_REGEXP = "^[a-zA-Z0-9]+[']*(s{0,1}[a-zA-Z0-9 ])*$";
  public static NUMBER_REGEXP = "^[a-zA-Z0-9]+$";
  public static ALPHANUMRIC_REGEXP = /^(?![0-9]*$)[a-zA-Z0-9-\s]+$/;
  public static PHONE_REGEXP = /\+\d \(\d{3}\) \d{3}-\d{4}/;
  public static NUMBER_WITHOUT_ZERO = "^([0-9]|[1-9][0-9])$";
  // tslint:disable-next-line:max-line-length
  // public static PHONE_REGEXP = /^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
  public static PASSWORD_REGEXP =
    /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%&'".,;:*+()[\]\/\\=?^_`<>{|}~.-]).{8,16}$/;
  public static ZIP_REGEXP = "^[0-9]{5}(-[0-9]{4})?$";
  public static FAX_REGEXP = "^(?=(?:D*d){10,12}D*$)[0-9 -()\\/]{1,16}$";
  public static WEBSITE_REGEXP =
    "[(http(s)?://)?([w-]+.)+[w-]+(/[w- ;,./?%&=]*)]";
  public static SUBSCRIPTION_TRIAL = "SUBSCRIPTION_TRIAL";
  public static PartNo_REXEXP =
    "[A-Za-z -_!@#$%^&*()?/<>.]*[0-9][A-Za-z -_!@#$%^&*()?/<>.]*";
  public static PartNo_REGEX =
    "\b((?=[A-Za-z/ -]{0,19}d)[A-Za-z0-9/ -]{4,20})\b";
  // public static Cost_REXEXP = '^([0-9]+)(\.[0-9]+)?$';
  // public static Cost_REXEXP = '^(0?.?[0-9][0-9]{0,2}(?:(,[0-9]{3})*|[0-9]*))(.[0-9]+){0,1}$';
  // public static Cost_REXEXP = "^[0-9]+.?[0-9]?[0-9]$";
  public static Cost_REXEXP = /^(?:\d*\.\d{1,2}|\d+)$/;
  // ^[0-9]+.?[0-9]?[0-9]$
  // public static Margin_REXEXP = '^([0-9]|[0-9][0-9]|[0-9][0-9][0-9])(\.[0-9]+)?$';
  public static Margin_REXEXP = "([0-9]*[.])?[0-9]+";
  public static priceValue = "([0-9]*[.])?[0-9]+";
  // public static Title_REGEXP = '[A-Za-z0-9 -]*[A-Za-z -][A-Za-z0-9 -_!@#$%^&*()?/<>.]*';
  public static Title_REGEXP =
    "[A-Za-z0-9-]*[A-Za-z-][A-Za-z0-9 -_!@#$%^&*()?/<>.]*";
  public static PricingMethod_REXEXP = "^([0-9]|[0-9][0-9])(.[0-9]+)?$";
  public static String_REXEXP = "[a-zA-Z .]+";
  public static Num_REXEXP = "[0-9]+";
  public static Category_REGEXP = "^[_!@#$%^&*()?/<>.a-zA-Z1-9]+.*";
  public static AnythingWhithoutSpace_REGEXP =
    // tslint:disable-next-line:max-line-length
    "[-_!@#$%^&*(`~)?/+<|>.{}0-9A-Za-z0-9-_!@#$%^&*(|)?/<>.{}]*[-_!@#$%^&*(`~|)+?/<>.{}0-9A-Za-z--_!@#$%^&*(|)?/<>.{}][0-9A-Za-z0-9 -_!@#$%^&*(+`~)?/<>.]*";
  // Autologout values
  // public static MINUTES_UNITL_AUTO_LOGOUT = 1; // in mins
  // public static CHECK_INTERVAL = 2000; // in ms
  // public static STORE_KEY = 'lastAction';

  public static PricingMethods = {
    List: "List",
    Markup: "Markup",
    Margin: "Margin",
    Discount: "Discount",
  };
}
