/*******************************************Confidential*****************************************
 * <copyright file = "aside.directive.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Directive, HostListener } from '@angular/core';

/**
* Allows the aside to be toggled via click.
*/
@Directive({
  selector: '[appAsideMenuToggler]',
})
export class AsideToggleDirective {
  constructor() { }

  @HostListener('click', ['$event'])
  toggleOpen($event: any) {
    $event.preventDefault();
    document.querySelector('body').classList.toggle('aside-menu-hidden');
  }
}
