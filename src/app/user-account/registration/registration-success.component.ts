/*******************************************Confidential*****************************************
 * <copyright file = "registration-sucess.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Injectable, NgZone, OnInit } from '@angular/core';
import { UserService } from 'src/app/user-account/user-service';
@Component({
  templateUrl: './registration-success.component.html'
})
export class RegistrationSuccessComponent implements OnInit {
  constructor() { }
  ngOnInit() { }
}
