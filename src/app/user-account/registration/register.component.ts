/*******************************************Confidential*****************************************
 * <copyright file = "register.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import {
  Component,
  OnInit,
  NgZone,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';
import {
  AbstractControl,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
// import { debug } from 'util'; //#Revisit_IgnoredAsNotUsed
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/user-account/user-service';
// import { debounce } from 'rxjs/operator/debounce'; //#Revisit_IgnoredAsNotUsed
import { Company } from 'src/app/_shared/model/company-model';
import { User } from 'src/app/_shared/model/user-model';
import { Constants } from 'src/app/_shared/constant';
// import { Key } from 'selenium-webdriver';//#Revisit_IgnoredAsNotUsed
// import { forEach } from '@angular/router/src/utils/collection'; //#Revisit_IgnoredAsNotUsed
import { Router } from '@angular/router';
import { RegistrationVM } from 'src/app/_shared/model/registrationVM';
// import { CompairValidator } from 'src/app/_shared/compair-validator.directive'; //#Revisit_IgnoredAsNotUsed
declare var require: any;
@Component({
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {
  agree: boolean;
  terms: any;
  public loading = false;
  constants: any;
  User: User;
  Company: Company;
  isFormSubmitted: boolean;
  Confirm: string;
  registrationVM: RegistrationVM;
  isEmailDuplicate: boolean;
  isContactDuplicate: boolean;
  isCompanyDuplicate: boolean;
  isDisabled: boolean;
  states: any;
  show = false;
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  newMask: string = " (000) 000-0000";
  mask: any[] = [
    '+',
    '1',
    ' ',
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  constructor(
    private _UserService: UserService,
    private _toastrService: ToastrService,
    private router: Router,
    private ngZone: NgZone
  ) {
    sessionStorage.clear();
    this.isFormSubmitted = false;
    this.registrationVM = new RegistrationVM();
    this.Company = new Company();
    this.User = new User();
    this.constants = Constants;
    this.isDisabled = false;
    this._toastrService.toastrConfig.preventDuplicates = true;
    this.GetStates();
    document.body.scrollTop = 0;
    this.terms = '';
  }

  ngOnInit() {
    this.terms = '';
    sessionStorage.clear();
    this.agree = false;
  }

  getConfiguration() {
    this._UserService.getConfiguration().subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            this.terms = res;
            // // console.log(res);
          } else {
            this.terms = null;
          }
        });
      },
      err => { }
    )

  }

  GetStates() {
    this.loading = true;
    this._UserService.GetStates().subscribe(
      res => {
        this.ngZone.run(() => {
          this.states = res;
          this.loading = false;
        });
      },
      err => {
        this.loading = false;
      }
    );
  }

  onSubmit(isValid: boolean) {
    this.show = false;
    if (isValid) {
      if (this.agree) {
        this.show = false;
        this.loading = true;
        this.isDisabled = true;
        this.isFormSubmitted = true;
        this.registrationVM.company = this.Company;
        this.registrationVM.user = this.User;
        this._UserService.AddCompany(this.registrationVM).subscribe(
          res => {
            this.loading = false;
            this.ngZone.run(() => {
              if (res.res) {
                this.loading = false;
                this.router.navigate(['/account', 'registration-success']);
              } else {
                this.isCompanyDuplicate = res.company;
                this.isEmailDuplicate = res.email;
              }
            });
          },
          err => {
            this.isDisabled = false;
            this.loading = false;
          }
        );
      } else {
        this.show = true;
        this.popupOpen(this.popupFor = 'Accept terms');
      }
    }
  }
  // messages display
  popupOpen(popupFor) {
    if (this.popupFor === 'Accept terms') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Accept terms and conditions';
    }
  }

  yes(ConfirmModal) {
  }

  no(ConfirmModal) {
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }
}
