/*******************************************Confidential*****************************************
 * <copyright file = "account.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserAccountRoutingModule } from './user-account-routing.module';
import { CommonModule } from '@angular/common';
import { EqualValidator } from '../_shared/equal-validator.directive';
// import { ChangePassword } from 'src/app/account/change-password.component';
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgxLoadingModule } from 'ngx-loading';
import { ForgotPassword } from './forgotPassword/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { VerificationComponent } from './verification/verification-component';
import { RegisterComponent } from './registration/register.component';
import { RegistrationSuccessComponent } from './registration/registration-success.component';
import { PasswordChangeComponent } from './passwordChange/password-change.component';
// import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from '../_shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { SettingsService } from '../settings/settings-service';
import { CommonTemplateModule } from '../common/common-template.module';
import { SuperadminService } from '../superadmin/superadmin-service';
import { PasswordGeneratedComponent } from './passwordGenerated/password-generated.component';
import { UserService } from './user-service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    UserAccountRoutingModule,
    ModalModule.forRoot(),
    FormsModule,
    CommonModule,
    HttpModule,
    HttpClientModule,
    NgxLoadingModule,
    NgxMaskModule,
    NgxMaskModule.forRoot(),
    NgxMaskModule.forChild(),
    ReactiveFormsModule,
    CommonTemplateModule,
    SharedModule
  ],
  providers: [SettingsService, SuperadminService, UserService],
  declarations: [
    ForgotPassword,
    LoginComponent,
    VerificationComponent,
    RegisterComponent,
    EqualValidator,
    // ChangePassword,
    RegistrationSuccessComponent,
    PasswordChangeComponent,
    PasswordGeneratedComponent
  ]
})
export class UserAccountModule { }
