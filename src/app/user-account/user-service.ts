/*******************************************Confidential*****************************************
 * <copyright file = "user-service.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Injectable, NgZone, OnInit, Renderer2 } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpParamsOptions, HttpParams } from '@angular/common/http';
// import { debounce } from 'rxjs/operator/debounce';//#Revisit_IgnoredAsNotUsed
import { Company } from 'src/app/_shared/model/company-model';
import { User } from 'src/app/_shared/model/user-model';
// import { CombineLatestOperator } from 'rxjs/operator/combineLatest';//#Revisit_IgnoredAsNotUsed
import { Constants } from 'src/app/_shared/constant';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// import { Jsonp } from '@angular/http/src/http';//#Revisit_IgnoredAsNotUsed
import { DataService } from 'src/app/_core/data.service';
import { PaymentInfo } from 'src/app/_shared/model/payment-model';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';
import { RegistrationVM } from '../_shared/model/registrationVM';
@Injectable()
export class UserService implements OnInit {
  constants: any;
  userInfo: any;
  msg: any;
  private eventCallback = new Subject<string>(); // Source
  _eventCallback = this.eventCallback.asObservable(); // Stream
  private eventCallbackCompany = new Subject<string>(); // Source
  _eventCallbackCompany = this.eventCallbackCompany.asObservable(); // Stream
  private eventCallbackExpairy = new Subject<string>(); // Source
  _eventCallbackExpairy = this.eventCallbackExpairy.asObservable(); // Stream

  currentUserInitialsForUpdateProfile = new BehaviorSubject(null);
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService
  ) {
    this.constants = Constants;
    toastrService.toastrConfig.preventDuplicates = true;
  }
  ngOnInit() { }
  GetUser() {
    return this.dataService.getData('User/GetUser').pipe(map(response => {
      return response;
    }));
  }
  AddCompany(registrationVM: RegistrationVM) {
    registrationVM.company.isActive = true;
    registrationVM.company.createdOn = new Date();
    registrationVM.user.createdOn = new Date();
    registrationVM.user.isActive = false;

    registrationVM.company.firstName = registrationVM.user.firstName;
    registrationVM.company.lastName = registrationVM.user.lastName;
    registrationVM.company.contactNo = registrationVM.user.contactNo;
    registrationVM.company.email = registrationVM.user.email;
    return this.dataService.postElementForAllowAnonymous('Company', registrationVM)
      .pipe(map(response => {
        return response;
      }));
  }
  AddUser(user: User): Observable<any> {
    return this.dataService.postElement('User', user).pipe(map(response => {
      return response;
    }));
  }
  Verify(Id: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const options = { headers: headers };
    return this.http.get(this.constants.BASE_URL + 'User/Verify/' + Id, options).pipe(map(res => {
      const data = res;
      return data;
    }));
  }
  CreateTenant(Id: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const options = { headers: headers };
    return this.http.get(this.constants.BASE_URL + 'Company/CreateTenant/' + Id, options).pipe(map(res => {
      const data = res;
      return data;
    }));
  }

  Login(userName: string, password: string, ip: any): Observable<any> {
    const user: User = new User();
    user.email = userName;
    user.password = password;
    user.updatedBy = ip;
    return this.dataService.postElementForAllowAnonymous('User/Login', user).pipe(map(res => {
      const data = res;
      if (data) {
        // if (data.isLoggedIn ) {
        if (data.isLoggedIn && data.isActive) {
          // code for multilogin
          this.msg = 'Already login';

          return this.msg;
        } else {
          if (!data.result.isActive) {
            // this.toastrService.error(
            //   'Verify your account from given email address',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.msg = 'msg';
            return this.msg;
          } else {
            sessionStorage.setItem('currentUser', JSON.stringify(data.result));
            //  sessionStorage.setItem('AccessToken', data.token.result.value);
            sessionStorage.setItem('AccessToken', data.token.value);
            return res;
          }
        }
      } else {
        // this.toastrService.error(
        //   'Invalid login credentials',
        //   '',
        //   this.constants.toastrConfig
        // );
        return false;
      }
    }));
  }
  killSession(user: User) {
    return this.dataService.postElementForAllowAnonymous('User/KillSession', user).pipe(map(res => {
      return res;
    }));
  }
  continueSession(user: User) {
    return this.dataService.postElementForAllowAnonymous('User/ContinueSession', user).pipe(map(res => {
      return res;
    }));
  }
  GenerateOTP(resetPassword: any): Observable<any> {
    const url = 'User/SendLink';
    return this.dataService.postElementForAllowAnonymous(url, resetPassword).pipe(map(response => {
      return response;
    }));
  }

  ForgotPassword(resetPassword: any): Observable<any> {
    const url = 'User/ForgotPassword';
    return this.dataService.postElementForAllowAnonymous(url, resetPassword).pipe(map(response => {
      return response;
    }));
  }
  Changepassword(Changepassword: any): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    const body = 'oldPassword=' + Changepassword.oldPassword + '&newPassword=' + Changepassword.password;
    const url = 'User/ChangePassword';
    return this.http.post(this.constants.BASE_URL + url, body, options).pipe(map(res => {
      const data = res;
      return data;
    }));
  }
  UpdateUser(user: User): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    const body = user;
    return this.http.put(this.constants.BASE_URL + 'User', body, options).pipe(map(res => {
      const data = res;
      this.eventCallback.next(JSON.stringify(user));
      return data;
    }));
  }
  DeleteUser(id: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    return this.http.delete(this.constants.BASE_URL + 'User/' + id, options).pipe(map(res => {
      const data = res;
      return data;
    }));
  }
  UpdateCompany(company: Company): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    const body = company;
    return this.http.put(this.constants.BASE_URL + 'Company', body, options).pipe(map(res => {
      const data = res;
      if (data) {
        this.eventCallbackCompany.next(JSON.stringify(company));
      }
      return data;
    }));
  }
  GetGenralUser(id: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    return this.http.get(this.constants.BASE_URL + 'User/GetGenralUser/' + id, options).pipe(map(res => {
      const data = res;
      return data;
    }));
  }
  GetCompany(): Observable<any> {
    // //#Revisit_PostFolderMigrationFixes - commenting the below and using data service
    // const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    // headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    // const options = { headers: headers };
    // return this.http.get(this.constants.BASE_URL + 'Company/GetCompany', options).pipe(map(res => {
    //   const data = res;
    //   return data;
    // }));
    return this.dataService.getData('Company/GetCompany').pipe(map(response => {
      return response;
    }));
  }

  GetExpairy() {
    return this.dataService.getData('Company/GetExpairy').pipe(map(response => {
      return response;
    }));
  }

  getLoginInfo(id) {
    return this.dataService.getElementThroughId('configuration/GetLoginDetails', id).pipe(map(response => {
      return response;
    }));
  }

  GetStates() {
    return this.dataService.getDataAllowAnonymous('configuration/Getstates').pipe(map(response => {
      return response;
    }));
  }

  AddPayment(payment: PaymentInfo): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    const body = payment;
    return this.http.post(this.constants.BASE_URL + 'Company/AddPayment', body, options).pipe(map((res: any) => {
      const data = res;
      this.eventCallbackExpairy.next(JSON.stringify(data.day));
      return data;
    }));
  }

  GetCurrentPlan(): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    return this.http.get(this.constants.BASE_URL + 'Company/GetCurrentPlan', options).pipe(map(res => {
      const data = res;
      return data;
    }));
  }
  GetCurrentRole(): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    return this.http.get(this.constants.BASE_URL + 'Company/GetCurrentRole', options).pipe(map(res => {
      const data = res;
      return data;
    }));
  }
  CancelSubscription(): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
    const options = { headers: headers };
    return this.http.get(this.constants.BASE_URL + 'Company/CancelSubscription', options).pipe(map(res => {
      const data = res;
      return data;
    }));
  }

  ActivateUser(id) {
    return this.dataService.getElementThroughId('User/ActivateUser', id).pipe(map(response => {
      return response;
    }));
  }

  GetPlans() {
    return this.dataService.getData('Configuration/GetPlans').pipe(map(response => {
      return response;
    }));
  }
  getActiveUsers(id) {
    return this.dataService.getElementThroughId('User/GetActiveUserCount', id).pipe(map(response => {
      return response;
    }));
  }

  UpdateUsers(user: User) {
    return this.dataService.updateElement('User', user).pipe(map(response => {
      return response;
    }));
  }

  // code for multilogin
  LogoutUser(id) {
    return this.dataService.getElementThroughId('Configuration/Logout', id).pipe(map(response => {
      return response;
    }));
  }

  //  LogoutUserForMultiLogin(id) {
  //   const token = sessionStorage.getItem('AccessToken');
  //   sessionStorage.clear();
  //   localStorage.clear();
  //   return this.dataService
  //     .getElementThroughIdForLogOut('Configuration/Logout', id, token)
  //     .pipe(map(response => {
  //       return response;
  //     });
  //  }

  forcedLogoutUser(id) {
    return this.dataService.getElementThroughId('Configuration/ForcedLogout', id).pipe(map(response => {
      return response;
    }));
  }

  forcedLogin(id) {
    return this.dataService.getElementThroughId('Configuration/ForcedLogin', id).pipe(map(response => {
      return response;
    }));
  }

  // code for multilogin
  getLoginInformation(method) {
    return this.dataService.getData(method).pipe(map(response => {
      return response;
    }));
  }

  // Testing API for Suubcription
  Suubcription(custId: string, source: string) {
    return this.dataService.getDataAllowAnonymous('Configuration/CreateSubcription/' + custId + '/' + source).pipe(map(response => {
      return response;
    }));
  }

  getConfiguration() {
    return this.dataService.getData('User/GetTermsofUse').pipe(map(response => {
      return response;
    }));
  }

  updateInitials() {
    this.currentUserInitialsForUpdateProfile.next(1);
  }

  getUserDetails(id) {
    return this.dataService.getData(`User/${id}`).pipe(map(response => {
      return response;
    }));
  }

  uploadImage(file) {
    return this.dataService.UploadFile('Configuration/fileCompress', file);
  }
}
