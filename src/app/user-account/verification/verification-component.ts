/*******************************************Confidential*****************************************
 * <copyright file = "verification.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Injectable, NgZone, OnInit } from '@angular/core';
import { UserService } from 'src/app/user-account/user-service';
@Component({
  templateUrl: './verification-component.html'
})
export class VerificationComponent implements OnInit {
  alreadyActive: boolean;
  constructor(
    private _route: Router,
    private route: ActivatedRoute,
    private _UserService: UserService,
    private ngZone: NgZone
  ) {
    // this.alreadyActive = false;
    this.alreadyActive = this.route.snapshot.queryParams['AlreadyActive'];
  }
  ngOnInit() {
    // tslint:disabl
    const auth: string = this.route.snapshot.queryParams['Auth'];

    this._UserService.Verify(auth).subscribe(
      res => {
        this.ngZone.run(() => {
          if (res) {
            if (res.isActive) {
              this._route.navigate(['/account/verification'], {
                queryParams: { AlreadyActive: true }
              });
            }
          }
        });
      },
      err => { }
    );
    if (!this.alreadyActive) {
      this._UserService.CreateTenant(auth).subscribe(
        res => {
          this.ngZone.run(() => { });
        },
        err => { }
      );
    }
  }
  // onClick() {
  //   const auth: string = this.route.snapshot.queryParams['Auth'];
  //   this._UserService.CreateTenant(auth).subscribe(
  //     res => {
  //       this.ngZone.run(() => {});
  //     },
  //     err => {}
  //   );
  //   this._route.navigate(['/account/login']);
  // }
}
