/*******************************************Confidential*****************************************
 * <copyright file = "account-routing.module.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForgotPassword } from './forgotPassword/forgot-password.component';
import { LoginComponent } from './login/login.component';
// import { ChangePassword } from 'src/app/account/change-password.component';
import { RouteGaurd } from '../_core/RouteGaurd';
import { RegisterComponent } from './registration/register.component';
import { VerificationComponent } from './verification/verification-component';
import { PasswordChangeComponent } from './passwordChange/password-change.component';
import { RegistrationSuccessComponent } from './registration/registration-success.component';
import { PasswordGeneratedComponent } from './passwordGenerated/password-generated.component';
const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      title: 'Account',
      isLogin: false
    },
    children: [
      {
        path: 'forgot-password',
        component: ForgotPassword,
        data: {
          title: 'Forgot Password'
        }
      },
      {
        path: 'login',
        component: LoginComponent,
        data: {
          title: 'Sign In'
        }
      },
      {
        path: 'register',
        component: RegisterComponent,
        data: {
          title: 'Sign Up'
        }
      },
      {
        path: 'verification',
        component: VerificationComponent,
        data: {
          title: 'Verification'
        }
      },
      {
        path: 'password-change-success',
        component: PasswordChangeComponent,
        data: {
          title: 'Change Password'
        }
      },
      {
        path: 'password-generated-success',
        component: PasswordGeneratedComponent,
        data: {
          title: 'Password generated'
        }
      },
      {
        path: 'registration-success',
        component: RegistrationSuccessComponent,
        data: {
          title: 'Regisrtation Success'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserAccountRoutingModule { }
