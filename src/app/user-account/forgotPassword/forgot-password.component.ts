/*******************************************Confidential*****************************************
 * <copyright file = "forgot-password.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component, NgZone, OnInit } from '@angular/core';
import { Constants } from 'src/app/_shared/constant';
import { UserService } from 'src/app/user-account/user-service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: './forgot-password.component.html'
})

// tslint:disable-next-line:component-class-suffix
export class ForgotPassword implements OnInit {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  validEmail = false;
  public loading = false;
  isOTPSend: boolean;
  isOTP: boolean;
  forgotPassword: any = {};
  constants: any;

  newUserForPasswordGeneration = false;
  constructor(
    private route: ActivatedRoute,
    private _UserService: UserService,
    private router: Router,
    private _toastrService: ToastrService,
    private ngZone: NgZone
  ) {
    this.isOTPSend = false;
    this.forgotPassword.email = '';
    this.forgotPassword.contactNo = '';
    this.forgotPassword.otp = '';
    this.forgotPassword.password = '';
    this.forgotPassword.confirmPassword = '';
    this.forgotPassword.isThisNewUser = 'No';
    this.constants = Constants;
    this._toastrService.toastrConfig.preventDuplicates = true;
  }

  ngOnInit() {
    this.forgotPassword.otp = this.route.snapshot.queryParams['auth'];
    this.forgotPassword.email = this.route.snapshot.queryParams['mail'];
    const isForNewUser = this.route.snapshot.queryParams['newuser']
      ? true
      : false;
    if (this.forgotPassword.otp && !isForNewUser) {
      this.isOTPSend = true;
    } else if (isForNewUser) {
      this.newUserForPasswordGeneration = true;
      this.isOTPSend = true;
    }
  }

  GenerateOTP(isValid: boolean) {
    this.loading = true;
    this.validEmail = false;
    if (isValid) {
      this._UserService.GenerateOTP(this.forgotPassword).subscribe(
        res => {
          this.loading = false;
          this.ngZone.run(() => {
            this.isOTP = res;
            if (this.isOTP === false) {
              this.loading = false;
              // this._toastrService.error(
              //   'Please provide valid details.',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.validEmail = true;
            }
          });
        },
        err => {
          // this._toastrService.error(
          //   'Please provide valid details.',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.loading = false;
        }
      );
    } else {
      this.loading = false;
      // this._toastrService.error(
      //   'Please provide valid details.',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  ResetPassword(isValid: boolean) {
    this.loading = true;
    if (isValid) {
      this.loading = true;
      this.forgotPassword.isThisNewUser = this.newUserForPasswordGeneration ? 'Yes' : 'No';
      this._UserService.ForgotPassword(this.forgotPassword).subscribe(
        res => {
          this.loading = false;
          this.ngZone.run(() => {
            if (res) {
              this.loading = false;
              // this._toastrService.success(
              //   'Password reset successfully!',
              //   '',
              //   this.constants.toastrConfig
              // );
              // this.router.navigate(['/account', 'login']);
              this.newUserForPasswordGeneration
                ? this.router.navigate(['/account', 'password-generated-success'])
                : this.router.navigate(['/account', 'password-change-success']);
            } else {
              // this._toastrService.error(
              //   'Link has been expaired!',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.show = true;
              this.popupOpen((this.popupFor = 'ExpierdLink'));
            }
          });
        },
        err => {
          this.loading = false;
        }
      );
    } else {
      this.loading = false;
      // this._toastrService.error(
      //   'Please provide all details.',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  // messages display
  popupOpen(popupFor) {
    // this.titletoClose();
    if (this.popupFor === 'ExpierdLink') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Link has been expaired!';
    }
  }

  yes(ConfirmModal) {
    this.show = false;
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }
}
