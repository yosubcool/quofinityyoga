/*******************************************Confidential*****************************************
 * <copyright file = "login.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component, NgZone, OnInit } from "@angular/core";
import { UserService } from "src/app/user-account/user-service";
import { Constants } from "src/app/_shared/constant";
// import { debounce } from "rxjs/operator/debounce";//#Revisit_IgnoredAsNotUsed
import { Router } from "@angular/router";
import { Http } from "@angular/http";
import { User } from "src/app/_shared/model/user-model";
import { ToastrService } from "ngx-toastr";
@Component({
  templateUrl: "./login.component.html",
})
export class LoginComponent implements OnInit {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  public loading = false;
  [x: string]: any;
  email: string;
  activeUser: User;
  password: string;
  constants: any;
  RememberMe: boolean;
  ipAddress: any;
  constructor(
    private _route: Router,
    private _UserService: UserService,
    private _toastrService: ToastrService,
    private ngZone: NgZone,
    private http: Http
  ) {
    this.constants = Constants;
    window.scrollTo(0, 0);
    // sessionStorage.clear();
  }

  ngOnInit(): void {
    const string = localStorage.getItem("userAuth");
    if (string && string !== "null") {
      const user = JSON.parse(atob(string));
      this.email = user.email;
      this.password = user.password;
      this.RememberMe = true;
    }
    // sessionStorage.clear(); /****** */
    let Data: any;
    this.http.get("https://jsonip.com").subscribe((data) => {
      Data = data.json();
      this.ipAddress = Data.ip;
    });
  }

  login(isValid: boolean) {
    if (isValid) {
      this.loading = true;
      this._UserService
        .Login(this.email, this.password, this.ipAddress)
        .subscribe({
          next: (res) => {
            this.loading = false;
            this.ngZone.run(() => {
              // if (res !== 'msg' && res !== 'Already login') {
              if (res !== "msg" && !res.alreadyLogin) {
                if (this.RememberMe) {
                  const user: any = {};
                  user.email = this.email;
                  user.password = this.password;
                  const string = btoa(JSON.stringify(user));
                  localStorage.setItem("userAuth", string);
                } else {
                  localStorage.setItem("userAuth", null);
                }
                this._route.navigate(["/dashboard"]);
              } else if (res === "msg") {
                this.show = true;
                this.popupOpen((this.popupFor = "Verify your account"));
                // } else if (res === 'Already login') {
              } else if (res.alreadyLogin) {
                // code for multilogin
                this.show = true;
                this.activeUser = res.result;
                this.popupOpen((this.popupFor = "Already login"));
              } else {
                this.show = true;

                this.popupOpen((this.popupFor = "Invalid details"));
              }
            });
          },
          error: (err) => {
            this.loading = false;
            this.show = true;
            this.popupOpen((this.popupFor = "Invalid details"));
          }
        }
        );
    } else {
      this.loading = false;
      this.show = true;
      this.popupOpen((this.popupFor = "Invalid details"));
      // this._toastrService.error(
      //   'Please provide valid details.',
      //   '',
      //   this.constants.toastrConfig
      // );
    }
  }

  // messages display
  popupOpen(popupFor) {
    // this.titletoClose();

    if (this.popupFor === "Invalid details") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Please provide valid details.";
    } else if (this.popupFor === "Verify your account") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = " Verify your account from given email address.";
    } else if (this.popupFor === "Already login") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = ` You presently have an active login session. \n
      <br><br> This is an indication that you are currently logged in on an additional browser or device, or you recently closed your browser or shutdown your device without logging out of the application.\n <br><br> Would you like to continue logging in and cancel your existing session ?`;
    }
  }

  yes(ConfirmModal) {
    this.show = false;
    if (this.popupFor === "Already login") {
      this._UserService.forcedLogin(this.activeUser.id).subscribe((res) => {
        if (res) {
          sessionStorage.clear();
          sessionStorage.setItem("currentUser", JSON.stringify(res.result));
          sessionStorage.setItem("AccessToken", res.token.value);
          this._route.navigate(["/dashboard"]);
        }
      });
    }
    ConfirmModal.hide();
  }

  no(ConfirmModal) {
    // this._UserService.forcedLogoutUser(this.activeUser.id).subscribe(res => {
    //   if (res) {
    //     localStorage.clear();
    //     sessionStorage.clear();
    //     localStorage.clear();
    //     this.router.navigate(['pages/app-home']);
    //   }
    // });
    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }
}
