import { Quote } from "src/app/_shared/model/quote-model";
/*******************************************Confidential*****************************************
 * <copyright file = "full-layout.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */
import { Constants } from "src/app/_shared/constant";
import {
  Component,
  OnInit,
  NgZone,
  OnDestroy,
  HostListener,
} from "@angular/core";
import { UserService } from "src/app/user-account/user-service";
// import { debug, debuglog } from "util";//#Revisit_IgnoredAsNotUsed
import {
  Router,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError,
} from "@angular/router";
import { Search } from "src/app/_shared/model/search-model";
import { DataService } from "src/app/_core/data.service";
// import { debounce } from "rxjs/operator/debounce"; //#Revisit_IgnoredAsNotUsed
import { AutoLogoutService } from "src/app/_core/autoLogoutService";
import { User } from "../_shared/model/user-model";
import { Company } from "../_shared/model/company-model";
import { SettingsService } from "src/app/settings/settings-service";
// import { ProfileComponent } from "src/app/settings/profile/profile.component"; //#Revisit_IgnoredAsNotUsed
// import "rxjs/add/observable/interval"; //#revisit_rxjsUnknownImport
import { Observable, Subscription, interval } from "rxjs";
import { HttpClient } from "@angular/common/http";
declare var $: any;
@Component({
  selector: "app-dashboard",
  templateUrl: "./full-layout.component.html",
  providers: [AutoLogoutService],
})
export class FullLayoutComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  isExpiry = false;
  company: Company;
  showBtn = 0;
  showSearch = false;
  body: any;
  searched = [];
  search = new Search();
  userName: string;
  public loading = false;
  public isPaymentDone: any;
  public companyLogo: string;
  public disabled = false;
  public tokenPayload: string;
  trialmessage: string;
  constants: any;
  collections = [];
  collection: string;
  index: number;
  isMsgShow = false;
  isCallSent = false;
  public status: { isopen: boolean } = { isopen: false };
  currentUser: User;
  expiryDays: number;
  logoutResponse: any;
  subscriptions: Subscription[] = [];
  token: any;
  Data: any;
  initials: string;
  callSequenceForSearchProduct = [];
  isRefresh: boolean;

  isAlreadyTimmedOut = false;
  isActionHappens = false;

  constructor(
    private userService: UserService,
    private dataService: DataService,
    private ngZone: NgZone,
    private router: Router,
    private settingsService: SettingsService,
    private _AutoLogoutService: AutoLogoutService,
    private http: HttpClient
  ) {
    this.expiryDays = 1;
    this.GetCompanys();
    this.subscriptions.push(
      router.events.subscribe((event: Event) => {
        // if (event instanceof NavigationStart) {
        //   // console.log(event.toString());
        // }
        this.subscriptions.push(
          interval(300000).subscribe((val) => {
            this.GetExpairy();
          })
        );

        // this.subscriptions.push(
        //   Observable.interval(120000).subscribe(val => {
        //     this.getLoginInfo();
        //   })
        // );

        if (event instanceof NavigationEnd) {
          if (
            event.url !== "/settings/profile" &&
            event.url !== "/settings/change-password" &&
            event.url !== "/admin/company" &&
            event.url !== "/settings/setting"
          ) {
            if (this.expiryDays === 0) {
              this.router.navigate(["settings/my-plans"]);
            }
          }
        }
        // if (event instanceof NavigationError) {
        //   // console.log(event.error);
        // }
      })
    );

    const that = this;
    // this.subscriptions.push(
    //   _AutoLogoutService.eventCallback$.subscribe(data => {
    //     const ConfirmModal = this.show;
    //     // this.LogOut();
    //     this.typeofMessage = 'Hello';
    //     this.messageToDisplay = 'Would you like to copy the quote:';
    //     this.noteToDisplay = null;
    //     this.showButtons = true;
    //     this.notification = null;
    //     // this.LogOut(); // TO Do un comment
    //   })
    // );

    _AutoLogoutService.eventCallback$.subscribe((data) => {
      if (data) {
        if (!this.isAlreadyTimmedOut) {
          this.isActionHappens = false;
          this.isAlreadyTimmedOut = true;
          this.show = true;
          this.typeofMessage = "Hello";
          this.messageToDisplay =
            "Your session is about to time out, would you like to continue?";
          this.noteToDisplay = null;
          this.showButtons = true;
          this.notification = null;
          setTimeout(() => {
            if (!this.isActionHappens) {
              console.log("auto-logout route");
              this.forcedLogOut();
              this.isActionHappens = true;
            }
          }, 60000);
        }
      }
    });
    this.constants = Constants;
    this.collections = this.constants.collections;
    this.currentUser = new User();
    this.company = new Company();
    // this.dataService.getIP();
    this.userService.currentUserInitialsForUpdateProfile.subscribe(
      (value: any) =>
        value ? this.getCurrentUserAndUpdateInitialsForProfile() : null
    );
  }

  // @HostListener('window:beforeunload', ['$event'])
  // beforeunloadHandler(event) {
  //   this.forcedLogOut();
  //   for (let index = 0; index < 50000; index++) {
  //     for (let i = 0; i < 120000; i++) {
  //       let j = 0;
  //       j = j + 1;
  //     }
  //   }
  // }

  ngOnInit(): void {
    // console.log('Before performance:', performance.navigation.type);
    if (performance.navigation.type === 1) {
      // console.log('Page reloaded');
      const currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
      // const interval = setInterval(() => {
      this.loading = true;
      // this.userService
      //   .forcedLogin(currentUser ? currentUser.id : null)
      //   .subscribe(
      //     (res: any) => {
      //       // if (res.isLoggedIn) {
      //       // console.log('ForceLogin Service completed: ', res);
      //       // clearInterval(interval);
      //       this.loading = false;
      //       // }
      //     },
      //     err => {
      //       // console.log('ForceLogin Service error: ' , err);
      //     }
      //   );
      // }, 2000);
    } else {
      // console.log('Page loaded first time');
      if (sessionStorage.getItem("callFromHistory")) {
        // console.log('callFromHistory');
        sessionStorage.clear();
        localStorage.clear();
        this.router.navigate(["/account/login"]);
      } else {
        sessionStorage.setItem("callFromHistory", "true");
      }
    }

    this.getCurrentUserAndUpdateInitialsForProfile();
    this.token = sessionStorage.getItem("AccessToken");

    const that = this;
    window.onbeforeunload = function (e) {
      //  that.forcedLogOut();
      //  localStorage.clear();
      for (let index = 0; index < 50000; index++) {
        for (let i = 0; i < 10000; i++) {
          let j = 0;
          j = j + 1;
        }
      }
    };

    window.onunload = function (e) {
      // sessionStorage.clear();
      // localStorage.clear();
      for (let index = 0; index < 50000; index++) {
        for (let i = 0; i < 120000; i++) {
          let j = 0;
          j = j + 1;
        }
        window.stop();
      }
    };

    this.http.get("https://jsonip.com").subscribe((data) => {
      this.Data = data;
    });

    this.GetExpairy();
    this.index = 0;
    this.collection = "All";

    // $('.toggle-menu-btn').click(function () {
    //   $('.left-side-menubar').toggleClass('menu-display');
    //   $('.left-menubar ul li a .menu-text').toggleClass('show-menu');
    //   $('.main-container').toggleClass('main-container-compress');
    //   $('.sidebar-fixed').toggleClass('sidebar-hidden');
    //   $('.aside-menu-hidden').toggleClass('aside-menu-fixed');
    // });

    $("#search-input").keyup(function () {
      $(".search-dropdown").show();
      const txt = $("#search-input");
      if (txt.val() != null && txt.val() !== "") {
        $(".search-dropdown").show();
        $(".search-dropdown-1").hide();
      } else {
        $(".search-dropdown").hide();
      }
    });

    if (this.currentUser) {
      this.subscriptions.push(
        this.settingsService
          .getAccessMatrix(this.currentUser.role.id)
          .subscribe(
            (res) => {
              this.ngZone.run(() => { });
            },
            (err) => { }
          )
      );
    }

    this.subscriptions.push(
      this.userService._eventCallbackExpairy.subscribe((data) => {
        this.expiryDays = JSON.parse(data);
        this.trialmessage = null; // this.expiryDays + ' subscription days left';
      })
    );

    this.subscriptions.push(
      this.userService._eventCallback.subscribe((data) => {
        this.currentUser = JSON.parse(data);
      })
    );

    this.subscriptions.push(
      this.userService._eventCallbackCompany.subscribe((data) => {
        this.company = JSON.parse(data);
      })
    );

    document.body.scrollTo(100, 200);
    // window.scrollTo(0, 0);
  }

  getCurrentUserAndUpdateInitialsForProfile() {
    this.currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    this.initials =
      this.currentUser.firstName.charAt(0) +
      this.currentUser.lastName.charAt(0);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  toggleSidebar() {
    $(".left-side-menubar").toggleClass("menu-display");
    $(".left-menubar ul li a .menu-text").toggleClass("show-menu");
    $(".main-container").toggleClass("main-container-compress");
    $(".sidebar-fixed").toggleClass("sidebar-hidden");
    $(".aside-menu-hidden").toggleClass("aside-menu-fixed");
  }

  public toggled(open: boolean): void {
    // console.log('Dropdown is now: ', open);
  }

  GetExpairy() {
    this.subscriptions.push(
      this.userService.GetExpairy().subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            if (res) {
              //#region #RemoveBeforeDeployment DEVELOPMENT PURPOSE MAKING TRAIL DAYS AS 10
              //***********REMOVE THIS BEFORE FINAL CHECK-IN*******************
              //#DONOTCHECKIN
              res.expairyDays = 100;
              res.subscription = this.constants.SUBSCRIPTION_TRIAL;
              //#endregion
              this.expiryDays = res.expairyDays;
              if (res.subscription === this.constants.SUBSCRIPTION_TRIAL) {
                this.trialmessage = res.expairyDays + " trial days left";
                // this.isExpiry = true;
              } else {
                this.trialmessage = null; // res.expairyDays + ' subscription days left';
                // this.router.navigate(['/dashboard']);
              }
              if (res.expairyDays === 0) {
                this.router.navigate(["settings/my-plans"]);
              }
            }
          });
        },
        error: (err) => { }
      }
      )
    );
  }

  GetCompanys() {
    this.loading = true;
    this.subscriptions.push(
      this.userService.GetCompany().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.company = res;
            this.loading = false;
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  // Search Implementation -Tejaswini
  onSearch(body) {
    // this.body = this.body.trimStart();

    if (body === null || body === "") {
      // this.loading = false;
    } else if (this.body) {
      // this.body = this.body.trimStart();
      this.searched = [];
      this.showSearch = true;
      this.search.accountName = this.body;
      this.search.productName = this.body;
      this.search.quoteName = this.body;
      this.search.contactName = this.body;
      this.search.kitName = this.body;
      this.search.manufacturerPartNumber = this.body;

      if (this.collection === "All" && this.index === 0) {
        this.callSequenceForSearchProduct.push(
          body + this.callSequenceForSearchProduct.length
        );
        this.dataService
          .getElementThroughAnyParam("User", this.search)
          .subscribe((res) => {
            // tslint:disable-next-line:max-line-length
            if (
              body.toString() +
              (this.callSequenceForSearchProduct.length - 1) ===
              this.callSequenceForSearchProduct[
              this.callSequenceForSearchProduct.length - 1
              ]
            ) {
              this.searched = [];
              Object.keys(res).forEach((key) => {
                this.searched.push({
                  collectionName: key.charAt(0).toUpperCase() + key.slice(1),
                  collectionData: res[key],
                });
              });
            }
          });

        // old
        // this.collections.forEach(collect => {
        //   this.dataService
        //     .getElementThroughAnyParam(collect, this.search)
        //     .subscribe(
        //       res => {
        //           // console.log(collect, '*: ', body);
        //           let data = [];
        //           data = res;
        //           let i = 0;
        //           // console.log('1: ', this.searched);

        //           if (this.searched) {
        //             this.searched.forEach(col => {
        //               if (col.collectionName === collect) {
        //                 this.searched.splice(i, 1);
        //               } else {
        //                 i = i + 1;
        //               }
        //             });
        //           }
        //           // console.log('2: ', this.searched);

        //           if (data.length) {
        //             this.searched.push({
        //               collectionName: collect,
        //               collectionData: data
        //             });
        //           } else {
        //             this.isMsgShow = true;
        //           }
        //           // console.log('3: ', this.searched);

        //       },
        //       err => {
        //         this.loading = false;
        //       }
        //     );
        // });
      } else {
        this.searched = [];
        this.dataService
          .getElementThroughAnyParam(this.collection, this.search)
          .subscribe(
            (res) => {
              let data = [];
              data = res;
              let i = 0;
              if (this.searched) {
                this.searched.forEach((col) => {
                  if (col.collectionName === this.collection) {
                    this.searched.splice(i, 1);
                  } else {
                    i = i + 1;
                  }
                });
              }
              if (data.length) {
                this.searched.push({
                  collectionName: this.collection,
                  collectionData: data,
                });
              } else {
                this.isMsgShow = true;
              }
            },
            (err) => {
              this.loading = false;
            }
          );
      }
    }
  }

  // Tab Selection for Search - Tejaswini
  showUndoBtn(index) {
    this.showBtn = index;
  }

  // navigate to details page on click in search - Tejaswini
  goToDetails(collection, id) {
    this.body = null;
    $(".search-dropdown").hide();
    this.showBtn = 0;
    if (this.collections[0] === collection) {
      this.router.navigate(["/accounts/accounts-detail"], {
        queryParams: { id: btoa(id) },
      });
    } else if (this.collections[1] === collection) {
      this.router.navigate(["/contacts/contact-detail"], {
        queryParams: { id: btoa(id) },
      });
      // this.router.navigate(['/contacts/contact-detail']);
    } else if (this.collections[2] === collection) {
      this.router.navigate(["quotes/quotes-detail"], {
        queryParams: { id: btoa(id) },
      });
      // this.router.navigate(['quotes/quotes-detail']);
    } else if (this.collections[3] === collection) {
      this.router.navigate(["/products/products-detail"], {
        queryParams: { id: btoa(id) },
      });
    } else if (this.collections[4] === collection) {
      this.router.navigate(["/kits/kits-detail"], {
        queryParams: { id: btoa(id) },
      });
    }
  }

  resetForm(form) {
    // tslint:disable-next-line:curly
    if (document.getElementById("nav-home-all") !== null)
      document.getElementById("nav-home-all").classList.add("active");
    // tslint:disable-next-line:curly
    if (document.getElementById("nav-account-tab") !== null)
      document.getElementById("nav-account-tab").classList.remove("active");
    // tslint:disable-next-line:curly
    if (document.getElementById("nav-contact-tab") !== null)
      document.getElementById("nav-contact-tab").classList.remove("active");
    // tslint:disable-next-line:curly
    if (document.getElementById("nav-quotes-tab") !== null)
      document.getElementById("nav-quotes-tab").classList.remove("active");
    // tslint:disable-next-line:curly
    if (document.getElementById("nav-product-tab") !== null)
      document.getElementById("nav-product-tab").classList.remove("active");
    // tslint:disable-next-line:curly
    if (document.getElementById("nav-kits-tab") !== null)
      document.getElementById("nav-kits-tab").classList.remove("active");
    this.showBtn = 0;
    this.collection = "All";
    this.index = 0;
    $(".search-dropdown").hide();
    this.body = null;
    form.resetForm();
  }

  // code for multilogin

  LogOut() {
    this.loading = true;
    this.subscriptions.push(
      this.userService.LogoutUser(this.currentUser.id).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.logoutResponse = res;
            if (this.logoutResponse !== null) {
              this.loading = false;
              this.currentUser = null;
              sessionStorage.clear();
              this.router.navigate(["pages/app-home"]);
              this.isAlreadyTimmedOut = false;
            }
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
    $(".sidebar-fixed").removeClass("sidebar-hidden");
  }

  forcedLogOut() {
    this.loading = true;
    // sessionStorage.clear();
    // localStorage.clear();
    this.subscriptions.push(
      this.userService.forcedLogoutUser(this.currentUser.id).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.logoutResponse = res;
            if (this.logoutResponse !== null) {
              this.loading = false;
              this.currentUser = null;
              sessionStorage.clear();
              this.router.navigate(["pages/app-home"]);
              this.isAlreadyTimmedOut = false;
            }
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
    $(".sidebar-fixed").removeClass("sidebar-hidden");
  }

  // getLoginInfo() {
  //   this.subscriptions.push(
  //     this.userService.getLoginInfo(this.currentUser.id).subscribe(
  //       res => {
  //         this.ngZone.run(() => {
  //           if (res) {
  //             const result = res;

  //             if (result.token === this.Data.ip) {
  //               // console.log('Stay login');
  //             } else {
  //               // this.LogOut();
  //               this.forcedLogOut();
  //             }
  //           }
  //         });
  //       },
  //       err => {}
  //     )
  //   );
  // }

  yes(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
    this.isActionHappens = true;
  }

  no(ConfirmModal) {
    this.LogOut();
    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
    this.isActionHappens = true;
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
    this.isActionHappens = true;
  }

}
