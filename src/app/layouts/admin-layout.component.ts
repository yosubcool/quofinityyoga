import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AutoLogoutService } from 'src/app/_core/autoLogoutService';
declare var $: any;

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  providers: [AutoLogoutService]
})
export class AdminLayoutComponent implements OnInit {
  isLogin: boolean;
  constructor(private router: Router
    , private _AutoLogoutService: AutoLogoutService
  ) {
    this.isLogin = false;
    router.events.subscribe(val => {
      const token = sessionStorage.getItem('AccessToken');
      if (token) {
        this.isLogin = true;
      } else {
        this.isLogin = false;
      }
    });
    _AutoLogoutService.eventCallback$.subscribe(data => {
      this.LogOut();
    });
  }

  ngOnInit() {
    // $('.toggle-menu-btn').click(function () {
    //   $('.left-side-menubar').toggleClass('menu-display');
    //   $('.left-menubar ul li a .menu-text').toggleClass('show-menu');
    //   $('.main-container').toggleClass('main-container-compress');
    //   $('.sidebar-fixed').toggleClass('sidebar-hidden');
    //   $('.aside-menu-hidden').toggleClass('aside-menu-fixed');
    // });
  }

  toggleSidebar() {
    $('.left-side-menubar').toggleClass('menu-display');
    $('.left-menubar ul li a .menu-text').toggleClass('show-menu');
    $('.main-container').toggleClass('main-container-compress');
    $('.sidebar-fixed').toggleClass('sidebar-hidden');
    $('.aside-menu-hidden').toggleClass('aside-menu-fixed');
  }

  LogOut() {
    sessionStorage.clear();
    this.router.navigate(['/superadmin/login']);
  }
}
