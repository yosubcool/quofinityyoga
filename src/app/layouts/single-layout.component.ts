/*******************************************Confidential*****************************************
 * <copyright file = "single-layout.component.ts">
 *   Copyright (c) BlueBenz Digitizations Pvt. Ltd. All rights reserved.
 *   NOTICE: Bluebenz Digitizations Core Framework SOFTWARE LICENSE AGREEMENT as per License file.
 * </copyright>
 * <author>Akshara,Tanveer</author>
 */

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pages',
  templateUrl: 'single-layout.component.html'
  // template: '<router-outlet></router-outlet>',
})
export class SingleLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }
}
