import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSlimScrollModule } from 'ngx-slimscroll';
import { ModalModule } from 'ngx-bootstrap/modal';
// import { HttpModule } from '@angular/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxMaskModule } from 'ngx-mask';
// import { DataTablesModule } from 'angular-datatables';
import { AccountsRoutingModule } from './accounts-routing.module';
import { SharedModule } from '../_shared/shared.module';
import { CommonTemplateModule } from '../common/common-template.module';
import { DashboardService } from '../dashboard/dashboard.service';
import { QuotesService } from '../quotes/quotes-service';
import { DataService } from '../_core/data.service';
import { AccountsDetailComponent } from './accounts-detail/accounts-detail.component';
import { AccountsListComponent } from './accounts-list/accounts-list.component';
import { AccountsService } from './accounts-service';
import { ContactsService } from '../contacts/contacts-service';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    AccountsRoutingModule,
    NgxLoadingModule,
    CommonTemplateModule,
    ModalModule.forRoot(),
    SharedModule,
    NgxMaskModule,
    TableModule,
    ButtonModule
  ],
  providers: [AccountsService, DataService, DashboardService, QuotesService, ContactsService],
  declarations: [AccountsListComponent, AccountsDetailComponent]
})
export class AccountsModule { }
