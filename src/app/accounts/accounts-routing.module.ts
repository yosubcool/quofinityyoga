import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteGaurd } from '../_core/RouteGaurd';
import { AccountsDetailComponent } from './accounts-detail/accounts-detail.component';
import { AccountsListComponent } from './accounts-list/accounts-list.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      title: 'Accounts',
      isLogin: true
    },
    children: [
      {
        path: 'accounts-detail',
        component: AccountsDetailComponent,
        data: {
          title: 'Accounts Detail'
        }
      },
      {
        path: 'accounts-list',
        component: AccountsListComponent,
        data: {
          title: 'Accounts List'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
