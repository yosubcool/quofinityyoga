import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
declare var $: any;
import { ISlimScrollOptions } from 'ngx-slimscroll';
import { AccountsService } from '../accounts-service';
import { Contact } from '../../_shared/model/contact-model';
import { History } from '../../_shared/model/history-model';
// import { PricePipe } from 'app/_shared/price_pipe';
import {
  Router,
  ActivatedRoute,
  Event,
  NavigationStart,
  NavigationEnd,
  NavigationError
} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Constants } from '../../_shared/constant';
import { Notes } from '../../_shared/model/notes-model';
// import { concat } from 'rxjs/observable/concat';
// import { Response } from '@angular/http/src/static_response';
// import { PaginatePipe, PaginationService } from 'ng2-pagination';
// import { Ng2PaginationModule } from 'ng2-pagination';
import { User } from '../../_shared/model/user-model';
import { Quote } from 'src/app/_shared/model/quote-model';
import { QuotesService } from 'src/app/quotes/quotes-service';
import { UserService } from 'src/app/user-account/user-service';
import { DataService } from 'src/app/_core/data.service';
import { Account } from 'src/app/_shared/model/account-model';
import { Actions } from 'src/app/_shared/model/actions-model';
import { Company } from 'src/app/_shared/model/company-model';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { Subscription } from 'rxjs';
import { ContactsService } from 'src/app/contacts/contacts-service';
// import { log } from 'util';
@Component({
  selector: 'app-accounts-detail',
  templateUrl: './accounts-detail.component.html',
  styles: []
})
export class AccountsDetailComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  cntPresent = false;
  searchContactForm = false;
  isContactAlreadyPresent = false;
  reverse = false;
  key: any;
  quoteRecords: any;
  isEmailRepated = false;
  record: boolean;
  historyData: any[];
  myContacts: any;
  allContacts: any;
  showtext = false;
  loading: boolean;
  accountDetailsForm = false;
  opts: ISlimScrollOptions;
  quote: Quote;
  allStatusCount: any = {};
  account: Account;
  contacts: any = [];
  notes: any;
  contactsList: any;
  accountTitleCopy = false;
  accountTitleEdit = false;
  quotes: any = [];
  oldHistoryData: any[];
  // isFormSubmitted = false;
  statusList: any;
  contact = new Contact();
  constants: any;
  AccountId: string;
  quotesInfo: Pagination;
  quoteStats: any = [];
  note: Notes;
  notenew: Notes;
  ContactId: string;
  isAccountRepated = false;
  totalCount: any;
  edit = false;
  contactItem: any;
  log: History = new History();
  activity: History[];
  editbtn = true;
  accountsList: Pagination;
  cntpopup = false;
  showallphone = false;
  user: User;
  accessValid: any;
  addQuote: boolean;
  selectedAlpha: string;
  status: string;
  pagesCount: number;
  subscriptions: Subscription[] = [];
  data: any;
  accountTitle = false;
  SearchText: string;
  accountName = false;
  selecteContact = new Contact();
  oldAccount: Account;
  alldata: any[];
  noteIndex: any;
  showmsg = false;
  presentContact = false;
  index: any;
  newMask: string = " (000) 000-0000";
  mask: any[] = [
    '+',
    '1',
    ' ',
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  isOpenPopup = false;
  company: Company;
  constructor(
    private accountsService: AccountsService,
    private dataService: DataService,
    private contactService: ContactsService,
    private route: ActivatedRoute,
    private _quotesService: QuotesService,
    private router: Router,
    private toastrService: ToastrService,
    private _route: Router,
    private ngZone: NgZone,
    private _UserService: UserService
  ) {
    this.company = new Company();
    this.accessValid = new Actions();
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    this.account = new Account();
    this.contacts = new Array<Contact>();
    this.contactsList = new Array<Contact>();
    this.quotes = new Array<Quote>();
    this.quote = new Quote();
    this.accountsList = new Pagination();
    this.contact = new Contact();
    this.log = new History();
    this.quotesInfo = new Pagination();
    this.note = new Notes();
    this.notenew = new Notes();
    this.oldAccount = new Account();
    toastrService.toastrConfig.preventDuplicates = true;
    this.constants = Constants;
    this.alldata = [];
    this.SearchText = null;
    _route.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        let id = this.route.snapshot.queryParams['id'];
        id = atob(id);
        if (this.AccountId && this.AccountId !== id) {
          this.loading = true;
          if (!id) {
            this._route.navigate(['/accounts/accounts-list']);
          }
          this.AccountId = id;

          this.getAccount(this.AccountId);
          $(window).scrollTop(0);
          this.getContact();
          this.opts = {
            position: 'right',
            barBackground: '#000000'
          };
          this.getQuotes(0);
          this.getModuleAccess();
          this.log.userId = this.user.id;

          if (document.getElementById('accounts') !== null) {
            document.getElementById('accounts').classList.add('active');
          }
        }
      }
    });
  }

  ngOnInit() {
    $(window).scrollTop(0);
    const id = this.route.snapshot.queryParams['id'];
    if (!id) {
      this._route.navigate(['/accounts/accounts-list']);
    }
    this.AccountId = atob(id);
    this.GetCompanys();
    this.getAccount(this.AccountId);
    this.getContact();
    this.opts = {
      position: 'right',
      barBackground: '#000000'
    };
    // this.getQuotes(0);
    this.getModuleAccess();
    this.log.userId = this.user.id;

    if (document.getElementById('accounts') !== null) {
      document.getElementById('accounts').classList.add('active');
    }
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
      // // console.log('Done');
    }, 500);
  }
  // goToDetails(id) {
  //   this._route.navigate(['/quotes/quotes-detail'], {
  //     queryParams: { id: btoa(id) }
  //   });
  // }

  goToDetails(id, ownerId, ConfirmModal) {
    if (ownerId === this.user.id) {
      this._route.navigate(['/quotes/quotes-detail'], {
        queryParams: { id: btoa(id) }
      });
    } else if (
      this.user.role.description === this.constants.Admin ||
      this.user.role.description === this.constants.SuperAdmin
    ) {
      this._route.navigate(['/quotes/quotes-detail'], {
        queryParams: { id: btoa(id) }
      });
    } else {
      this.popupOpen((this.popupFor = 'Can not access others quote'));
      ConfirmModal.show();
    }
  }

  ngOnDestroy() {
    if (document.getElementById('accounts') !== null) {
      document.getElementById('accounts').classList.remove('active');
    }
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }

  getModuleAccess() {
    this.subscriptions.push(
      this.accountsService.getModuleAccess().subscribe(res => {
        this.accessValid = res.accounts;
        this.addQuote = res.quote.create;
        if (!this.accessValid.view) {
          this._route.navigate(['/accounts/accounts-list']);
        }
      })
    );
  }

  getAccount(id) {
    this.loading = true;
    this.subscriptions.push(
      this.accountsService.getAccountById(id).subscribe(response => {
        this.loading = false;
        this.account = response.accounts;
        this.oldAccount = JSON.parse(JSON.stringify(response.accounts));
        this.contacts = response.contacts;
        this.quotes = response.quote;
        this.quoteStats = response.stats;
        this.historyData = response.history;
        this.oldHistoryData = JSON.parse(JSON.stringify(response.history));
        this.alldata = [];
        this.notes = response.accounts.notes;
        this.sortNote();
        this.historyData.forEach(element => {
          this.alldata.push(element);
        });
        this.notes.forEach(element => {
          this.alldata.push(element);
        });
        if (this.quotes.length) {
          this.showmsg = true;
        }
        this.getAllData();

        if (this.account.faxNo == null && this.account.homeNo == null) {
          this.showallphone = false;
        } else {
          this.showallphone = true;
        }
      })
    );
  }

  getQuotes(pageNo) {
    this.quotesInfo.sortBy = 'CreatedOn';
    this.quotesInfo.sortType = 'desc';
    this.loading = true;
    if (this.key) {
      this.quotesInfo.sortBy = this.key;
    }
    if (this.reverse) {
      this.quotesInfo.sortType = 'asc';
    } else {
      this.quotesInfo.sortType = 'desc';
    }
    this.quotesInfo.pageNumber = pageNo;
    this.quotesInfo.pageSize = 10;
    this.quotesInfo.accountId = this.AccountId;
    this.quotesInfo.status = this.status === 'All' ? '' : this.status;
    this.subscriptions.push(
      this.accountsService.getStatusWiseQuote(this.quotesInfo).subscribe({
        next: res => {
          this.ngZone.run(() => {
            this.quotesInfo = res;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            this.loading = false;
          });
        },
        error: err => {
          this.loading = false;
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
        }
      }
      )
    );
  }

  pageChanged(no) {
    this.getQuotes(no);
  }
  SearchQuotes(e) {
    this.status = e;
    this.getQuotes(0);
  }

  getContact() {
    this.subscriptions.push(
      this.accountsService.GetContactWithoutAccount().subscribe(
        res => {
          this.ngZone.run(() => {
            this.myContacts = res;
          });
        },
        err => {
          // // console.log(err);
        }
      )
    );
  }

  AddNote(isValid) {
    if (isValid && this.note.description) {
      this.loading = true;
      this.note.createdOn = new Date();
      this.note.updatedBy = this.user.id;
      this.note.isActive = true;
      // tslint:disable-next-line:curly
      if (this.account.notes === undefined)
        this.account.notes = new Array<Notes>();
      this.account.notes.push(this.note);
      this.note = new Notes();
      this.accountsService.UpdateAccount(this.account).subscribe(response => {
        this.loading = false;
        this.notes = response.notes;
        this.sortNote();

        this.alldata = [];
        this.notes.forEach(element => {
          this.alldata.push(element);
        });
        this.historyData.forEach(element => {
          this.alldata.push(element);
        });
        this.sortHistory();
        this.getAllData();
      });
    }
    this.loading = false;
  }

  AddQuotes(valid, modal, form) {
    // this.isFormSubmitted = true;
    if (valid) {
      this.quote.contact = this.contacts.filter(
        x => x.id === this.quote.contact.id
      )[0];
      this.log.name = ' Quote Added: ' + this.quote.name;
      this.quote.createdOn = new Date();
      this.quote.createdOn = new Date(
        this.quote.createdOn.getUTCFullYear(),
        this.quote.createdOn.getUTCMonth(),
        this.quote.createdOn.getUTCDate(),
        this.quote.createdOn.getUTCHours(),
        this.quote.createdOn.getUTCMinutes(),
        this.quote.createdOn.getUTCSeconds()
      );
      this.quote.expirationDate = this.quote.createdOn;
      this.quote.updatedOn = this.quote.createdOn;
      this.quote.expirationDate.setDate(
        this.quote.expirationDate.getDate() + this.constants.Expairy[0]
      );
      this.quote.expairyDays = this.constants.Expairy[0];
      this.quote.ownerName = this.user.firstName + ' ' + this.user.lastName;
      if (this.company) {
        this.quote.termsandConditions = this.company.termsConditions;
      }
      this.accountsService.AddQuote(this.quote).subscribe(
        response => {
          //   this.isFormSubmitted = false;
          modal.hide();
          this.ngZone.run(() => {
            this._route.navigate(['/quotes/quotes-detail'], {
              queryParams: { id: btoa(response.id) }
            });
            this.cancelQuotesDetails(form, modal);
            // this.toastrService.success(
            //   'Quote added successfully',
            //   '',
            //   this.constants.toastrConfig
            // );

            this.log.accountId = this.quote.contact.accounts.id;
            this.addHistory(this.log);
            this.loading = false;
          });
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
          this.loading = false;
        }
      );
    }
  }

  deleteAccount(model) {
    if (this.quotes.length !== 0) {
      // this.toastrService.warning(
      //   'Account can not be deleted',
      //   '',
      //   this.constants.toastrConfig
      // );
      this.showmsg = true;
    } else {
      const id = this.route.snapshot.queryParams['id'];
      this.account.isActive = false;
      this.log.isActive = false;
      this.accountsService.deleteAccount(this.account).subscribe(
        response => {
          // this.toastrService.error(
          //   'Account deleted successfully',
          //   '',
          //   this.constants.toastrConfig
          // );
          this.showmsg = false;
          model.hide();
          this.addHistory(this.log);
          this._route.navigate(['/accounts/accounts-list']);
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
          this.loading = false;
        }
      );
    }
  }

  RemoveContact(contact: Contact) {
    this.log.name = 'Contact Un-Linked:' + ' ' + contact.firstName;
    this.log.accountId = contact.accounts.id;
    contact.accounts.id = null;
    this.accountsService.UpdateContact(contact).subscribe(response => {
      this.ngZone.run(() => {
        this.myContacts.push(contact);
        this.contacts.splice(
          this.contacts.findIndex(item => item.id === contact.id),
          1
        );
        this.addHistory(this.log);
      });
    });
  }

  cancelQuotesDetails(form, popup) {
    // this.isFormSubmitted = false;
    popup.hide();
    this.isEmailRepated = false;
    //  form.reset();
    // this.contact = new Contact();
  }

  editAccount() {
    this.edit = true;
    this.accountTitleCopy = false;
    this.editbtn = false;
    if (
      (this.account.faxNo != null && this.account.faxNo !== '') ||
      (this.account.homeNo != null && this.account.homeNo !== '')
    ) {
      this.showtext = true;
      this.showallphone = true;
    }
  }

  discard(id) {
    this.isAccountRepated = false;
    if (
      (this.account.faxNo !== null && this.account.faxNo !== '') ||
      (this.account.homeNo !== null && this.account.homeNo !== '')
    ) {
      this.showtext = true;
    } else {
      this.showtext = false;
    }

    this.account = this.oldAccount;
    this.edit = false;
    this.editbtn = true;
    // this.toastrService.info(
    //   'Change discarded',
    //   '',
    //   this.constants.toastrConfig
    // );
  }
  goToContactDetails(id) {
    this.router.navigate(['/contacts/contact-detail'], {
      queryParams: { id: btoa(id) }
    });
  }
  saveAccount(account, valid) {
    this.accountDetailsForm = true;
    if (valid) {
      this.edit = false;
      this.editbtn = true;
      this.accountDetailsForm = false;
      this.isAccountRepated = false;
      this.log.accountId = this.account.id;
      this.accountsService.UpdateAccount(account).subscribe(
        response => {
          if (response === false) {
            this.isAccountRepated = true;
          } else {
            this.account = response;
            if (this.account.homeNo !== null && this.account.homeNo !== '') {
              this.showtext = true;
            } else {
              this.showtext = false;
            }

            if (account.address !== this.oldAccount.address) {
              this.log.name =
                'Address Modified: From ' +
                ' ' +
                this.oldAccount.address +
                ' ' +
                ' to' +
                ' ' +
                account.address;

              this.addHistory(this.log);
            }
            if (account.website !== this.oldAccount.website) {
              if (account.website === '') {
                this.log.name =
                  ' Website  ' +
                  ' ' +
                  this.oldAccount.website +
                  ' ' +
                  'is Deleted';

                this.addHistory(this.log);
              } else if (
                this.oldAccount.website == null ||
                this.oldAccount.website === ''
              ) {
                this.log.name = 'Website is added : ' + account.website;
                this.addHistory(this.log);
              } else {
                this.log.name =
                  'Website Modified: From' +
                  ' ' +
                  this.oldAccount.website +
                  ' ' +
                  ' to' +
                  ' ' +
                  account.website;

                this.addHistory(this.log);
              }
            }
            if (account.contactNo !== this.oldAccount.contactNo) {
              this.log.name =
                'Work No Modified: From ' +
                ' ' +
                this.oldAccount.contactNo +
                ' ' +
                ' to' +
                ' ' +
                account.contactNo;

              this.addHistory(this.log);
            }

            if (account.faxNo !== this.oldAccount.faxNo) {
              if (
                (this.oldAccount.faxNo === null && account.faxNo) ||
                this.oldAccount.faxNo === ''
              ) {
                this.log.name = '  Fax No Added:' + ' ' + account.faxNo;

                this.addHistory(this.log);
              } else if (
                this.oldAccount.faxNo != null &&
                account.faxNo === ''
              ) {
                this.log.name =
                  ' Fax No  ' +
                  ' ' +
                  this.oldAccount.faxNo +
                  ' ' +
                  'is Deleted';

                this.addHistory(this.log);
              } else {
                this.log.name =
                  'Fax No Modified: From ' +
                  ' ' +
                  this.oldAccount.faxNo +
                  ' ' +
                  '  to' +
                  ' ' +
                  account.faxNo;

                this.addHistory(this.log);
              }
            }

            this.oldAccount = account;
          }
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
          this.loading = false;
        }
      );
      // this.isFormSubmitted = false;
    } else {
      this.edit = true;
      this.editbtn = false;
    }
  }
  saveAccountName(account, valid) {
    if (valid) {
      this.accountTitleEdit = true;
      this.accountTitleCopy = false;
      this.log.accountId = account.id;
      this.accountsService.UpdateAccount(account).subscribe(
        response => {
          if (response === false) {
            this.isAccountRepated = true;
            this.accountTitleEdit = false;
            this.accountTitleCopy = true;
            this.accountName = true;
          } else {
            this.account = response;
            // this.accountTitleEdit =false;

            if (account.name !== this.oldAccount.name) {
              this.log.name =
                'Name Modified: From' +
                ' ' +
                this.oldAccount.name +
                ' ' +
                ' to' +
                ' ' +
                account.name;
              this.addHistory(this.log);
              this.oldAccount = account;
            }

            // account=response;
          }
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
          }
          this.loading = false;
        }
      );
      //   this.isFormSubmitted = false;
    } else {
      this.accountName = true;
    }
  }
  CancelNote() {
    this.note = new Notes();
  }

  getContacts() {
    this.subscriptions.push(
      this._quotesService.GetContactWithAccount().subscribe(
        res => {
          this.ngZone.run(() => {
            this.contacts = res;
          });
        },
        err => {
          // // console.log(err);
        }
      )
    );
  }
  saveContactOnSelectContact(e) {
    e.item.accounts = this.account;
    this.selecteContact = e.item;
  }
  deleteNote(note) {
    // this.notes.splice(index, 1);
    // this.account.notes = this.notes;
    const noteData: number = this.notes.indexOf(note);
    if (noteData !== -1) {
      this.notes.splice(noteData, 1);
    }
    this.account.notes = this.notes;
    this.accountsService.UpdateAccount(this.account).subscribe(response => {
      this.account = response;
      this.notes = response.notes;
      this.sortNote();

      this.alldata = [];
      this.notes.forEach(element => {
        this.alldata.push(element);
      });
      this.historyData.forEach(element => {
        this.alldata.push(element);
      });
      this.sortHistory();
      this.getAllData();
    });
  }
  AddContact(model) {
    const cont = new Contact();
    this.presentContact = false;
    let isDuplicate = false;
    this.searchContactForm = true;
    if (this.selecteContact != null && this.selecteContact.id != null) {
      if (this.selecteContact.firstName !== null) {
        this.contacts.forEach(element => {
          if (element.id === this.selecteContact.id) {
            isDuplicate = true;
            this.isContactAlreadyPresent = true;
            this.selecteContact = null;
          } else {
            this.isContactAlreadyPresent = false;
          }
        });

        if (isDuplicate === false) {
          let usedCnt = false;
          this.myContacts.forEach(element => {
            if (element.id === this.selecteContact.id) {
              usedCnt = true;
            }
          });

          if (usedCnt !== false) {
            this.accountsService.UpdateContact(this.selecteContact).subscribe(
              response => {
                this.ngZone.run(() => {
                  this.isContactAlreadyPresent = false;
                  this.searchContactForm = false;
                  this.SearchText = null;
                  model.hide();
                  this.log.accountId = this.AccountId;
                  this.log.name =
                    '	Contact Linked: ' + ' ' + this.selecteContact.firstName;
                  this.addHistory(this.log);
                  this.contacts.push(response);
                  this.loading = false;
                });
                // this.contacts.push();
                // this.getAccount(this.AccountId);
              },
              err => {
                // // console.log(err);
                if (err.status === 403) {
                  // tslint:disable-next-line:one-line
                  // this.toastrService.warning(
                  //   'Access denied',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                }
                this.loading = false;
              }
            );
          } else {
            this.isContactAlreadyPresent = true;
            this.ngZone.run(() => {
              // this.toastrService.warning(
              //   ' Contact already  linked',
              //   '',
              //   this.constants.toastrConfig
              // );

              this.loading = false;
            });
          }
        } else {
          this.isContactAlreadyPresent = true;
          this.ngZone.run(() => {
            // this.toastrService.warning(
            //   ' Contact already  linked',
            //   '',
            //   this.constants.toastrConfig
            // );

            this.loading = false;
          });
        }
      } else {
        this.isContactAlreadyPresent = false;
        this.ngZone.run(() => {
          // this.toastrService.warning(
          //   'Contact not present',
          //   '',
          //   this.constants.toastrConfig
          // );

          this.loading = false;
        });
      }
    } else {
      this.presentContact = true;
    }
  }

  addContactDetailModal(valid, abc, contactform) {
    // this.isFormSubmitted = true;
    // this.allContacts.forEach(element => {
    //   if (element.email === this.contact.email) {
    //     this.isEmailRepated = true;
    //   }
    // });
    this.accountsService.findEmail(this.contact.email).subscribe(mail => {
      this.ngZone.run(() => {
        this.loading = false;
        if (mail) {
          this.isEmailRepated = true;
        } else if (valid && !this.isEmailRepated) {
          this.contact.accounts = this.account;
          this.contactService.addContacts(this.contact).subscribe(
            response => {
              if (response === false) {
                this.isEmailRepated = true;
              } else {
                this.ngZone.run(() => {
                  this.log.accountId = this.AccountId;
                  this.log.name =
                    ' Contact Added:' + ' ' + this.contact.firstName;
                  this.addHistory(this.log);
                  this.router.navigate(['/contacts/contact-detail'], {
                    queryParams: { id: btoa(response.id) }
                  });
                  this.cancelQuotesDetails(contactform, abc);
                  // this.toastrService.success(
                  //   'Contact added successfully',
                  //   '',
                  //   this.constants.toastrConfig
                  // );
                });
                this.loading = false;
              }
            },

            err => {
              // // console.log(err);
              if (err.status === 403) {
                // tslint:disable-next-line:one-line
                // this.toastrService.warning(
                //   'Access denied',
                //   '',
                //   this.constants.toastrConfig
                // );
              }
              this.loading = false;
            }
          );
          // this.isFormSubmitted = false;
        }
      });
    });
  }

  FindContact() {
    this.isContactAlreadyPresent = false;
    this.selecteContact = new Contact();
    this.accountsService.FindContact(this.SearchText).subscribe(
      res => {
        this.ngZone.run(() => {
          this.contactsList = res;
        });
      },
      err => {
        // // console.log(err);
      }
    );
  }

  addHistory(history) {
    this.accountsService.postHistory(history).subscribe(activity => {
      this.activity = activity;
      this.oldHistoryData.push(activity);
      this.historyData = this.oldHistoryData;
      this.sortHistory();
      this.alldata = [];
      this.oldHistoryData.forEach(element => {
        this.alldata.push(element);
      });
      this.notes.forEach(element => {
        this.alldata.push(element);
      });
      this.getAllData();
    });
  }

  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
    this.getQuotes(0);
  }
  getAllData() {
    this.alldata.sort(function (a, b) {
      // tslint:disable-next-line:curly
      if (a.createdOn < b.createdOn) return -1;
      // tslint:disable-next-line:curly
      if (a.createdOn > b.createdOn) return 1;
      return 0;
    });
  }
  sortNote() {
    this.notes.sort(function (a, b) {
      // tslint:disable-next-line:curly
      if (a.createdOn > b.createdOn) return -1;
      // tslint:disable-next-line:curly
      if (a.createdOn < b.createdOn) return 1;
      return 0;
    });
  }
  sortHistory() {
    this.historyData.sort(function (a, b) {
      // tslint:disable-next-line:curly
      if (a.createdOn > b.createdOn) return -1;
      // tslint:disable-next-line:curly
      if (a.createdOn < b.createdOn) return 1;
      return 0;
    });
  }
  openNewTab(event, url) {
    const proto = url.match(/^([a-z0-9]+):\/\//);
    if (proto === null) {
      const website = 'https://' + url;
      window.open(website, '_blank');
    } else {
      window.open(url, '_blank');
    }
  }

  // messages display
  popupOpen(popupFor) {
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.messageToDisplay = null;
    this.typeofMessage = 'Error';
    if (this.popupFor === 'deleteAccount') {
      if (this.quotes.length) {
        this.notification =
          ' * If the account is linked with quote then account can not be deleted';
        this.showButtons = false;
      } else {
        this.notification = ' Are you sure you want to delete this account?';
        this.showButtons = true;
      }
    } else if (this.popupFor === 'deleteNote') {
      this.notification = 'Are you sure you want to delete this note?';
      this.showButtons = true;
    } else if (this.popupFor === 'quotenocontact') {
      if (!this.contacts.length) {
        this.notification = '* Please add contact first';
        this.showButtons = false;
      }
    } else if (this.popupFor === 'contact') {
      this.notification = 'Are you sure you want to delete this contact?';
      this.showButtons = true;
    } else if (this.popupFor === 'Can not access others quote') {
      this.typeofMessage = 'Error';
      this.showButtons = false;
      this.notification =
        'Access Denied! You are not the quote owner. For access, please see your administrator.';
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === 'deleteAccount') {
      this.deleteAccount(ConfirmModal);
      ConfirmModal.hide();
    } else if (this.popupFor === 'deleteNote') {
      this.deleteNote(this.noteIndex);
      ConfirmModal.hide();
    } else if (this.popupFor === 'quotenocontact') {
      this.RemoveContact(this.contactItem);
      ConfirmModal.hide();
    } else if (this.popupFor === 'contact') {
      this.RemoveContact(this.contactItem);
      ConfirmModal.hide();
    }
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }

  GetCompanys() {
    this.loading = true;
    this.subscriptions.push(
      this._UserService.GetCompany().subscribe(
        res => {
          this.ngZone.run(() => {
            this.company = res;
            this.loading = false;
          });
        },
        err => {
          this.loading = false;
        }
      )
    );
  }
  goToQuoteList(status) {
    this.router.navigate(['/quotes/quotes-list'], {
      queryParams: { status: btoa(status), accountId: btoa(this.AccountId) }
    });
  }
}
