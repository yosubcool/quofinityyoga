import { Component, OnInit, OnDestroy, NgZone, ViewChild, DoCheck, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Response } from '@angular/http/src/static_response';
// import { forEach } from '@angular/router/src/utils/collection';
// import { PaginatePipe, PaginationService } from 'ng2-pagination';
import { ngxCsv } from 'ngx-csv';
import { map, Subscription } from 'rxjs';
import { DashboardService } from 'src/app/dashboard/dashboard.service';
import { Constants } from 'src/app/_shared/constant';
import { Account } from 'src/app/_shared/model/account-model';
import { Actions } from 'src/app/_shared/model/actions-model';
import { Pagination } from 'src/app/_shared/model/pagination-model';
import { User } from 'src/app/_shared/model/user-model';
import { AccountsService } from '../accounts-service';
import { HttpClient } from '@angular/common/http';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
declare var $: any;

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})

export class AccountsListComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;

  show = false;
  validAccount: any;
  totalAccount: any;
  showData = false;
  invalidAccount: any;
  showWarning = false;
  tobeEdit: string;
  items = [];
  columns = [];
  editProperty: string;
  itemCount = 0;
  reverse = false;
  key: any;
  sampleHeaders: string[];
  accountforExport: any;
  myheaders: any;
  subscriptions: Subscription[] = [];
  flag = true;
  data: any;
  myFilename: any;
  pagesCount: number;
  updateDetails: any;
  accCount: any;
  pagination: boolean;
  account = new Account();
  accountAdd = new Account();
  myProduct: any = {};
  isFormSubmitted = false;
  isShowTextBox = false;
  textId = 0;
  textId2 = 0;
  currentUser: User;
  isNameEdit = false;
  textId1 = 0;
  accName = true;
  website = true;
  phone = false;
  fax = false;
  contacts = false;
  quotes = true;
  inProgress = true;
  quoted = true;
  accepted = true;
  rejected = true;
  expired = true;
  address1 = true;
  constants: any;
  accNameArray: any;
  accountsList: Pagination;
  selectedAlpha: string;
  textval: boolean;
  isValidName = true;
  showFile = false;
  showcol = false;
  notShow = true;
  mask: any[] = ['+', '1', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  @ViewChild('fileImportInput')
  fileImportInput: any;
  uplodedData: any;
  allowedFiles = ['csv'];
  loading: boolean;
  csvRecords = [];
  csvData: any[];
  isAccountRepated = false;
  accessValid: any;
  websiteShow: boolean;
  addressShow: boolean;
  isOpenPopup = false;
  //primeNg for Migration
  newMask: string = " (000) 000-0000";
  first: number = 0;
  selectedItemsList = [];
  checkedIDs = [];
  checkboxesDataList = [];
  isAccountName: boolean;
  isAddress: boolean;
  isWebsite: boolean;
  isWorkNumber: boolean;
  isFaxNumber: boolean;
  isContact: boolean;
  isContactNo: boolean;
  isAccepted: boolean;
  isQuotes: boolean;
  isQuoted: boolean;
  isRejected: boolean;
  isExpired: boolean;
  isInprogress: boolean;
  modalRef: BsModalRef;
  constructor(
    private accountsService: AccountsService,
    private _dashboardService: DashboardService,
    private router: Router,
    private ngZone: NgZone,
    private _toastrService: ToastrService,
    private http: HttpClient,
    private modalService: BsModalService
  ) {
    this.accessValid = new Actions();
    this.currentUser = new User();
    this.accountsList = new Pagination();
    this.constants = Constants;
    this.selectedAlpha = 'All';
    this.accNameArray = [];
    this.myheaders = [];
    this.accountforExport = [];
    this.sampleHeaders = ['Account Name', 'Address', 'Website', 'Work Number', 'Fax Number'];
    this.accountsList.pageSize = 10;
  }

  ngOnInit() {
    $(window).scrollTop(0);
    this.getAccountsList(0);
    this.getModuleAccess();
    this.isAccountName = true;
    this.isAccepted = true;
    this.isAddress = true;
    this.isWebsite = false;
    this.isFaxNumber = false;
    this.isContactNo = false;
    this.isRejected = true;
    this.isExpired = true;
    this.isQuoted = true;
    this.isQuotes = true;
    this.isInprogress = true;
    this.isContact = false;
    this.checkboxesDataList = [{ property: 'name', header: 'Account Name', ischecked: true }, { property: "address", header: "Address", ischecked: true }, { property: "website", header: "Website", ischecked: false }, { property: "contactNo", header: "Contact No", ischecked: false }, { property: "faxNo", header: "FaxNo", ischecked: false }, { property: "contact", header: "Contact", ischecked: false },
    { property: "quotes", header: "Quotes", ischecked: true },
    { property: "inProgress", header: "In progress", ischecked: true },
    { property: "quoted", header: "Quoted", ischecked: true }, { property: "accepted", header: "Accepted", ischecked: true }, { property: "rejected", header: "Rejected", ischecked: true },
    { property: "expired", header: "Expired", ischecked: true }];
    // { property: "address", header: "Address" } -- Removing As David Suggested
    this.columns = [{ property: 'name', header: 'Account Name' },
    { property: "quotes", header: "Quotes" }, { property: "inProgress", header: "In progress" },
    { property: "quoted", header: "Quoted" }, { property: "accepted", header: "Accepted" },
    { property: "rejected", header: "Rejected" },
    { property: "expired", header: "Expired" }];
    this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    if (window.innerWidth <= 1024) {
      this.address1 = false;
      this.website = false;
      this.quotes = false;
      this.inProgress = false;
      this.quoted = false;
      this.accepted = false;
      this.rejected = false;
      this.expired = false;
    } else {
      this.address1 = true;
      this.website = true;
      this.quotes = true;
      this.inProgress = true;
      this.quoted = true;
      this.accepted = true;
      this.rejected = true;
      this.expired = true;
    }
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
      // // console.log('Done');
    }, 500);
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      animated: true,
      // backdrop: 'static',
      class: 'modal-sm  modal-right'
    });
  }
  changeSelection() {
    this.fetchSelectedItems();
    console.log(this.selectedItemsList);
    this.columns = this.selectedItemsList;
    this.isAccountName = false;
    this.isAccepted = false;
    this.isAddress = false;
    this.isWebsite = false;
    this.isFaxNumber = false;
    this.isContactNo = false;
    this.isRejected = false;
    this.isExpired = false;
    this.isQuoted = false;
    this.isQuotes = false;
    this.isInprogress = false;
    this.isContact = false;
    this.columns.forEach(item => {
      console.log(item.property);
      if (item.property === "name") {
        this.isAccountName = true;

      }
      else if (item.property === "address") {
        this.isAddress = true;

      }
      else if (item.property === "website") {
        this.isWebsite = true;

      }
      else if (item.property === "contactNo") {
        this.isContactNo = true;

      }
      else if (item.property === "contact") {
        this.isContact = true;

      }
      else if (item.property === "faxNo") {
        this.isFaxNumber = true;

      }
      else if (item.property === "quotes") {
        this.isQuotes = true;

      }
      else if (item.property === "inProgress") {
        this.isInprogress = true;

      }
      else if (item.property === "accepted") {
        this.isAccepted = true;

      }
      else if (item.property === "rejected") {
        this.isRejected = true;
      }
      else if (item.property === "expired") {
        this.isExpired = true;

      }
      else if (item.property === "quoted") {
        this.isQuoted = true;

      }
      else {
        this.isAccountName = false;
        this.isAccepted = false;
        this.isAddress = false;
        this.isWebsite = false;
        this.isFaxNumber = false;
        this.isContactNo = false;
        this.isContact = false;
        this.isRejected = false;
        this.isExpired = false;
        this.isQuoted = false;
        this.isQuotes = false;
        this.isInprogress = false;
      }


    })
    //this.exportToCSV();
  }

  fetchSelectedItems() {
    this.selectedItemsList = this.checkboxesDataList.filter((value, index) => {
      return value.ischecked
    });
  }

  fetchCheckedIDs() {
    this.checkedIDs = []
    this.checkboxesDataList.forEach((value, index) => {
      if (value.isChecked) {
        this.checkedIDs.push(value.id);
      }
    });
  }
  getModuleAccess() {
    this.subscriptions.push(
      this.accountsService.getModuleAccess().subscribe(res => {
        this.accessValid = res.accounts;
      })
    );
  }
  ngOnDestroy() {
    this.subscriptions.forEach(element => {
      element.unsubscribe();
    });
  }
  onSave(valid, myavalue) {
    const arr = this.accountsList.records.filter(x => x.accounts.name === myavalue.accounts.name);
    const flag = arr.length > 1 ? true : false;
    if (valid) {
      if (!flag) {
        this.accountAdd = myavalue.accounts;
        this.accountAdd.updatedOn = new Date();
        this.accountAdd.name = this.accountAdd.name.trim();
        this.accountsService.UpdateAccount(this.accountAdd).subscribe(
          response => {
            if (response) {
              this.tobeEdit = 'nun';
              this.getAccountsList(this.accountsList.pageNumber);
            }
          },
          err => {
            // // console.log(err);
            this.isNameEdit = true;
            if (err.status === 403) {
              this.show = true;
              this.popupOpen((this.popupFor = 'Access denied'));
            }
          }
        );
      } else {
        this.isAccountRepated = true;
      }
    }
  }
  discard() {
    this.tobeEdit = 'nun';
    this.items = JSON.parse(JSON.stringify(this.accountsList.records));
    this.isAccountRepated = false;
  }

  goToDetails(id) {
    this.router.navigate(['/accounts/accounts-detail'], {
      queryParams: { id: btoa(id) }
    });
  }

  SearchAccount(e) {
    this.selectedAlpha = e;
    this.getAccountsList(0);
  }

  pageChanged(no) {
    this.getAccountsList(no);
  }
  getAccountsList(pageNo) {
    let sortingData = pageNo;
    //Adding this logic to accomodate inclusion of PrimeNg. Event is sent by the p-table.
    if (isNaN(pageNo)) {
      pageNo = pageNo.first / pageNo.rows + 1;
    }
    this.accountsList.sortBy = 'name';
    this.accountsList.sortType = 'asc';
    this.loading = true;
    if (sortingData != 0) {
      if (sortingData.sortOrder === 1) {
        this.accountsList.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.accountsList.sortType = 'asc';
      }
      else {
        this.accountsList.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.accountsList.sortType = 'desc';
      }
    }
    this.accountsList.pageNumber = pageNo;

    this.accountsList.alphabet = this.selectedAlpha === 'All' ? '' : this.selectedAlpha;
    this.subscriptions.push(
      this.accountsService.getAccounts(this.accountsList).subscribe(
        res => {
          this.accNameArray = res.records;
          this.ngZone.run(() => {
            this.accountsList = res;
            this.items = JSON.parse(JSON.stringify(this.accountsList.records));
            this.itemCount = this.accountsList.totalCount;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize);
            // number of pages
            this.tobeEdit = 'nun';
            this.loading = false;
          });
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false;
        }
      )
    );
  }

  //  To get list of columns headers - Suryakant
  getChangedColumns(columns) {
    console.log(columns.datatable.columns);
    this.columns = columns.datatable.columns._results;
    this.tobeEdit = 'nun';
    if (this.accountsList.records) {
      this.items = JSON.parse(JSON.stringify(this.accountsList.records));
    }
  }
  toEdit(item, edit) {
    this.editProperty = item.accounts.id;
    this.tobeEdit = edit;
    if (this.accountsList.records) {
      this.items = JSON.parse(JSON.stringify(this.accountsList.records));
    }
  }
  searchAccount(name) {
    this.subscriptions.push(
      this.accountsService.getDataByAlphabate(name).subscribe(response => {
        this.accountsList.records = response;
      })
    );
  }

  DownloadTemplate() {
    const parsedResponse = this.sampleHeaders;
    //#revisit_CommentForV1
    // if (navigator.appVersion.toString().indexOf('.NET') > 0) {
    //   const bb = new MSBlobBuilder();
    //   bb.append(parsedResponse);
    //   const blob1 = bb.getBlob('csv');
    //   window.navigator.msSaveBlob(blob1, 'Account.csv');
    //   const url = window.URL.createObjectURL(blob1);
    //   window.URL.revokeObjectURL(url);
    // } else {
    // const blob = new Blob([parsedResponse]   //#Revisit_PreviousCode
    const blob = new Blob([parsedResponse.toString()], { type: 'csv' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'Account.csv';
    document.body.appendChild(a);
    a.click();
    setTimeout(function () {
      document.body.removeChild(a);
      window.URL.revokeObjectURL(url);
    }, 0);
    // }
  }

  ConvertToCSV(objArray) {
    const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = '';

    // tslint:disable-next-line:forin
    for (const index in objArray[0]) {
      // Now convert each value to string and comma-separated

      row += index + ',';
    }
    row = row.slice(0, -1);
    // append Label row with line break
    str += row + '\r\n';

    for (let i = 0; i < array.length; i++) {
      let line = '';
      // tslint:disable-next-line:forin
      for (const index in array[i]) {
        // tslint:disable-next-line:curly
        if (line !== '') line += ',';

        line += array[i][index];
      }
      str += line + '\r\n';
    }
    return str;
  }

  exportToCSV() {
    this.subscriptions.push(
      this.accountsService.GetAccountsForExport().subscribe(
        res => {
          this.accountforExport.push(res);
          const data: any = [];
          if (this.accountforExport.length) {
            console.log(this.accountforExport);
            this.accountforExport[0].forEach(element => {
              const obj: any = {};
              console.log(this.columns);
              this.columns.forEach(col => {
                const prop = col.property;
                const primaryProp = (prop === 'name' || prop === 'address' || prop === 'website' || prop === 'contactNo' || prop === 'faxNo') ? true : false;
                if (primaryProp) {
                  obj[prop] = (element.accounts[prop] == null || undefined) ? '' : element.accounts[prop];
                  this.myheaders.push(col.header);
                }
                else {
                  obj[prop] = element[prop];
                  this.myheaders.push(col.header);
                }
              });
              data.push(obj);
              console.log(data);
            });
            const uniqueheader = this.myheaders.filter((v, i, a) => a.indexOf(v) === i);
            const options = {
              fieldSeparator: ',',
              quoteStrings: '"',
              decimalseparator: '.',
              showLabels: true,
              showTitle: true,
              headers: uniqueheader
            };
            this.myheaders = [];
            // tslint:disable-next-line:no-unused-expression
            new ngxCsv(data, 'Accounts', options);
          } else {
            // this._toastrService.warning(
            //   'No data available to export!',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'No data available to export!'));
            this.loading = false;
          }
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
        }
      )
    );
  }

  isUniqueAccountName(account) {
    this.isAccountRepated = false;
    const arr = this.accNameArray.filter(x => x.accounts.name === account.name);
    this.isAccountRepated = arr.length === 0 ? false : true;
  }

  addAccountModal(valid, abc, form) {
    this.isFormSubmitted = true;
    if (valid && !this.isAccountRepated && this.isValidName) {
      abc.hide();
      this.account.createdOn = new Date();
      this.account.updatedOn = new Date();
      this.account.createdBy = this.currentUser.id;
      this.account.updatedBy = this.currentUser.id;
      this.account.name = this.account.name.trim();
      this.accountsService.addAccounts(this.account).subscribe(
        response => {
          this.ngZone.run(() => {
            this.router.navigate(['/accounts/accounts-detail'], {
              queryParams: { id: btoa(response.id) }
            });

            this.cancelAccoutDetail(form, abc);
          });
        },
        err => {
          // // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this._toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = 'Access denied'));
          }
          this.loading = false;
        }
      );
    }
  }
  cancelAccoutDetail(form, popup) {
    form.reset();
    this.isValidName = true;
    this.account = new Account();
    this.isFormSubmitted = false;
    popup.hide();
  }

  isInArray(array, word) {
    return array.indexOf(word.toLowerCase()) > -1;
  }

  fileChangeListener(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.loading = true;
      const file1: File = fileList[0];
      const filename = file1.name;
      const Extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
      if (this.isInArray(this.allowedFiles, Extension)) {
        this.myFilename = filename;
        const formData: FormData = new FormData();
        formData.append('uploadFile', file1, file1.name);
        const headers = new Headers();
        headers.append('Content-Type', 'multipart/form-data');
        const file = new FormData();

        file.append('file', file1);
        file.append('Authorization', 'Bearer ' + sessionStorage.getItem('AccessToken'));
        this.http
          .post(this.constants.BASE_URL + 'Accounts/UploadFile', file)
          .pipe(map(res => res))
          .subscribe({
            next: (response: any) => {
              console.log(response);
              this.showData = true;
              this.showWarning = false;
              this.loading = false;
              this.invalidAccount = response.failedAccounts.length;
              this.validAccount = response.successfulAccounts.length;
              this.totalAccount = this.validAccount + this.invalidAccount;
              if (this.validAccount || this.invalidAccount) {
                this.showFile = true;
                this.loading = false;
                event.target.value = '';
                this.uplodedData = response;
                this.validAccount = response.successfulAccounts.length;
              } else {
                this.showFile = false;
                this.showWarning = true;
                this.showData = false;
              }
            },
            error: error => ((this.loading = false), (this.showFile = false), (this.showWarning = true))
          }
          );

        // this._toastrService.error('Please select valid file', '', this.constants.toastrConfig)));
      } else {
        console.log("else");
        this.loading = false;
        event.target.value = '';
        this.showWarning = true;
        this.showData = false;
        this.fileImportInput.nativeElement.value = '';
        this.uplodedData = null;

        return;
      }
    }
  }

  cancelUpload() {
    console.log("cancel");
    this.fileImportInput.nativeElement.value = '';
    this.myFilename = '';
    this.uplodedData = null;
  }

  addAccountFile() {
    console.log("add");
    if (this.uplodedData.successfulAccounts.length) {
      this.uplodedData.successfulAccounts.forEach(element => {
        element.createdOn = new Date();
        element.updatedOn = new Date();
        element.notes = [];

        if (element.contactNo != null) {
          let workNo = /^[2-9]\d{2}[2-9]\d{2}\d{4}$/;
          const digits = element.contactNo.replace(/\D/g, '');
          if (digits.length === 10) {
            if (element.contactNo.length <= 3) {
              workNo = element.contactNo.replace(/^(\d{0,3})/, '($1)');
            } else if (element.contactNo.length <= 6) {
              workNo = element.contactNo.replace(/^(\d{0,3})(\d{0,3})/, '($1) $2');
            } else {
              workNo = element.contactNo.replace(/^(\d{0,3})(\d{0,3})(.*)/, '($1) $2-$3');
            }
            element.contactNo = '+1' + ' ' + workNo;
          }
        }

        if (element.faxNo != null && element.faxNo.length === 10) {
          let faxNo;
          if (element.faxNo.length <= 3) {
            faxNo = element.faxNo.replace(/^(\d{0,3})/, '($1)');
          } else if (element.faxNo.length <= 6) {
            faxNo = element.faxNo.replace(/^(\d{0,3})(\d{0,3})/, '($1) $2');
          } else {
            faxNo = element.faxNo.replace(/^(\d{0,3})(\d{0,3})(.*)/, '($1) $2-$3');
          }
          element.faxNo = '+1' + ' ' + faxNo;
        }

        this.accountsService.addAccounts(element).subscribe(res => {
          this.ngZone.run(() => {
            this.getAccountsList(0);
          });
        });
      });
    }
  }

  private handleError(error: any) {
    const errMsg = error.message ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return errMsg;
  }

  sort(key) {
    this.key = key.sortBy;
    this.reverse = key.sortAsc;
    this.getAccountsList(0);
    this.tobeEdit = 'nun';
  }

  openNewTab(event, url) {
    const proto = url.match(/^([a-z0-9]+):\/\//);
    if (proto === null) {
      const website = 'https://' + url;
      window.open(website, '_blank');
    } else {
      window.open(url, '_blank');
    }
  }

  // messages display
  popupOpen(popupFor) {
    // this.titletoClose();
    if (this.popupFor === 'Access denied') {
      this.typeofMessage = 'Error';
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = 'Access denied';
    } else if (this.popupFor === 'No data available to export!') {
      this.typeofMessage = 'Error';
      this.notification = 'No data available to export!';
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.messageToDisplay = null;
    }
  }

  yes(ConfirmModal) {
    this.show = false;
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = 'none';
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }

  clickNow(event) {
    console.log('Event Triggered');
    console.log(event);
    this.onSave(true, event.data);
  }

  clonedAccounts: { [s: string]: any; } = {};

  onRowEditInit(item: any) {
    this.clonedAccounts[item.accounts.id] = JSON.parse(JSON.stringify(item));
    console.log('Initial Value Before Edit');
    console.log(this.clonedAccounts[item.accounts.id]);
  }

  onRowEditSave(item: any) {
    console.log('Event Triggered');
    console.log(item);
    delete this.clonedAccounts[item.accounts.id];
    this.onSave(true, item);
  }

  onRowEditCancel(item: any, index: number) {
    this.items[index] = this.clonedAccounts[item.accounts.id];
    delete this.clonedAccounts[item.accounts.id];
  }

}
