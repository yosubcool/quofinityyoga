import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs';
import { DataService } from '../_core/data.service';
import { Constants } from '../_shared/constant';
import { Pagination } from '../_shared/model/pagination-model';
import { Search } from '../_shared/model/search-model';
import { History } from '../_shared/model/history-model';
import { User } from '../_shared/model/user-model';
@Injectable()
export class AccountsService {
  account: Search = new Search();
  history: History = new History();
  pagination = new Pagination();
  user: User;
  constants: any;
  constructor(private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService) {
    this.constants = Constants;
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
  }

  getStatusWiseQuote(pagination: Pagination) {

    return this.dataService.getElementThroughAnyObject('Quote/GetStatusWiseQuotes', pagination).pipe(map(response => {
      return response;
    }));
  }
  FindContact(Id) {
    return this.dataService.getElementThroughId('Contact/Find', Id).pipe(map(response => {
      return response;
    }));
  }

  getAccounts(pagination: Pagination) {
    return this.dataService.getElementThroughAnyObject('Accounts/GetAccounts', pagination).pipe(map(response => {
      return response;
    }));
  }
  getContacts() {
    return this.dataService.getData('Contact').pipe(map(response => {
      return response;
    }));
  }

  getModuleAccess() {
    return this.dataService.getData('TenantMaster/GetAccessRoleMatrix/').pipe(map(response => {
      return response;
    }));
  }

  getAccountsData() {
    return this.dataService.getData('Accounts').pipe(map(response => {
      return response;
    }));
  }

  postHistory(History: any) {
    History.createdOn = new Date();
    History.updatedOn = new Date();
    History.createdBy = new Date();
    History.updatedBy = this.user.id;
    return this.dataService.postElement('History', History).pipe(map(response => {
      return response;
    }));
  }


  getAccountById(Id: string) {
    return this.dataService.getData('Accounts/' + Id).pipe(map(response => {
      return response;
    }));
  }

  DeleteContact(id: string) {
    return this.dataService.deleteElement('Contact', id).pipe(map(response => {
      return response;
    }));
  }

  UpdateContact(data) {
    data.updatedOn = new Date();
    data.createdOn = new Date();
    data.updatedBy = this.user.id;
    return this.dataService.updateElement('Contact', data).pipe(map(response => {
      return response;
    }));
  }
  AddQuote(quote) {
    quote.isActive = true;
    quote.createdOn = new Date();
    quote.updatedOn = new Date();
    quote.updatedBy = this.user.id;
    quote.createdBy = this.user.id;
    quote.ownerId = this.user.id;
    return this.dataService.postElement('Quote', quote).pipe(map(response => {
      return response;
    }));
  }


  UpdateAccount(data) {
    data.updatedOn = new Date();
    return this.dataService.updateElement('Accounts', data).pipe(map(response => {
      return response;
    }));
  }
  deleteAccount(data) {
    return this.dataService.updateElement('Accounts/DeleteAccount', data).pipe(map(response => {
      return response;
    }));
  }
  GetAccountsForExport() {
    return this.dataService.getData('Accounts/GetAccountsForExport').pipe(map(response => {
      return response;
    }));
  }

  getDataByAlphabate(model) {
    this.account.accountName = model;
    return this.dataService
      .getElementThroughAnyParam('History', this.account)
      .pipe(map(res => {
        return res;
      }));
  }


  addAccounts(Accounts) {
    Accounts.createdOn = new Date();
    Accounts.updatedOn = new Date();
    Accounts.createdBy = this.user.id;
    Accounts.updatedBy = this.user.id;
    return this.dataService.postElement('Accounts', Accounts).pipe(map(response => {
      return response;
    }));
  }

  uploadFile(file) {
    return this.dataService.postElement('Accounts/UploadFile', file).pipe(map(response => {
      return response;
    }));
  }

  GetContactWithoutAccount() {
    return this.dataService
      .getData('Contact/GetContactWithoutAccount')
      .pipe(map(response => {
        return response;
      }));
  }


  findEmail(email) {
    return this.dataService
      .getElementThroughId('Contact/FindEmail', email)
      .pipe(map(response => {
        return response;
      }));
  }

  FindAccount(account) {
    return this.dataService
      .getElementThroughId('Accounts/FindAccount', account)
      .pipe(map(response => {
        return response;
      }));
  }
}
