import { Constants } from "./../../_shared/constant";
import { Component, OnInit, OnDestroy, NgZone, DoCheck } from "@angular/core";
import { QuotesService } from "../quotes-service";
import { Router, Event, NavigationEnd, ActivatedRoute } from "@angular/router";
import { Pagination } from "../../_shared/model/pagination-model";
// import { PaginatePipe, PaginationService } from "ng2-pagination";//#Revisit_IgnoredAsNotUsed
import { Contact } from "../../_shared/model/contact-model";
import { Quote } from "../../_shared/model/quote-model";
import { ToastrService } from "ngx-toastr";
import { ngxCsv } from "ngx-csv";
import { Subscription } from "rxjs";
import { UserService } from "src/app/user-account/user-service";
import { Account } from "src/app/_shared/model/account-model";
import { Actions } from "src/app/_shared/model/actions-model";
import { Company } from "src/app/_shared/model/company-model";
import { User } from "src/app/_shared/model/user-model";
// import { json } from "d3"; //#Revisit_IgnoredAsNotUsed
declare var $: any;
@Component({
  selector: "app-quotes-list",
  templateUrl: "./quotes-list.component.html",
  styleUrls: ['./quotes-list.component.scss']
})
export class QuotesListComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  reverse = false;
  key: any;
  headers: any;
  myheaders: any;
  subscriptions: Subscription[] = [];
  constants: any;
  quotesInfo: Pagination;
  selectedAlpha: string;
  pagesCount: number;
  contacts: any = [];
  quoteforExport: any = [];
  quote: Quote;
  data: any;
  editId = 0;
  editId1 = 0;
  isFormSubmitted = false;
  isShowTextBox = false;
  textId = 0;
  // isNameEdit = false;
  quoteAdd = new Quote();
  textId1 = 0;
  quoteName = true;
  unique = true;
  account = true;
  status = true;
  value = true;
  Expaired = false;
  Created = false;
  contactName = true;
  isValidName = true;
  loading: boolean;
  user: User;
  accessValid: any;
  itemCount = 0;
  items = [];
  editProperty: string;
  columns = [];
  toBeEdit = "none";

  searchContact: any;
  contactsList = [];
  presentContact = false;
  isOpenPopup = true;
  company: Company;
  showContactClearButton = false;
  showAccountClearButton = false;
  cName: string;
  aName: string;
  accounts: any;
  constructor(
    private _quotesService: QuotesService,
    private ngZone: NgZone,
    private router: Router,
    private toastrService: ToastrService,
    private _UserService: UserService,
    private route: ActivatedRoute
  ) {
    this.accessValid = new Actions();
    this.headers = [];
    this.constants = Constants;
    this.selectedAlpha = "All";
    this.quotesInfo = new Pagination();
    this.contacts = new Array<Contact>();
    this.accounts = new Array<Account>();
    this.quote = new Quote();
    this.user = JSON.parse(sessionStorage.getItem("currentUser"));
    this.quotesInfo.pageSize = 10;
    this.company = new Company();
    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        const status = this.route.snapshot.queryParams["status"];
        const accountId =
          this.route.snapshot.queryParams["accountId"] === undefined
            ? null
            : atob(this.route.snapshot.queryParams["accountId"]);
        const contactId =
          this.route.snapshot.queryParams["contactId"] === undefined
            ? null
            : atob(this.route.snapshot.queryParams["contactId"]);
        if (status !== undefined) {
          this.quotesInfo.status = atob(status) === "All" ? null : atob(status);
          this.quotesInfo.accountId = accountId;
          this.quotesInfo.contactId = contactId;
          this.showContactClearButton =
            this.quotesInfo.contactId === null ? false : true;
          this.showAccountClearButton =
            this.quotesInfo.accountId === null ? false : true;
        }
        this.getQuotes(0);
      }
    });
  }

  ngOnInit() {
    $(window).scrollTop(0);

    this.GetCompanys();
    this.getModuleAccess();
    if (window.innerWidth <= 1024) {
      this.unique = false;
      this.status = false;
      this.value = false;
    } else {
      this.unique = true;
      this.status = true;
      this.value = true;
    }
    this.columns = [{ property: "uniqueId", header: "Quote ID" }, { property: "name", header: "Quote Title" }, { property: "contact", header: "Contacts" }, { property: "accounts", header: "Accounts" }, { property: "status", header: "Status" }, { property: "createdOn", header: "Total" },
    ];
  }

  setFocus() {
    this.isOpenPopup = true;
    setTimeout(() => {
      this.isOpenPopup = false;
    }, 600);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  getModuleAccess() {
    this.subscriptions.push(
      this._quotesService.getModuleAccess().subscribe((res) => {
        this.accessValid = res.quote;
      })
    );
  }

  SearchQuotes(e) {
    this.selectedAlpha = e;
    this.getQuotes(0);
  }

  pageChanged(no) {
    this.getQuotes(no);
  }

  goToDetails(id) {
    this.router.navigate(["/quotes/quotes-detail"], {
      queryParams: { id: btoa(id) },
    });
  }

  getQuotes(pageNo) {
    let sortingData = pageNo;
    //Adding this logic to accomodate inclusion of PrimeNg. Event is sent by the p-table.
    if (isNaN(pageNo)) {
      pageNo = pageNo.first / pageNo.rows + 1;
    }
    this.quotesInfo.sortBy = "CreatedOn";
    this.quotesInfo.sortType = "asc";
    this.loading = true;
    if (sortingData != 0) {
      if (sortingData.sortOrder === 1) {
        this.quotesInfo.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.quotesInfo.sortType = 'asc';
      }
      else {
        this.quotesInfo.sortBy = sortingData.sortField ? sortingData.sortField : 'CreatedOn';
        this.quotesInfo.sortType = 'desc';
      }
    }
    this.quotesInfo.pageNumber = pageNo;

    this.quotesInfo.alphabet =
      this.selectedAlpha === "All" ? "" : this.selectedAlpha;
    this.subscriptions.push(
      this._quotesService.getQuote(this.quotesInfo).subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            this.quotesInfo = res;
            this.items = JSON.parse(JSON.stringify(this.quotesInfo.records));
            this.itemCount = this.quotesInfo.totalCount;
            this.pagesCount = Math.ceil(res.totalCount / res.pageSize); // number of pages
            this.loading = false;
            this.getContacts();
            this.getAccounts();
          });
        },
        error: (err) => {
          this.loading = false;
          // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      }
      )
    );
  }
  onSave(valid, myquote) {
    if (valid) {
      this.editId = 0;
      this.textId = 0;
      this.toBeEdit = "none";
      this.quoteAdd = myquote;
      // this.quoteAdd.name = this.quote.name.replace(/^\s+|\s+$/g, ""); #PreviourCode. quote.name was referred in html input ngmodel 
      this.quoteAdd.name = myquote.name?.replace(/^\s+|\s+$/g, "");
      this._quotesService.UpdateQuoteFromList(this.quoteAdd).subscribe(
        (response) => {
          if (response) {
            this.getQuotes(this.quotesInfo.pageNumber);
          }
        },
        (err) => {
          this.toBeEdit = "name";
          // console.log(err);
          if (err.status === 403) {
            // tslint:disable-next-line:one-line
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      );
    }
  }

  discard() {
    this.editId = 1;
    this.toBeEdit = "none";
    this.textId = 1;
    this.items = JSON.parse(JSON.stringify(this.quotesInfo.records));
  }

  getContacts() {
    this.subscriptions.push(
      this._quotesService.GetContactWithAccount().subscribe(
        (res) => {

          this.ngZone.run(() => {
            this.contacts = res;
            this.cName =
              this.quotesInfo.contactId !== null
                ? this.contacts.filter(
                  (ele) => ele.id === this.quotesInfo.contactId
                )[0].firstName
                : null;
          });
        },
        (err) => {
          // console.log(err);
        }
      )
    );
  }

  FindContact() {
    this.quote.contact = new Contact();
    this.contactsList = this.contacts.filter(
      (cont) =>
        cont.firstName === this.searchContact ||
        cont.lastName === this.searchContact
    );
  }

  saveContactOnSelectContact(e) {
    this.quote.contact = e.item;
  }

  AddQuotes(valid, modal) {
    this.isFormSubmitted = true;
    if (valid) {
      if (this.quote.contact.id) {
        modal.hide();
        this.quote.name = this.quote.name.replace(/^\s+|\s+$/g, "");
        // this.quote.contact = this.contacts.filter(
        //   x => x.id === this.quote.contact.id
        // )[0];
        this.quote.createdBy = this.user.id;
        this.quote.isActive = true;
        this.quote.createdOn = new Date();
        this.quote.createdOn = new Date(
          this.quote.createdOn.getUTCFullYear(),
          this.quote.createdOn.getUTCMonth(),
          this.quote.createdOn.getUTCDate(),
          this.quote.createdOn.getUTCHours(),
          this.quote.createdOn.getUTCMinutes(),
          this.quote.createdOn.getUTCSeconds()
        );
        this.quote.expirationDate = this.quote.createdOn;
        this.quote.updatedOn = this.quote.createdOn;
        this.quote.expirationDate.setDate(
          this.quote.expirationDate.getDate() + this.constants.Expairy[0]
        );
        this.quote.expairyDays = this.constants.Expairy[0];
        this.quote.ownerId = this.user.id;
        this.quote.ownerName = this.user.firstName + " " + this.user.lastName;
        if (this.company) {
          this.quote.termsandConditions = this.company.termsConditions;
        }
        this._quotesService.AddQuote(this.quote).subscribe(
          (response) => {
            this.router.navigate(["/quotes/quotes-detail"], {
              queryParams: { id: btoa(response.id) },
            });
          },
          (err) => {
            // console.log(err);
            if (err.status === 403) {
              // tslint:disable-next-line:one-line
              // this.toastrService.warning(
              //   'Access denied',
              //   '',
              //   this.constants.toastrConfig
              // );
              this.show = true;
              this.popupOpen((this.popupFor = "Access denied"));
            }
          }
        );
      } else {
        this.presentContact = true;
      }
    }
  }
  cancelQuotes(form, popup) {
    form.reset();
    this.quote = new Quote();
    this.isFormSubmitted = false;
    this.presentContact = false;
    popup.hide();
  }

  sort(key) {
    this.key = key.sortBy;
    this.reverse = key.sortAsc;
    this.getQuotes(0);
    this.toBeEdit = "none";
  }

  //  To get list of columns headers - Suryakant
  getChangedColumns(columns) {
    this.columns = columns.datatable.columns._results;
    this.toBeEdit = "none";
    if (this.quotesInfo.records) {
      this.items = JSON.parse(JSON.stringify(this.quotesInfo.records));
    }
  }

  // export To CSV
  exportToCSV() {
    this.loading = true;
    this.subscriptions.push(
      this._quotesService.getQuoteForExport().subscribe(
        (res) => {
          this.quoteforExport.push(res);
          if (this.quoteforExport.length) {
            this.loading = false;
            const data = [];
            this.quoteforExport[0].forEach((element) => {
              const obj: any = {};
              this.columns.forEach((col) => {
                const prop = col.property;
                if (
                  prop === "status" &&
                  element.status.description !== null
                ) {
                  obj[prop] = element.status.description;
                  this.headers.push(col.header);
                } else if (
                  prop === "accounts" &&
                  element.contact.accounts.name !== null
                ) {
                  obj[prop] = element.contact.accounts.name;
                  this.headers.push(col.header);
                } else if (
                  prop === "contact" &&
                  element.contact.firstName !== null
                ) {
                  obj[prop] = element.contact.firstName;
                  this.headers.push(col.header);
                } else if (
                  prop === "createdOn" &&
                  element.createdOn !== null
                ) {
                  const date = new Date(element.createdOn);
                  const CreatedOn =
                    date.getFullYear() +
                    "-" +
                    (date.getMonth() + 1) +
                    "-" +
                    date.getDate();
                  obj.createdOn = CreatedOn;
                  this.headers.push(col.header);
                } else if (
                  prop === "expirationDate" &&
                  element.expirationDate !== null
                ) {
                  const date = new Date(element.expirationDate);
                  const ExpirationDate =
                    date.getFullYear() +
                    "-" +
                    (date.getMonth() + 1) +
                    "-" +
                    date.getDate();
                  obj.expirationDate = ExpirationDate;
                  this.headers.push(col.header);
                } else {
                  obj[prop] = element[prop];
                  this.headers.push(col.header);
                }
              });
              data.push(obj);
            });
            const header = this.headers.filter((v, i, a) => a.indexOf(v) === i);
            const options = {
              fieldSeparator: ",",
              quoteStrings: '"',
              decimalseparator: ".",
              showLabels: true,
              showTitle: true,
              headers: header,
            };
            this.headers = [];
            // tslint:disable-next-line:no-unused-expression
            new ngxCsv(data, "Quotes", options);
            this.loading = false;
          } else {
            // this.toastrService.warning(
            //   'No data available to export!',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = "No data available to export!"));
            this.loading = false;
          }
        },
        (err) => {
          // console.log(err);
          if (err.status === 403) {
            // this.toastrService.warning(
            //   'Access denied',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
          this.loading = false;
        }
      )
    );
  }

  itemEdit(item, edit) {
    this.editProperty = item.id;
    this.toBeEdit = edit;
    this.items = JSON.parse(JSON.stringify(this.quotesInfo.records));
  }

  // messages displayfor toasters
  popupOpen(popupFor) {
    if (this.popupFor === "Access denied") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Access denied";
    } else if (this.popupFor === "No data available to export!") {
      this.typeofMessage = "Error";
      this.notification = "No data available to export!";
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.messageToDisplay = null;
    }
  }

  yes(ConfirmModal) {
    ConfirmModal.hide();
    this.show = false;
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
  }

  hide(ConfirmationModel) {
    ConfirmationModel.hide();
    this.show = false;
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.notification = null;
  }

  GetCompanys() {
    this.loading = true;
    this.subscriptions.push(
      this._UserService.GetCompany().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.company = res;
            this.loading = false;
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
  }
  goToAccountDetails(id) {
    this.router.navigate(["/accounts/accounts-detail"], {
      queryParams: { id: btoa(id) },
    });
  }
  goToContactDetails(id) {
    this.router.navigate(["/contacts/contact-detail"], {
      queryParams: { id: btoa(id) },
    });
  }

  getAccounts() {
    this._quotesService.getAccountsData().subscribe((res) => {
      this.accounts = res;
      if (res) {
        this.aName =
          this.quotesInfo.accountId !== null
            ? this.accounts.filter(
              (ele) => ele.id === this.quotesInfo.accountId
            )[0].name
            : null;
      }
    });
  }

  discardEdit() {
    this.toBeEdit = "none";
    this.items = JSON.parse(JSON.stringify(this.quotesInfo.records));
  }

  onKeyDown() {
    if (
      this.quotesInfo.contactId === "" &&
      this.quotesInfo.contactId !== null
    ) {
      this.getQuotes(0);
    }
    if (
      this.quotesInfo.accountId === "" &&
      this.quotesInfo.accountId !== null
    ) {
      this.getQuotes(0);
    }
    if (this.quotesInfo.status === "" && this.quotesInfo.status.length === 0) {
      this.getQuotes(0);
    }
  }

  onContactSelect(event) {
    this.quotesInfo.contactId = event.item.id;
    this.getQuotes(0);
  }

  onAccountSelect(event) {
    this.quotesInfo.accountId = event.item.id;
    this.getQuotes(0);
  }

  onStatusSelect(event) {
    // this.quotesInfo.status = event.target.value;
    this.getQuotes(0);
  }

  clonedQuotes: { [s: string]: any; } = {};

  onRowEditInit(item: any) {
    this.clonedQuotes[item.id] = JSON.parse(JSON.stringify(item));
    console.log('Initial Value Before Edit');
    console.log(this.clonedQuotes[item.id]);
  }

  onRowEditSave(item: any) {
    delete this.clonedQuotes[item.id];
    this.onSave(true, item);
  }

  onRowEditCancel(item: any, index: number) {
    this.items[index] = this.clonedQuotes[item.id];
    delete this.clonedQuotes[item.id];
  }
}
