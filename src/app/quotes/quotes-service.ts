import { Injectable, NgZone, OnInit } from "@angular/core";
import { Pagination } from "../_shared/model/pagination-model";
import { DataService } from "../_core/data.service";
import { Router } from "@angular/router";
import { map } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class QuotesService implements OnInit {
  constants: any;
  userInfo: any;
  constructor(
    private dataService: DataService,
    private http: HttpClient,
    private ngZone: NgZone,
    private router: Router
  ) { }

  ngOnInit() { }

  // gets quotes for last 14 days - Tejaswini
  getQuote(pagination: Pagination) {
    return this.dataService
      .getElementThroughAnyObject("Quote/GetQuotes", pagination)
      .pipe(map((response) => {
        return response;
      }));
  }

  getQuoteThroughtId(Id) {
    return this.dataService.getElementThroughId("Quote", Id).pipe(map((response) => {
      return response;
    }));
  }

  getQuoteByCompanyId(Id: any, compantId: any) {
    // return this.dataService.getElementThroughId("Quote", Id).pipe(map((response) => {
    //   return response;
    // });
    return this.dataService
      .getDataAllowAnonymous("Quote/" + Id + "/" + compantId)
      .pipe(map((response) => {
        return response;
      }));
  }

  GetContactWithAccount() {
    return this.dataService
      .getData("Contact/GetContactWithAccount")
      .pipe(map((response) => {
        return response;
      }));
  }

  GetContactByAccount(id) {
    return this.dataService
      .getElementThroughId("Contact/GetContactByAccount", id)
      .pipe(map((response) => {
        return response;
      }));
  }

  AddQuote(quote) {
    quote.createdOn = new Date();
    return this.dataService.postElement("Quote", quote).pipe(map((response) => {
      return response;
    }));
  }

  UpdateQuote(quote) {
    return this.dataService.updateElement("Quote", quote).pipe(map((response) => {
      return response;
    }));
  }

  DeleteQuote(quote) {
    return this.dataService
      .updateElement("Quote/DeleteQuote", quote)
      .pipe(map((response) => {
        return response;
      }));
  }

  UpdateQuoteFromList(quote) {
    return this.dataService
      .updateElement("Quote/UpdateQuote", quote)
      .pipe(map((response) => {
        return response;
      }));
  }

  FindProduct(Id) {
    return this.dataService
      .getElementThroughId("Products/Find", Id)
      .pipe(map((response) => {
        return response;
      }));
  }

  GetTax() {
    return this.dataService.getData("TenantMaster/GetTax").pipe(map((response) => {
      return response;
    }));
  }

  GetDisclaimer() {
    return this.dataService
      .getDataAllowAnonymous("Configuration/GetDisclaimer")
      .pipe(map((response) => {
        return response;
      }));
  }

  ExportToPDF(quote) {
    return this.dataService
      .postElement("Quote/ExportToPDF", quote)
      .pipe(map((response) => {
        return response;
      }));
  }

  ExportToPDFOnly(quote) {
    return this.dataService
      .postElement("Quote/ExportToPDFOnly", quote)
      .pipe(map((response) => {
        return response;
      }));
  }

  SendQuoteMail(quote) {
    return this.dataService
      .postElement("Quote/SendQuoteMail", quote)
      .pipe(map((response) => {
        return response;
      }));
  }

  SendQuoteLink(quote) {
    return this.dataService
      .postElement("Quote/SendQuoteLink", quote)
      .pipe(map((response) => {
        return response;
      }));
  }

  getQuoteStatus() {
    return this.dataService
      .getData("Configuration/GetAllStatus")
      .pipe(map((response) => {
        return response;
      }));
  }

  QuoteConfirmation(id: string, com: string, isAccept: boolean) {
    return this.dataService
      .getDataAllowAnonymous(
        "Quote/QuoteConfirmation/" + id + "/" + com + "/" + isAccept
      )
      .pipe(map((response) => {
        return response;
      }));
  }

  QuoteInfo(id: string, com: string) {
    return this.dataService
      .getDataAllowAnonymous("Quote/QuoteInfoAllowAnonymous/" + id + "/" + com)
      .pipe(map((response) => {
        return response;
      }));
  }
  getQuoteForExport() {
    return this.dataService.getData("Quote").pipe(map((response) => {
      return response;
    }));
  }

  getModuleAccess() {
    return this.dataService
      .getData("TenantMaster/GetAccessRoleMatrix/")
      .pipe(map((response) => {
        return response;
      }));
  }

  GetGenralUser(id: string) {
    return this.dataService
      .getElementThroughId("User/GetGenralUser", id)
      .pipe(map((response) => {
        return response;
      }));
  }

  getAllPricingMethods() {
    return this.dataService
      .getData("TenantMaster/GetAllMethods/")
      .pipe(map((response) => {
        return response;
      }));
  }

  getAccountsData() {
    return this.dataService.getData("Accounts").pipe(map((response) => {
      return response;
    }));
  }
}
