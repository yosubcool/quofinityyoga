import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { QuotesRoutingModule } from "./quotes-routing.module";
import { QuotesDetailComponent } from "./quotes-detail/quotes-detail.component";
import { QuotesListComponent } from "./quotes-list/quotes-list.component";
import { QuotesService } from "./quotes-service";
import { NgxPaginationModule } from "ngx-pagination";
import { ModalModule } from "ngx-bootstrap/modal";
import { FormsModule } from "@angular/forms";
import { TypeaheadModule } from "ngx-bootstrap/typeahead";
import { NgxLoadingModule } from "ngx-loading";
import { BrowseComponent } from "../browse/browse/browse.component";
import { BrowseService } from "../browse/browse-service";
import { CommonTemplateService } from "../common/common-template.service";
import { BrowseModule } from "../browse/browse.module";
import { QuillModule } from "ngx-quill";
// import { DataTablesModule } from "angular-datatables";
import { CommonTemplateModule } from "../common/common-template.module";
import { SharedModule } from "../_shared/shared.module";
import { QuotesSendComponent } from "./quotes-send/quote-send.component";
import { KitsService } from "../kits/kits-service";
import { ProductsService } from "../products/products-service";
import { ButtonModule } from "primeng/button";
import { TableModule } from "primeng/table";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CommonModule,
    QuotesRoutingModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    TypeaheadModule.forRoot(),
    CommonTemplateModule,
    NgxLoadingModule,
    SharedModule,
    BrowseModule,
    QuillModule,
    QuillModule.forRoot(),
    // DataTablesModule,
    TableModule,
    ButtonModule
  ],
  declarations: [
    QuotesDetailComponent,
    QuotesListComponent,
    QuotesSendComponent,
  ],
  providers: [
    QuotesService,
    ProductsService,
    BrowseService,
    KitsService,
    CommonTemplateService,
  ],
})
export class QuotesModule { }
