import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RouteGaurd } from "../_core/RouteGaurd";
import { QuotesDetailComponent } from "./quotes-detail/quotes-detail.component";
import { QuotesListComponent } from "./quotes-list/quotes-list.component";
import { QuotesSendComponent } from "./quotes-send/quote-send.component";

const routes: Routes = [
  {
    path: "",
    canActivate: [RouteGaurd],
    data: {
      title: "Accounts",
      isLogin: true,
    },
    children: [
      {
        path: "quotes-detail",
        component: QuotesDetailComponent,
        data: {
          title: "Quotes Detail",
        },
      },
      {
        path: "quotes-list",
        component: QuotesListComponent,
        data: {
          title: "Quotes List",
        },
      },
      {
        path: "quotes-send",
        component: QuotesSendComponent,
        data: {
          title: "Quotes Send",
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuotesRoutingModule { }
