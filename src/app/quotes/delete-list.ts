export class DeleteList {
  id: string;
  type: string;
  index: number;
  constructor() {
    this.id = null;
    this.type = null;
    this.index = null;
  }
}
