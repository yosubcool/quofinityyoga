import { Router, ActivatedRoute, NavigationEnd, Event } from "@angular/router";
import {
  OnInit,
  OnDestroy,
  Component,
  NgZone,
  ViewChild,
  ElementRef,
  DoCheck,
} from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Quote, SendQuoteDetail } from "../../_shared/model/quote-model";
import { Company } from "../../_shared/model/company-model";
import { Subscription } from "rxjs";
import { UserService } from "src/app/user-account/user-service";
import { Constants } from "src/app/_shared/constant";
import { QuotesService } from "../quotes-service";
// import { isArray } from "util";  //#Revisit_PreviousCode
// import * as jsPDF from 'jspdf';
declare var $: any;
@Component({
  selector: "app-quotes-send",
  templateUrl: "./quotes-send.component.html",
})
export class QuotesSendComponent implements OnInit, OnDestroy {
  // Popup
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;

  quoteId: string;
  @ViewChild("content")
  content: ElementRef;
  quotesInfo: Quote;
  quotePreparerMail: string;
  company: Company;
  loading: boolean;
  constants: any;
  subscriptions: Subscription[] = [];
  isLink: boolean;
  isMail: boolean;
  isPdf: boolean;
  start = null;
  urlString: any;
  isIteamizedLabor: any;
  newExpiry: any;
  quoteStatusList: any;
  Type = [
    { name: "Detailed", value: "Detailed" },
    { name: "Semi Detailed", value: "SemiDetailed" },
    { name: "Non Detailed", value: "NonDetailed" },
    { name: "Detailed Kit(s) w/Header", value: "DetailedKitHeader" },
    { name: "Semi Detailed Kit(s) w/Header", value: "SemiDetailedKitHeader" },
    // { name: 'Detailed Kit(s) ', value: 'DetailedKit' },
    // { name: 'Semi Detailed Kit(s)', value: 'SemiDetailedKit' },
  ];
  quoteUrl: any;
  constructor(
    private _quotesService: QuotesService,
    private _UserService: UserService,
    private ngZone: NgZone,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private _route: Router
  ) {
    this.company = new Company();
    this.constants = Constants;
    this.subscriptions.push(
      _route.events.subscribe((event: Event) => {
        if (event instanceof NavigationEnd) {
          const id = this.route.snapshot.queryParams["id"];
          if (!id) {
            this._route.navigate(["/quotes/quotes-list"]);
          }
          this.quoteId = atob(id);
          this.getQuotes(this.quoteId);
        }
      })
    );
  }
  ngOnInit(): void {
    $(window).scrollTop(0);
    $('input[type="radio"]').click(() => {
      const inputValue = $(this).attr("value");
      const targetBox = $("." + inputValue);
      $(".detail-type").not(targetBox).hide();
      $(targetBox).show();
    });
    const id = this.route.snapshot.queryParams["id"];
    if (!id) {
      this._route.navigate(["/quotes/quotes-list"]);
    }
    this.quoteId = atob(id);
    this.getQuotes(this.quoteId);
    this.GetCompanys();
  }

  onCancel() {
    this.isPdf = false;
    this.isLink = false;
    this.isMail = false;
  }

  LaborChange(val: boolean) {
    this.isIteamizedLabor = val;
  }
  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  GetCompanys() {
    this.loading = true;
    this.subscriptions.push(
      this._UserService.GetCompany().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.company = res;
            this.loading = false;
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
  }

  getQuotes(id) {
    this._quotesService.getQuoteThroughtId(id).subscribe({

      next: (res) => {
        this.ngZone.run(() => {

          this.quotesInfo = res.quote;
          this.getPreparerMail(this.quotesInfo.ownerId);
          this.addTotalCostWithoutLabor();
          const sendDate = new Date(this.quotesInfo.sentDate);
          if (
            this.quotesInfo.status.description === this.constants.InProgress
          ) {
            this.getQuoteUrl();
            this.newExpiry = new Date();
            this.newExpiry.setDate(
              sendDate.getDate() + this.quotesInfo.expairyDays
            );
          } else {
            this.getQuoteUrl();
            this.newExpiry = new Date(this.quotesInfo.expirationDate);
          }

          if (this.quotesInfo.details.type === "NonDetailed") {
            this.quotesInfo.details.isIteamizedLabor = this.start;
          }
          if (
            this.quotesInfo.status.description === this.constants.Accepted ||
            this.quotesInfo.status.description === this.constants.Rejected ||
            this.quotesInfo.status.description === this.constants.Expired
          ) {
            this._route.navigate(["/quotes/quotes-detail"], {
              queryParams: { id: btoa(this.quoteId) },
            });
          }
          // tslint:disable-next-line:curly
          if (!this.quotesInfo.details)
            this.quotesInfo.details = new SendQuoteDetail();
          // tslint:disable-next-line:max-line-length
          this.quotesInfo.details.isIteamizedLabor =
            this.quotesInfo.details.isIteamizedLabor != null
              ? this.quotesInfo.details.isIteamizedLabor
              : "Iteamized";
          this.isIteamizedLabor = this.quotesInfo.details.isIteamizedLabor;
          this.quotesInfo.details.type =
            this.quotesInfo.details.type != null
              ? this.quotesInfo.details.type
              : "Detailed";
        });
      },
      error: (err) => {
        // console.log(err);
      }
    }
    );
  }

  ExportToPDF() {
    // if (this.quotesInfo.details.name && this.quotesInfo.details.title) {
    this.loading = true;
    this._quotesService.ExportToPDF(this.quotesInfo).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.loading = false;
            this.downloadFile(res, "application/pdf", this.quotesInfo.name);
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
    // } else {
    //   this.toastrService.error(
    //     'Please provide name and title details.',
    //     '',
    //     this.constants.toastrConfig
    //   );
    // }
  }
  goToDetails(id) {
    this._route.navigate(["/products/products-detail"], {
      queryParams: { id: btoa(id) },
    });
  }
  getKitDetails(id) {
    this.loading = true; // Loader Enable
    this._route.navigate(["/kits/kits-detail"], {
      queryParams: { id: btoa(id) },
    });
    this.loading = false; // Loader Disable
  }

  SendQuoteLink() {

    this.loading = true;
    this._quotesService.SendQuoteLink(this.quotesInfo).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            let selBox = document.createElement("textarea");
            selBox.style.position = "fixed";
            selBox.style.left = "0";
            selBox.style.top = "0";
            selBox.style.opacity = "0";
            selBox.value = this.quoteUrl;
            document.body.appendChild(selBox);
            selBox.focus();
            selBox.select();
            document.execCommand("copy");
            document.body.removeChild(selBox);
            this.loading = false;
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }
  myFunction() {

    this.quoteUrl =
      this.urlString +
      "&isIteamizedLabor=" +
      btoa(this.quotesInfo.details.isIteamizedLabor) +
      "&type=" +
      btoa(this.quotesInfo.details.type);
  }
  getQuoteUrl() {

    this.loading = true;
    this._quotesService.SendQuoteLink(this.quotesInfo).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {

            this.quoteUrl = res.replace("quote-detail", "quote-view");
            this.urlString = res.replace("quote-detail", "quote-view");
            this.quoteUrl =
              this.urlString +
              "&isIteamizedLabor=" +
              btoa(this.quotesInfo.details.isIteamizedLabor) +
              "&type=" +
              btoa(this.quotesInfo.details.type);
            this.loading = false;
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }
  SendQuote() {

    this.loading = true;
    this._quotesService.SendQuoteLink(this.quotesInfo).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            window.open(
              "http://localhost:4200/#/confirmation/quote-detail?id=NjE5MjQ1OGY0M2Q4NTU4MDY4M2MzNjQy&cid=NWM1ZjIwNjdiYTJkMjkxYTAwMjMxNWFi"
            );
            // this.toastrService.success(
            //   'Link send successfully',
            //   '',
            //   this.constants.toastrConfig
            // );
            this.loading = false;
            // this._route.navigate(["/quotes/quotes-detail"], {
            //   queryParams: { id: btoa(this.quoteId) },
            // });
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }
  // SendQuote() {
  //   //  if (this.quotesInfo.details.name && this.quotesInfo.details.title) {
  //   this.loading = true;
  //   this._quotesService.SendQuoteMail(this.quotesInfo).subscribe(
  //     (res) => {
  //       this.ngZone.run(() => {
  //         if (res) {
  //           // this.toastrService.success(
  //           //   'Mail send successfully',
  //           //   '',
  //           //   this.constants.toastrConfig
  //           // );
  //           this.loading = false;
  //           this._route.navigate(["/quotes/quotes-detail"], {
  //             queryParams: { id: btoa(this.quoteId) },
  //           });
  //         }
  //       });
  //     },
  //     (err) => {
  //       this.loading = false;
  //     }
  //   );
  //   // } else {
  //   //   this.toastrService.error(
  //   //     'Please provide name and title details.',
  //   //     '',
  //   //     this.constants.toastrConfig
  //   //   );
  //   // }
  // }

  downloadFile(data: string, fileType: any, FileName) {
    // tslint:disable-next-line:prefer-const
    let byteCharacters = atob(data);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: fileType });
    const url = window.URL.createObjectURL(blob);
    const downloadPDF = document.createElement("a");
    downloadPDF.href = url;
    downloadPDF.download = FileName + ".pdf";
    downloadPDF.click();
    this.loading = false;
    this._route.navigate(["/quotes/quotes-detail"], {
      queryParams: { id: btoa(this.quoteId) },
    });
  }

  goToQuote() {
    this._route.navigate(["/quotes/quotes-detail"], {
      queryParams: { id: btoa(this.quoteId) },
    });
  }

  getQuoteStatus() {
    this.subscriptions.push(
      this._quotesService.getQuoteStatus().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.quoteStatusList = res;
          });
        },
        (err) => {
          // console.log(err);
        }
      )
    );
  }

  // messages display
  popupOpen(popupFor) {
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.messageToDisplay = null;
    this.typeofMessage = "Error";
    if (this.popupFor === "mail") {
      this.notification = "Are you ready to mail your quote?";
      this.showButtons = true;
    } else if (this.popupFor === "pdf") {
      this.notification = "Are you ready to export your quote?";
      this.showButtons = true;
    } else if (this.popupFor === "link") {
      this.notification = "Are you ready to share your quote link?";
      this.showButtons = true;
    } else if (this.popupFor === "copy") {
      this.notification = "Are you ready to copy your quote link?";
      this.showButtons = true;
    }
  }

  yes(ConfirmModal) {
    if (this.popupFor === "mail") {
      this.SendQuote();
      ConfirmModal.hide();
    } else if (this.popupFor === "pdf") {
      this.ExportToPDF();
      ConfirmModal.hide();
    } else if (this.popupFor === "link") {
      this.SendQuoteLink();
      ConfirmModal.hide();
    } else if (this.popupFor === "copy") {
      this.SendQuoteLink();
      ConfirmModal.hide();
    }
  }

  no(ConfirmModal) {
    ConfirmModal.hide();
    this.popupFor = "none";
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
  }

  addTotalCostWithoutLabor() {
    this.quotesInfo.kits.forEach((element) => {
      element.totalWithoutLabor = 0;
      if (Array.isArray(element.kit.products)) {
        element.kit.products.forEach((iItem) => {
          if (iItem.product.productType.description !== "Labor") {
            // element.totalWithoutLabor = element.totalWithoutLabor + iItem.price;
            element.totalWithoutLabor =
              element.totalWithoutLabor + iItem.price * iItem.quantity;
          }
        });
      }
    });
    //   let kitCost = 0;
    //   if (kit ? isArray(kit.kit.products) : false) {
    //     kit.products.forEach(element => {
    //       kitCost =
    //         kitCost + element.product.productType.description !== 'Labor'
    //           ? element.price
    //           : 0;
    //     });
    //   }
    //   return kitCost;
    // }
  }

  getPreparerMail(ownerId) {
    this._UserService.getUserDetails(ownerId).subscribe((response) => {
      if (response) {
        this.quotePreparerMail = response.email;
      }
    });
  }
}
