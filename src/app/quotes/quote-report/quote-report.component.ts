// import { concat } from "rxjs/observable/concat"; //#Revisit_IgnoredAsNotUsed
// import { forEach } from "@angular/router/src/utils/collection"; //#Revisit_IgnoredAsNotUsed
import { Kit } from "./../../_shared/model/kit-model";
import { Component, NgZone, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { QuotesService } from "../quotes-service";
import { Title } from "@angular/platform-browser";
import { Subscription } from "rxjs";
import { CustomItem } from "src/app/_shared/model/customItem-model";
import { ProductDetail } from "src/app/_shared/model/productDetail-model";
@Component({
  selector: "app-quote-report",
  templateUrl: "./quote-report.component.html",
  styleUrls: ["./quote-report.component.scss"],
})
export class QuoteReportComponent implements OnInit, OnDestroy {
  groupByProducts: any[];
  quotesInfo: any;
  ownerId: any;
  popupFor: string;
  account: any;
  contact: any;
  loading: boolean;
  show: boolean;
  subscriptions: Subscription[] = [];
  masterMethodList: any;
  isInfinity: boolean;
  companyId: any;
  customProducts: CustomItem[];
  constructor(
    private _quotesService: QuotesService,
    private ngZone: NgZone,
    private route: ActivatedRoute,
    private _route: Router,
    private titleService: Title
  ) {
    this.titleService.setTitle("Quofinity - Financial Snapshot");
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const id = params["id"];
      this.companyId = params["companyId"];
      this.getQuotes(id);
      this.customProducts = new Array<CustomItem>();
    });
  }

  getQuotes(id) {
    this.subscriptions.push(
      this._quotesService.getQuoteByCompanyId(id, this.companyId).subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            let groups = new Set(
              res.quote.products.map((item) =>
                item.product.productType.description !== null
                  ? item.product.productType.description
                  : ""
              )
            ),
              results = [];
            groups.forEach((g) =>
              results.push({
                name: g,
                values: res.quote.products.filter(
                  (i) => i.product.productType.description === g
                ),
              })
            );
            var productList = new Array<ProductDetail>();
            res.quote.kits.forEach((item) => {
              item.kit.products.forEach((prod) => {
                for (let i = 0; i < item.quantity; i++) {
                  productList.push(prod);
                }
              });
            });
            var productTypes = new Array<string>();
            res.quote.kits.forEach((item) => {
              item.kit.products.forEach((prod) => {
                if (
                  !productTypes.includes(prod.product.productType.description)
                )
                  productTypes.push(prod.product.productType.description);
              });
            });
            if (
              res.quote.customItem != "undefined" &&
              res.quote.customItem.length != 0
            ) {
              res.quote.customItem.forEach((element) => {
                this.customProducts.push(element);
              });
            }
            res.quote.kits.forEach((element) => {
              if (
                element.kit.customItem != "undefined" &&
                element.kit.customItem != null &&
                element.kit.customItem.length != 0
              ) {
                element.kit.customItem.array.forEach((customElement) => {
                  this.customProducts.push(customElement);
                });
              }
            });
            results.forEach((r) => {
              if (productTypes.indexOf(r.name) !== -1) {
                r.values = r.values.concat(
                  productList.filter(
                    (i) => i.product.productType.description === r.name
                  )
                );
                productTypes.splice(productTypes.indexOf(r.name), 1);
              }
            });
            productTypes.forEach((a) => {
              results.push({
                name: a,
                values: productList.filter(
                  (i) => i.product.productType.description === a
                ),
              });
            });
            this.groupByProducts = results;

            this.quotesInfo = res.quote;
            if (this.quotesInfo.products) {
              this.checkMethodsInProduct(this.quotesInfo);
            }
            if (this.quotesInfo.kits) {
              this.checkMethodsInKit(this.quotesInfo);
            }
            this.ownerId = this.quotesInfo.ownerId;
            this.account = this.quotesInfo.contact.accounts;
            this.contact = this.quotesInfo.contact;
            this.loading = false;
            // const currentUser = JSON.parse(
            //   sessionStorage.getItem('currentUser')
            // );
            // if (currentUser.role.description !== 'Admin') {
            //   if (this.quotesInfo.ownerId !== currentUser.id) {
            //     // this._route.navigate(['/quotes/quotes-list']);
            //     // this.show = true;
            //     // this.popupOpen((this.popupFor = 'dont have permission'));
            //   }
            // }
          });
        },
        error: (err) => {
          this.loading = false;
        }
      }
      )
    );
  }

  groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
      // const key = keyGetter(item.product.productType);
      const collection = map.get(keyGetter);
      if (!collection) {
        map.set(keyGetter, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  }
  // method to check if method property absent, if so then update method
  checkMethodsInProduct(detail) {
    const collectionInQuote = JSON.parse(JSON.stringify(detail));
    let change = false;
    if (collectionInQuote.products) {
      collectionInQuote.products.forEach((prod) => {
        if (prod.product.pricingMethod) {
          prod.product.pricingMethod.forEach((pricingMethods) => {
            if (
              !pricingMethods["method"] ||
              pricingMethods["method"] === null
            ) {
              this.masterMethodList.forEach((masterMethod) => {
                if (masterMethod.name === pricingMethods.name) {
                  pricingMethods.method = pricingMethods.name;
                  pricingMethods.name =
                    pricingMethods.value + pricingMethods.name;
                  change = true;
                }
              });
            } else if (pricingMethods["method"] !== null) {
              this.ownerId = collectionInQuote.ownerId;
              this.account = collectionInQuote.contact.accounts;
              this.contact = collectionInQuote.contact;
            }
          });
          if (prod.margin) {
            if (!prod.margin["method"] || prod.margin["method"] === null) {
              prod.margin.method = prod.margin.name;
              prod.margin.name = prod.margin.value + prod.margin.name;
              change = true;
            }
          }
        }
      });
    }
    if (change) {
      this.quotesInfo = JSON.parse(JSON.stringify(collectionInQuote));
    }
  }

  // method to check if method property absent, if so then update method
  checkMethodsInKit(detail) {
    let change = false;
    const collectionInQuote = JSON.parse(JSON.stringify(detail));
    if (collectionInQuote.kits) {
      collectionInQuote.kits.forEach((k) => {
        if (k.kit && k.kit.products) {
          k.kit.products.forEach((prod) => {
            if (prod.product.pricingMethod) {
              prod.product.pricingMethod.forEach((pricingMethods) => {
                if (
                  !pricingMethods["method"] ||
                  pricingMethods["method"] === null
                ) {
                  this.masterMethodList.forEach((masterMethod) => {
                    if (masterMethod.name === pricingMethods.name) {
                      pricingMethods.method = pricingMethods.name;
                      pricingMethods.name =
                        pricingMethods.value + pricingMethods.name;
                      change = true;
                    }
                  });
                } else if (pricingMethods["method"] !== null) {
                  this.ownerId = collectionInQuote.ownerId;
                  this.account = collectionInQuote.contact.accounts;
                  this.contact = collectionInQuote.contact;
                }
              });
              if (prod.margin) {
                if (!prod.margin["method"] || prod.margin["method"] === null) {
                  prod.margin.method = prod.margin.name;
                  prod.margin.name = prod.margin.value + prod.margin.name;
                  change = true;
                }
              }
            }
          });
        }
      });
    }
    if (change) {
      this.quotesInfo = JSON.parse(JSON.stringify(collectionInQuote));
    }
  }

  GetCost() {
    let customTotal: any = 0;
    let productTotal: any = 0;
    if (this.quotesInfo != undefined) {
      if (this.quotesInfo.products != null && this.quotesInfo.products.length) {
        this.quotesInfo.products.forEach((element) => {
          if (element.product) {
            productTotal =
              productTotal + element.product.cost * element.quantity;
          }
        });
      }
      if (
        this.quotesInfo.customItem != null &&
        this.quotesInfo.customItem.length
      ) {
        this.quotesInfo.customItem.forEach((element) => {
          // tslint:disable-next-line:curly
          if (element.cost)
            customTotal = customTotal + element.cost * element.quantity;
        });
      }
      if (this.quotesInfo.kits != null) {
        this.quotesInfo.kits.forEach((element) => {
          if (element.kit && element.kit.products) {
            element.kit.products.forEach((ele) => {
              if (ele.product) {
                productTotal =
                  productTotal +
                  ele.product.cost * (element.quantity * ele.quantity);
              }
            });
          }
        });
      }
      if (this.quotesInfo.kits != null) {
        this.quotesInfo.kits.forEach((element) => {
          if (element.kit && element.kit.customItem != null) {
            element.kit.customItem.forEach((ele) => {
              customTotal =
                customTotal + ele.cost * (ele.quantity * element.quantity);
            });
          }
        });
      }
      let cost = customTotal + productTotal;
      cost = cost.toFixed(2);
      return cost;
    }
  }

  GetTax(tax) {
    let productTotal: any = 0;
    if (this.quotesInfo.products != null) {
      this.quotesInfo.products.forEach((element) => {
        if (element.product && element.product.isTaxable) {
          productTotal = productTotal + element.price * element.quantity;
        }
      });
    }
    if (
      this.quotesInfo.customItem != null &&
      this.quotesInfo.customItem.length
    ) {
      this.quotesInfo.customItem.forEach((element) => {
        if (element && element.isTaxable) {
          productTotal = productTotal + element.price * element.quantity;
        }
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.products) {
          element.kit.products.forEach((ele) => {
            if (ele.product && ele.product.isTaxable) {
              productTotal =
                productTotal + ele.price * (element.quantity * ele.quantity);
            }
          });
          if (element.kit.customItem) {
            element.kit.customItem.forEach((ele) => {
              if (ele && ele.isTaxable) {
                productTotal =
                  productTotal + ele.price * (element.quantity * ele.quantity);
              }
            });
          }
        }
      });
    }
    let taxableAmount: any = 0;
    if (this.quotesInfo.discount > 0) {
      productTotal = productTotal - this.quotesInfo.discount;
    }
    if (productTotal > 0) {
      taxableAmount = (productTotal / 100) * tax;
    } else {
      taxableAmount = 0;
    }
    taxableAmount = taxableAmount.toFixed(2);
    this.quotesInfo.total =
      parseFloat(this.quotesInfo.subtotal) +
      parseFloat(taxableAmount) -
      parseFloat(this.quotesInfo.discount);
    this.quotesInfo.total = isNaN(this.quotesInfo.total)
      ? 0
      : this.quotesInfo.total;
    // tslint:disable-next-line:triple-equals
    if (this.quotesInfo.saleTax.value == tax) {
      this.quotesInfo.taxAmount = taxableAmount;
    }
    return taxableAmount;
  }

  GetProfitMargin() {
    let cost: any = 0;
    let customTotal: any = 0;
    let productTotal: any = 0;
    if (this.quotesInfo.products != null) {
      this.quotesInfo.products.forEach((element) => {
        if (element.product) {
          productTotal = productTotal + element.product.cost * element.quantity;
        }
      });
    }
    if (
      this.quotesInfo.customItem != null &&
      this.quotesInfo.customItem.length
    ) {
      this.quotesInfo.customItem.forEach((element) => {
        // tslint:disable-next-line:curly
        if (element.cost)
          customTotal = customTotal + element.cost * element.quantity;
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.products) {
          element.kit.products.forEach((ele) => {
            if (ele.product) {
              productTotal =
                productTotal +
                ele.product.cost * (element.quantity * ele.quantity);
            }
          });
        }
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.customItem != null) {
          element.kit.customItem.forEach((ele) => {
            customTotal =
              customTotal + ele.cost * (ele.quantity * element.quantity);
          });
        }
      });
    }
    cost = customTotal + productTotal;
    this.quotesInfo.profit =
      parseFloat(this.quotesInfo.subtotal) -
      parseFloat(cost) -
      parseFloat(this.quotesInfo.discount);
    this.quotesInfo.profit = isNaN(this.quotesInfo.profit)
      ? 0
      : this.quotesInfo.profit;
    this.quotesInfo.profit = this.quotesInfo.profit;
    this.quotesInfo.profitMargin =
      (this.quotesInfo.profit /
        (parseFloat(this.quotesInfo.subtotal) - this.quotesInfo.discount)) *
      100;
    this.quotesInfo.profitMargin = isNaN(this.quotesInfo.profitMargin)
      ? 0
      : this.quotesInfo.profitMargin;
    if (
      this.quotesInfo.profitMargin === -Infinity ||
      this.quotesInfo.profitMargin === Infinity
    ) {
      this.quotesInfo.profitMargin = 0;
    }
    if (
      parseFloat(this.quotesInfo.subtotal) - this.quotesInfo.discount <= 0 &&
      this.quotesInfo.discount > 0
    ) {
      this.isInfinity = true;
    } else {
      this.isInfinity = false;
    }
    this.quotesInfo.profitMargin =
      Math.round((this.quotesInfo.profitMargin + Number.EPSILON) * 100) / 100;
    return this.quotesInfo.profitMargin;
  }

  GetProductWiseCost(item) {
    let productTotal: any = 0;
    if (item.values != null && item.values.length) {
      item.values.forEach((element) => {
        if (element.product) {
          productTotal = productTotal + element.product.cost * element.quantity;
        }
      });
    }

    let cost = productTotal;
    cost = cost.toFixed(2);
    return cost;
  }

  GetProjectWiseProfitMargin(item) {
    let productTotal: any = 0;
    let pMargin: any = 0;
    let profit: any = 0;
    let subtotal: any = 0;
    let discount: any = 0;
    if (item.name === "Recurring") {
      if (item.values != null && item.values.length) {
        item.values.forEach((element) => {
          if (element.product) {
            productTotal =
              productTotal + element.product.cost * element.quantity;
            subtotal = subtotal + element.price * element.quantity;
          }
          if (element.margin.method == "Discount") {
            let dis = element.price * (element.margin.value / 100);
            discount = discount + dis;
          }
        });
      }
      profit = subtotal - productTotal;
    }
    if (item.name === "Material") {
      if (item.values != null && item.values.length) {
        item.values.forEach((element) => {
          if (element.product) {
            productTotal =
              productTotal + element.product.cost * element.quantity;
            subtotal = subtotal + element.price * element.quantity;
          }
          if (element.margin.method == "Discount") {
            let dis = element.price * (element.margin.value / 100);
            discount = discount + dis;
          }
        });
      }
      profit = subtotal - productTotal;
    }
    if (item.name === "Labor") {
      if (item.values != null && item.values.length) {
        item.values.forEach((element) => {
          if (element.product) {
            productTotal =
              productTotal + element.product.cost * element.quantity;
            subtotal = subtotal + element.price * element.quantity;
          }
        });
      }
      profit = subtotal - productTotal;
      profit = parseFloat(profit);
      profit = isNaN(profit) ? 0 : profit.toFixed(2);
    }

    pMargin = (profit / subtotal) * 100; //) / parseFloat(productTotal);
    pMargin = isNaN(pMargin) ? 0 : pMargin;
    if (pMargin === -Infinity || pMargin === Infinity) {
      pMargin = 0;
    }
    return pMargin;
  }

  GetProjectWiseProfit(item) {
    let productTotal: any = 0;
    let subTotal: any = 0;
    let profit: any = 0;
    let discount: any = 0;
    if (item.name === "Recurring") {
      if (item.values != null && item.values.length) {
        item.values.forEach((element) => {
          if (element.product) {
            productTotal =
              productTotal + element.product.cost * element.quantity;
            subTotal = subTotal + element.price * element.quantity;
            // productTotal = subTotal - productTotal;
          }
          if (element.margin.method == "Discount") {
            let dis = element.price * (element.margin.value / 100);
            discount = discount + dis;
          }
        });
        profit = subTotal - productTotal;
      }
    }
    if (item.name === "Material") {
      if (item.values != null && item.values.length) {
        item.values.forEach((element) => {
          if (element.product) {
            productTotal =
              productTotal + element.product.cost * element.quantity;
            subTotal = subTotal + element.price * element.quantity;
            // productTotal = subTotal - productTotal;
          }
          if (element.margin.method == "Discount") {
            let dis = element.price * (element.margin.value / 100);
            discount = discount + dis;
          }
        });
        profit = subTotal - productTotal;
      }
    }
    if (item.name === "Labor") {
      if (item.values != null && item.values.length) {
        item.values.forEach((element) => {
          if (element.product) {
            productTotal =
              productTotal + element.product.cost * element.quantity;
            subTotal = subTotal + element.price * element.quantity;
            // productTotal = subTotal;
          }
        });
        productTotal = subTotal - productTotal;
        profit = parseFloat(productTotal);
      }
    }
    profit = isNaN(profit) ? 0 : profit.toFixed(2);
    return profit;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
  }

  getCustomProductProfit() {
    let subTotal = 0;
    let productTotal = 0;
    let profit = 0;
    this.customProducts.forEach((a) => {
      subTotal = subTotal + a.price * a.quantity;
      productTotal = productTotal + a.cost * a.quantity;
    });
    profit = subTotal - productTotal;
    return profit;
  }
  getCustomProductProfitMargin() {
    let subTotal = 0;
    let productTotal = 0;
    let profit = 0;
    let profitMargin = 0;
    this.customProducts.forEach((a) => {
      subTotal = subTotal + a.price * a.quantity;
      productTotal = productTotal + a.cost * a.quantity;
    });
    profit = subTotal - productTotal;
    profitMargin = (profit / subTotal) * 100; //) / productTotal;
    profitMargin = Number.isNaN(profitMargin) ? 0 : profitMargin;
    if (profitMargin === -Infinity || profitMargin === Infinity) {
      profitMargin = 0;
    }
    return profitMargin;
  }
  getCustomProductCost() {
    let productTotal = 0;
    this.customProducts.forEach((a) => {
      productTotal = productTotal + a.cost * a.quantity;
    });
    return productTotal;
  }
  roundNumber(num: number, places: number) {
    const factor = 10 ** places;
    return Math.round(num * factor) / factor;
  }
}
