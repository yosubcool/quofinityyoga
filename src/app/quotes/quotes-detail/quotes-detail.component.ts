import {
  Component,
  OnInit,
  NgZone,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit,
  AfterViewChecked,
} from "@angular/core";
import { QuotesService } from "../quotes-service";
import {
  Router,
  ActivatedRoute,
  Route,
  NavigationEnd,
  Event,
} from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Quote } from "../../_shared/model/quote-model";
import { Contact } from "../../_shared/model/contact-model";
import { Account } from "../../_shared/model/account-model";
import { Constants } from "../../_shared/constant";
import { Product } from "../../_shared/model/product-model";
import {
  ProductDetail,
  KitDetail,
} from "../../_shared/model/productDetail-model";
import { Taxes } from "../../_shared/model/tenantMaster-model";
import { Kit } from "../../_shared/model/kit-model";
import { CompairValidator } from "../../_shared/compair-validator.directive";
import { Company } from "../../_shared/model/company-model";
import { Actions } from "../../_shared/model/actions-model";
import { User } from "../../_shared/model/user-model";
import { NgForm } from "@angular/forms";
import { ngxCsv } from "ngx-csv";
// import { json } from "../../../../node_modules/@types/d3";//#Revisit_PreviousCode
import { DeleteList } from "../delete-list";
import { ModalDirective } from "ngx-bootstrap/modal";
import { ProductsService } from "src/app/products/products-service";
import { UserService } from "src/app/user-account/user-service";
import { DataService } from "src/app/_core/data.service";
import { CustomItem } from "src/app/_shared/model/customItem-model";
import { PricingMethod } from "src/app/_shared/model/pricing-model";
import { Search } from "src/app/_shared/model/search-model";
import { Subscription } from "rxjs";
// import { forEach } from "@angular/router/src/utils/collection"; //#Revisit_PreviousCode
declare var $: any;
@Component({
  selector: "app-quotes-detail",
  templateUrl: "./quotes-detail.component.html",
  styles: [],
})
export class QuotesDetailComponent implements OnInit, OnDestroy, AfterViewInit {
  // Popup
  @ViewChild("ConfirmModal")
  confirmModal: ModalDirective;
  @ViewChild("viewKitDetailModal")
  viewKitDetailModal: ModalDirective;
  typeofMessage: string;
  messageToDisplay: string;
  noteToDisplay: string;
  dataToDisplay: string;
  showButtons = false;
  popupFor: string;
  notification: string;
  show = false;
  item: any;
  selectedList: DeleteList[];
  kitIndex: number;
  kitDetails: any;
  quotesInfo: Quote;
  account: Account;
  contact: Contact;
  quoteId: string;
  quoteStatusList: any = [];
  constants: any;
  SearchTextForQuote: string;
  ProductList: any = [];
  customItem: any;
  Products: any = [];
  PricingMethods: any = [];
  masterMethodList = [];
  isquoteTitleEdit: boolean;
  Taxes: Taxes[];
  isSend: boolean;
  isDelete: boolean;
  isCopy: boolean;
  isAccept: boolean;
  isReject: boolean;
  isreOpen: boolean;
  isProductDelete: boolean;
  isCustomProductDelete: boolean;
  isKitDelete: boolean;
  productItem: number;
  kitItem: number;
  CustomItemDelete: number;
  loading: boolean;
  company: Company;
  contactid: any = null;
  contacts: any = [];
  allContacts: any = [];
  isContactExists: boolean;
  accessValid: Actions;
  subscriptions: Subscription[] = [];
  users: any = [];
  user: any;
  currentUser: User;
  ownerId: string;
  showSearch = false;
  searched = [];
  search = new Search();
  collectionsForQuote = [];
  collectionInQuote: string;
  index: number;
  showBtn = 0;
  Disclaimer: string;
  clearBrowse = false;
  isInfinity = false;
  isProductCostEdit = false;
  public editorOptions = {
    placeholder: "insert content...",
    enable: false,
  };

  termsQuill: any;
  scopeQuill: any;
  allMethods = [];
  searchContact: any;
  presentContact = false;
  toBeEdit: string;
  editProperty: string;
  isCostValid: boolean;
  costReg: string;
  groupByProducts: any[];
  pmethod: any;
  blukPMargin: any;
  blukPMarkup: any;
  blukPDiscount: any;
  bulkUpload: {
    pricingMethod: number;
    margin: number;
    markup: number;
    discount: number;
  };
  companyId: string;
  isMsrpValueNull = false;
  editPropertyIndex: any;
  constructor(
    private _quotesService: QuotesService,
    private _UserService: UserService,
    private ngZone: NgZone,
    private route: ActivatedRoute,
    private toastrService: ToastrService,
    private productsService: ProductsService,
    private _route: Router,
    private dataService: DataService
  ) {
    this.companyId = JSON.parse(sessionStorage.getItem("currentUser"))[
      "subscriberId"
    ];
    this.user = JSON.parse(sessionStorage.getItem("currentUser"));
    this.accessValid = new Actions();
    this.contacts = new Array<Contact>();
    this.allContacts = new Array<Contact>();
    this.account = new Account();
    this.quotesInfo = new Quote();
    this.selectedList = new Array<DeleteList>();
    this.contact = new Contact();
    this.constants = Constants;
    this.ProductList = new Array<Product>();
    this.customItem = new CustomItem();
    this.Products = new Array<Product>();
    this.Taxes = new Array<Taxes>();
    this.company = new Company();
    this.isContactExists = true;
    this.currentUser = new User();
    this.index = 0;
    this.collectionsForQuote = ["Products", "Kit"];
    this.collectionInQuote = "Products";
    this.costReg = this.constants.Cost_REXEXP;
    _route.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        if (event.url !== "/quotes/quotes-detail") {
          let id = this.route.snapshot.queryParams["id"];
          id = atob(id);
          this.quoteId = "test"; //#Revisit_Pradeep
          if (this.quoteId && this.quoteId !== id) {
            this.currentUser = JSON.parse(
              sessionStorage.getItem("currentUser")
            );
            this.loading = true;
            if (!id) {
              this._route.navigate(["/quotes/quotes-list"]);
            }
            this.quoteId = id;
            this.getModuleAccess();
            this.getQuotes(this.quoteId);
            this.getDisclaimer();
            this.getPricing();
            this.getTaxes();
            this.getQuoteStatus();
            this.GetCompanys();
            // $('html, body').animate({ scrollTop: 0 }, 'slow');
          }
        }
      }
    });
    this.groupByProducts = new Array<any>();
    this.bulkUpload = {
      pricingMethod: null,
      margin: null,
      markup: null,
      discount: null,
    };
  }

  ngOnDestroy() {
    this.subscriptions.forEach((element) => {
      element.unsubscribe();
    });
    if (
      this.accessValid.update &&
      this.quotesInfo.status.description === this.constants.InProgress
    ) {
      this.UpdateQuote(this.quotesInfo);
    }
    if (document.getElementById("quotes") !== null) {
      document.getElementById("quotes").classList.remove("active");
    }
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $(window).scrollTop(0);
  }

  ngOnInit() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $(window).scrollTop(0);
    $(document).ready(() => {
      $(window).scroll(() => {
        if ($(window).scrollTop() > 50) {
          $("#back-to-top").fadeIn();
        } else {
          $("#back-to-top").fadeOut();
        }
      });
      // scroll body to 0px on click
      $("#back-to-top").click(function () {
        $("body,html").animate(
          {
            scrollTop: 0,
          },
          800
        );
        return false;
      });
    });
    this.currentUser = JSON.parse(sessionStorage.getItem("currentUser"));
    this.loading = true;
    const id = this.route.snapshot.queryParams["id"];
    if (!id) {
      this._route.navigate(["/quotes/quotes-list"]);
    }
    this.quoteId = atob(id);
    this.getModuleAccess();
    this.GetAllPricingMethods();
    this.getQuotes(this.quoteId);
    this.getDisclaimer();
    this.getPricing();
    this.getTaxes();
    this.getQuoteStatus();
    this.GetCompanys();

    $(".btn-expand button").click(function () {
      $(".expand-toggle").removeClass("expand-toggle3");
      $(".quote-status-box, .expand-toggle").slideToggle();
    });

    $(".btn-expand2 button").click(function () {
      $(".btn-expand3 button").show();
      $(".btn-expand2 button").hide();
      $(".expand-toggle").toggleClass("expand-toggle3").slideUp();
      $(".expand-toggle2").slideToggle();
    });

    $(".btn-expand3 button").click(function () {
      $(".btn-expand2 button").show();
      $(".btn-expand3 button").hide();
      $(".expand-toggle").toggleClass("expand-toggle3").slideDown();
      $(".expand-toggle2").slideToggle();
    });

    $(".btn-expand4 button").click(function () {
      $(".btn-expand4 button").show();
      $(".btn-expand3 button").hide();
      $(".expand-toggle").toggleClass("expand-toggle3").slideDown();
      $(".expand-toggle2").slideToggle();
    });

    $(".subtotal-box .btn-tax-dropdown").click(function () {
      $(".tax-type").toggle();
    });

    if (document.getElementById("quotes") !== null) {
      document.getElementById("quotes").classList.add("active");
    }

    $("#search-Product-Kit").keyup(function () {
      $(".search-dropdown-1").show();
      const txt = $("#search-Product-Kit");
      if (txt.val() != null && txt.val() !== "") {
        $(".search-dropdown-1").show();
      } else {
        $(".search-dropdown-1").hide();
      }
    });
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $(window).scrollTop(0);
  }

  getModuleAccess() {
    this.subscriptions.push(
      this._quotesService.getModuleAccess().subscribe((res) => {
        this.accessValid = res.quote;
      })
    );
  }

  GetAllPricingMethods() {
    this.subscriptions.push(
      this._quotesService.getAllPricingMethods().subscribe((res) => {
        this.allMethods = res;
      })
    );
  }

  onCancel() {
    this.isSend = false;
    this.isDelete = false;
    this.isCopy = false;
    this.isAccept = false;
    this.isReject = false;
    this.isreOpen = false;
    this.isProductDelete = false;
    this.isCustomProductDelete = false;
    this.isKitDelete = false;
  }

  discard() {
    this.getQuotes(this.quoteId);
    this.isquoteTitleEdit = false;
  }

  addTitle(valid) {
    if (valid) {
      this.UpdateQuote(this.quotesInfo);
      this.isquoteTitleEdit = false;
    }
  }

  ViewKit(i) {
    this.kitIndex = i;
    // this.kitDetails = JSON.parse(JSON.stringify(this.quotesInfo.kits[i]));
    this.kitDetails = Object.assign({}, this.quotesInfo.kits[i]);
  }

  SavelKit() {
    // this.quotesInfo.kits[this.kitIndex] = JSON.parse(
    //   JSON.stringify(this.kitDetails)
    // );
    this.quotesInfo.kits[this.kitIndex] = Object.assign({}, this.kitDetails);

    this.UpdateQuote(this.quotesInfo);
    this.viewKitDetailModal.hide();
  }

  hideKit() {

    this.kitDetails = new KitDetail();
    this.getQuotes(this.quoteId);
    this.kitDetails = Object.assign({}, this.quotesInfo.kits[this.kitIndex]);
    this.viewKitDetailModal.hide();
  }
  GetCompanys() {
    this.loading = true;
    this.subscriptions.push(
      this._UserService.GetCompany().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.company = res;
            this.loading = false;
            this.GetGenralUser();
          });
        },
        (err) => {
          this.loading = false;
        }
      )
    );
  }

  getTaxes() {
    this.subscriptions.push(
      this._quotesService.GetTax().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.Taxes = res;
          });
        },
        (err) => {
          // console.log(err);
        }
      )
    );
  }

  getDisclaimer() {
    this.subscriptions.push(
      this._quotesService.GetDisclaimer().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.Disclaimer = res;
          });
        },
        (err) => {
          // console.log(err);
        }
      )
    );
  }

  GetKitTotal() {
    if (this.kitIndex !== undefined) {
      let customTotal: any = 0;
      let productTotal: any = 0;
      this.kitDetails.kit.products.forEach((element) => {
        if (element.product) {
          productTotal = productTotal + +element.price * element.quantity;
        }
      });
      if (this.kitDetails != null) {
        if (this.kitDetails.kit.customItem != null) {
          this.kitDetails.kit.customItem.forEach((ele) => {
            customTotal = customTotal + +ele.price * ele.quantity;
          });
        }
      }
      this.kitDetails.total = customTotal + productTotal;
      this.kitDetails.total = this.kitDetails.total.toFixed(2);
      return this.kitDetails.total;
    }
  }

  GotoBrowse() {
    this.show = true;
    this.popupOpen((this.popupFor = "can not add Product/Kit"));
  }

  ChangeExpairy(e) {
    this.quotesInfo.expirationDate = new Date(this.quotesInfo.createdOn);
    // tslint:disable-next-line:radix
    this.quotesInfo.expirationDate.setDate(
      this.quotesInfo.expirationDate.getDate() + parseInt(e, 0)
    );
    this.UpdateQuote(this.quotesInfo);
  }

  getQuotes(id) {
    this.subscriptions.push(
      this._quotesService.getQuoteThroughtId(id).subscribe({
        next: (res) => {
          this.ngZone.run(() => {
            const groups = new Set(
              res.quote.products.map((item) =>
                item.product.productType.description !== null
                  ? item.product.productType.description
                  : ""
              )
            ),
              results = [];
            res.quote.products = res.quote.products.map((obj, i) => ({
              ...obj,
              parentIndex: i,
            }));
            groups.forEach((g) =>
              results.push({
                name: g,
                values: res.quote.products.filter(
                  (i) => i.product.productType.description === g
                ),
              })
            );
            this.groupByProducts = results;
            this.quotesInfo = Object.assign({}, res.quote);
            this.quotesInfo.discount = this.quotesInfo.discount.toFixed(2);
            if (this.quotesInfo.products) {
              this.checkMethodsInProduct(this.quotesInfo);
            }
            if (this.quotesInfo.kits) {
              this.checkMethodsInKit(this.quotesInfo);
            }
            this.ownerId = this.quotesInfo.ownerId;
            this.account = this.quotesInfo.contact.accounts;
            this.contact = this.quotesInfo.contact;
            this.getContacts(this.account.id);
            this.loading = false;
            const currentUser = JSON.parse(
              sessionStorage.getItem("currentUser")
            );
            if (currentUser.role.description !== "Admin") {
              if (this.quotesInfo.ownerId !== currentUser.id) {
                this._route.navigate(["/quotes/quotes-list"]);
                // this.toastrService.warning(
                //   'Sorry,You dont have permission to access this quote',
                //   '',
                //   this.constants.toastrConfig
                // );
                this.show = true;
                this.popupOpen((this.popupFor = "dont have permission"));
              }
            }
          });
        },
        error: (err) => {
          this.loading = false;
          // console.log(err);
        }
      }
      )
    );
    // $("html, body").animate({ scrollTop: 0 }, "slow");
    // $(window).scrollTop(0);
  }
  getQuoteStatus() {
    this.subscriptions.push(
      this._quotesService.getQuoteStatus().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.quoteStatusList = res;
          });
        },
        (err) => {
          if (err.status === 403) {
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      )
    );
  }

  DeleteQuote() {
    this.loading = true;
    this.quotesInfo.isActive = false;
    this._quotesService.DeleteQuote(this.quotesInfo).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            // this.quotesInfo.products.forEach(element => {
            //   element.product.isUsed = element.product.isUsed - 1;
            //   this.updateProduct(element.product);
            // });
            // this.toastrService.success(
            //   'Quote deleted successfully',
            //   '',
            //   this.constants.toastrConfig
            // );
            this._route.navigate(["/quotes/quotes-list"]);
          }
          this.loading = false;
        });
      },
      (err) => {
        this.loading = false;
        if (err.status === 403) {
          this.show = true;
          this.popupOpen((this.popupFor = "Access denied"));
        }
      }
    );
  }

  UpdateQuote(quotesInfo) {
    this._quotesService.UpdateQuote(quotesInfo).subscribe(
      (res) => {
        this.ngZone.run(() => {
          this.getQuotes(quotesInfo.id);
        });
      },
      (err) => {
        if (err.status === 403) {
          this.show = true;
          this.popupOpen((this.popupFor = "Access denied"));
        }
      }
    );
  }

  AddProduct(e) {
    this.SearchTextForQuote = null;
    $(".search-dropdown-1").hide();
    this.showBtn = 0;
    if (e.cost > 0) {
      const Prod: ProductDetail = new ProductDetail();
      Prod.product = e;
      Prod.quantity = 1;
      this.quotesInfo.products.push(Prod);
      // Prod.product.isUsed = Prod.product.isUsed + 1;
      this.checkMethodsInProduct(this.quotesInfo);
      // this.updateProduct(Prod.product);
      this.UpdateQuote(this.quotesInfo);
    } else {
      this.show = true;
      this.popupOpen((this.popupFor = "verify product cost"));
    }
  }

  AddKit(e) {
    this.SearchTextForQuote = null;
    $(".search-dropdown-1").hide();
    this.showBtn = 0;
    if (e.total > 0) {
      const kit: KitDetail = new KitDetail();
      kit.Kit = e;
      kit.quantity = 1;
      kit.total = e.total;
      if (this.quotesInfo.kits === null) {
        this.quotesInfo.kits = new Array<KitDetail>();
      }
      this.quotesInfo.kits.push(kit);
      this.checkMethodsInKit(this.quotesInfo);
      this._quotesService.UpdateQuote(this.quotesInfo).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.getQuotes(this.quoteId);
          });
        },
        (err) => {
          if (err.status === 403) {
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      );
    } else {
      this.show = true;
      this.popupOpen((this.popupFor = "verify kit"));
    }
  }

  onModelClose() {
    this.customItem = new CustomItem();
  }

  addCustomProduct(customItemForm: NgForm, model) {
    if (customItemForm.valid) {
      if (this.quotesInfo.customItem === null) {
        this.quotesInfo.customItem = new Array<CustomItem>();
      }
      this.quotesInfo.customItem.push(this.customItem);
      this.customItem = new CustomItem();
      this.UpdateQuote(this.quotesInfo);
      model.hide();
      customItemForm.resetForm();
    }
  }

  QuoteReOpen() {
    this.quotesInfo.status = this.quoteStatusList.filter(
      (x) => x.description === this.constants.InProgress
    )[0];
    this.quotesInfo.isActive = true;
    this.quotesInfo.createdOn = new Date();
    this.quotesInfo.expirationDate = new Date();
    this.quotesInfo.updatedOn = new Date();
    this.quotesInfo.expirationDate.setDate(
      this.quotesInfo.expirationDate.getDate() + this.constants.Expairy[0]
    );
    this.quotesInfo.expairyDays = this.constants.Expairy[0];
    this.UpdateQuote(this.quotesInfo);
  }

  QuoteAccept(isAccept) {
    this.loading = true;
    this.subscriptions.push(
      this._quotesService.getQuoteThroughtId(this.quotesInfo.id).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.quotesInfo = res.quote;
            if (
              this.quotesInfo.status.description === this.constants.Accepted
            ) {
              this.show = true;
              this.popupOpen((this.popupFor = "already accepted"));
              this.loading = false;
              return;
            } else if (
              this.quotesInfo.status.description === this.constants.Rejected
            ) {
              this.show = true;
              this.popupOpen((this.popupFor = "Quote already rejected"));
              this.loading = false;
              return;
            }
            if (isAccept) {
              this.quotesInfo.status = this.quoteStatusList.filter(
                (x) => x.description === this.constants.Accepted
              )[0];
            } else {
              this.quotesInfo.status = this.quoteStatusList.filter(
                (x) => x.description === this.constants.Rejected
              )[0];
            }
            this.UpdateQuote(this.quotesInfo);
            this.loading = false;
          });
        },
        (err) => {
          this.loading = false;
          // console.log(err);
        }
      )
    );
  }

  RemoveCustomItem(i) {
    this.quotesInfo.customItem.splice(i, 1);
    this.UpdateQuote(this.quotesInfo);
  }

  RemoveProduct(i) {
    const prod = this.quotesInfo.products[i].product;
    // prod.isUsed = prod.isUsed !== 0 ? prod.isUsed - 1 : prod.isUsed;
    // this.updateProduct(prod);
    this.quotesInfo.products.splice(i, 1);
    this.UpdateQuote(this.quotesInfo);
  }

  RemoveKit(i) {
    this.quotesInfo.kits.splice(i, 1);
    this.UpdateQuote(this.quotesInfo);
  }

  onMarginChange(p, pi, e) {
    // const pi = this.quotesInfo.products.findIndex(
    //   (product) => product.id === p.id
    // );
    if (pi !== -1) {
      this.quotesInfo.products[pi].margin = JSON.parse(
        JSON.stringify(this.allMethods.filter((x) => x.id === e)[0])
      );
      if (this.quotesInfo.products[pi].margin.method === "List") {
        this.quotesInfo.products[pi].margin.value =
          this.quotesInfo.products[pi].product.msrp;
      }
      this.quotesInfo.products[pi].price = this.CalculateCost(
        this.quotesInfo.products[pi]
      );
      // this.GetSubTotal();
      this.UpdateQuote(this.quotesInfo);
    }
  }

  GetSubTotal() {
    this.GetLabor();
    let customTotal: any = 0;
    let productTotal: any = 0;
    if (this.quotesInfo.products != null) {
      this.quotesInfo.products.forEach((element) => {
        if (element.product) {
          productTotal =
            productTotal + parseFloat(element.price) * element.quantity;
        }
      });
    }
    if (
      this.quotesInfo.customItem != null &&
      this.quotesInfo.customItem.length
    ) {
      this.quotesInfo.customItem.forEach((element) => {
        // tslint:disable-next-line:curly
        if (element.price)
          customTotal = customTotal + element.price * element.quantity;
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit) {
          productTotal =
            productTotal + Math.abs(element.total * element.quantity);
        }
      });
    }
    this.quotesInfo.subtotal =
      parseFloat(customTotal) + parseFloat(productTotal);
    this.quotesInfo.subtotal = this.quotesInfo.subtotal.toFixed(2);
    return this.quotesInfo.subtotal;
  }

  GetLabor() {
    let productTotal: any = 0;
    if (this.quotesInfo.products != null) {
      this.quotesInfo.products.forEach((element) => {
        if (
          element.product &&
          element.product.productType &&
          element.product.productType.description === "Labor"
        ) {
          productTotal = productTotal + element.price * element.quantity;
        }
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.products) {
          element.kit.products.forEach((ele) => {
            if (
              ele.product &&
              ele.product.productType &&
              ele.product.productType.description === "Labor"
            ) {
              productTotal =
                productTotal + ele.price * (element.quantity * ele.quantity);
            }
          });
        }
      });
    }
    this.quotesInfo.laborCharge = productTotal;
  }

  onTaxChange(tax) {
    this.quotesInfo.saleTax = tax;
    this.quotesInfo.taxAmount = this.GetTax(tax.value);
    this.UpdateQuote(this.quotesInfo);
    $(".tax-type").toggle();
  }

  GetTax(tax) {
    let productTotal: any = 0;
    if (this.quotesInfo.products != null) {
      this.quotesInfo.products.forEach((element) => {
        if (element.product && element.product.isTaxable) {
          productTotal = productTotal + element.price * element.quantity;
        }
      });
    }
    if (
      this.quotesInfo.customItem != null &&
      this.quotesInfo.customItem.length
    ) {
      this.quotesInfo.customItem.forEach((element) => {
        if (element && element.isTaxable) {
          productTotal = productTotal + element.price * element.quantity;
        }
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.products) {
          element.kit.products.forEach((ele) => {
            if (ele.product && ele.product.isTaxable) {
              productTotal =
                productTotal + ele.price * (element.quantity * ele.quantity);
            }
          });
          if (element.kit.customItem) {
            element.kit.customItem.forEach((ele) => {
              if (ele && ele.isTaxable) {
                productTotal =
                  productTotal + ele.price * (element.quantity * ele.quantity);
              }
            });
          }
        }
      });
    }
    let taxableAmount: any = 0;
    if (this.quotesInfo.discount > 0) {
      productTotal = productTotal - this.quotesInfo.discount;
    }
    if (productTotal > 0) {
      taxableAmount = (productTotal / 100) * tax;
    } else {
      taxableAmount = 0;
    }
    taxableAmount = taxableAmount.toFixed(2);
    this.quotesInfo.total =
      parseFloat(this.quotesInfo.subtotal) +
      parseFloat(taxableAmount) -
      parseFloat(this.quotesInfo.discount);
    this.quotesInfo.total = isNaN(this.quotesInfo.total)
      ? 0
      : this.quotesInfo.total;
    // tslint:disable-next-line:triple-equals
    if (this.quotesInfo.saleTax.value == tax) {
      this.quotesInfo.taxAmount = taxableAmount;
    }
    return taxableAmount;
  }

  GetProfitMargin() {
    let cost: any = 0;
    let customTotal: any = 0;
    let productTotal: any = 0;
    if (this.quotesInfo.products != null) {
      this.quotesInfo.products.forEach((element) => {
        if (element.product) {
          productTotal = productTotal + element.product.cost * element.quantity;
        }
      });
    }
    if (
      this.quotesInfo.customItem != null &&
      this.quotesInfo.customItem.length
    ) {
      this.quotesInfo.customItem.forEach((element) => {
        // tslint:disable-next-line:curly
        if (element.cost)
          customTotal = customTotal + element.cost * element.quantity;
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.products) {
          element.kit.products.forEach((ele) => {
            if (ele.product) {
              productTotal =
                productTotal +
                ele.product.cost * (element.quantity * ele.quantity);
            }
          });
        }
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.customItem != null) {
          element.kit.customItem.forEach((ele) => {
            customTotal =
              customTotal + ele.cost * (ele.quantity * element.quantity);
          });
        }
      });
    }
    cost = customTotal + productTotal;
    this.quotesInfo.profit =
      parseFloat(this.quotesInfo.subtotal) -
      parseFloat(cost) -
      parseFloat(this.quotesInfo.discount);
    this.quotesInfo.profit = isNaN(this.quotesInfo.profit)
      ? 0
      : this.quotesInfo.profit;
    this.quotesInfo.profit = this.quotesInfo.profit;
    this.quotesInfo.profitMargin =
      (this.quotesInfo.profit /
        (parseFloat(this.quotesInfo.subtotal) - this.quotesInfo.discount)) *
      100;
    this.quotesInfo.profitMargin = isNaN(this.quotesInfo.profitMargin)
      ? 0
      : this.quotesInfo.profitMargin;
    if (
      this.quotesInfo.profitMargin === -Infinity ||
      this.quotesInfo.profitMargin === Infinity
    ) {
      this.quotesInfo.profitMargin = 0;
    }
    if (
      parseFloat(this.quotesInfo.subtotal) - this.quotesInfo.discount <= 0 &&
      this.quotesInfo.discount > 0
    ) {
      this.isInfinity = true;
    } else {
      this.isInfinity = false;
    }
    this.quotesInfo.profitMargin =
      Math.round((this.quotesInfo.profitMargin + Number.EPSILON) * 100) / 100;
    return this.quotesInfo.profitMargin;
  }

  CopyQuote() {

    const quote = this.quotesInfo;
    quote.id = null;
    quote.createdOn = new Date();
    quote.expirationDate = quote.createdOn;
    quote.updatedOn = quote.createdOn;
    quote.expirationDate.setDate(
      quote.expirationDate.getDate() + this.constants.Expairy[0]
    );
    quote.expairyDays = this.constants.Expairy[0];
    quote.status = this.quoteStatusList.filter(
      (x) => x.description === this.constants.InProgress
    )[0];

    quote.createdBy = this.user.id;
    quote.ownerId = this.user.id;
    quote.ownerName = this.user.firstName + " " + this.user.lastName;
    quote.name = quote.name + "_Copy";
    this._quotesService.AddQuote(quote).subscribe((response) => {
      this.ngZone.run(() => {
        this.getQuotes(response.id);
        // if (quote.products != null) {
        //   quote.products.forEach(element => {
        //     element.product.isUsed = element.product.isUsed + 1;
        //     this.updateProduct(element.product);
        //   });
        // }
        // this._route.navigate(["/quotes/quotes-detail"], {
        //   queryParams: { id: btoa(response.id) },
        // });
      });
    });
  }

  sendQuotes() {
    if (this.quotesInfo.total < 0) {
      this.show = true;
      this.popupOpen((this.popupFor = "sendquote"));
      return;
    }

    const status = this.quoteStatusList.filter(
      (x) => x.description === this.constants.InProgress
    )[0];

    if (this.quotesInfo.status.description === status.description) {
      this.quotesInfo.sentDate = new Date();
      this._quotesService.UpdateQuote(this.quotesInfo).subscribe(
        (res) => {
          this.ngZone.run(() => {
            if (res) {
              this._route.navigate(["/quotes/quotes-send"], {
                queryParams: { id: btoa(this.quotesInfo.id) },
              });
            }
          });
        },
        (err) => {
          if (err.status === 403) {
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      );
    } else {
      this._route.navigate(["/quotes/quotes-send"], {
        queryParams: { id: btoa(this.quotesInfo.id) },
      });
    }
  }

  GetKitCost(i) {
    let customTotal: any = 0;
    let productTotal: any = 0;
    if (
      this.quotesInfo.kits &&
      (this.quotesInfo.kits[i].kit
        ? this.quotesInfo.kits[i].kit.products
        : this.quotesInfo.kits[i].kit)
    ) {
      this.quotesInfo.kits[i].kit
        ? this.quotesInfo.kits[i].kit.products.forEach((ele) => {
          if (ele.product) {
            // tslint:disable-next-line:max-line-length
            productTotal = productTotal + ele.product.cost * ele.quantity;
          }
        })
        : null;
    }
    if (
      this.quotesInfo.kits &&
      (this.quotesInfo.kits[i].kit
        ? this.quotesInfo.kits[i].kit.customItem
        : this.quotesInfo.kits[i].kit)
    ) {
      this.quotesInfo.kits[i].kit
        ? this.quotesInfo.kits[i].kit.customItem.forEach((ele) => {
          if (ele != null) {
            customTotal = customTotal + ele.cost * ele.quantity;
          }
        })
        : null;
    }
    return customTotal + productTotal;
  }

  updateProduct(product) {
    this.productsService.postElement("Products/productUsed", product).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            // // console.log('success');
          }
        });
      },
      (err) => { }
    );
  }

  updateProdQuantity(item) {
    // const val = parseInt(item.quantity, 0);
    // if (val > 0) {
    //   this.UpdateQuote(this.quotesInfo);
    // } else {
    //   item.quantity = 1;
    //   this.UpdateQuote(this.quotesInfo);
    // }
    item.quantity =
      item.product.productType.description === "Material"
        ? parseInt(item.quantity, 0) > 0
          ? parseInt(item.quantity, 0)
          : 1
        : item.quantity > 0
          ? item.quantity
          : 1;

    const arr = item.quantity.toString().split(".");
    item.quantity =
      arr.length >= 2
        ? +(arr[0] + "." + arr[1].substring(0, 2))
        : item.quantity;
    this.UpdateQuote(this.quotesInfo);
  }

  ValueCheck(item) {
    // const val = parseInt(item.quantity, 0);
    // if (val > 0) {
    // } else {
    //   item.quantity = 1;
    // }
    item.quantity =
      item.product.productType.description === "Material"
        ? parseInt(item.quantity, 0) > 0
          ? parseInt(item.quantity, 0)
          : 1
        : item.quantity > 0
          ? item.quantity
          : 1;

    const arr = item.quantity.toString().split(".");
    item.quantity =
      arr.length >= 2
        ? +(arr[0] + "." + arr[1].substring(0, 2))
        : item.quantity;
  }

  PriceCheck(item) {
    const val = parseInt(item.price, 0);
    if (val > 0) {
    } else {
      item.price = 1;
    }
  }

  getPricing() {
    this.subscriptions.push(
      this.productsService.getData("Configuration/GetPricingMethod").subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.masterMethodList = res;
          });
        },
        (err) => { }
      )
    );
  }

  updateItemQuantity(item) {
    item.quantity =
      item.product.productType.description === "Material"
        ? parseInt(item.quantity, 0) > 0
          ? parseInt(item.quantity, 0)
          : 1
        : item.quantity > 0
          ? item.quantity
          : 1;
    this.UpdateQuote(this.quotesInfo);

    // const val = parseInt(item.quantity, 0);
    // if (val > 0) {
    //   this.UpdateQuote(this.quotesInfo);
    // } else {
    //   item.quantity = 1;
    //   this.UpdateQuote(this.quotesInfo);
    // }
  }

  updateItemPrice(item) {
    const val = parseFloat(item.price);
    if (val > 0) {
      this.UpdateQuote(this.quotesInfo);
    } else {
      item.price = 1;
      this.UpdateQuote(this.quotesInfo);
    }
  }

  updateItemCost(item) {
    const val = parseFloat(item.cost);
    if (val > 0) {
    } else {
      item.cost = null;
    }
  }

  updateCustomItemPrice(item) {
    const val = parseFloat(item.price);
    if (val > 0) {
    } else {
      item.price = null;
    }
  }

  updateCustomItemQuantity(item) {
    const val = parseInt(item.quantity, 0);
    if (val > 0) {
      item.quantity = val;
      this.UpdateQuote(this.quotesInfo);
    } else {
      item.quantity = null;
    }
  }

  updateKitQuantity(item) {
    const val = parseInt(item.quantity, 0);
    item.quantity = val > 0 ? val : 0;
    this.UpdateQuote(this.quotesInfo);

    // const val = parseInt(item.quantity, 0);
    // if (val > 0) {
    //   this.UpdateQuote(this.quotesInfo);
    // } else {
    //   item.quantity = 1;
    //   this.UpdateQuote(this.quotesInfo);
    // }
  }

  getContacts(id) {
    this.subscriptions.push(
      this._quotesService.GetContactWithAccount().subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.allContacts = res;
            this.contacts = res.filter((_) => _.accounts.id === id);
            const data = this.contacts.filter(
              (_) => _.id === this.quotesInfo.contact.id
            )[0];
            if (!data) {
              this.isContactExists = false;
            }
          });
        },
        (err) => {
          // console.log(err);
        }
      )
    );
  }

  AddContact(addContactModal, from) {
    if (from.valid && this.contactid) {
      this.loading = true;
      const data = this.allContacts.filter((_) => _.id === this.contactid)[0];
      this.contact = data;
      this.account = data.accounts;
      this.quotesInfo.contact = data;
      this._quotesService.UpdateQuote(this.quotesInfo).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.isContactExists = true;
            addContactModal.hide();
            this.loading = false;
          });
        },
        (err) => {
          if (err.status === 403) {
            this.loading = false;
            this.show = true;
            this.popupOpen((this.popupFor = "Access denied"));
          }
        }
      );
    }
  }

  GetGenralUser() {
    this.subscriptions.push(
      this._quotesService.GetGenralUser(this.company.id).subscribe(
        (res) => {
          this.ngZone.run(() => {
            this.users = res.filter((_) => _.isActive);
            if (this.quotesInfo.id) {
              const user = res.filter(
                (_) => _.id === this.quotesInfo.ownerId
              )[0];
              this.quotesInfo.ownerName = user.firstName + " " + user.lastName;
            }
          });
        },
        (err) => { }
      )
    );
  }

  updateOwner(id) {
    const user = this.users.filter((x) => x.id === id)[0];
    this.quotesInfo.ownerId = id;
    this.quotesInfo.ownerName = user.firstName + " " + user.lastName;
    this.UpdateQuote(this.quotesInfo);
  }

  CalculateCost(element) {
    if (element.margin && element.margin.id) {
      if (element.margin.method === this.constants.PricingMethods.List) {
        element.price = element.product.msrp;
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;

        return addZeroes(element.price);
      } else if (
        element.margin.method === this.constants.PricingMethods.Markup
      ) {
        element.price = element.product.cost * (1 + element.margin.value / 100);
        // element.price = element.price.toFixed(2) ;
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return addZeroes(element.price);
      } else if (
        element.margin.method === this.constants.PricingMethods.Margin
      ) {
        element.price = element.product.cost / (1 - element.margin.value / 100);
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return addZeroes(element.price);
      } else if (
        element.margin.method === this.constants.PricingMethods.Discount
      ) {
        element.price =
          element.product.msrp -
          (element.product.msrp * element.margin.value) / 100;
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return addZeroes(element.price);
      }
    } else if (
      (element.margin.value !== "00" || element.margin.value !== null) &&
      (element.margin.method !== "0" || element.margin.method !== null)
    ) {
      if (element.margin.method === this.constants.PricingMethods.List) {
        element.price = element.product.msrp;
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return addZeroes(element.price);
      } else if (
        element.margin.method === this.constants.PricingMethods.Markup
      ) {
        element.price = element.product.cost * (1 + element.margin.value / 100);
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return addZeroes(element.price);
      } else if (
        element.margin.method === this.constants.PricingMethods.Margin
      ) {
        element.price = element.product.cost / (1 - element.margin.value / 100);
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return addZeroes(element.price);
      } else if (
        element.margin.method === this.constants.PricingMethods.Discount
      ) {
        element.price =
          element.product.msrp -
          (element.product.msrp * element.margin.value) / 100;
        // element.price = element.price.toFixed(2);
        element.price =
          Math.round((element.price + Number.EPSILON) * 100) / 100;
        return addZeroes(element.price);
      }
    }
    // element.price = +element.product.cost;
    // element.price = element.price.toFixed(2);
    element.price = Math.round((element.price + Number.EPSILON) * 100) / 100;
    return addZeroes(element.price);
  }

  goToContactDetails(id) {
    if (this.contact.isActive === true) {
      this._route.navigate(["/contacts/contact-detail"], {
        queryParams: { id: btoa(id) },
      });
    } else {
      this.show = true;
      this.popupOpen((this.popupFor = "contact is not available"));
    }
  }

  goToAccountDetails(id) {
    this._route.navigate(["/accounts/accounts-detail"], {
      queryParams: { id: btoa(id) },
    });
  }

  onSearchForQuote(body) {
    if (body.length) {
      this.searched = [];
      this.showSearch = true;
      this.search.productName = body;
      this.search.manufacturerPartNumber = body;
      this.search.kitName = body;
      this.dataService
        .getElementThroughAnyParam(this.collectionInQuote, this.search)
        .subscribe((res) => {
          let data = [];
          data = res;
          this.searched = [];
          if (data.length) {
            this.searched.push({
              collectionName: this.collectionInQuote,
              collectionData: data,
            });
          }
        });
    }
  }

  // Tab Selection for Search - Tejaswini
  showUndoBtnInQuote(index) {
    this.showBtn = index;
  }

  onMarginChangeKit(i, e) {
    this.kitDetails.kit.products[i].margin = JSON.parse(
      JSON.stringify(this.allMethods.filter((x) => x.id === e)[0])
    );
    if (this.kitDetails.kit.products[i].margin.method === "List") {
      this.kitDetails.kit.products[i].margin.value =
        this.kitDetails.kit.products[i].product.msrp;
    }
    this.kitDetails.kit.products[i].price = this.CalculateCost(
      this.kitDetails.kit.products[i]
    );
  }

  // method to check if method property absent, if so then update method
  checkMethodsInProduct(detail) {
    const collectionInQuote = JSON.parse(JSON.stringify(detail));
    let change = false;
    if (collectionInQuote.products) {
      collectionInQuote.products.forEach((prod) => {
        if (prod.product.pricingMethod) {
          prod.product.pricingMethod.forEach((pricingMethods) => {
            if (
              !pricingMethods["method"] ||
              pricingMethods["method"] === null
            ) {
              this.masterMethodList.forEach((masterMethod) => {
                if (masterMethod.name === pricingMethods.name) {
                  pricingMethods.method = pricingMethods.name;
                  pricingMethods.name =
                    pricingMethods.value + pricingMethods.name;
                  change = true;
                }
              });
            } else if (pricingMethods["method"] !== null) {
              this.ownerId = collectionInQuote.ownerId;
              this.account = collectionInQuote.contact.accounts;
              this.contact = collectionInQuote.contact;
              this.getContacts(this.account.id);
            }
          });
          if (prod.margin) {
            if (!prod.margin["method"] || prod.margin["method"] === null) {
              prod.margin.method = prod.margin.name;
              prod.margin.name = prod.margin.value + prod.margin.name;
              change = true;
            }
          }
        }
      });
    }
    if (change) {
      this.quotesInfo = JSON.parse(JSON.stringify(collectionInQuote));
      this.UpdateQuote(this.quotesInfo);
    }
  }

  // method to check if method property absent, if so then update method
  checkMethodsInKit(detail) {
    let change = false;
    const collectionInQuote = JSON.parse(JSON.stringify(detail));
    if (collectionInQuote.kits) {
      collectionInQuote.kits.forEach((k) => {
        if (k.kit && k.kit.products) {
          k.kit.products.forEach((prod) => {
            if (prod.product.pricingMethod) {
              prod.product.pricingMethod.forEach((pricingMethods) => {
                if (
                  !pricingMethods["method"] ||
                  pricingMethods["method"] === null
                ) {
                  this.masterMethodList.forEach((masterMethod) => {
                    if (masterMethod.name === pricingMethods.name) {
                      pricingMethods.method = pricingMethods.name;
                      pricingMethods.name =
                        pricingMethods.value + pricingMethods.name;
                      change = true;
                    }
                  });
                } else if (pricingMethods["method"] !== null) {
                  this.ownerId = collectionInQuote.ownerId;
                  this.account = collectionInQuote.contact.accounts;
                  this.contact = collectionInQuote.contact;
                  this.getContacts(this.account.id);
                }
              });
              if (prod.margin) {
                if (!prod.margin["method"] || prod.margin["method"] === null) {
                  prod.margin.method = prod.margin.name;
                  prod.margin.name = prod.margin.value + prod.margin.name;
                  change = true;
                }
              }
            }
          });
        }
      });
    }
    if (change) {
      this.quotesInfo = JSON.parse(JSON.stringify(collectionInQuote));
      this.UpdateQuote(this.quotesInfo);
    }
  }

  GetCost() {
    let customTotal: any = 0;
    let productTotal: any = 0;
    if (this.quotesInfo.products != null && this.quotesInfo.products.length) {
      this.quotesInfo.products.forEach((element) => {
        if (element.product) {
          productTotal = productTotal + element.product.cost * element.quantity;
        }
      });
    }
    if (
      this.quotesInfo.customItem != null &&
      this.quotesInfo.customItem.length
    ) {
      this.quotesInfo.customItem.forEach((element) => {
        // tslint:disable-next-line:curly
        if (element.cost)
          customTotal = customTotal + element.cost * element.quantity;
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.products) {
          element.kit.products.forEach((ele) => {
            if (ele.product) {
              productTotal =
                productTotal +
                ele.product.cost * (element.quantity * ele.quantity);
            }
          });
        }
      });
    }
    if (this.quotesInfo.kits != null) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.customItem != null) {
          element.kit.customItem.forEach((ele) => {
            customTotal =
              customTotal + ele.cost * (ele.quantity * element.quantity);
          });
        }
      });
    }
    let cost = customTotal + productTotal;
    // cost = cost.toFixed(2);
    cost = Math.round((cost + Number.EPSILON) * 100) / 100;
    return addZeroes(cost);
  }

  onTermsEditorCreated(quill) {
    this.termsQuill = quill;
    this.termsQuill.disable();
  }
  onScopeEditorCreated(quill) {
    this.scopeQuill = quill;
    this.scopeQuill.disable();
  }

  DiscountCheck(discount) {
    discount = parseFloat(discount).toFixed(2);
    if (isNaN(discount)) {
      this.quotesInfo.discount = "0.00";
    } else {
      this.quotesInfo.discount = discount;
    }
    this.UpdateQuote(this.quotesInfo);
  }

  clearBrowseData() {
    this.clearBrowse = false;
    this.SearchTextForQuote = null;
    this.showSearch = false;
  }

  // messages display
  popupOpen(popupFor) {

    if (this.popupFor === "copyquote") {
      this.typeofMessage = "Hello";
      this.messageToDisplay = "Would you like to copy the quote: ";
      this.noteToDisplay = null;
      this.dataToDisplay = this.quotesInfo.name + " ?";
      this.showButtons = true;
      this.notification = null;
    } else if (this.popupFor === "deletequote") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this quote?";
    } else if (this.popupFor === "deleteproduct") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this product?";
    } else if (this.popupFor === "deleteCustompPoduct") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this customproduct?";
    } else if (this.popupFor === "bulkDelete") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification =
        "Are you sure you want to delete the selected items from this quote?";
    } else if (this.popupFor === "deletekit") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Are you sure you want to delete this kit?";
    } else if (this.popupFor === "sendquote") {
      if (this.quotesInfo.total < 0) {
        this.typeofMessage = "Error";
        this.messageToDisplay = null;
        this.noteToDisplay = null;
        this.dataToDisplay = null;
        this.showButtons = false;
        this.notification = "Quote total can not be less then zero!";
      } else {
        this.typeofMessage = "Hello";
        this.messageToDisplay = "Are you ready to send the quote:";
        this.noteToDisplay = null;
        this.dataToDisplay = this.quotesInfo.name + "?";
        this.showButtons = true;
        this.notification = null;
        this.quotesInfo.products.forEach((element) => {
          // if (element.margin === null || element.margin.id === null) {
          //   this.messageToDisplay =
          //     'Not all products have a pricing method selected, are you sure you want to send quote';
          // }
          if (element.price <= element.product.cost) {
            this.messageToDisplay =
              "There are products on your quote that have a price equal to or less than the cost, are you sure you want to send quote ...?";
          }
        });
        this.quotesInfo.kits.forEach((element) => {
          element.kit.products.forEach((ele) => {
            // if (ele.margin === null || ele.margin.id === null) {
            //   this.messageToDisplay =
            //     'Not all products have a pricing method selected, are you sure you want to proceed';
            // }
            if (ele.price <= ele.product.cost) {
              this.messageToDisplay =
                "There are products on your quote that have a price equal to or less than the cost, are you sure you want to send quote ...?";
            }
          });
        });
      }
    } else if (this.popupFor === "reject") {
      this.typeofMessage = "Error";
      this.messageToDisplay = "Are you sure you want to reject this quote: ";
      this.noteToDisplay = null;
      this.dataToDisplay = this.quotesInfo.name + " ?";
      this.showButtons = true;
      this.notification = null;
    } else if (this.popupFor === "reOpen") {
      this.typeofMessage = "Hello";
      this.messageToDisplay = `Are you sure you want to re-open the quote:`;
      this.noteToDisplay = null;
      this.dataToDisplay = this.quotesInfo.name + " ?";
      this.showButtons = true;
      this.notification = null;
    } else if (this.popupFor === "accept") {
      this.typeofMessage = "Hello";
      this.messageToDisplay = "Are you sure you want to accept the quote: ";
      this.noteToDisplay = null;
      this.dataToDisplay = this.quotesInfo.name + " ?";
      this.showButtons = true;
      this.notification = null;
    } else if (this.popupFor === "Access denied") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = true;
      this.notification = "Access denied";
    } else if (this.popupFor === "can not add Product/Kit") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Sorry,You can not add Product/Kit!";
      // this.notification = null;
      // this.typeofMessage = "Error";
      // this.messageToDisplay = null;
      // this.noteToDisplay = null;
      // this.dataToDisplay = null;
      // this.showButtons = false;
      // this.notification = "Sorry,You can not add Product/Kit!";
    } else if (this.popupFor === "dont have permission") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification =
        "   Sorry,You dont have permission to access this quote";
    } else if (this.popupFor === "verify product cost") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification =
        "The product you are attempting to add has no cost, please update the cost and try again.";
      // this.notification = "Please verify product cost and try again";
    } else if (this.popupFor === "contact is not available") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "This contact is not available!";
    } else if (this.popupFor === "verify kit") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = " Please verify kit and try again!";
    } else if (this.popupFor === "can not be zero") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Quote total can not be zero!";
    } else if (this.popupFor === "already accepted") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Quote already accepted!";
    } else if (this.popupFor === "already rejected") {
      this.typeofMessage = "Error";
      this.messageToDisplay = null;
      this.noteToDisplay = null;
      this.dataToDisplay = null;
      this.showButtons = false;
      this.notification = "Quote already rejected!";
    }
  }
  deleteProductList(i, type, id) {
    const deletes = new DeleteList();
    deletes.index = i;
    deletes.id = id;
    deletes.type = type;
    if (
      this.selectedList.findIndex(
        (a) => a.id === id && a.type === type && a.index === i
      ) !== -1
    ) {
      this.selectedList.splice(
        this.selectedList.findIndex(
          (a) => a.id === id && a.type === type && a.index === i
        ),
        1
      );
    } else {
      this.selectedList.push(deletes);
    }
  }

  checkIfSelected(id, type, i) {
    if (
      this.selectedList.findIndex(
        (a) => a.id === id && a.type === type && a.index === i
      ) !== -1
    ) {
      return true;
    } else {
      return false;
    }
  }

  bulkDelete() {
    // for (let i = 0; i < this.selectedList.length; i++) {
    //   if (this.selectedList[i].type === 'product') {

    //   } else {
    //     if (this.selectedList[i].type === 'customItem') {
    //       this.quotesInfo.customItem.splice(i, 1);
    //     } else {
    //       if (this.selectedList[i].type === 'kits') {
    //         this.quotesInfo.kits.splice(i, 1);
    //       }
    //     }
    //   }
    // }
    if (this.selectedList.length !== 0) {
      this.selectedList.forEach((element) => {
        if (element.type === "product") {
          const pindex = this.quotesInfo.products.findIndex(
            (a, i) => a.id === element.id // && a["parentIndex"] === i
          );
          if (pindex !== -1) {
            this.quotesInfo.products.splice(pindex, 1);
            // const prod = this.quotesInfo.products[pindex].product;
            // prod.isUsed = prod.isUsed !== 0 ? prod.isUsed - 1 : prod.isUsed;
            // this.updateProduct(prod);
          }
        } else if (element.type === "customItem") {
          const pindex = this.quotesInfo.customItem.findIndex(
            (a, i) => a.id === element.id
          );
          if (pindex !== -1) {
            this.quotesInfo.customItem.splice(pindex, 1);
          }
        } else if (element.type === "kits") {
          const pindex = this.quotesInfo.kits.findIndex(
            (a, i) => a.id === element.id
          );
          if (pindex !== -1) {
            this.quotesInfo.kits.splice(pindex, 1);
          }
        }
      });
      this.UpdateQuote(this.quotesInfo);
      this.selectedList = new Array<DeleteList>();
    } else {
    }
  }

  yes(ConfirmModal) {

    if (this.popupFor === "copyquote") {
      this.CopyQuote();
      ConfirmModal.hide();
    } else if (this.popupFor === "deletequote") {
      this.DeleteQuote();
      ConfirmModal.hide();
    } else if (this.popupFor === "bulkDelete") {
      this.bulkDelete();
      ConfirmModal.hide();
    } else if (this.popupFor === "deleteproduct") {
      this.RemoveProduct(this.productItem);
      ConfirmModal.hide();
    } else if (this.popupFor === "deleteCustompPoduct") {
      this.RemoveCustomItem(this.CustomItemDelete);
      ConfirmModal.hide();
    } else if (this.popupFor === "deletekit") {
      this.RemoveKit(this.kitItem);
      ConfirmModal.hide();
    } else if (this.popupFor === "sendquote") {
      this.sendQuotes();
      ConfirmModal.hide();
    } else if (this.popupFor === "reject") {
      this.QuoteAccept(false);
      ConfirmModal.hide();
    } else if (this.popupFor === "accept") {
      this.QuoteAccept(true);
      ConfirmModal.hide();
    } else if (this.popupFor === "reOpen") {
      this.QuoteReOpen();
      ConfirmModal.hide();
    } else if (this.popupFor === "Cannot Add Item") {
      this.show = false;
    } else if (this.popupFor === "Access denied") {
      this.show = false;
    } else if (this.popupFor === "went wrong") {
      this.show = false;
    } else if (this.popupFor === "can not add Product/Kit") {
      this.show = false;
    } else if (this.popupFor === "dont have permission") {
      this.show = false;
    } else if (this.popupFor === "verify product cost") {
      this.show = false;
    } else if (this.popupFor === "contact is not available") {
      this.show = false;
    } else if (this.popupFor === "verify kit") {
      this.show = false;
    } else if (this.popupFor === "can not be zero") {
      this.show = false;
    } else if (this.popupFor === "already accepted") {
      this.show = false;
    } else if (this.popupFor === "already rejected") {
      this.show = false;
    } else if (this.popupFor === "can not add Product/Kit") {
      ConfirmModal.hide();
      this.show = false;
    }
  }

  no(ConfirmModal) {

    ConfirmModal.hide();
    this.popupFor = "none";
    this.show = false;
  }

  hide(ConfirmModal) {
    ConfirmModal.hide();
    this.typeofMessage = null;
    this.messageToDisplay = null;
    this.noteToDisplay = null;
    this.dataToDisplay = null;
    this.showButtons = false;
    this.show = false;
  }

  exportToCSV() {

    const list = new Array<any>();
    if (this.quotesInfo.products && this.quotesInfo.products.length) {
      this.quotesInfo.products.forEach((element) => {
        const obj: any = {};
        obj.col1 = "-";
        obj.col2 = "-";
        obj.col3 = element.product.partNo;
        obj.col4 = element.product.name;
        obj.col5 = "$" + element.product.cost.toFixed(2);
        obj.col6 = element.quantity;
        obj.col7 = "$" + element.price;
        obj.col8 =
          "$" +
          (parseFloat(element.price) * parseFloat(element.quantity)).toFixed(2);
        if (element.margin.name) {
          obj.col9 =
            element.margin.name +
            " (" +
            element.margin.method +
            " - " +
            element.margin.value +
            ")";
        } else {
          obj.col9 = "-";
        }
        obj.col10 =
          element.product.suppliers != null &&
            element.product.suppliers.length > 0
            ? element.product.suppliers[0].description
            : "";
        obj.col11 = element.product.manufacturerPartNumber
          ? element.product.manufacturerPartNumber
          : "";
        obj.col12 = element.product.manufacturer
          ? element.product.manufacturer
          : "";
        obj.col13 = element.product.unitOfMeasure
          ? element.product.unitOfMeasure
          : "";
        list.push(obj);
      });
    }
    if (this.quotesInfo.customItem && this.quotesInfo.customItem.length) {
      this.quotesInfo.customItem.forEach((element) => {
        const obj: any = {};
        obj.col1 = "-";
        obj.col2 = "-";
        obj.col3 = "-";
        obj.col4 = element.name;
        obj.col5 = "$" + element.cost.toFixed(2);
        obj.col6 = element.quantity;
        obj.col7 = "$" + element.price.toFixed(2);
        obj.col8 =
          "$" +
          (parseFloat(element.price) * parseFloat(element.quantity)).toFixed(2);
        obj.col9 = "-";
        list.push(obj);
      });
    }
    if (this.quotesInfo.kits && this.quotesInfo.kits.length) {
      this.quotesInfo.kits.forEach((element) => {
        if (element.kit && element.kit.products) {
          element.kit.products.forEach((ele) => {
            if (ele.product) {
              const obj: any = {};
              obj.col1 = element.kit.uniqueId;
              obj.col2 = element.kit.name;
              obj.col3 = ele.product.partNo;
              obj.col4 = ele.product.name;
              obj.col5 = "$" + ele.product.cost.toFixed(2);
              obj.col6 =
                parseFloat(ele.quantity) * parseFloat(element.quantity);
              obj.col7 = "$" + ele.price.toFixed(2);
              obj.col8 =
                "$" +
                (
                  parseFloat(ele.price) *
                  parseFloat(ele.quantity) *
                  parseFloat(element.quantity)
                ).toFixed(2);
              if (ele.margin.name) {
                obj.col9 =
                  ele.margin.name +
                  " (" +
                  ele.margin.method +
                  " - " +
                  ele.margin.value +
                  ")";
              } else {
                obj.col9 = "-";
              }
              obj.col10 =
                ele.product.suppliers != null &&
                  ele.product.suppliers.length > 0
                  ? ele.product.suppliers[0].description
                  : "";
              obj.col11 = ele.product.manufacturerPartNumber
                ? ele.product.manufacturerPartNumber
                : "";
              obj.col12 = ele.product.manufacturer
                ? ele.product.manufacturer
                : "";
              obj.col13 = ele.product.unitOfMeasure
                ? ele.product.unitOfMeasure
                : "";
              list.push(obj);
            }
          });
          if (element.kit && element.kit.customItem != null) {
            element.kit.customItem.forEach((ele) => {
              const obj: any = {};
              obj.col1 = element.kit.uniqueId;
              obj.col2 = element.kit.name;
              obj.col3 = "-";
              obj.col4 = ele.name;
              obj.col5 = "$" + ele.cost.toFixed(2);
              obj.col6 =
                parseFloat(ele.quantity) * parseFloat(element.quantity);
              obj.col7 = "$" + ele.price.toFixed(2);
              obj.col8 =
                "$" +
                (
                  parseFloat(ele.price) *
                  parseFloat(ele.quantity) *
                  parseFloat(element.quantity)
                ).toFixed(2);
              obj.col9 = "-";
              list.push(obj);
            });
          }
        }
      });
    }
    const obj: any = {};
    obj.col1 = "";
    obj.col2 = "";
    obj.col3 = "";
    obj.col4 = "";
    obj.col5 = "";
    obj.col6 = "";
    obj.col7 = "";
    obj.col8 = "Subtotal:";
    obj.col9 = "$" + this.quotesInfo.subtotal;
    obj.col10 = "";
    obj.col11 = "";
    obj.col12 = "";
    list.push(obj);
    let obj1: any = {};
    obj1 = JSON.parse(JSON.stringify(obj));
    obj1.col8 = "Discount:";
    obj1.col9 = this.quotesInfo.discount;
    list.push(obj1);
    let obj2: any = {};
    obj2 = JSON.parse(JSON.stringify(obj));
    obj2.col8 =
      "Tax (" +
      this.quotesInfo.saleTax.name +
      " " +
      this.quotesInfo.saleTax.value +
      "%):";
    obj2.col9 = this.quotesInfo.taxAmount;
    list.push(obj2);
    let obj3: any = {};
    obj3 = JSON.parse(JSON.stringify(obj));
    obj3.col8 = "Total:";
    obj3.col9 = "$" + this.quotesInfo.total.toFixed(2);
    list.push(obj3);
    const options = {
      fieldSeparator: ",",
      quoteStrings: '"',
      decimalseparator: ".",
      showLabels: true,
      showTitle: true,
      title: [
        "Quote ID:",
        this.quotesInfo.uniqueId,
        "Quote Title:",
        this.quotesInfo.name,
      ],
      headers: [
        "Kit No.",
        "Kit Title",
        "Part No.",
        "Product Title",
        "Cost",
        "Quantity",
        "Price",
        "Total",
        "Pricing Method",
        "Supplier",
        "Manufacturer Part Number",
        "Manufacturer",
        "Unit Of Measure",
      ],
    };

    // tslint:disable-next-line:no-unused-expression
    new ngxCsv(list, this.quotesInfo.uniqueId, options);
  }

  ContactChange(pop, form) {
    if (form.valid) {
      const data = this.allContacts.filter(
        (_) => _.firstName === this.searchContact
      )[0];
      if (data && data.id) {
        this.loading = true;
        this.contact = data;
        this.account = data.accounts;
        this.quotesInfo.contact = data;
        this._quotesService.UpdateQuote(this.quotesInfo).subscribe(
          (res) => {
            this.ngZone.run(() => {
              this.searchContact = null;
              this.presentContact = false;
              pop.hide();
              this.loading = false;
            });
          },
          (err) => {
            if (err.status === 403) {
              this.loading = false;
              this.show = true;
              this.popupOpen((this.popupFor = "Access denied"));
            }
          }
        );
      } else {
        this.presentContact = true;
      }
    }
  }

  setFocus() {
    // document.getElementById("SearchContact").focus();
  }

  ExportToPDF() {
    this.loading = true;
    this._quotesService.ExportToPDFOnly(this.quotesInfo).subscribe(
      (res) => {
        this.ngZone.run(() => {
          if (res) {
            this.loading = false;
            this.downloadFile(res, "application/pdf", this.quotesInfo.name);
          }
        });
      },
      (err) => {
        this.loading = false;
      }
    );
  }

  downloadFile(data: string, fileType: any, fileName) {
    // tslint:disable-next-line:prefer-const
    let byteCharacters = atob(data);
    const byteNumbers = new Array(byteCharacters.length);
    for (let i = 0; i < byteCharacters.length; i++) {
      byteNumbers[i] = byteCharacters.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    const blob = new Blob([byteArray], { type: fileType });
    const url = window.URL.createObjectURL(blob);
    const downloadPDF = document.createElement("a");
    downloadPDF.href = url;
    downloadPDF.download = fileName + ".pdf";
    downloadPDF.click();
    this.loading = false;
    this._route.navigate(["/quotes/quotes-detail"], {
      queryParams: { id: btoa(this.quoteId) },
    });
  }

  validateLaborQuantity(evt) {
    evt.target.value.split(".").length >= 2
      ? // tslint:disable-next-line:no-unused-expression
      (evt.target.value =
        evt.target.value.split(".")[0] +
        "." +
        evt.target.value.split(".")[1].substring(0, 2))
      : null;
  }

  editItem(item, edit, index) {
    this.editProperty = item.id;
    this.editPropertyIndex = index;
    this.toBeEdit = edit;
    // this.quotesInfo.products = JSON.parse(JSON.stringify(this.quotesInfo.products));
  }

  // Saving Cost - Vaibhav
  saveItemCost(item, form: NgForm, ind) {
    if (form.valid && (item !== null || !item)) {
      this.CheckCost(item.product.cost);
      if (!this.isCostValid) {
        this.isCostValid = false;
        this.toBeEdit = "none";
        this.editPropertyIndex = null;
        item.price = this.CalculateCost(item);
        this.quotesInfo.products[ind] = item;
        this.UpdateQuote(this.quotesInfo);
      } else {
        this.isCostValid = true;
        this.toBeEdit = "cost";
      }
    }
  }
  checkCustomItem() {
    if (this.selectedList.filter((a) => a.type === "customItem").length <= 0) {
      return false;
    } else {
      return true;
    }
  }
  saveItemPrice(item, form: NgForm, ind) {

    if (form.valid && (item !== null || !item)) {
      this.CheckCost(item.price);
      if (!this.isCostValid) {
        this.isCostValid = false;
        this.toBeEdit = "none";
        const index = this.quotesInfo.products.findIndex(
          (x) => x.id === item.id
        );
        this.quotesInfo.products[index] = item;
        this.quotesInfo.products[index].margin = new PricingMethod();
        this.UpdateQuote(this.quotesInfo);
      } else {
        this.isCostValid = true;
        this.toBeEdit = "price";
      }
    }
  }

  saveKitItemPrice(item, form: NgForm, ind) {

    if (form.valid && (item !== null || !item)) {
      this.CheckCost(item.price);
      if (!this.isCostValid) {
        this.isCostValid = false;
        this.toBeEdit = "none";

        const kpIndex = this.kitDetails.kit.products.findIndex(
          (x) => x.product.id === item.product.id
        );

        if (kpIndex !== -1) {
          let price = Math.round((+item.price + Number.EPSILON) * 100) / 100;
          this.kitDetails.kit.products[kpIndex].price = +addZeroes(price);
          this.kitDetails.kit.products[kpIndex].margin = new PricingMethod();
        }

        // this.quotesInfo.kits.forEach((element) => {
        //   element.kit.products.forEach((ele) => {
        //     if (ele.product.id === item.product.id) {
        //       let price =
        //         Math.round((+item.price + Number.EPSILON) * 100) / 100;
        //       ele.price = addZeroes(price);
        //       ele.margin = new PricingMethod();
        //     }
        //   });
        // });
        // this.UpdateQuote(this.quotesInfo);
      } else {
        this.isCostValid = true;
        this.toBeEdit = "price";
      }
    }
  }

  saveKitItemCost(item, form: NgForm, ind) {
    if (form.valid && (item !== null || !item)) {
      this.CheckCost(item.product.cost);
      if (!this.isCostValid) {
        this.isCostValid = false;
        this.toBeEdit = "none";
        const kpIndex = this.kitDetails.kit.products.findIndex(
          (x) => x.product.id === item.product.id
        );

        if (kpIndex !== -1) {
          this.kitDetails.kit.products[kpIndex].product.cost =
            +item.product.cost;
          this.kitDetails.kit.products[kpIndex].product.price =
            +this.CalculateCost(item);
        }
        // this.quotesInfo.kits.forEach((element) => {
        //   element.kit.products.forEach((ele) => {
        //     if (ele.product.id === item.product.id) {
        //       ele.product.cost = item.product.cost;
        //       ele.price = this.CalculateCost(ele);
        //     }
        //   });
        // });
        // this.UpdateQuote(this.quotesInfo);
      } else {
        this.isCostValid = true;
        this.toBeEdit = "cost";
      }
    }
  }

  CheckCost(item) {
    // const val = parseInt(item, 0);
    const val = Number(item);
    if (val === 0 || isNaN(val)) {
      // tslint:disable-next-line:one-line
      this.isCostValid = true;
    } else {
      this.isCostValid = false;
    }
  }
  // onBlur() {
  //   // console.log('clicked');
  //   // setTimeout( () => {
  //   //   this.SearchTextForQuote = null;
  //   //   this.showSearch = false;
  //   // }, 300);
  // }


  // discardEdit(from, form: NgForm) { //#Revisit_IgnoredAsNotUsed
  discardEdit(from) {
    this.toBeEdit = "none";
    this.isCostValid = false;
    if (from === "kit") {
      // this.getQuotes(this.quoteId);
      this.kitDetails = JSON.parse(
        JSON.stringify(this.quotesInfo.kits[this.kitIndex])
      );
      // form.resetForm();
    } else {
      this.toBeEdit = "none";
      // form.reset();
      this.getQuotes(this.quoteId);
      // this.items = JSON.parse(JSON.stringify(this.productsList.records));
    }
  }

  goToProductDetails(id) {
    this.productsService.getData("Products/" + id).subscribe((res) => {
      if (res !== null) {
        if (!res.isActive) {
          this.typeofMessage = "Error";
          this.messageToDisplay = null;
          this.noteToDisplay = null;
          this.dataToDisplay = null;
          this.showButtons = false;
          this.notification = "This product is no longer active.";
          this.confirmModal.show();
          return;
        }
        this._route.navigate(["/products/products-detail"], {
          queryParams: { id: btoa(id) },
        });
      }
    });
  }

  applyPricingConfirm(modal) {
    this.isMsrpValueNull = false;
    const isMsrpNull = (currentValue) => currentValue.product.msrp === null;
    let noMsrpIndex;
    this.selectedList.every((ele) => {
      noMsrpIndex = this.quotesInfo.products.findIndex(
        (p) =>
          p.id === ele.id && (p.product.msrp === null || +p.product.msrp === 0)
      );
      if (noMsrpIndex !== -1) {
        return false;
      } else {
        return true;
      }
    });
    // if (noMsrpIndex !== -1) {
    //   this.typeofMessage = 'Error';
    //   this.messageToDisplay = null;
    //   this.noteToDisplay = null;
    //   this.dataToDisplay = null;
    //   this.showButtons = false;
    //   this.notification =
    //     'Discount pricing methods cannot be applied to products with no MSRP, please deselect items with no MSRP and try again.';
    //   this.confirmModal.show();
    // } else {
    //   modal.show();
    // }
    if (noMsrpIndex !== -1) {
      this.isMsrpValueNull = true;
    }
    modal.show();
  }

  onMarginChangeBulk(form: NgForm, data, modal) {
    if (this.selectedList.length !== 0) {
      this.selectedList.forEach((element) => {
        let i;
        if (element.type === "kits") {
          i = this.quotesInfo.kits.findIndex(
            (ele, ind) => ele.id === element.id
          );
        } else {
          i = this.quotesInfo.products.findIndex(
            (ele, ci) => ele.id === element.id && ele["parentIndex"] === ci
          );
        }
        if (i !== -1) {
          if (element.type !== "kits") {
            if (data.pricingMethod !== null) {
              this.quotesInfo.products[i].margin = JSON.parse(
                JSON.stringify(
                  this.allMethods.filter((x) => x.id === data.pricingMethod)[0]
                )
              );
              if (this.quotesInfo.products[i].margin.method === "List") {
                this.quotesInfo.products[i].margin.value =
                  this.quotesInfo.products[i].product.msrp;
              }
              this.quotesInfo.products[i].price = this.CalculateCost(
                this.quotesInfo.products[i]
              );
            } else if (data.margin !== null) {
              this.quotesInfo.products[i].margin.method = "Margin";
              this.quotesInfo.products[i].margin.value = +data.margin;
            } else if (data.markup !== null) {
              this.quotesInfo.products[i].margin.method = "Markup";
              this.quotesInfo.products[i].margin.value = +data.markup;
            } else if (data.discount !== null) {
              this.quotesInfo.products[i].margin.method = "Discount";
              this.quotesInfo.products[i].margin.value = +data.discount;
            }
            this.quotesInfo.products[i].price = this.CalculateCost(
              this.quotesInfo.products[i]
            );

            this.quotesInfo.products[i].margin = new PricingMethod();
          } else if (element.type === "kits") {
            if (data.pricingMethod !== null) {
              this.quotesInfo.kits[i].margin = JSON.parse(
                JSON.stringify(
                  this.allMethods.filter((x) => x.id === data.pricingMethod)[0]
                )
              );
              this.quotesInfo.kits[i].kit.products.forEach((elementp) => {
                elementp.margin = JSON.parse(
                  JSON.stringify(
                    this.allMethods.filter(
                      (x) => x.id === data.pricingMethod
                    )[0]
                  )
                );
              });
              if (this.quotesInfo.kits[i].margin.method === "List") {
                // this.quotesInfo.kits[i].margin.value = this.quotesInfo.kits[
                //   i
                // ].product.msrp;

                this.quotesInfo.kits[i].kit.products.forEach((element0p) => {
                  element0p.margin.value = element0p.product.msrp;
                });
              }
              // this.quotesInfo.kits[i].price = this.CalculateCost(
              //   this.quotesInfo.kits[i]
              // );
              this.quotesInfo.kits[i].kit.products.forEach((p) => {
                p.price = this.CalculateCost(p);
              });
            } else if (data.margin !== null) {
              this.quotesInfo.kits[i].kit.products.forEach((p) => {
                p.margin.method = "Margin";
                p.margin.value = +data.margin;
              });
            } else if (data.markup !== null) {
              this.quotesInfo.kits[i].kit.products.forEach((p) => {
                p.margin.method = "Markup";
                p.margin.value = +data.markup;
              });
            } else if (data.discount !== null) {
              this.quotesInfo.kits[i].kit.products.forEach((p) => {
                p.margin.method = "Discount";
                p.margin.value = +data.discount;
              });
            }
            this.quotesInfo.kits[i].kit.products.forEach((p) => {
              p.price = this.CalculateCost(p);
            });
            this.kitIndex = i;
            this.kitDetails = this.quotesInfo.kits[i];
            this.quotesInfo.kits[i].total = this.GetKitTotal();
            this.quotesInfo.kits[i].kit.products.forEach((p) => {
              p.margin = new PricingMethod();
            });
          }
        }

        let isDiscountPricing;
        if (element.type !== "kits") {
          isDiscountPricing =
            this.quotesInfo.products[i].margin.method ===
              this.constants.PricingMethods.Discount
              ? true
              : false;
        } else {
          isDiscountPricing = this.quotesInfo.kits[i].kit.products.some(
            (p) => p.margin.method === this.constants.PricingMethods.Discount
          );
        }
        if (this.isMsrpValueNull && isDiscountPricing) {
          this.typeofMessage = "Success";
          this.messageToDisplay = null;
          this.noteToDisplay = null;
          this.dataToDisplay = null;
          this.showButtons = false;
          this.notification =
            "Discount pricing methods cannot be applied to products with no MSRP.";
          this.confirmModal.show();
          this.isMsrpValueNull = false;
        }
      });
    }

    const pricingMethod = modal.hide();
    form.reset();
    this.bulkUpload = {
      pricingMethod: null,
      margin: null,
      markup: null,
      discount: null,
    };
    this.selectedList = new Array<DeleteList>();
    // this.GetSubTotal();
    this.UpdateQuote(this.quotesInfo);
  }

  isParentDeleted(id) {
    this.productsService.getData("Products/" + id).subscribe((res) => {
      if (res !== null) {
        return res.isActive;
      }
    });
  }

  GetProductWiseCost(item) {
    let productTotal: any = 0;
    if (item.values != null && item.values.length) {
      item.values.forEach((element) => {
        if (element.product) {
          productTotal = productTotal + element.product.cost * element.quantity;
        }
      });
    }

    let cost = productTotal;
    cost = Math.round((cost + Number.EPSILON) * 100) / 100;
    return addZeroes(cost);
  }

  GetProjectWiseProfitMargin(item) {
    let productTotal: any = 0;
    let pMargin: any = 0;
    let profit: any = 0;
    if (item.values != null && item.values.length) {
      item.values.forEach((element) => {
        if (element.product) {
          productTotal = productTotal + element.product.cost * element.quantity;
        }
      });
    }

    profit = parseFloat(productTotal);
    profit = isNaN(profit) ? 0 : profit.toFixed(2);

    pMargin = (profit / parseFloat(productTotal)) * 100;
    pMargin = isNaN(pMargin) ? 0 : pMargin;
    return pMargin;
  }

  GetProjectWiseProfit(item) {
    let productTotal: any = 0;
    let profit: any = 0;
    if (item.values != null && item.values.length) {
      item.values.forEach((element) => {
        if (element.product) {
          productTotal = productTotal + element.product.cost * element.quantity;
        }
      });
    }
    profit = parseFloat(productTotal);
    profit = isNaN(profit) ? 0 : profit.toFixed(2);
    return profit;
  }

  getSelectedProjectCount(item, type) {
    let sCount: any = 0;
    if (type === "product") {
      item.values.forEach((ele, eleindex) => {
        if (
          this.selectedList.findIndex(
            (a) => a.id === ele.id && a.type === type && a.index === eleindex
          ) !== -1
        ) {
          sCount += 1;
        }
      });
    } else if (type === "customItem") {
      item.forEach((ele) => {
        if (
          this.selectedList.findIndex(
            (a) => a.id === ele.id && a.type === type
          ) !== -1
        ) {
          sCount += 1;
        }
      });
    } else if (type === "kits") {
      item.forEach((ele) => {
        if (
          this.selectedList.findIndex(
            (a, i) => a.id === ele.id && a.type === type
          ) !== -1
        ) {
          sCount += 1;
        }
      });
    }
    return sCount;
  }

  ngAfterViewInit() {
    let element = document.getElementById("collapse-Material0");
    if (element)
      element.click;
  }
}

function addZeroes(num) {
  num = Number(num);
  if (
    String(num).split(".").length < 2 ||
    String(num).split(".")[1].length <= 2
  ) {
    num = num.toFixed(2);
  }
  return num;
}
