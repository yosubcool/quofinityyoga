import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PricingComponent } from './pricing.component';
import { PaymentSuccessComponent } from './payment-success.component';
import { RouteGaurd } from '../_core/RouteGaurd';

const routes: Routes = [
  {
    path: '',
    canActivate: [RouteGaurd],
    data: {
      isLogin: true,
      // title: 'Example Pages'
    },
    children: [
      {
        path: 'pricing',
        component: PricingComponent,
        // canActivate: [RouteGaurd],
        data: {
          isLogin: true,
          title: 'Pricing'
        }
      },
      {
        path: 'payment-success',
        component: PaymentSuccessComponent,
        // canActivate: [RouteGaurd],
        data: {
          isLogin: true,
          title: 'Payement Success'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
