import { Component, OnInit } from '@angular/core';
// import { PaymentService } from '../Services/PaymentService.service';
// import { Constants } from 'app/shared/constant';
import { Router } from '@angular/router';
@Component({
  selector: 'app-payement-success',
  templateUrl: './payment-success.component.html'
})
export class PaymentSuccessComponent implements OnInit {
  BillingInfo: any;
  constants: any;
  // private _paymentService: PaymentService,
  constructor( private router: Router) {
    // this.constants = Constants;
  }

  ngOnInit() {
    localStorage.clear();
    this.getPaymentDetails();
  }

  getPaymentDetails() {
    // this.BillingInfo = this._paymentService.BillingInfo;
    this.BillingInfo.stripeJson = JSON.parse(this.BillingInfo.stripeJson);
  }

  redirecttoLogin() {
    sessionStorage.clear();
    this.router.navigate(['/account', 'login']);
  }
}
