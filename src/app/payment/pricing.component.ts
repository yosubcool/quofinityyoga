// import { Billing } from "./../Model/billing-model";
import { Component, Renderer2, NgZone, OnDestroy, OnInit } from '@angular/core';
// import { UserService } from "app/Services/User-service";
// import { PaymentService } from "app/Services/PaymentService.service";
import { Router } from '@angular/router';
// import { RegistrationVM } from 'app/Model/RegistrationVM';
@Component({
  moduleId: module.id,
  // selector: 'sd-stripe-form',
  templateUrl: './pricing.component.html',
  styleUrls: ['stripe-form.component.css']
})
export class PricingComponent implements OnDestroy, OnInit {
  public loading = false;
  globalListener: any;
  SubscriptionType: any;
  id: any;
  data: any;
  tax: any = 0;
  // billing = new Billing();
  token_triggered: boolean;
  header: string;
  constructor(
    private renderer: Renderer2,
    // private _UserService: UserService,
    // private _paymentService: PaymentService,
    private ngZone: NgZone,
    private router: Router
  ) {
    this.header = 'Welcome to GoChargeBack';
    this.SubscriptionType = [
      { Description: 'One-Year Subscription', amount: '250', subType: 1 },
      { Description: 'Two-Year Subscription', amount: '450', subType: 2 },
      { Description: 'Three-Year Subscription', amount: '675', subType: 5 }
    ];
  }

  openCheckout(description, amount, subType) {
    const handler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_XusvHCsALL0T8FzTjukpEoyj',
      locale: 'auto',
      token: (token: any) => {
        this.addPayment(token, description, amount, subType);
      }
    });

    handler.open({
      // amount: amount * 100,
      amount: (amount * (this.tax / 100) + parseFloat(amount)) * 100,
      image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
      name: 'Quofinity',
      description: description,
      allowRememberMe: true
    });

    // this.renderer.listenGlobal( //#Revisit_PreviousCode
    this.globalListener = this.renderer.listen(
      'window',
      'popstate',
      () => {
        handler.close();
      }
    );
  }

  ngOnInit() {
    this.loading = true;
    const companyVM: any = JSON.parse(sessionStorage.getItem('currentUser'));
    if (
      companyVM.result.company.property[0].billingInfo &&
      companyVM.result.company.property[0].billingInfo.length
    ) {
      // tslint:disable-next-line:one-line
      this.header = 'Subscription renewal';
    }
    // this._UserService.GetTax().subscribe(
    //   res => {
    //     this.loading = false;
    //     this.ngZone.run(() => {
    //       if (res) {
    //         this.tax = parseFloat(res.tax);
    //       } else {
    //         this.tax = 0.0;
    //       }
    //     });
    //   },
    //   err => {
    //     this.loading = false;
    //   }
    // );
  }

  ngOnDestroy() {

    if (this.globalListener) {
      this.globalListener();
    }
  }

  addPayment(token, description, amount, subType) {
    this.loading = true;
    // // this.billing.stripeJson = JSON.stringify(token);
    // this.billing.subscription = description;
    // this.billing.tax = this.tax;
    // this.billing.planPrice = amount;
    // this.billing.isActive = true;
    // const total = amount * (this.tax / 100) + parseFloat(amount);
    // this.billing.total = parseFloat(total.toString());
    // this.billing.last4Digit = token.card.last4;
    // this.billing.subscriptionStartDate = new Date();
    // this.billing.transcationId = token.card.id;
    // const tempDate = new Date();
    // tempDate.setFullYear(
    //   // this.billing.subscriptionStartDate.getFullYear() + subType
    // );
    // this.billing.subscriptionEndDate = tempDate;
    // this._UserService.AddPayment(this.billing).subscribe(
    //   res => {
    //     this.ngZone.run(() => {
    //       this.loading = false;
    //       this._paymentService.BillingInfo = res;
    //       this.router.navigate(['/payment/payment-success']);
    //       // this.router.navigate(['/dashboard']);
    //     });
    //   },
    //   err => {
    //     this.loading = false;
    //   }
    // );
  }
}
