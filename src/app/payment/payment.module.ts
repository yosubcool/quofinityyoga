import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { PaymentRoutingModule } from './payment-routing.module';
import { PricingComponent } from './pricing.component';
import { CommonModule } from '@angular/common';
import { PaymentSuccessComponent } from './payment-success.component';
import { NgxLoadingModule } from 'ngx-loading';
@NgModule({
  imports: [
    PaymentRoutingModule,
    ModalModule.forRoot(),
    CommonModule,
    FormsModule,
    NgxLoadingModule
  ],
  declarations: [
    PricingComponent,
    PaymentSuccessComponent,
  ]
})
export class PaymentModule { }
